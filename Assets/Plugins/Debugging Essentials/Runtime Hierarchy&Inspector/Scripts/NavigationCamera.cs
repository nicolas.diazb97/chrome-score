﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DebuggingEssentials
{
    [DefaultExecutionOrder(99000000)]
    public class NavigationCamera : MonoBehaviour
    {
        public static float fov;
        public static bool followTarget;
        public static Camera cam;

        public SO_NavigationCamera data;

        Transform t;
        GameObject go;
        

        Vector3 mousePosOld;
        Vector3 currentSpeed;
        Vector3 position, startPosition, navPosition, followPosition;
        Quaternion rotation, startRotation, navRotation, followRotation;

        float tStamp, deltaTime;

        float oldFov;
        float scrollWheel;

        GameObject selectionIndicatorGO;
        Transform selectionIndicatorT;


        public static void ResetStatic()
        {
            followTarget = false;
        }

        void Awake()
        {
            go = gameObject;
            t = transform;

            cam = GetComponent<Camera>();

            tStamp = Time.realtimeSinceStartup;

            startPosition = navPosition = t.position;
            startRotation = navRotation = t.rotation;

            navRotation = ResetRotZ(navRotation, false);

            fov = oldFov = cam.fieldOfView;

            selectionIndicatorGO = RuntimeInspector.selectionIndicatorGO;
            selectionIndicatorT = selectionIndicatorGO.transform;

            if (followTarget) ResetFollowPosRot();
        }

        void OnDestroy()
        {
            RestoreCam();
        }

        void Update()
        {
            if (RuntimeInspector.instance == null)
            {
                Destroy(this);
                return;
            }

            if (Input.GetKeyDown(RuntimeInspector.instance.followCameraKey))
            {
                followTarget = !followTarget;

                if (followTarget) ResetFollowPosRot();
            }

            if (WindowManager.eventHandling.hasEntered) scrollWheel = 0;
            else scrollWheel = Input.mouseScrollDelta.y * data.mouseScrollWheelMulti.value;
        }

        Quaternion ResetRotZ(Quaternion rot, bool roundTo180 = true)
        {
            Vector3 euler = rot.eulerAngles;
            if (roundTo180) euler.z = Mathf.Round(euler.z / 180) * 180;
            else euler.z = 0;
            return Quaternion.Euler(euler);
        }

        void LateUpdate()
        {
            if (RuntimeInspector.instance == null)
            {
                Destroy(this);
                return;
            }

            cam.fieldOfView = fov;
            deltaTime = Time.realtimeSinceStartup - tStamp;
            tStamp = Time.realtimeSinceStartup;

            Vector2 deltaMouse = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
            Vector3 speed = Vector3.zero;

            if (followTarget)
            {
                RuntimeInspector.UpdateSelectedIndicatorTransform();
                FollowTarget();
            }

            if (Input.GetMouseButton(1))
            {
                Quaternion oldRot = t.rotation;
                if (followTarget) t.rotation = rotation; else t.rotation = navRotation;

                t.Rotate(0, deltaMouse.x * data.mouseSensitity.value, 0, Space.World);
                t.Rotate(-deltaMouse.y * data.mouseSensitity.value, 0, 0, Space.Self);

                if (followTarget) followRotation = Quaternion.Inverse(ResetRotZ(selectionIndicatorT.rotation, false)) * ResetRotZ(t.rotation);
                else navRotation = ResetRotZ(t.rotation);

                t.rotation = oldRot;

                if (Input.GetKey(KeyCode.W)) speed.z = 1;
                else if (Input.GetKey(KeyCode.S)) speed.z = -1;

                if (Input.GetKey(KeyCode.D)) speed.x = 1;
                else if (Input.GetKey(KeyCode.A)) speed.x = -1;

                if (Input.GetKey(KeyCode.E)) speed.y = 1;
                else if (Input.GetKey(KeyCode.Q)) speed.y = -1;

                speed *= GetSpeedMulti();
            }

            if (Input.GetMouseButton(2))
            {
                speed.x = -(deltaMouse.x / deltaTime) / 60;
                speed.y = -(deltaMouse.y / deltaTime) / 60;

                speed *= GetSpeedMulti() * data.mouseStrafeMulti.value;
                currentSpeed = speed;
            }
            else Lerp2Way(ref currentSpeed, speed, data.accelMulti.value, data.decelMulti.value);

            GameObject selectionIndicatorGO = RuntimeInspector.selectionIndicatorGO;

            if (selectionIndicatorGO.activeInHierarchy) 
            {
                if (Input.GetKey(RuntimeInspector.instance.focusCameraKey) && !Input.GetKey(RuntimeInspector.instance.alignWithViewKey) && !Input.GetKey(RuntimeInspector.instance.moveToViewKey))
                {
                    ResetFollowPosRot();
                    FollowTarget();
                }
            }

            if (!followTarget)
            {
                t.rotation = rotation = navRotation;
                
                navPosition += t.TransformDirection(currentSpeed * deltaTime) + (t.forward * scrollWheel * deltaTime);
                t.position = position = navPosition;
            }

            if (RuntimeInspector.selectedGO != null)
            {
                if (Input.GetKey(RuntimeInspector.instance.focusCameraKey))
                {
                    if (Input.GetKey(RuntimeInspector.instance.alignWithViewKey))
                    {
                        RuntimeInspector.selectedGO.transform.position = t.position;
                        RuntimeInspector.selectedGO.transform.forward = t.forward;
                    }
                    else if (Input.GetKey(RuntimeInspector.instance.moveToViewKey))
                    {
                        RuntimeInspector.selectedGO.transform.position = t.position + (t.forward * 2);
                    }
                }
            }
        }

        void FollowTarget()
        {
            if (RuntimeInspector.selectedGO == go || RuntimeInspector.selectedGO == RuntimeInspector.selectionIndicatorGO)
            {
                followTarget = false;
                return;
            }

            t.rotation = rotation = navRotation = ResetRotZ(selectionIndicatorT.rotation, false) * followRotation;

            followPosition += selectionIndicatorT.InverseTransformDirection(t.TransformDirection(currentSpeed * deltaTime) + (t.forward * scrollWheel * deltaTime));
            t.position = position = navPosition = selectionIndicatorT.position + (selectionIndicatorT.rotation * followPosition);
        }

        public void ResetFollowPosRot()
        {
            Transform selectionIndicatorT = RuntimeInspector.selectionIndicatorGO.transform;

            t.rotation = selectionIndicatorT.rotation;
            t.position = selectionIndicatorT.position;

            followPosition = new Vector3(0, 0, -2);
            followRotation = Quaternion.identity;
        }

        public void SetCam()
        {
            t.rotation = rotation;
            t.position = position;
        }

        public void RestoreCam()
        {
            t.position = startPosition;
            t.rotation = startRotation;
            cam.fieldOfView = oldFov;
        }

        float GetSpeedMulti()
        {
            if (Input.GetKey(KeyCode.LeftShift)) return data.speedFast.value;
            if (Input.GetKey(KeyCode.LeftControl)) return data.speedSlow.value;
            else return data.speedNormal.value;
        }

        void Lerp2Way(ref Vector3 v, Vector3 targetV, float upMulti, float downMulti)
        {
            Lerp2Way(ref v.x, targetV.x, upMulti, downMulti);
            Lerp2Way(ref v.y, targetV.y, upMulti, downMulti);
            Lerp2Way(ref v.z, targetV.z, upMulti, downMulti);
        }

        void Lerp2Way(ref float v, float targetV, float upMulti, float downMulti)
        {
            float multi;
            if (Mathf.Abs(v) < Mathf.Abs(targetV)) multi = upMulti; else multi = downMulti;
            v = Mathf.Lerp(v, targetV, multi * deltaTime);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace DebuggingEssentials
{
    [CustomEditor(typeof(WindowManager))]
    public class WindowManagerEditor : Editor
    {
        void OnEnable()
        {
            GUIDraw.editorSkinMulti = EditorGUIUtility.isProSkin ? 1 : 0.55f;
        }

        public override void OnInspectorGUI()
        {
            var windowManager = (WindowManager)target;

            // base.OnInspectorGUI();
            DrawSpacer(5, 5, 1);
            // EditorGUILayout.BeginHorizontal();
            GUI.backgroundColor = Color.green * GUIDraw.editorSkinMulti;
            if (GUILayout.Button("Open Documentation"))
            {
                Application.OpenURL("http://www.terraincomposer.com/de-documentation/");
            }
            GUI.backgroundColor = Color.white;
            DrawSpacer(2, 5, 1);
            GUI.backgroundColor = Color.magenta * GUIDraw.editorSkinMulti;
            if (GUILayout.Button(new GUIContent("Quick Discord Support", "With instant chatting I'm able to give very fast support.\nWhen I'm online I try to answer right away.")))
            {
                Application.OpenURL("https://discord.gg/HhepjD9");
            }
            GUI.backgroundColor = Color.white;
            // EditorGUILayout.EndHorizontal();
            DrawSpacer(2, 5, 1);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Open Editor Logs"))
            {
                EditorUtility.RevealInFinder(Helper.GetConsoleLogPath());
                // Application.OpenURL("file://" + Application.consoleLogPath);
            }
            if (GUILayout.Button("Open Build Logs"))
            {
                // EditorUtility.RevealInFinder(Application.persistentDataPath + "/Player.log");
                Application.OpenURL("file://" + Application.persistentDataPath);
            }
            GUILayout.EndHorizontal();
            DrawSpacer(2, 5, 2);

            serializedObject.Update();

            GUIDraw.DrawHeader(Helper.GetGUIContent("SETTINGS"), Color.green);
            GUI.changed = false;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(new GUIContent("Use DontDestroyOnLoad Scene", "This will put this Debugging Essentials GameObject as don't destroy on load and will display the 'DontDestroyOnLoad' Scane."));
            windowManager.windowData.useDontDestroyOnLoadScene = EditorGUILayout.Toggle(windowManager.windowData.useDontDestroyOnLoadScene);
            EditorGUILayout.EndHorizontal();
            if (GUI.changed)
            {
                if (Application.isPlaying && windowManager.windowData.useDontDestroyOnLoadScene)
                {
                    DontDestroyOnLoad(windowManager.gameObject);
                }
                EditorUtility.SetDirty(windowManager.windowData);
            }

            serializedObject.ApplyModifiedProperties();

            GUI.changed = false;
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel(new GUIContent("GUI Scale", "GUI scale of the Runtime Windows"));
            windowManager.windowData.guiScale.value = EditorGUILayout.Slider(windowManager.windowData.guiScale.value, 0.55f, 10f);
            if (GUI.changed)
            {
                windowManager.windowData.guiScale.UpdateText();
                EditorUtility.SetDirty(windowManager.windowData);
            }
            if (GUILayout.Button(new GUIContent("R", "Reset the scale to 1"), GUILayout.Width(25)))
            {
                windowManager.windowData.guiScale.SetValueText(1);
                EditorUtility.SetDirty(windowManager.windowData);
            }
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
            DrawSpacer(2, 5, 2);
        }

        static public void DrawSpacer(float spaceBegin = 5, float height = 5, float spaceEnd = 5)
        {
            GUILayout.Space(spaceBegin - 1);
            EditorGUILayout.BeginHorizontal();
            GUI.color = new Color(0.5f, 0.5f, 0.5f, 1);
            GUILayout.Button("", GUILayout.Height(height));
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(spaceEnd);

            GUI.color = Color.white;
        }
    }
}
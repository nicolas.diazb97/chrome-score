﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AuxCrearGrupoRankingScript : MonoBehaviour {

	//public List<int> userInviteList = new List<int>();
	public int guestUserID;
	public Text nameRankingGroup;

	public GameObject rankingGroup;

	private string urlPhpPronostico = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/UserTiroAmarillo.php?";

	public static AuxCrearGrupoRankingScript auxCrearGrupoRanking;

	// Use this for initialization
	void Start () {
		auxCrearGrupoRanking = this;
	}

	/*void Update(){

		Debug.Log (nameRankingGroup.text);
	}*/

	public void RunMakeRankingGroup(){
	
		//rankingGroup.GetComponent<ScrollRankingGrupoScript> ().InitMakeGroupRanking (nameRankingGroup.text, guestUserID);

		StartCoroutine (MakeGroupRanking (nameRankingGroup.text, guestUserID));
	}

	public IEnumerator MakeGroupRanking (string rankingName, int userID){

		gameObject.SetActive (true);

		ShowUpdatePopup ();

		//string idAdmin = "&userID=" + 1234; // DataApp.main.GetMyID();
		string idAdmin = "&userID=" + DataApp.main.GetMyID();
		string groupName = "&rankingName=" + rankingName;
		string userInserID = "&userRankingID=" + userID;
		string chatServerId = "&IDchatServer=" + "id_n_server";
		int groupId;

		bool error = false;

		string urlPronostico = urlPhpPronostico + "indata=crearGrupoRanking" + idAdmin + groupName + userInserID + chatServerId;

		WWW setData = new WWW (urlPronostico.Replace(" ", "%20"));
		yield return setData;

		if (string.IsNullOrEmpty (setData.error)) {

			groupId = int.Parse(setData.text);
			Debug.Log ("Sii!! = " + groupId);
			Debug.Log (int.Parse(setData.text));

			rankingGroup.GetComponent<ScrollRankingGrupoScript> ().ShowRankingGroupID(groupId);

		} else {

			error = true;
			ShowMessagePopup("¡CONNECTION FAILED!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			Debug.Log ("Noo!!");
		}
	}

	private void ShowUpdatePopup(){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowUpdateDataPanel();
	}

	private void HideUpdatePopup(){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().HideUpdateDataPanel();
	}

	private void ShowMessagePopup(string message, int indexTabContinue){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopup (message, indexTabContinue);
	}
}

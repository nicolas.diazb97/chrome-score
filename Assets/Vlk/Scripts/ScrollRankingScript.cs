﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Globalization;

public class ScrollRankingScript : MonoBehaviour {

	public GameObject prefabContentRanking;
	public GameObject ScrollContent;

	public Dropdown rankingOptions;

	//public Text fechaText;
	public Text infNoPuntajeText;

	[SerializeField]
	private ContentRankingListScript rankingList;

	[SerializeField]
	private ContentRankingListScript userRanking;

	private bool rankingIsLoad = false;

	private bool imageRankingIsLoad = false;

	private string urlPhpPuntajes =  "TraerYActualizarPuntaje.php?";

	private string urlPhpUltimoPartido = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/GetDate.php?";

	private float initContentPosY = 0f;

	private bool isRefresh = false;

	//----------------------

	public List<GameObject> rankingsData = new List<GameObject>();

	[SerializeField]
	private ContentRankingScript userDataRankingHeader;

	[SerializeField]
	private ContentRankingScript userDataRankingFooter;

	[SerializeField]
	public DateClassList fechaList;

	// Use this for initialization
	void Awake () {

	}

	void Start () {

		ScrollContent.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);

		initContentPosY = ScrollContent.GetComponent<RectTransform>().position.y - 100f;
	}

	// Update is called once per frame
	void Update () {

		if(ScrollContent.GetComponent<RectTransform>().position.y < initContentPosY && !isRefresh && !Input.GetMouseButton(0)){
			print ("refrescar");
			isRefresh = true;

			ChangeToRanking();
		}
	}

	private IEnumerator ValidateStatusRanking ()
	{

		//LoadingDummy.Loading.EnableLoading ();
		WWW getData = new WWW (DataApp2.main.host+ urlPhpPuntajes + "indata=traerUsuarios");
		Debug.Log( "URL: "+DataApp2.main.host+ urlPhpPuntajes + "indata=traerUsuarios" );
		yield return getData;
		//LoadingDummy.Loading.DisableLoading();
		Debug.Log("data text " +getData.text );

		if (string.IsNullOrEmpty (getData.error)) 
		{

			if (getData.text != "PredictingUncreated") 
			{

				rankingList = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
				//rankingList = JsonUtility.FromJson<ContentRankingListScript> ("{\"dataList\":[{\"userID\":\"54958\",\"nameText\":\"Xavier Orlando Salguero Salas\",\"points\":\"38583\"},{\"userID\":\"50595\",\"nameText\":\"Vini Defas\",\"points\":\"37767\"},{\"userID\":\"12544\",\"nameText\":\"Jorge Arturo  Jim\\u00c3\\u00a9nez Carpio \",\"points\":\"36249\"},{\"userID\":\"4486\",\"nameText\":\"Daniel Andres Paredes Suarez\",\"points\":\"34986\"},{\"userID\":\"3186\",\"nameText\":\"Donny Andres Torres Mendez \",\"points\":\"33903\"},{\"userID\":\"15431\",\"nameText\":\"Carlos  Paez\",\"points\":\"30585\"},{\"userID\":\"14113\",\"nameText\":\"Harold Jose Saltos Moreira\",\"points\":\"29328\"},{\"userID\":\"15750\",\"nameText\":\"Solon Stanford  Alvarez Vargas\",\"points\":\"28965\"},{\"userID\":\"1547\",\"nameText\":\"Edison Leonardo Soto Solis\",\"points\":\"28500\"},{\"userID\":\"56662\",\"nameText\":\"Adriana Borbor\",\"points\":\"28482\"},{\"userID\":\"49784\",\"nameText\":\"Esteban Floreano\",\"points\":\"28368\"},{\"userID\":\"25398\",\"nameText\":\"Jose Martinez\",\"points\":\"27342\"},{\"userID\":\"3035\",\"nameText\":\"Gregory Kelvin Barreiro Mora\",\"points\":\"26802\"},{\"userID\":\"15680\",\"nameText\":\"Nestor Fabian Lapo Sanchez\",\"points\":\"25074\"},{\"userID\":\"50517\",\"nameText\":\"Geovanny Morales \",\"points\":\"24822\"},{\"userID\":\"51227\",\"nameText\":\"Victor Emilio Sanchez Santamaria\",\"points\":\"24198\"},{\"userID\":\"44420\",\"nameText\":\"Henry Wilson Aviles De La Torre\",\"points\":\"24093\"},{\"userID\":\"51695\",\"nameText\":\"Andr\\u00c3\\u00a9s  Mendieta\",\"points\":\"23991\"},{\"userID\":\"1738\",\"nameText\":\"Alexander Franco Lainez\",\"points\":\"23844\"},{\"userID\":\"51301\",\"nameText\":\"Nelson Herrera\",\"points\":\"23730\"},{\"userID\":\"5183\",\"nameText\":\"Paolo Antonio Campoverde Huertas\",\"points\":\"23622\"},{\"userID\":\"1859\",\"nameText\":\"Ricardo Augusto Ponce Jaramillo\",\"points\":\"23265\"},{\"userID\":\"1788\",\"nameText\":\"Xavier Arboleda\",\"points\":\"23184\"},{\"userID\":\"51435\",\"nameText\":\"Marlon  Cede\\u00f1o\",\"points\":\"23046\"},{\"userID\":\"941\",\"nameText\":\"Cesar Andres Calderon Tobar\",\"points\":\"22989\"},{\"userID\":\"5122\",\"nameText\":\"Bladimir Alejandro Narvaez Solano\",\"points\":\"22872\"},{\"userID\":\"49849\",\"nameText\":\"Samuel Reyes\",\"points\":\"22569\"},{\"userID\":\"772\",\"nameText\":\"Ricardo Esteban Moya Palacios\",\"points\":\"22260\"},{\"userID\":\"16162\",\"nameText\":\"Stalin Gustavo  Romero Machuca\",\"points\":\"22077\"},{\"userID\":\"57431\",\"nameText\":\"Walter Pacheco\",\"points\":\"21651\"},{\"userID\":\"54793\",\"nameText\":\"Paulo Ocampo\",\"points\":\"20925\"},{\"userID\":\"935\",\"nameText\":\"Andy Erickson Proa\\u00c3\\u00b1o Tigua\",\"points\":\"20916\"},{\"userID\":\"835\",\"nameText\":\"M\\u00c3\\u00b3nica Mercedes Yance Bermeo\",\"points\":\"20727\"},{\"userID\":\"1869\",\"nameText\":\"Ariel Salazar\",\"points\":\"20709\"},{\"userID\":\"51062\",\"nameText\":\"Lucas Jaramillo\",\"points\":\"20691\"},{\"userID\":\"49848\",\"nameText\":\"Rodolfo Alc\\u00c3\\u00advar \",\"points\":\"20349\"},{\"userID\":\"15446\",\"nameText\":\"Xavier Franco Matamoros\",\"points\":\"20343\"},{\"userID\":\"14728\",\"nameText\":\"Rafael Antonio Ortega Touriz\",\"points\":\"20286\"},{\"userID\":\"11743\",\"nameText\":\"Jorge Cordova\",\"points\":\"20010\"},{\"userID\":\"57388\",\"nameText\":\"Marlon Cevallos\",\"points\":\"19572\"},{\"userID\":\"21614\",\"nameText\":\"Carlos Luis Villa Oviedo\",\"points\":\"19263\"},{\"userID\":\"15634\",\"nameText\":\"Adrian Armando Guerrero Cruz\",\"points\":\"19215\"},{\"userID\":\"5049\",\"nameText\":\"Michael Martillo\",\"points\":\"19215\"},{\"userID\":\"4194\",\"nameText\":\"Javier  Heredia Jim\\u00c3\\u00a9nez\",\"points\":\"19182\"},{\"userID\":\"14756\",\"nameText\":\"Alfredo Rene Mu\\u00c3\\u00b1oz Perez\",\"points\":\"19128\"},{\"userID\":\"2538\",\"nameText\":\"Byron Andres Arcentales Loaiza\",\"points\":\"18963\"},{\"userID\":\"2985\",\"nameText\":\"Byron Alejandro Mina Ponce\",\"points\":\"18864\"},{\"userID\":\"12215\",\"nameText\":\"Jorge Gregorio Perez Cordero\",\"points\":\"18807\"},{\"userID\":\"56903\",\"nameText\":\"Jose Veintimilla \",\"points\":\"18600\"},{\"userID\":\"11851\",\"nameText\":\"Gustavo Guadalupe \",\"points\":\"18264\"},{\"userID\":\"13973\",\"nameText\":\"Jos\\u00c3\\u00a9 Luis  Cordero Jimenez \",\"points\":\"18189\"},{\"userID\":\"13781\",\"nameText\":\"Miguel Angel Romero Nu\\u00c3\\u00b1ez\",\"points\":\"17838\"},{\"userID\":\"2646\",\"nameText\":\"Victor Ariel Carri\\u00c3\\u00b3n Calle\",\"points\":\"17673\"},{\"userID\":\"2060\",\"nameText\":\"Jose Luis Diarte Cardenas\",\"points\":\"17664\"},{\"userID\":\"734\",\"nameText\":\"Axel  Franco\",\"points\":\"17472\"},{\"userID\":\"25697\",\"nameText\":\"Daniel Santos\",\"points\":\"17352\"},{\"userID\":\"11918\",\"nameText\":\"Marco Misael Jaramillo Fiallos \",\"points\":\"17310\"},{\"userID\":\"13923\",\"nameText\":\"Oscar Affonso Loja Culcay\",\"points\":\"17115\"},{\"userID\":\"2155\",\"nameText\":\"Jhon Washington Romero Bello\",\"points\":\"16998\"},{\"userID\":\"1832\",\"nameText\":\"Willians Jose Choez Caicedo\",\"points\":\"16932\"},{\"userID\":\"980\",\"nameText\":\"David Izurieta\",\"points\":\"16878\"},{\"userID\":\"4739\",\"nameText\":\"Jos\\u00c3\\u00a9 Javier Jaramillo Roblero\",\"points\":\"16785\"},{\"userID\":\"16865\",\"nameText\":\"Marcos  Hernandez\",\"points\":\"16770\"},{\"userID\":\"13584\",\"nameText\":\"Chris Aldas\",\"points\":\"16713\"},{\"userID\":\"712\",\"nameText\":\"Jonathan Mauricio\",\"points\":\"16665\"},{\"userID\":\"4674\",\"nameText\":\"Laura Garcia Yupanqui\",\"points\":\"16455\"},{\"userID\":\"2493\",\"nameText\":\"Jonathan Jair Riasco Indio\",\"points\":\"16332\"},{\"userID\":\"7772\",\"nameText\":\"Bryan Gonzalo Gualotu\\u00c3\\u00b1a Criollo\",\"points\":\"16323\"},{\"userID\":\"3468\",\"nameText\":\"Pedro Rodrigo  Risue\\u00c3\\u00b1o Guzm\\u00c3\\u00a1n\",\"points\":\"16317\"},{\"userID\":\"18224\",\"nameText\":\"Luis Prado\",\"points\":\"16134\"},{\"userID\":\"20279\",\"nameText\":\"Jesua Navarrete\",\"points\":\"15885\"},{\"userID\":\"2524\",\"nameText\":\"Jaime Leonardo Cantos Tejada\",\"points\":\"15879\"},{\"userID\":\"52704\",\"nameText\":\"Javier Maldonado\",\"points\":\"15765\"},{\"userID\":\"8127\",\"nameText\":\"Jose Villagran\",\"points\":\"15537\"},{\"userID\":\"4353\",\"nameText\":\"Danny Zambrano Toro \",\"points\":\"15522\"},{\"userID\":\"15466\",\"nameText\":\"Ruben Ramiro Sacoto Salazar\",\"points\":\"15390\"},{\"userID\":\"1012\",\"nameText\":\"Jos\\u00c3\\u00a9 Xavier  Tocres Zambrano \",\"points\":\"15324\"},{\"userID\":\"515\",\"nameText\":\"Christian Andres Espinoza Naranjo \",\"points\":\"15147\"},{\"userID\":\"2557\",\"nameText\":\"Winston Darlin Anton Loor\",\"points\":\"15135\"},{\"userID\":\"2187\",\"nameText\":\"Miguel Samuel Bonifaccini Parra \",\"points\":\"15081\"},{\"userID\":\"15670\",\"nameText\":\"Alex Reinoso Mora\",\"points\":\"15069\"},{\"userID\":\"5029\",\"nameText\":\"Jorge Xavier Mu\\u00c3\\u00b1oz Apolinario\",\"points\":\"14877\"},{\"userID\":\"13795\",\"nameText\":\"Hugo Herrera\",\"points\":\"14802\"},{\"userID\":\"14590\",\"nameText\":\"Alejandro Arias\",\"points\":\"14784\"},{\"userID\":\"2591\",\"nameText\":\"Geovanny Esteban Marcillo Quijije\",\"points\":\"14751\"},{\"userID\":\"4395\",\"nameText\":\"David Veliz\",\"points\":\"14730\"},{\"userID\":\"49823\",\"nameText\":\"Bryan Guadalupe\",\"points\":\"14679\"},{\"userID\":\"2158\",\"nameText\":\"Tom\\u00c3\\u00a1s Andr\\u00c3\\u00a9s Yaguana Espin\",\"points\":\"14331\"},{\"userID\":\"20359\",\"nameText\":\"Steven Baja\\u00c3\\u00b1a Delgado\",\"points\":\"14259\"},{\"userID\":\"51935\",\"nameText\":\"Rodrigo Toledo Idrovo\",\"points\":\"14043\"},{\"userID\":\"10745\",\"nameText\":\"Jean paul mu\\u00c3\\u00b1oz torres\",\"points\":\"13890\"},{\"userID\":\"18976\",\"nameText\":\"Elias  Molina \",\"points\":\"13875\"},{\"userID\":\"10363\",\"nameText\":\"Iv\\u00c3\\u00a1n Marcelo Saltos Medina\",\"points\":\"13869\"},{\"userID\":\"13140\",\"nameText\":\"Adrian Enrique Moreira Demera\",\"points\":\"13803\"},{\"userID\":\"3054\",\"nameText\":\"Dami\\u00c3\\u00a1n Camino Gonz\\u00c3\\u00a1lez\",\"points\":\"13701\"},{\"userID\":\"2167\",\"nameText\":\"Jos\\u00c3\\u00a9 Andr\\u00c3\\u00a9s Vintimilla\",\"points\":\"13683\"},{\"userID\":\"50838\",\"nameText\":\"Vicente Alfonso Vera\",\"points\":\"13638\"},{\"userID\":\"4203\",\"nameText\":\"Jaime Paul Piza Alarcon\",\"points\":\"13521\"},{\"userID\":\"14010\",\"nameText\":\"Carlos Ullauri\",\"points\":\"13518\"},{\"userID\":\"13094\",\"nameText\":\"Luis Alberto Desanima Mereci\",\"points\":\"13434\"}]}");
				StartCoroutine (ShowRanking ());
			} 
			else 
			{

				//dataIsValidated = true;
				//predecirBtn.SetActive (true);
				//StartCoroutine (gameObject.GetComponent<GetFootballPlayersDataScript> ().GetFootballPlayersData ());
			}
		} 
		else 
		{

			DataApp2.main.popUpInformative (true, "Error", "Fallo en la conexión");
		}

	}

	private IEnumerator ValidateStatusRankingFecha (){

		WWW getData = new WWW (urlPhpUltimoPartido + "indata=getNextDate" + "&idDate=" + 2);
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "PredictingUncreated") {

				fechaList = JsonUtility.FromJson<DateClassList> (getData.text);
				StartCoroutine (GetUsersPorFecha ());
			} else {

				Debug.Log("¡No hay datos!");
			}
		} else {

			Debug.Log("¡CONNECTION FAILED!");
		}

		Debug.Log ("GETDATA: " + getData.text);
	}

	private IEnumerator GetUsersPorFecha(){

		DateTime date = DateTime.Parse (fechaList.dataList [0].fecha);
		//DateTime nextDate = DateTime.Parse (fechaList.dataList [0].fecha + " " + fechaList.dataList [0].hora);

		CultureInfo ci = new CultureInfo("es-ES");
		//DateTime pastMonth = Date.AddMonths(0);
		string mes =  date.ToString("MMMM",ci);
		string dia =  date.ToString("dddd",ci);
		string diaNum = date.ToString("dd",ci).ToUpper();

		//fechaText.text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dia) + " " + diaNum + " de " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(mes);

		string fechaEncuentro = "&buscarFecha=" + date.Year + "-" + date.Month + "-" + date.Day;

		WWW getData = new WWW (urlPhpPuntajes + "indata=PronosticoRankingFecha" + fechaEncuentro);
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "PredictingUncreated") {

				rankingList = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
				StartCoroutine (ShowRanking ());
			} else {

				//EndUpdateData ("none");
				StartCoroutine(SetUserDataRanking ());
			}
		} else {

			DataApp2.main.popUpInformative (true, "Error", "Fallo en la conexión");
		}

		Debug.Log (getData.text);

		Debug.Log ("FECHA PARTIDO ANTERIOR: " + date.Year + "-" + date.Month + "-" + date.Day);
	}

	private IEnumerator ShowRanking(){

		StartCoroutine(SetUserDataRanking ());

		ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);
		DataApp2.main.EnableLoading ();
		int index = 1;

		foreach(ContentRanking obj in rankingList.dataList){

			ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, ScrollContent.GetComponent<RectTransform> ().sizeDelta.y+105f );

			GameObject panelData = Instantiate (prefabContentRanking);
			//panelData.GetComponent<CanvasGroup> ().alpha = 0f;
			panelData.transform.SetParent (ScrollContent.transform);
			rankingsData.Add (panelData);
			panelData.GetComponent<RectTransform> ().anchoredPosition = new Vector3 (0f, (index - 1) * -105f, 0f);
			panelData.GetComponent<Transform>().localPosition = new Vector3 (0f, (index - 1) * -105f, 0f);
			panelData.GetComponent<RectTransform> ().localRotation = Quaternion.Euler (0,0,0);
			panelData.GetComponent<RectTransform>().localScale = new Vector3 (1f, 1f, 1f);
			yield return new WaitForEndOfFrame (); //WaitForSeconds(0.1f);
			//panelData.GetComponent<ContentRankingScript> ().posRankingText = index;
			//panelData.GetComponent<ContentRankingScript> ().userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking ("PadrinoOro2.png");
			//	panelData.GetComponent<ContentRankingScript> ().userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");
			panelData.GetComponent<ContentRankingScript> ().userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");
			panelData.GetComponent<ContentRankingScript> ().nameText.text = obj.nameText;
			panelData.GetComponent<ContentRankingScript> ().pointsText.text = obj.points.ToString ();
			panelData.GetComponent<ContentRankingScript> ().cup.gameObject.SetActive (true);
			panelData.GetComponent<ContentRankingScript> ().cupRankingText.text = index.ToString ();
			if (index == 1) {
				panelData.GetComponent<ContentRankingScript> ().cup.color = new Color32 (250, 157, 0, 255);

			} else if (index == 2) {
				panelData.GetComponent<ContentRankingScript> ().cup.color = new Color32 (170, 170, 170, 255);

			} else if (index == 3) {
				panelData.GetComponent<ContentRankingScript> ().cup.color = new Color32 (124, 55, 24, 255);

			}

			index++;
		}

		//EndUpdateData ("none");
		DataApp2.main.DisableLoading ();
		//StartCoroutine(SetUserDataRanking ());
	}

	private IEnumerator SetUserDataRanking()
    {
		DataApp2.main.EnableLoading ();
		string urlPhp = "";

		//if(rankingOptions.value == 0){

		urlPhp = DataApp2.main.host+ urlPhpPuntajes + "indata=buscarUserRanking&idUser=" + DataApp2.main.GetMyID();
		Debug.Log (urlPhp);

		WWW getData = new WWW(urlPhp);

		yield return getData;

		Debug.Log ("datos usuario actual "+getData.text);

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "PredictingUncreated") {
				Debug.Log(getData.text);
				userRanking = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
				//userRanking = JsonUtility.FromJson<ContentRankingListScript> ("{\"dataList\":[{\"userID\":141,\"nameText\":\"Diego Fernando Cuesta Arevalo\",\"points\":\"9780\"}]}");

				StartCoroutine (ShowUserRanking ());
			} else {

				//dataIsValidated = true;
				//predecirBtn.SetActive (true);
				//StartCoroutine (gameObject.GetComponent<GetFootballPlayersDataScript> ().GetFootballPlayersData ());
				StartCoroutine(NoHayDatos());
				//DataApp2.main.DisableLoading ();
			}
		} else {
			DataApp2.main.DisableLoading ();
			DataApp2.main.popUpInformative (true, "Error", "Fallo en la conexión");
		}


		//Debug.Log (getData.text);
	}

	private IEnumerator ShowUserRanking(){

		ContentRanking obj = userRanking.dataList[0];

		userDataRankingHeader.posRankingText.text = obj.userID.ToString (); //CALCULAR LA POSICION
		userDataRankingHeader.nameText.text = obj.nameText +" " +User.main.GetMyLastName();
		userDataRankingHeader.pointsText.text = obj.points.ToString ();

		userDataRankingHeader.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (DataApp2.main.GetMyID() + ".jpg");

//		userDataRankingFooter.posRankingText.text = obj.userID.ToString ();
//		userDataRankingFooter.nameText.text = obj.nameText+" " +User.main.GetMyLastName();
//		userDataRankingFooter.pointsText.text = obj.points.ToString ();
//
//
//
//		userDataRankingFooter.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");



//		if(obj.userID == DataApp2.main.GetMyID()){
//			Debug.Log ("realidad");
//			userDataRankingHeader.nameText.text = obj.nameText+" " +User.main.GetMyLastName();
//			userDataRankingHeader.pointsText.text = obj.points.ToString ();
//
//			userDataRankingHeader.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");
//
//			userDataRankingFooter.nameText.text = obj.nameText;
//			userDataRankingFooter.pointsText.text = obj.points.ToString ();
//
//			userDataRankingFooter.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");
//		}


		//userDataRankingFooter2.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");

		yield return null;
		DataApp2.main.DisableLoading ();
		EndUpdateData ("none");
	}

	private IEnumerator NoHayDatos(){

		infNoPuntajeText.text = "No se ha generado puntaje de la ultima fecha";

		//ContentRanking obj = userRanking.dataList[0];

		userDataRankingHeader.posRankingText.text = "0";
		//userDataRankingHeader.nameText.text = obj.nameText;
		userDataRankingHeader.pointsText.text = "0";

		//userDataRankingHeader.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (1 + ".jpg");

		userDataRankingFooter.posRankingText.text = "0";
		//userDataRankingFooter.nameText.text = obj.nameText;
		userDataRankingFooter.pointsText.text = "0";

		//userDataRankingFooter.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");

		//Debug.Log (obj.userID.ToString ());

		yield return null;

		EndUpdateData ("none");
	}

	public void ChangeToRanking(){

		//		Debug.Log ("llamamos al metodo cargar");
		//		LoadingDummy.Loading.EnableLoading ();

		//fechaText.text = "";
		//infNoPuntajeText.text = "";

		StartCoroutine(DestroyCurrentRankingData ());

		//		if(rankingOptions.value == 0){
		//
		//			StartCoroutine(ValidateStatusRanking ());
		//
		//		} else if(rankingOptions.value == 1){
		//
		//			StartCoroutine(ValidateStatusRankingFecha ());
		//		}
	}

	private IEnumerator DestroyCurrentRankingData(){

		foreach(GameObject obj in rankingsData){

			Destroy (obj);

			yield return obj;
		}

		rankingsData.Clear ();

		if(rankingOptions.value == 0){

			StartCoroutine(ValidateStatusRanking ());

		} else if(rankingOptions.value == 1){

			StartCoroutine(ValidateStatusRankingFecha ());
		}
	}

	void OnEnable() {

		if (rankingIsLoad == false /*&& TransactionalProcess.isActive == 1*/ ) {
			StartCoroutine(ValidateStatusRanking ());
		}else{
			DataApp2.main.DisableLoading ();
		}
	}

	public void EndUpdateData(string state){

		isRefresh = false;
		rankingIsLoad = true;
		DataApp2.main.DisableLoading ();

	}

	//	private void ShowMessagePopup(string message, int indexTabContinue){
	//
	//		GameObject eventManager = GameObject.Find ("EventManager");
	//		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopup (message, indexTabContinue);
	//	}
}

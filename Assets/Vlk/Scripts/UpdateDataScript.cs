﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateDataScript : MonoBehaviour {

	public GameObject imgUpdate;
	public GameObject popupMessage;
	public GameObject popupConfirmation;
	public Button acceptBtn;

	private GameObject objConfirmationPopup;
	private string actionDelete;

	public float rotationSpeed = 0.1f;
	public float timeWaitAfterLoad;

	private int indexTabContinue;

	private string updateState = "none";
	public string _updateState {get{ return updateState; } set{ updateState = value; }}

	// Use this for initialization
	void Start () {
	
		//acceptBtn.GetComponent<Button> ().onClick.AddListener (ClosePopup);

		InvokeRepeating("RotateImg",0,rotationSpeed);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void RunUpdatePanel(){

		gameObject.SetActive (true);

		InvokeRepeating("RotateImg",0,rotationSpeed);
	}

	public void StopUpdatePanel(){
		DataApp2.main.EnableLoading();
		StartCoroutine (StopUpdate ());
	}

	private IEnumerator StopUpdate(){
	
		yield return new WaitForSeconds(timeWaitAfterLoad);

		CancelInvoke("RotateImg");

		//imgUpdate.SetActive (false);
		gameObject.SetActive (false);
		popupConfirmation.SetActive (false);
	}

	private void RotateImg () {

		popupMessage.SetActive (false);
		popupConfirmation.SetActive (false);
		
		//imgUpdate.SetActive (true);
		//imgUpdate.transform.Rotate (0,0,-40);
	}

	public void RunPopup(string text, int indexTab){

		CancelInvoke("RotateImg");
		//imgUpdate.SetActive (false);
		popupConfirmation.SetActive (false);

		updateState = "isOpen";
		indexTabContinue = indexTab;
		popupMessage.SetActive (true);
		popupMessage.transform.Find ("Text").gameObject.GetComponent<Text> ().text = text;
	}

	public void ClosePopup(){

		updateState = "isClose";
		popupMessage.SetActive (false);
		gameObject.SetActive (false);
		popupConfirmation.SetActive (false);

		GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ().SelectedButtonPanel(indexTabContinue);
	}

	public void RunPopupConfirmation(string text, int indexTab, string action, GameObject obj){

		CancelInvoke("RotateImg");
		//imgUpdate.SetActive (false);
		popupMessage.SetActive (false);
		indexTabContinue = indexTab;

		actionDelete = action;
		objConfirmationPopup = obj;

		popupConfirmation.SetActive (true);
		popupConfirmation.transform.Find ("Text").gameObject.GetComponent<Text> ().text = text;
	}

	public void AcceptConfirmation(){
		
		popupMessage.SetActive (false);
		gameObject.SetActive (false);
		popupConfirmation.SetActive (false);

		if(actionDelete == "deleteUser"){
			
			objConfirmationPopup.GetComponent<ScrollRankingGrupoScript> ().RemoveUserToRanking ();
			
		} else if (actionDelete == "deleteGroup"){

			objConfirmationPopup.GetComponent<ScrollTusRankingScript> ().RemoveGroupRanking ();
		}
	}

	public void CancelConfirmation(){

		popupMessage.SetActive (false);
		gameObject.SetActive (false);
		popupConfirmation.SetActive (false);

		GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ().SelectedButtonPanel(indexTabContinue);
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Globalization;

public class ScrollRankingGrupoScript : MonoBehaviour {

	public GameObject prefabContentRankingGroup;
	public GameObject ScrollContent;
	public AuxCrearGrupoRankingScript dataCurrentGroup;

	public Dropdown rankingOptions;

	public Text fechaText;
	public Text infNoPuntajeText;

	private int groupIDToShow = 0;
	private int currentGroupID = 0;

	private string urlPhpPronostico = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/GetRankingsUserChat.php?";

	private string urlPhpUltimoPartido = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/GetDate.php?";

	[SerializeField]
	private ContentRankingListScript rankingGroupList;

	public List<GameObject> rankingsGroupData = new List<GameObject>();

	private int idUserDelete;

	[SerializeField]
	public DateClassList fechaList;

	[SerializeField]
	private ContentRankingListScript userRanking;

	[SerializeField]
	private ContentRankingScript userDataRankingHeader;

	[SerializeField]
	private ContentRankingScript userDataRankingFooter;

	[SerializeField]
	private List<int> idUsersChat = new List<int>();

	public Text GroupTitleTxt;//Eduardo
	public string groupName;//Eduardo
	// Use this for initialization
	void Start () {
		GroupTitleTxt.text = groupName;//Eduardo
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	private IEnumerator ValidateStatusRankingGrupo (){

		rankingGroupList.dataList.Clear();

		WWW getData;

		foreach(int userIdChat in idUsersChat){

			getData = new WWW (urlPhpPronostico + "indata=buscarUsersRankingChat&userID=" + userIdChat);

			yield return getData;

			if (string.IsNullOrEmpty (getData.error)) {
	
				if (getData.text != "PredictingUncreated") {

					string cortada = getData.text.Replace("[","");

					cortada = cortada.Replace("]","");

					Debug.Log (getData.text);

					ContentRanking dato = JsonUtility.FromJson<ContentRanking> (cortada);
	
					rankingGroupList.dataList.Add(dato);
//					rankingGroupList = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
					//StartCoroutine (ShowUserRankingGroup ());
				} else {
	
					Debug.Log ("El grupo no tiene usuarios");
					EndUpdateData ("none");
				}
			} else {
	
				ShowMessagePopup("¡CONNECTION FAILED!", 0);
			}
		}

		rankingGroupList.dataList.Sort(
		
			delegate (ContentRanking a, ContentRanking b){
			
				return (b.points.CompareTo(a.points));
			}
		);


		Debug.Log (groupIDToShow);

		StartCoroutine (ShowUserRankingGroup ());
	}

//	private IEnumerator ValidateStatusRankingGrupo (){
//
//		Debug.Log (groupIDToShow);
//
//		WWW getData = new WWW (urlPhpPronostico + "indata=buscarUsersRankingGrupo&buscarDataID=" + groupIDToShow);
//		yield return getData;
//
//		if (string.IsNullOrEmpty (getData.error)) {
//
//			if (getData.text != "PredictingUncreated") {
//
//				rankingGroupList = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
//				StartCoroutine (ShowUserRankingGroup ());
//			} else {
//
//				Debug.Log ("El grupo no tiene usuarios");
//				//dataIsValidated = true;
//				//predecirBtn.SetActive (true);
//				//StartCoroutine (gameObject.GetComponent<GetFootballPlayersDataScript> ().GetFootballPlayersData ());
//				EndUpdateData ("none");
//			}
//		} else {
//
//			ShowMessagePopup("¡CONNECTION FAILED!", 0);
//		}
//
//		Debug.Log (getData.text);
//	}

	private IEnumerator ValidateStatusRankinGroupFecha (){

		WWW getData = new WWW (urlPhpUltimoPartido + "indata=getNextDate" + "&idDate=" + 2);
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "PredictingUncreated") {

				fechaList = JsonUtility.FromJson<DateClassList> (getData.text);
				StartCoroutine (GetUsersPorFecha ());
			} else {

				Debug.Log("¡No hay datos!");
			}
		} else {

			Debug.Log("¡CONNECTION FAILED!");
		}

		Debug.Log ("GETDATA: " + getData.text);
	}

	private IEnumerator GetUsersPorFecha(){

		DateTime date = DateTime.Parse (fechaList.dataList [0].fecha);
		//DateTime nextDate = DateTime.Parse (fechaList.dataList [0].fecha + " " + fechaList.dataList [0].hora);

		CultureInfo ci = new CultureInfo("es-ES");
		//DateTime pastMonth = Date.AddMonths(0);
		string mes =  date.ToString("MMMM",ci);
		string dia =  date.ToString("dddd",ci);
		string diaNum = date.ToString("dd",ci).ToUpper();

		fechaText.text = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(dia) + " " + diaNum + " de " + CultureInfo.CurrentCulture.TextInfo.ToTitleCase(mes);

		string fechaEncuentro = "&buscarFecha=" + date.Year + "-" + date.Month + "-" + date.Day;
		string idGrupoUser = "&userRankingID=" + currentGroupID;

		WWW getData = new WWW (urlPhpPronostico + "indata=PronosticoRankingFechaGrupo" + fechaEncuentro + idGrupoUser);
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "PredictingUncreated") {

				rankingGroupList = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
				StartCoroutine (ShowUserRankingGroup ());
			} else {

				//EndUpdateData ("none");
				//StartCoroutine(SetUserDataRanking ());
			}
		} else {

			ShowMessagePopup("¡CONNECTION FAILED!", 0);
		}

		Debug.Log (getData.text);

		Debug.Log ("FECHA PARTIDO ANTERIOR: " + date.Year + "-" + date.Month + "-" + date.Day);
	}

	private void SetUserDataRanking(ContentRanking obj, int index){

		if(obj.userID == DataApp.main.GetMyID()){

			userDataRankingHeader.posRankingText.text = index.ToString (); //CALCULAR LA POSICION
			userDataRankingHeader.nameText.text = obj.nameText;
			userDataRankingHeader.pointsText.text = obj.points.ToString ();

			userDataRankingHeader.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");

			userDataRankingFooter.posRankingText.text = index.ToString ();
			userDataRankingFooter.nameText.text = obj.nameText;
			userDataRankingFooter.pointsText.text = obj.points.ToString ();

			userDataRankingFooter.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");
		}
	}

//	private IEnumerator SetUserDataRanking(){
//
//		string urlPhp = "";
//
//		if(rankingOptions.value == 0){
//
//			urlPhp = urlPhpPronostico + "indata=buscarUserRanking&buscarUserID=" + DataApp.main.GetMyID();
//
//		} else if(rankingOptions.value == 1){
//
//			DateTime date = DateTime.Parse (fechaList.dataList [0].fecha);
//
//			string fechaEncuentro = "&buscarFecha=" + date.Year + "-" + date.Month + "-" + date.Day;
//
//			urlPhp = urlPhpPronostico + "indata=buscarUserRankingPorFecha" + fechaEncuentro + "&buscarUserID=" + DataApp.main.GetMyID();
//		}
//
//		WWW getData = new WWW(urlPhp);
//
//		yield return getData;
//
//		if (string.IsNullOrEmpty (getData.error)) {
//
//			if (getData.text != "PredictingUncreated") {
//
//				userRanking = JsonUtility.FromJson<ContentRankingListScript> (getData.text);
//				StartCoroutine (ShowUserRanking ());
//			} else {
//
//				//dataIsValidated = true;
//				//predecirBtn.SetActive (true);
//				//StartCoroutine (gameObject.GetComponent<GetFootballPlayersDataScript> ().GetFootballPlayersData ());
//				StartCoroutine(NoHayDatos());
//			}
//		} else {
//
//			ShowMessagePopup("¡CONNECTION FAILED!", 0);
//		}
//
//		Debug.Log (getData.text);
//	}

	private IEnumerator ShowUserRanking(){

		ContentRanking obj = userRanking.dataList[0];

		userDataRankingHeader.posRankingText.text = obj.userID.ToString (); //CALCULAR LA POSICION
		userDataRankingHeader.nameText.text = obj.nameText;
		userDataRankingHeader.pointsText.text = obj.points.ToString ();

		userDataRankingHeader.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID  + ".jpg");

		userDataRankingFooter.posRankingText.text = obj.userID.ToString ();
		userDataRankingFooter.nameText.text = obj.nameText;
		userDataRankingFooter.pointsText.text = obj.points.ToString ();

		userDataRankingFooter.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");

		Debug.Log (obj.userID.ToString ());

		yield return null;

		EndUpdateData ("none");
	}

	private IEnumerator NoHayDatos(){

		infNoPuntajeText.text = "No se ha generado puntake de la ultima fecha";

		//ContentRanking obj = userRanking.dataList[0];

		userDataRankingHeader.posRankingText.text = "0";
		//userDataRankingHeader.nameText.text = obj.nameText;
		userDataRankingHeader.pointsText.text = "0";

		//userDataRankingHeader.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (1 + ".jpg");

		userDataRankingFooter.posRankingText.text = "0";
		//userDataRankingFooter.nameText.text = obj.nameText;
		userDataRankingFooter.pointsText.text = "0";

		//userDataRankingFooter.userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID + ".jpg");

		//Debug.Log (obj.userID.ToString ());

		yield return null;

		EndUpdateData ("none");
	}

	private IEnumerator ShowUserRankingGroup(){

		ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);

		int index = 1;

		foreach(ContentRanking obj in rankingGroupList.dataList){
			
			ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, ScrollContent.GetComponent<RectTransform> ().sizeDelta.y + 100);

			GameObject panelData = Instantiate (prefabContentRankingGroup);
			//panelData.GetComponent<CanvasGroup> ().alpha = 0f;
			panelData.transform.SetParent (ScrollContent.transform);
			rankingsGroupData.Add (panelData);
			panelData.GetComponent<RectTransform>().anchoredPosition = new Vector3 (0, (index-1) * -100, 0);
			panelData.GetComponent<RectTransform>().localScale = new Vector3 (1, 1, 1);
			yield return new WaitForEndOfFrame (); //WaitForSeconds(0.1f);
			//panelData.GetComponent<ContentRankingScript> ().posRankingText = index;
			//panelData.GetComponent<ContentRankingScript> ().userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking ("PadrinoOro2.png");
			panelData.GetComponent<ContentRankingScript> ().objRankingsGroup = this.gameObject;
			panelData.GetComponent<ContentRankingScript> ().userID = obj.userID;
			panelData.GetComponent<ContentRankingScript> ().userPhoto.GetComponent<LoadImgRankingScript> ().loadImgRanking (obj.userID  + ".jpg");
			panelData.GetComponent<ContentRankingScript> ().nameText.text = obj.nameText;
			panelData.GetComponent<ContentRankingScript> ().pointsText.text = obj.points.ToString ();

			if (index == 1 || index == 2 || index == 3) {

				panelData.GetComponent<ContentRankingScript> ().cup.gameObject.SetActive (true);
				panelData.GetComponent<ContentRankingScript> ().cupRankingText.text = index.ToString ();

				if (index == 1) {
					panelData.GetComponent<ContentRankingScript> ().cup.color = new Color32 (250, 157, 0, 255);

				} else if (index == 2) {
					panelData.GetComponent<ContentRankingScript> ().cup.color = new Color32 (170, 170, 170, 255);

				} else if (index == 3) {
					panelData.GetComponent<ContentRankingScript> ().cup.color = new Color32 (124, 55, 24, 255);

				}
			} else {
				panelData.GetComponent<ContentRankingScript> ().posRankingText.gameObject.SetActive (true);
				panelData.GetComponent<ContentRankingScript> ().posRankingText.text = index.ToString ();

			}

			SetUserDataRanking(obj, index);

			index++;
		}

		if (ScrollContent.GetComponent<RectTransform> ().sizeDelta.y < 690) {

			ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 690);
		}


		EndUpdateData ("none");

		//StartCoroutine(SetUserDataRanking ());
	}

	public void ShowRankingGroupID(int rankingGroupID) {
//		DataApp2.main.EnableLoading();
		Debug.Log(rankingGroupID);

		gameObject.SetActive (true);

	
		groupIDToShow = rankingGroupID;

		if (currentGroupID != groupIDToShow) {

			ShowUpdatePopup ();

			idUsersChat.Clear();

			Parameter p1 = new Parameter("ChatId", rankingGroupID);
			List <Parameter> parameters = new List<Parameter>();
			parameters.Add(p1);

			ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
				foreach(ChatsMembers cm in chatmbs.list)
				{
					idUsersChat.Add(cm.MemberId);
					print("Nombre = " + cm.Members.DisplayName + " MemberId = " + cm.MemberId);
				}

				StartCoroutine(DestroyCurrentRankingData ());

			} ,PCI.CHATS_MEMBERS,parameters));

		}else{
//			DataApp2.main.DisableLoading();
		}

		//////////

//		groupIDToShow = rankingGroupID;
//
//		gameObject.SetActive (true);
//
//		if (currentGroupID != groupIDToShow) {
//
//			ShowUpdatePopup ();
//
//			StartCoroutine(DestroyCurrentRankingData ());
//
//			StartCoroutine(ValidateStatusRankingGrupo ());
//
//		}
	}

	private IEnumerator DestroyCurrentRankingData(){

		foreach(GameObject obj in rankingsGroupData){

			Destroy (obj);

			yield return obj;
		}

		rankingsGroupData.Clear ();

		StartCoroutine(ValidateStatusRankingGrupo ());
	}

	public void RunInserUserToRanking(){
	
		StartCoroutine (InsertUserToRanking (AuxCrearGrupoRankingScript.auxCrearGrupoRanking.guestUserID));
	}

	private IEnumerator InsertUserToRanking (int insertUserID){

		ShowUpdatePopup ();

		//string idAdmin = "&userID=" + 1234; // DataApp.main.GetMyID();
		string groupID = "&rankingID=" + currentGroupID;//rankingId;
		string userInserID = "&userRankingID=" + insertUserID;//userID;

		bool error = false;

		string urlPronostico = urlPhpPronostico + "indata=insertUserRanking" + groupID + userInserID;

		WWW setData = new WWW (urlPronostico.Replace(" ", "%20"));
		yield return setData;

		if (string.IsNullOrEmpty (setData.error)) {

			if(setData.text == "noExisteFueCreado"){

				UpdateScrollRankingGroup ();

			} else if(setData.text == "Existe"){

				ShowMessagePopup("¡Este usuario ya esta en el grupo!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			}

			Debug.Log (setData.text);

		} else {

			error = true;
			ShowMessagePopup("¡Connection Failed!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			Debug.Log ("Noo!!");
		}
																															
	}

	public void UpdateScrollRankingGroup (){

		int currentG_id = currentGroupID;

		currentGroupID = -1;

		rankingOptions.value = 0;

		ShowRankingGroupID (currentG_id);
	}

	public void RunRemoveUserToRanking (int idUserRemove, string name){

		idUserDelete = idUserRemove;

		ShowPopupConfirmation ("Quieres Eliminar al Usuario " + name +"?", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel, "deleteUser", this.gameObject);
	}

	public void RemoveUserToRanking (){

		StartCoroutine (RemoveUser ());
	}

	private IEnumerator RemoveUser (){
	
		Debug.Log (idUserDelete);

		ShowUpdatePopup ();

		//string idAdmin = "&userID=" + 1234; // DataApp.main.GetMyID();
		string groupID = "&rankingID=" + currentGroupID;//rankingId;
		string userInserID = "&userRankingID=" + idUserDelete;//userID;

		bool error = false;

		string urlPronostico = urlPhpPronostico + "indata=RemoveUserRanking" + groupID + userInserID;

		WWW setData = new WWW (urlPronostico.Replace(" ", "%20"));
		yield return setData;

		if (string.IsNullOrEmpty (setData.error)) {

			if(int.Parse(setData.text) == 1){

				UpdateScrollRankingGroup ();

			} else {

				ShowMessagePopup("¡Error al remover el usuario!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			}

			Debug.Log (setData.text);

		} else {

			error = true;
			ShowMessagePopup("¡Connection Failed!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			Debug.Log ("Noo!!");
		}

		//yield return null;
		HideUpdatePopup ();
	}

	/*void OnEnable() {

		if (currentGroupID != groupIDToShow) {

			ShowUpdatePopup ();

			StartCoroutine(ValidateStatusRankingGrupo ());

		}
	}*/


	void OnEnable (){
		
	}

	public void EndUpdateData(string state){

		currentGroupID = groupIDToShow;
		HideUpdatePopup ();
//		LoadingDummy.Loading.DisableLoading();
	}

	//ESCOGER RANKING GENERAL O POR PARTIDO:
	public void ChangeToRanking(){

		ShowUpdatePopup ();

		fechaText.text = "";
		infNoPuntajeText.text = "";

		StartCoroutine(DestroyCurrentRankingData ());

		if(rankingOptions.value == 0){

			StartCoroutine(ValidateStatusRankingGrupo ());

		} else if(rankingOptions.value == 1){

			StartCoroutine(ValidateStatusRankinGroupFecha ());
		}
	}

	private void ShowPopupConfirmation(string text, int indexTab, string action, GameObject obj){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopupConfirmation(text, indexTab, action, obj);
	}

	private void ShowUpdatePopup(){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowUpdateDataPanel();
	}

	private void HideUpdatePopup(){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().HideUpdateDataPanel();
	}

	private void ShowMessagePopup(string message, int indexTabContinue){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopup (message, indexTabContinue);
	}
}

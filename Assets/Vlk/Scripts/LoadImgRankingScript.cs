﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadImgRankingScript : MonoBehaviour {

	private string urlImage = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/Pronostico/Patrocinadores/";

	private bool imageIsLoad = false;

	private bool error = false;

	private string userPhotoName;

	private bool photoNameIsReady = false;

	IEnumerator LoadImage(){

		//WWW img = new WWW(urlImage + userPhotoName);
		WWW img = new WWW(DataApp2.main.urlUserMetadata + "Photos/" + userPhotoName);
		//WWW img = new WWW("http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/ImagenesComprimidasRanking/" + userPhotoName);
		//print("http://fcf.2waysports.com/2waysports/Colombia/Users/Photos" + userPhotoName);
		yield return img;

		if (string.IsNullOrEmpty (img.error)) {
			
			Rect size = new Rect (0, 0, img.texture.width, img.texture.height);
			gameObject.GetComponent<Image> ().sprite = Sprite.Create (img.texture, size, Vector2.zero);

		} else {

			error = true;
			//Debug.Log ("Load Image Failed");
			/*
			Debug.Log ("Load Image Failed");
			GameObject eventManager = GameObject.Find ("EventManager");
			eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopup ("Load Image Failed", eventManager.GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			*/
		}

		//imageIsLoad = (!error) ? true : false;
		imageIsLoad = true;
//		DataApp2.main.DisableLoading ();

	}

	public void loadImgRanking(string namePhoto){
//		DataApp2.main.EnableLoading ();
		userPhotoName = namePhoto;
		photoNameIsReady = true;	
			//StartCoroutine (LoadImage());
	}


	void OnEnable() {

		if (imageIsLoad == false && photoNameIsReady == true) {
				StartCoroutine (LoadImage());
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GroupRankingScript : MonoBehaviour {

	[SerializeField]
	private int groupID;
	public int _groupID {get{ return groupID; } set{ groupID = value; }}

	[SerializeField]
	private Text groupName;
	public Text _groupName {get{ return groupName; } set{ groupName = value; }}

	[SerializeField]
	private Button seeGroupBtn;
	public Button _seeGroupBtn {get{ return seeGroupBtn; } set{ seeGroupBtn = value; }}

	[SerializeField]
	private Button deleteGroupBtn;
	public Button _deleteGroupBtn {get{ return deleteGroupBtn; } set{ deleteGroupBtn = value; }}

	private GameObject tusRankingPanel;
	public GameObject _tusRankingPanel {get{ return tusRankingPanel; } set{ tusRankingPanel = value; }}

	private GameObject rankingGrupoPanel;
	public GameObject _rankingGrupoPanel {get{ return rankingGrupoPanel; } set{ rankingGrupoPanel = value; }}

	// Use this for initialization
	void Start () {

		seeGroupBtn.onClick.AddListener (() => OpenRankingGrupoPanel(tusRankingPanel, rankingGrupoPanel));
	}

	private void OpenRankingGrupoPanel(GameObject currentP, GameObject nextP){

		DisableTusRankingPanel (currentP);
		EnableRankingGrupoPanel (nextP);
	}

	private void DisableTusRankingPanel (GameObject currentP){

		currentP.SetActive (false);

		Debug.Log ("+++++" + currentP.name + " -> Panel Disable +++++");
	}

	private void EnableRankingGrupoPanel (GameObject nextP){


		nextP.GetComponent<ScrollRankingGrupoScript> ().ShowRankingGroupID (groupID);

		//nextP.SetActive (true);

		Debug.Log ("+++++" + nextP.name + " -> Panel Enable +++++");

	}

	public void DeleteGroupRanking(){

		tusRankingPanel.GetComponent<ScrollTusRankingScript> ().RemoveRanking (groupID, groupName.text);
	}
}

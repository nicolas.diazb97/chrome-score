﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ContentRankingScript : MonoBehaviour {

	public GameObject objRankingsGroup;

	public int userID;
	
	public Image cup;
	public Text cupRankingText;
	public Text posRankingText;

	public Image userPhoto;
	public Text nameText;

	public Text pointsText;

	public Button deleteButton;

	void Start () {

		//deleteButton.onClick.AddListener (() => DeleteUser());
	}

	public void DeleteUser(){
	
		objRankingsGroup.GetComponent<ScrollRankingGrupoScript> ().RunRemoveUserToRanking (userID, nameText.text);
	}
}

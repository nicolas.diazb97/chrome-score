﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NavigationTabPanelsScript : MonoBehaviour {

	public GameObject tabButtonsPanel;
	public List<Button> objButtons;
	public List<GameObject> objPanels;
	public LoadingDummy  Load;
	private int idCurrentEnablePanel;
	public int _idCurrentEnablePanel {get{ return idCurrentEnablePanel; } set{ idCurrentEnablePanel = value; }}

	[SerializeField]
	private GameObject updateDataPanel;
	public GameObject _updateDataPanel {get{ return updateDataPanel; } set{ updateDataPanel = value; }}

	private float speed = 8f;

	// Use this for initialization
	void Start () {
		LoadingDummy.Loading = Load;
		idCurrentEnablePanel = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DisableTabButtons (){

		tabButtonsPanel.GetComponent<CanvasGroup> ().blocksRaycasts = false;

		/*
		foreach(Button btn in objButtons){

			btn.interactable = false;
		}
		*/
	}

	public void EnableTabButtons (){

		tabButtonsPanel.GetComponent<CanvasGroup> ().blocksRaycasts = true;
		/*
		foreach(Button btn in objButtons){

			btn.interactable = true;
		}
		*/
	}

	public int GetidCurrentEnablePanel(){
	
		return idCurrentEnablePanel;
	}

	public void SelectedButtonPanel(int idButton){

		if (idCurrentEnablePanel != idButton) {

			int idCurrent = idCurrentEnablePanel;

			idCurrentEnablePanel = idButton;

			disableCurrentTabButton (idCurrent);

			EnableNextTabButton (idButton);

			//DisableCurrentPanel (idCurrent);
			StartCoroutine(DisableCurrentPanel (idCurrent));

			//EnableNextPanel (idButton);
			StartCoroutine(EnableNextPanel (idButton));

			//idCurrentEnablePanel = idButton;
		}
	}

	IEnumerator DisableCurrentPanel (int indexPanel){

		GameObject panelCurrent = objPanels [indexPanel];
		//panelCurrent.SetActive (false);

		CanvasGroup canvasGroup = panelCurrent.GetComponent<CanvasGroup> ();
		//canvasGroup.alpha = 0f;

		while(canvasGroup.alpha > 0f){
			canvasGroup.alpha -= Time.deltaTime * speed;
			yield return null;
		}

		panelCurrent.SetActive (false);

		Debug.Log ("+++++" + panelCurrent.name + " -> Panel Disable +++++");
	}

	IEnumerator EnableNextPanel (int indexPanel){

		GameObject panelNext = objPanels [indexPanel];
		//panelNext.SetActive (true);

		CanvasGroup canvasGroup = panelNext.GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 0f;
		panelNext.SetActive (true);

		while(canvasGroup.alpha < 1f){
			canvasGroup.alpha += Time.deltaTime * speed;
			yield return null;
		}

		Debug.Log ("+++++" + panelNext.name + " -> Panel Enable +++++");
	}

	void disableCurrentTabButton (int indexButton){
	
		Button buttonCurrent = objButtons [indexButton];
		buttonCurrent.GetComponent<Image>().color = new Color(1,1,1,0);
		Image img = buttonCurrent.transform.Find("Image").GetComponent<Image>();
		img.color = new Color32(122, 108, 19, 255);

		Debug.Log ("<<<<<" + buttonCurrent.name + " -> Button Disable >>>>>");
	}

	void EnableNextTabButton (int indexButton){

		Button buttonNext = objButtons [indexButton];
		buttonNext.GetComponent<Image>().color = new Color(0,0,0,255);
		Transform img = buttonNext.transform.Find("Image");
		img.GetComponent<Image>().color = new Color32(238,194,27,255);

		Debug.Log ("<<<<<" + buttonNext.name + " -> Button Enable >>>>>");
	}

	public void ShowUpdateDataPanel(){

		updateDataPanel.SetActive (true);
		updateDataPanel.GetComponent<UpdateDataScript> ().RunUpdatePanel ();
	}

	public void HideUpdateDataPanel(){
		
		updateDataPanel.GetComponent<UpdateDataScript> ().StopUpdatePanel ();
	}

	public void ShowPopup(string text, int indexTabContinue){

		//gameObject.GetComponent<FadePanelsScript> ().FadeInPanel (updateDataPanel,1);
		updateDataPanel.SetActive (true);
		updateDataPanel.GetComponent<UpdateDataScript> ().RunPopup (text, indexTabContinue);
	}

	public void HideUpdaPopup(){

		updateDataPanel.GetComponent<UpdateDataScript> ().StopUpdatePanel ();
	}

	public void ShowPopupConfirmation(string text, int indexTab, string action, GameObject obj){

		updateDataPanel.SetActive (true);
		updateDataPanel.GetComponent<UpdateDataScript> ().RunPopupConfirmation (text, indexTab, action, obj);
	}
}

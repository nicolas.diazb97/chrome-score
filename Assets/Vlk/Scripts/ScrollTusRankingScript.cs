﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScrollTusRankingScript : MonoBehaviour {

	public GameObject prefabContentRanking;
	public GameObject ScrollContent;

	public GameObject currentPanel;
	public GameObject nextPanel;

	[SerializeField]
	private GroupsRankingListScript groupsRankingList;

	public List<GameObject> groupsRankingsData = new List<GameObject>();

	private string urlPhpPronostico = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/UserTiroAmarillo.php?";

	private bool groupsRankingIsLoad = false;

	private int idRankingDelete;

	private IEnumerator ValidateStatusGroupsRanking (){

		//WWW getData = new WWW (urlPhpPronostico + "indata=buscarGroupsRanking&buscarUserID=1234"); //DataApp.main.GetMyID ();
		WWW getData = new WWW (urlPhpPronostico + "indata=buscarGroupsRanking&buscarUserID=" + DataApp.main.GetMyID ());
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "GroupsRankingUncreated") {

				groupsRankingList = JsonUtility.FromJson<GroupsRankingListScript> (getData.text);
				StartCoroutine (ShowGroupsRanking ());
			} else {

				//dataIsValidated = true;
				//predecirBtn.SetActive (true);
				//StartCoroutine (gameObject.GetComponent<GetFootballPlayersDataScript> ().GetFootballPlayersData ());
				EndUpdateData ("none");
			}
		} else {

			ShowMessagePopup("¡CONNECTION FAILED!", 0);
		}

		Debug.Log (getData.text);
	}

	private IEnumerator ShowGroupsRanking(){
		
		ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 0);

		int index = 0;

		foreach(GroupsRanking obj in groupsRankingList.dataList){

			ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, ScrollContent.GetComponent<RectTransform> ().sizeDelta.y + 85);

			GameObject panelData = Instantiate (prefabContentRanking);
			//panelData.GetComponent<CanvasGroup> ().alpha = 0f;
			panelData.transform.SetParent (ScrollContent.transform);
			groupsRankingsData.Add (panelData);
			panelData.GetComponent<RectTransform>().anchoredPosition = new Vector3 (0, index * -85, 0);
			panelData.GetComponent<RectTransform>().localScale = new Vector3 (1, 1, 1);
			yield return new WaitForEndOfFrame (); //WaitForSeconds(0.1f);
			panelData.GetComponent<GroupRankingScript> ()._tusRankingPanel = currentPanel;
			panelData.GetComponent<GroupRankingScript> ()._rankingGrupoPanel = nextPanel;
			panelData.GetComponent<GroupRankingScript> ()._groupID = obj.groupID;
			panelData.GetComponent<GroupRankingScript> ()._groupName.text = obj.groupName;

			index++;
		}

		if (ScrollContent.GetComponent<RectTransform> ().sizeDelta.y < 690) {
		
			ScrollContent.GetComponent<RectTransform> ().sizeDelta = new Vector2 (0, 690);
		}

		EndUpdateData ("none");

		//StartCoroutine(SetUserDataRanking ());
	}

	public void NewRankingIsCreate(){ //Esto es con el fin de que cuando regrece a TusRankings se muestre el nuevo grupo creado
	
		groupsRankingIsLoad = false;
	}

	public void RemoveRanking(int idRankingRemove, string name){

		groupsRankingIsLoad = false;

		idRankingDelete = idRankingRemove;

		ShowPopupConfirmation ("Quieres Eliminar al Ranking " + name +"?", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel, "deleteGroup", this.gameObject);

	}

	public void RemoveGroupRanking (){

		StartCoroutine (RemoveRankingG ());
	}

	private IEnumerator RemoveRankingG (){

		Debug.Log (idRankingDelete);

		ShowUpdatePopup ();

		//string idAdmin = "&userID=" + 1234; // DataApp.main.GetMyID();
		string groupID = "&rankingID=" + idRankingDelete;//rankingId;

		bool error = false;

		string urlPronostico = urlPhpPronostico + "indata=RemoveRanking" + groupID;

		WWW setData = new WWW (urlPronostico.Replace(" ", "%20"));
		yield return setData;

		if (string.IsNullOrEmpty (setData.error)) {

			if(int.Parse(setData.text) == 1){

				StartCoroutine(DestroyCurrentRankingData ());

				StartCoroutine(ValidateStatusGroupsRanking ());

			} else {

				ShowMessagePopup("¡Error al remover el usuario!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			}

			Debug.Log (setData.text);

		} else {

			error = true;
			ShowMessagePopup("¡Connection Failed!", GameObject.Find ("EventManager").GetComponent<NavigationTabPanelsScript> ()._idCurrentEnablePanel);
			Debug.Log ("Noo!!");
		}

		//yield return null;
		//HideUpdatePopup ();
	}

	private IEnumerator DestroyCurrentRankingData(){

		foreach(GameObject obj in groupsRankingsData){

			Destroy (obj);

			yield return obj;
		}

		groupsRankingsData.Clear ();
	}

	void OnEnable() {

		if (groupsRankingIsLoad == false) {

			ShowUpdatePopup ();

			StartCoroutine(DestroyCurrentRankingData ());

			StartCoroutine(ValidateStatusGroupsRanking ());

		}
	}	

	private void ShowPopupConfirmation(string text, int indexTab, string action, GameObject obj){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopupConfirmation(text, indexTab, action, obj);
	}

	public void EndUpdateData(string state){

		groupsRankingIsLoad = true;
		HideUpdatePopup ();
	}

	private void ShowUpdatePopup(){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowUpdateDataPanel();
	}

	private void HideUpdatePopup(){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().HideUpdateDataPanel();
	}

	private void ShowMessagePopup(string message, int indexTabContinue){

		GameObject eventManager = GameObject.Find ("EventManager");
		eventManager.GetComponent<NavigationTabPanelsScript> ().ShowPopup (message, indexTabContinue);
	}
}

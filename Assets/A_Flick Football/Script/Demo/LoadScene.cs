﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Net;


public class LoadScene : MonoBehaviour
{

    public static int countLoadScene = 2;
    public string sceneName;
    public GameObject Loading, comprarNivelesPanel;
    public GameObject PanelLog;
    public Text InfoInternetFail;
    public GameObject checkInternet;
    public GameObject PanelRegsitrar;
    public bool hasInternet;
    WWW getVotosTotalCamisetas;

    public Text avisoCerrarSesion;


    private bool jugar;

    public GameObject PlayersHolder;
    public Text LogTxt;
    public Button WelcomeExitBtn;

    void Update()
    {
        LogTxt.text = (DataApp2.main.IsRegistered()) ? "Cerrar Sesión" : "Iniciar Sesión";
    }

    public void LoginBtnHandler()
    {
        if (DataApp2.main.IsRegistered())
        {
            LogTxt.text = "Cerrar Sesión";
            CerrarSesion();
        }
        else
        {
            WelcomeExitBtn.onClick.AddListener(() =>
            {
                PlayersHolder.SetActive(true);
                WelcomeExitBtn.onClick.RemoveAllListeners();
            });
            PanelLog.SetActive(true);
        }
    }

    public void CerrarSesion()
    {
        StartCoroutine(_CerrarSesion());
    }

    IEnumerator _CerrarSesion()
    {

        avisoCerrarSesion.text = "Cerrando Sesión ...";
        DataApp2.main.EnableLoading();
        yield return new WaitForSeconds(5);
        PlayerPrefs.DeleteAll();
        TransactionalProcess.isActive = 0;
        Conection.tempId = "";
        Conection.isLog = false;
        Conection.pass = "";
        SceneManager.LoadScene("TiroTriColor");
    }


    //tiros libres
    public void LoadSceneName(int nivel)
    {
        DataApp2.main.EnableLoading();
        PlayerPrefs.SetInt("Creloj", 0);
        respawn.nivel = nivel;
        print("tiros libres");

        if (nivel == 1)
        {
            if (PlayerPrefs.GetInt("tn1") != 2)
            {
                PlayerPrefs.SetInt("estrellasGanadasN1", 0);
                PlayerPrefs.SetInt("tn1", 1);
            }
            jugar = true;
        }

        if (nivel == 2)
        {
            if (PlayerPrefs.GetInt("tn1") == 2)
            {
                if (PlayerPrefs.GetInt("tn2") != 2)
                {
                    PlayerPrefs.SetInt("estrellasGanadasN2", 0);
                    PlayerPrefs.SetInt("tn2", 1);
                }
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 3)
        {
            if (PlayerPrefs.GetInt("tn2") == 2)
            {
                if (PlayerPrefs.GetInt("tn3") != 2)
                {
                    PlayerPrefs.SetInt("estrellasGanadasN3", 0);
                    PlayerPrefs.SetInt("tn3", 1);
                }
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 4)
        {
            if (PlayerPrefs.GetInt("tn3") == 2)
            {
                if (PlayerPrefs.GetInt("tn4") != 2)
                {
                    PlayerPrefs.SetInt("estrellasGanadasN4", 0);
                    PlayerPrefs.SetInt("tn4", 1);
                }
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }
        if (nivel == 5)
        {
            if (PlayerPrefs.GetInt("tn4") == 2)
            {
                if (PlayerPrefs.GetInt("tn5") != 2)
                {
                    PlayerPrefs.SetInt("estrellasGanadasN5", 0);
                    PlayerPrefs.SetInt("tn5", 1);
                }
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        DataApp2.main.DisableLoading();
        if (jugar)
        {
            jugar = false;
            StartCoroutine(WaitResponsive());
        }
    }

    //tiros libres contra el reloj
    public void LoadContraReloj()
    {
        Debug.Log("Preparando la escena de contra reloj");
        DataApp2.main.EnableLoading();
        PlayerPrefs.SetInt("Creloj", 1);
        DataApp2.main.DisableLoading();
        RegisterLoginManager.panel = 1;

        StartCoroutine(WaitResponsive());
    }

    //reto 1

    //los playerpref de los retos funcionan con uno para entrar y dos para saber si paso el nivel
    public void Loadreto1(int nivel)
    {
        DataApp2.main.EnableLoading();
        PlayerPrefs.SetInt("Creloj", 2);
        respawn.nivel = nivel;

        Debug.Log("Este es el reto 1");
        Debug.Log("Respawn on level..." + nivel);
        Debug.Log("Creloj Pref Value = " + PlayerPrefs.GetInt("Creloj"));

        print("reto 1");
        if (nivel == 1)
        {
            if (PlayerPrefs.GetInt("r1n1") != 2)
                PlayerPrefs.SetInt("r1n1", 1);
            jugar = true;
        }

        if (nivel == 2)
        {
            if (PlayerPrefs.GetInt("r1n1") == 2)
            {
                if (PlayerPrefs.GetInt("r1n2") != 2)
                    PlayerPrefs.SetInt("r1n2", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 3)
        {
            if (PlayerPrefs.GetInt("r1n2") == 2)
            {
                if (PlayerPrefs.GetInt("r1n3") != 2)
                    PlayerPrefs.SetInt("r1n3", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 4)
        {
            if (PlayerPrefs.GetInt("r1n3") == 2)
            {
                if (PlayerPrefs.GetInt("r1n4") != 2)
                    PlayerPrefs.SetInt("r1n4", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        DataApp2.main.DisableLoading();
        if (jugar)
        {
            jugar = false;
            StartCoroutine(WaitResponsive());
        }

    }

    //reto 2
    public void Loadreto2(int nivel)
    {
        DataApp2.main.EnableLoading();
        PlayerPrefs.SetInt("Creloj", 3);
        respawn.nivel = nivel;

        Debug.Log("Este es el reto 2");
        Debug.Log("Respawn on level..." + nivel);
        Debug.Log("Creloj Pref Value = " + PlayerPrefs.GetInt("Creloj"));

        if (nivel == 1)
        {
            if (PlayerPrefs.GetInt("r2n1") != 2)
                PlayerPrefs.SetInt("r2n1", 1);
            jugar = true;
        }

        if (nivel == 2)
        {
            if (PlayerPrefs.GetInt("r2n1") == 2)
            {
                if (PlayerPrefs.GetInt("r2n2") != 2)
                    PlayerPrefs.SetInt("r2n2", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 3)
        {
            if (PlayerPrefs.GetInt("r2n2") == 2)
            {
                if (PlayerPrefs.GetInt("r2n3") != 2)
                    PlayerPrefs.SetInt("r2n3", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 4)
        {
            if (PlayerPrefs.GetInt("r2n3") == 2)
            {
                if (PlayerPrefs.GetInt("r2n4") != 2)
                    PlayerPrefs.SetInt("r2n4", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        DataApp2.main.DisableLoading();
        if (jugar)
        {
            jugar = false;
            StartCoroutine(WaitResponsive());
        }
    }

    //reto 3
    public void Loadreto3(int nivel)
    {
        DataApp2.main.EnableLoading();
        PlayerPrefs.SetInt("Creloj", 4);
        respawn.nivel = nivel;

        Debug.Log("Este es el reto 3");
        Debug.Log("Respawn on level..." + nivel);
        Debug.Log("Creloj Pref Value = " + PlayerPrefs.GetInt("Creloj"));

        if (nivel == 1)
        {
            if (PlayerPrefs.GetInt("r3n1") != 2)
                PlayerPrefs.SetInt("r3n1", 1);
            jugar = true;
        }

        if (nivel == 2)
        {
            if (PlayerPrefs.GetInt("r3n1") == 2)
            {
                if (PlayerPrefs.GetInt("r3n2") != 2)
                    PlayerPrefs.SetInt("r3n2", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 3)
        {
            if (PlayerPrefs.GetInt("r3n2") == 2)
            {
                if (PlayerPrefs.GetInt("r3n3") != 2)
                    PlayerPrefs.SetInt("r3n3", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 4)
        {
            if (PlayerPrefs.GetInt("r3n3") == 2)
            {
                if (PlayerPrefs.GetInt("r3n4") != 2)
                    PlayerPrefs.SetInt("r3n4", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        DataApp2.main.DisableLoading();
        if (jugar)
        {
            jugar = false;
            StartCoroutine(WaitResponsive());
        }
    }

    //reto 4
    public void Loadreto4(int nivel)
    {
        DataApp2.main.EnableLoading();
        PlayerPrefs.SetInt("Creloj", 5);
        respawn.nivel = nivel;

        Debug.Log("Este es el reto 4");
        Debug.Log("Respawn on level..." + nivel);
        Debug.Log("Creloj Pref Value = " + PlayerPrefs.GetInt("Creloj"));

        if (nivel == 1)
        {
            if (PlayerPrefs.GetInt("r4n1") != 2)
                PlayerPrefs.SetInt("r4n1", 1);
            jugar = true;
        }

        if (nivel == 2)
        {
            if (PlayerPrefs.GetInt("r4n1") == 2)
            {
                if (PlayerPrefs.GetInt("r4n2") != 2)
                    PlayerPrefs.SetInt("r4n2", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 3)
        {
            if (PlayerPrefs.GetInt("r4n2") == 2 )
            {
                if (PlayerPrefs.GetInt("r4n3") != 2)
                    PlayerPrefs.SetInt("r4n3", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        if (nivel == 4)
        {
            if (PlayerPrefs.GetInt("r4n3") == 2)
            {
                if (PlayerPrefs.GetInt("r4n4") != 2)
                    PlayerPrefs.SetInt("r4n4", 1);
                jugar = true;
            }
            else
            {
                DataApp2.main.popUpInformative(true, "No puedes jugar este nivel", "Para jugar este nivel debes haber superado el anterior");
            }
        }

        DataApp2.main.DisableLoading();
        if (jugar)
        {
            jugar = false;
            StartCoroutine(WaitResponsive());
        }
    }


    IEnumerator WaitResponsive()
    {
        DataApp2.main.EnableLoading();
        countLoadScene = 2;
        Application.backgroundLoadingPriority = ThreadPriority.Low;
        yield return new WaitForSeconds(2f);
        if(respawn.nivel == 1)
            SceneManager.LoadSceneAsync("EscenaTiros");
        else if(respawn.nivel < 4)
            SceneManager.LoadSceneAsync("EscenaTirosEstadio1");
        else
            SceneManager.LoadSceneAsync("EscenaTirosEstadio2");
    }

    private IEnumerator CheckInternet()
    {
        Debug.Log("Busca internet por aca");

        DataApp2.main.EnableLoading();
        //		getVotosTotalCamisetas = new WWW ("http://2waysports.com/2waysports/Ecuador/Barcelona/Camiseta/getVotosTotalCamisetas.php");
        getVotosTotalCamisetas = new WWW("http://fcf.2waysports.com/goloficial//IsConnection.php");

        yield return getVotosTotalCamisetas;
        //		hasInternet = !string.IsNullOrEmpty (getVotosTotalCamisetas.text);
        hasInternet = (getVotosTotalCamisetas.text == "ConexionEstablecida");

        if (hasInternet)
        {
            if (DataApp2.main.IsRegistered())
            {
                DataApp2.main.DisableLoading();
                SceneManager.LoadScene("Partido_02");
            }
            else if (!DataApp2.main.IsRegistered())
            {
                DataApp2.main.DisableLoading();
                //				PanelRegsitrar.SetActive(true);
                PanelLog.SetActive(true);
            }
        }
        else if (!hasInternet)
        {
            DataApp2.main.popUpInformative(true, "", "Por favor conectate a internet para poder jugar.");
            DataApp2.main.DisableLoading();
        }
    }

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Runtime.InteropServices;
using UnityEngine.SceneManagement;
using System.CodeDom.Compiler;


public class respawn : MonoBehaviour
{
    public static int nivel = 1;
    public static bool primeraInstancia;
    public Text showTiros, puntosPorNivelText, txtTutorialRetos;
    public Text NivelText, TextPuntajes, textoTiempoCR, tiempoAdicionalText, cuentaAtras, nombreNivel;
    public string textoTiros = "";
    public static int cantidadTiros = 1;
    public static int cantidadGoles = 0;
    public int bGoalCount = 0;

    public GameObject rellenoBarra, particulas, estrellas, barraTiros, barraContraReloj, panelComprar, barrera, flechasRetos, panelTutorialTiros, panelTutorialRetos;
    public Sprite linea, reto1, reto2, reto3, reto4, estrellaDorada;
    public Image imageNivel;
    public Image imageFelicitaciones;
    public Sprite felicitaciones1;
    public Sprite felicitaciones2;
    public Sprite felicitaciones3DRAW;
    public GameObject Loading;
    public GameObject Tutorial;
    public GameObject PanelResponsive, exploder, botonContinuar, obj_TutBar, obj_BarMessage;
    public GameObject plataformaDerecha, plataformaIzquierda, platillo, aro, plataformaTutorial, platilloTutorial, aroTutorial, moneda;
    public float tiempo, tiempoAdicional, tiempoTotal;
    public Color CYellow, Cred;
    public StringEvent onEnemyGoal;
    int p1Score = 0;
    int p2Score = 0;
    public Text ownScore;
    public Text enemyScore;
    public Text winnerText;
    public bool readyToShowResults;
    public Animator p1Anim;
    public Animator p2Anim;


    [SerializeField]
    private int puntaje = 0;
    private int userID = 0;
    public bool nextLevel;

    private bool showImage;

    public GameObject _NivelText, _MsgText;//_DisparosText;
    public CanvasGroup PanelPerder, PanelGanar, PanelPuntaje;

    float temTime = 0.3f;
    public static bool ControlLose;
    float count = 3;
    private Vector2 textoPuntosPosicionInicial;
    private bool primerGol;
    public int puntosPorNivel, golesTotales;
    private float tiempoReloj;
    private bool desvanecer, asignarValores;
    private Color tAdicionalColorInicial;

    //posiciones plataforma
    [Header("Debugging Params")]
    public bool _controlLose;
    int uploadFramesCounts = 0;
    bool bNewRecord = false;

    void Awake()
    {
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
        //		if () {
        //			
        //		}
        //Screen.orientation = ScreenOrientation.Landscape;
        //

        PlayerPrefs.SetInt("Tutorial", 0);

        if (PlayerPrefs.GetInt("Tutorial") == 0)
        {
            Tutorial.SetActive(true);
            _NivelText.SetActive(false);
            panelTutorialRetos.SetActive(false);
            panelTutorialTiros.SetActive(false);

            if (PlayerPrefs.GetInt("Creloj") == 0)
            {
                Debug.Log("Tutorial Tiros Libres? " + PlayerPrefs.GetInt("Creloj"));
                panelTutorialTiros.SetActive(true);
            }
            else if (PlayerPrefs.GetInt("Creloj") == 1)
            {
                //                Debug.Log ("Tutorial Contrareloj " + PlayerPrefs.GetInt ("Creloj"));
                panelTutorialRetos.SetActive(true);
                if (GlobalData.idiomaActivo == Idioma.ES)
                    txtTutorialRetos.text = "Suma puntos marcando goles\n antes de que se acabe el tiempo";
                else
                    txtTutorialRetos.text = "Score points by scoring goals\n before the time runs out";
                obj_BarMessage.SetActive(false);
                obj_TutBar.SetActive(false);
            }
            else if (PlayerPrefs.GetInt("Creloj") == 2)
            {
                Debug.Log("Tutorial Rebote " + PlayerPrefs.GetInt("Creloj"));
                panelTutorialRetos.SetActive(true);
                if (GlobalData.idiomaActivo == Idioma.ES)
                    txtTutorialRetos.text = "Deberás disparar hacia la pared para\n que los rebotes entren en el arco";
                else
                    txtTutorialRetos.text = "You should shoot towards the wall to\n let the rebounds enter the goal";
            }
            else if (PlayerPrefs.GetInt("Creloj") == 3)
            {
                Debug.Log("Tutorial Aros " + PlayerPrefs.GetInt("Creloj"));
                panelTutorialRetos.SetActive(true);
                if (GlobalData.idiomaActivo == Idioma.ES)
                    txtTutorialRetos.text = "Deberás hacer cruzar el balón por \n todos los aros y anotar el gol";
                else
                    txtTutorialRetos.text = "You should have the ball crossed by\n all the rings and score the goal";
            }
            else if (PlayerPrefs.GetInt("Creloj") == 4)
            {
                Debug.Log("Tutorial Tiro Largo " + PlayerPrefs.GetInt("Creloj"));
                panelTutorialRetos.SetActive(true);
                txtTutorialRetos.text = "Deberás patear al arco a larga distancia.";
            }
            else if (PlayerPrefs.GetInt("Creloj") == 5)
            {
                Debug.Log("Tutorial Platillos " + PlayerPrefs.GetInt("Creloj"));
                exploder.SetActive(true);
                panelTutorialRetos.SetActive(true);
                txtTutorialRetos.text = "Dale a los platillos y anota el gol";
                //				plataformaTutorial.SetActive (false);
                //				aroTutorial.SetActive (false);
                //				platilloTutorial.SetActive (false);
            }

            //_DisparosText.SetActive(false);
        }

        //cantidad de tiros para las diferentes modalidades
        //pd: puto el que lo lea
        PuntosPorNivel();
        valoresParaCadaModoDeJuego();
        puntosPorNivelText.text = "<color=#FFFFFFFF>" + puntosPorNivel.ToString() + "</color>";
        CambiarImgYTextoNivel();
        imageFelicitaciones.transform.parent.gameObject.SetActive(false);
        //StartCoroutine(LoadingReser());

    }

    public void LoadSceneNamein(string name)
    {
        SceneManager.LoadScene(name);
    }


    void Start()
    {
        //		Debug.Log ("respawn started");
        CambiarImgYTextoNivel();
        DatabaseManager.main.OnMatchEnded.AddListener(() =>
        {
            Debug.LogError("se acaboooooooooooooooooo 2");
            readyToShowResults = true;
        });
        onEnemyGoal.AddListener((_resultsP1, _resultsP2) =>
        {
            int tempP1Score = 0;
            int tempP2Score = 0;
            Debug.LogError("AHI VIEBEEEEEEEEEEEEEEEE");
            foreach (var item in _resultsP1)
            {
                int convertedInt;
                // attempt to parse the value using the TryParse functionality of the integer type
                int.TryParse(item, out convertedInt);
                tempP1Score += convertedInt;
            }
            foreach (var item in _resultsP2)
            {
                int convertedInt;
                // attempt to parse the value using the TryParse functionality of the integer type
                int.TryParse(item, out convertedInt);
                tempP2Score += convertedInt;
            }
            ownScore.text = "puntaje:       " + tempP1Score.ToString();
            enemyScore.text = "puntaje:       " + tempP2Score.ToString();

            if (p2Score < tempP2Score)
            {
                p2Anim.SetTrigger("goal");
            }
            if (p1Score < tempP1Score)
            {
                p1Anim.SetTrigger("goal");
            }
            if (MatchManager.main.currPlayerNumber == "P1")
            {
                if (p2Score < tempP2Score)
                {
                    GoalDetermine.share.EnemyGoal();
                }
            }
            else
            {
                if (p1Score < tempP1Score)
                {
                    GoalDetermine.share.EnemyGoal();
                }
            }
            p1Score = tempP1Score;
            p2Score = tempP2Score;
        });
        DatabaseManager.main.OnEnemyGoal.AddListener((_resultsP1, _resultsP2) =>
        {
            onEnemyGoal.Invoke(_resultsP1, _resultsP2);
        });
        DatabaseManager.main.RegisterForNewKick();
        PlayerPrefs.SetInt("r1p", 0);
        PlayerPrefs.SetInt("r2p", 0);
        PlayerPrefs.SetInt("r3p", 0);
        PlayerPrefs.SetInt("r4p", 0);
        //		if( LoadScene.countLoadScene > 0 ){
        //			Debug.Log("reset: " + LoadScene.countLoadScene);
        //			LoadScene.countLoadScene--;
        //			SceneManager.LoadScene( "Partido_02" );
        //		}

        if (PlayerPrefs.GetInt("Creloj") == 2)
        {
            BarraMovil.NumeroRandom();
        }
        else if (PlayerPrefs.GetInt("Creloj") == 5)
        {
            primeraInstancia = true;
        }

        //textoPuntosPosicionInicial = GoalDetermine.share.PuntajeText.rectTransform.anchoredPosition;
        rellenoBarra.GetComponent<Slider>().value = 0;
        //Debug.Log("llamado");
        imageFelicitaciones.transform.parent.gameObject.SetActive(false);
        cantidadGoles = 0;
        PlayerPrefs.SetInt("level", nivel);
        tiempo = 0;
        ControlLose = false;

        GoalDetermine.Puntaje = 0;
        scoreUploaded = false;

        PlayerPrefsTracking.main.LogGameplayScreen();
    }

    IEnumerator VelocityTutorial()
    {
        temTime = temTime + 0.015f;
        yield return new WaitForSeconds(1f);
    }



    public void TerminarTutorial()
    {
        if (PlayerPrefs.GetInt("Creloj") == 1)
        {
            PlayerPrefs.SetInt("TimeTrial", 1);
        }

        Tutorial.SetActive(false);
        PlayerPrefs.SetInt("Tutorial", 1);
        _NivelText.SetActive(true);
        //_DisparosText.SetActive(true);
        _MsgText.SetActive(true);
    }


    void Update()
    {
        bGoalCount = cantidadGoles;
        //        Debug.Log("Estado del tutorial = " + PlayerPrefs.GetInt("Tutorial"));
        /**Aqui ocultamos el menu del tutorial mientras se patea para no afectar la visualizacion del jugador*/
        {
            if (PlayerPrefs.GetInt("Tutorial") == 0)
            {
                if (PlayerPrefs.GetInt("Creloj") == 0)
                {
                    panelTutorialTiros.SetActive(!Shoot.share.afterShoot);

                }
                else
                {
                    panelTutorialRetos.SetActive(!Shoot.share.afterShoot);
                }
            }

        }
        _controlLose = ControlLose;
        //	Screen.orientation = ScreenOrientation.Landscape;
        //		
        //Debug.Log("controlLose "+ControlLose);
        Destroy(GameObject.Find("New Game Object"));

        //print ("cantidad tiros "+cantidadTiros);


        //		if(PlayerPrefs.GetInt ("r1p")==1){
        //			print("punto reto 1");
        //		}else if(PlayerPrefs.GetInt ("r2p")==1){
        //			print("punto reto 2");
        //		}else if(PlayerPrefs.GetInt ("r3p")==1){
        //			print("punto reto 3");
        //		}else if(PlayerPrefs.GetInt ("r4p")==1){
        //			print("punto reto 4");
        //		}

        //print ("puntaje "+GoalDetermine.Puntaje);

        if (PlayerPrefs.GetInt("Creloj") == 3)
        {
            if (PlayerPrefs.GetInt("r2p") == 1)
            {
                print("punto reto 2");
            }
            else if (PlayerPrefs.GetInt("r2p") == 0)
            {
                print("putas ganas de joder");
            }
        }

        if (PlayerPrefs.GetInt("Tutorial") == 0 && PlayerPrefs.GetInt("Creloj") == 0 && PlayerPrefs.GetInt("tn1") == 1)
        {
            if (!collisionBall.isTouchBall)
            {
                Time.timeScale = temTime;
                if (temTime < 1)
                    StartCoroutine(VelocityTutorial());
            }
            else
            {
                temTime = 0.3f;
                Time.timeScale = temTime;
            }
        }

        if (PlayerPrefs.GetInt("Creloj") == 1 && PlayerPrefs.GetInt("TimeTrial") == 1 ||
            PlayerPrefs.GetInt("Creloj") == 0 && PlayerPrefs.GetInt("tn1") == 2 ||
            PlayerPrefs.GetInt("Creloj") == 2 && PlayerPrefs.GetInt("r1n1") == 2 ||
            PlayerPrefs.GetInt("Creloj") == 3 && PlayerPrefs.GetInt("r2n1") == 2 ||
            PlayerPrefs.GetInt("Creloj") == 4 && PlayerPrefs.GetInt("r3n1") == 2 ||
            PlayerPrefs.GetInt("Creloj") == 5 && PlayerPrefs.GetInt("r4n1") == 2)
        {
            //            print("Salga del tutorial");
            TerminarTutorial();
        }
        else
        {
            print("Quedarse en el tutorial");
        }


        ConfiguracionProx();

        VerificarSiSeHizoGol();

        if (desvanecer)
        {
            //print ("desvancer el puto texto");
            StartCoroutine(DesvanecerTiempoAdicional());
        }
        //Debug.Log ("puntaje "+GoalDetermine.Puntaje);



        if (PlayerPrefs.GetInt("Tutorial") == 1)
        {
            if (PlayerPrefs.GetInt("restarTiro") == 0)
            {
                Debug.LogError("esta en tutorial esta sapa mierda " + cantidadTiros);
                if (cantidadTiros <= 15)
                    DatabaseManager.main.WriteNewScore(cantidadTiros.ToString(), MatchManager.main.GetResultByIndex(cantidadTiros), MatchManager.main.currPlayerNumber);
                cantidadTiros--;
                MatchManager.main.currShoot = cantidadTiros;
                if (cantidadTiros <= 0)
                {
                    cantidadTiros = 0;
                }
                textoTiros = cantidadTiros.ToString();
                PlayerPrefs.SetInt("restarTiro", 1);
            }
        }


        showTiros.text = textoTiros;
        if (cantidadTiros == 0)
        {
            DatabaseManager.main.PlayerEndMatch(cantidadTiros.ToString(), MatchManager.main.currPlayerNumber);
        }
        if (cantidadTiros == 0 && ControlLose == false && asignarValores)
        {
            print("perder");
            ControlLose = true;
            // PIERDE EL JUEGO -----
            PanelPerder.alpha = 1f;
            PanelGanar.alpha = 0f;
            tiempo += Time.deltaTime;
            TextPuntajes.gameObject.SetActive(false);
            PanelGanar.gameObject.SetActive(false);
            cambiarIMGfelicitaciones(felicitaciones2);
            fondoEstrellas.SetActive(false);
            panelSoloGanar.SetActive(false);
            panelSoloPerder.SetActive(true);
            btnShare.SetActive(false);
            imageFelicitaciones.transform.parent.gameObject.SetActive(true);
        }

        if (cantidadTiros == 0 && asignarValores && !readyToShowResults)
        {

            cambiarIMGfelicitaciones(felicitaciones3DRAW);
        }
        if (cantidadTiros == 0 && asignarValores && readyToShowResults)
        {
            if (p1Score > p2Score)
            {
                if (MatchManager.main.currPlayerNumber == "P1")
                {
                    cambiarIMGfelicitaciones(felicitaciones1);
                }
                else
                {
                    cambiarIMGfelicitaciones(felicitaciones2);
                }
                winnerText.text = MatchManager.main.data.players[0] + " Ganó";
            }
            if (p1Score < p2Score)
            {
                if (MatchManager.main.currPlayerNumber == "P1")
                {
                    cambiarIMGfelicitaciones(felicitaciones2);
                }
                else
                {
                    cambiarIMGfelicitaciones(felicitaciones1);
                }
                winnerText.text = MatchManager.main.data.players[1] + " Ganó";
            }

            if (p1Score == p2Score)
            {

                winnerText.text = "Empate";
                cambiarIMGfelicitaciones(felicitaciones3DRAW);
            }
        }

        if (PlayerPrefs.GetInt("Creloj") == 1 && tiempoReloj <= 0)
        {
            PanelPerder.alpha = 0f;
            PanelGanar.alpha = 1f;

            int min = (int)(tiempoTotal / 60);
            int sec = (int)(tiempoTotal % 60);
            string minutes = (min < 10) ? "0" + min.ToString() : min.ToString();
            string seconds = (sec < 10) ? "0" + sec.ToString() : sec.ToString();

            string formattedTime = minutes + ":" + seconds;

            if (GlobalData.idiomaActivo == Idioma.ES)
                TextPuntajes.GetComponentsInChildren<Text>()[1].text = "Tiempo Total " + formattedTime;
            else
                TextPuntajes.GetComponentsInChildren<Text>()[1].text = "Total Time " + formattedTime;

            if (puntaje > 0)
            {
                Debug.Log("Gano el contrareloj");
                cambiarIMGfelicitaciones(felicitaciones1);
                btnShare.SetActive(true);
                panelSoloGanar.SetActive(true);
                fondoEstrellas.SetActive(true);

            }
            else
            {
                Debug.Log("Perdio el contrareloj");
                cambiarIMGfelicitaciones(felicitaciones2);
                fondoEstrellas.SetActive(false);
                panelSoloGanar.SetActive(true);
                btnShare.SetActive(false);
            }

            imageFelicitaciones.transform.parent.gameObject.SetActive(true);

            if (scoreUploaded == false)
                FinishScore();
        }


        //TextPuntajes.color = Cred;
        TextPuntajes.text = GoalDetermine.Puntaje.ToString();
    }

    public void setNextLevel()
    {
        nextLevel = true;
    }

    public void AumentarBarra(float n)
    {
        float valorAllenar = 0;
        if (PlayerPrefs.GetInt("Creloj") == 0)
        {
            valorAllenar = GoalDetermine.puntajePorGol * (0.000125f / n);
        }
        else
        {
            valorAllenar = GoalDetermine.puntajePorGol * (0.0002f / n);
        }
        //print ("valor a llenar "+valorAllenar);
        float valorAMoverTextoPuntaje = (valorAllenar * 29) / 0.1f;
        GoalDetermine.share.PuntajeText.gameObject.SetActive(true);
        //Debug.Log ("valor a mover texto "+valorAMoverTextoPuntaje);
        //GoalDetermine.share.PuntajeText.rectTransform.anchoredPosition = new Vector2 (GoalDetermine.share.PuntajeText.rectTransform.anchoredPosition.x,GoalDetermine.share.PuntajeText.rectTransform.anchoredPosition.y + valorAMoverTextoPuntaje);   
        rellenoBarra.GetComponent<Slider>().value += valorAllenar;

    }

    [Header("Paneles de Victoria")]
    public GameObject panelSoloGanar;
    public GameObject panelSoloPerder;
    public GameObject puntajesT;
    public GameObject btnNext;
    public GameObject btnShare;
    public GameObject fondoEstrellas;

    [Header("Cambio Segun Nivel")]
    public ChangeTexture m_ArqueroCambio;
    public ChangeTexture m_PastoCambio;

    private void TirosLibres()
    {
        m_ArqueroCambio.SetTextureArrayByLevel(nivel - 1);
        //m_PastoCambio.SetTextureByLevel(nivel - 1);

        if (nivel == 1)
        {
            PlayerPrefs.SetInt("level", 1);
            if (GoalDetermine.puntajeTotal >= puntosPorNivel)
            {
                {
                    Debug.Log("Gano!!!! Mapache en " + nivel.ToString());
                    panelSoloGanar.SetActive(true);
                    //					panelSoloPerder.SetActive (true);
                    puntajesT.SetActive(true);
                }

                puntosPorNivelText.text = "<color=#F5D519FF>" + puntosPorNivelText.text + "</color>";
                particulas.SetActive(true);
                PanelPerder.alpha = 0f;
                PanelGanar.alpha = 1f;
                tiempo += Time.deltaTime;
                GoalDetermine.share.PuntajeText.gameObject.SetActive(false);
                if (!nextLevel)
                {
                    //cambiar el valor del playerpref si pasa el reto
                    cambiarValoresPPRetos(nivel);
                    cambiarIMGfelicitaciones(felicitaciones1);
                    btnShare.SetActive(true);
                    fondoEstrellas.SetActive(true);
                    asignarValores = false;
                    if (PlayerPrefs.GetInt("Creloj") == 0)
                    {
                        print("estrellas nivel 1");
                        LlenarEstrellas(cantidadTiros);
                    }
                    if (tiempo >= 1f)
                    {
                        //print ("puntos de mierdecita");
                        imageFelicitaciones.transform.parent.gameObject.SetActive(true);
                    }
                }
                if (nextLevel)
                {
                    //if (PlayerPrefs.GetInt("pago") == 1)
                    //{
                    //    //Debug.Log ("cambiar nivel");
                    //    nextLevel = false;
                    //    CambioNivel();
                    //}
                    //else
                    //{
                    //    nextLevel = false;
                    //    panelComprar.SetActive(true);
                    //    print("no pago asi que vale queso");
                    //}
                    //
                    nextLevel = false;
                    CambioNivel();
                }
            }
        }
        else if (nivel == 2)
        {
            PlayerPrefs.SetInt("level", 2);

            if (GoalDetermine.puntajeTotal >= puntosPorNivel)
            {
                {
                    Debug.Log("Gano!!!! Mapache en " + nivel.ToString());
                    panelSoloGanar.SetActive(true);
                    //					panelSoloPerder.SetActive (true);
                    puntajesT.SetActive(true);
                }

                puntosPorNivelText.text = "<color=#F5D519FF>" + puntosPorNivelText.text + "</color>";
                particulas.SetActive(true);
                PanelPerder.alpha = 0f;
                PanelGanar.alpha = 1f;
                tiempo += Time.deltaTime;
                GoalDetermine.share.PuntajeText.gameObject.SetActive(false);
                if (!nextLevel)
                {
                    cambiarValoresPPRetos(nivel);
                    cambiarIMGfelicitaciones(felicitaciones1);
                    btnShare.SetActive(true);
                    fondoEstrellas.SetActive(true);
                    asignarValores = false;
                    if (PlayerPrefs.GetInt("Creloj") == 0)
                    {
                        LlenarEstrellas(cantidadTiros);
                    }
                    if (tiempo >= 1f)
                    {
                        //print ("puntos de mierdecita 2");
                        imageFelicitaciones.transform.parent.gameObject.SetActive(true);
                    }

                }
                if (nextLevel)
                {
                    nextLevel = false;
                    CambioNivel();
                }
            }
        }
        else if (nivel == 3)
        {
            PlayerPrefs.SetInt("level", 3);

            if (GoalDetermine.puntajeTotal >= puntosPorNivel)
            {
                {
                    Debug.Log("Gano!!!! Mapache en " + nivel.ToString());
                    panelSoloGanar.SetActive(true);
                    //					panelSoloPerder.SetActive (true);
                    puntajesT.SetActive(true);
                }

                puntosPorNivelText.text = "<color=#F5D519FF>" + puntosPorNivelText.text + "</color>";
                particulas.SetActive(true);
                PanelPerder.alpha = 0f;
                PanelGanar.alpha = 1f;
                tiempo += Time.deltaTime;
                GoalDetermine.share.PuntajeText.gameObject.SetActive(false);
                if (!nextLevel)
                {
                    cambiarValoresPPRetos(nivel);
                    cambiarIMGfelicitaciones(felicitaciones1);
                    btnShare.SetActive(true);
                    fondoEstrellas.SetActive(true);
                    asignarValores = false;
                    if (PlayerPrefs.GetInt("Creloj") == 0)
                    {
                        LlenarEstrellas(cantidadTiros);
                    }
                    if (tiempo >= 1f)
                    {
                        //print ("puntos de mierdecita 3");
                        imageFelicitaciones.transform.parent.gameObject.SetActive(true);
                    }
                }
                if (nextLevel)
                {
                    nextLevel = false;
                    CambioNivel();
                }
            }
        }
        else if (nivel == 4)
        {
            PlayerPrefs.SetInt("level", 4);

            if (GoalDetermine.puntajeTotal >= puntosPorNivel)
            {
                {
                    Debug.Log("Gano!!!! Mapache en " + nivel.ToString());
                    panelSoloGanar.SetActive(true);
                    //					panelSoloPerder.SetActive (true);
                    puntajesT.SetActive(true);
                }

                puntosPorNivelText.text = "<color=#F5D519FF>" + puntosPorNivelText.text + "</color>";
                particulas.SetActive(true);
                PanelPerder.alpha = 0f;
                PanelGanar.alpha = 1f;
                tiempo += Time.deltaTime;
                GoalDetermine.share.PuntajeText.gameObject.SetActive(false);
                if (!nextLevel)
                {
                    cambiarValoresPPRetos(nivel);
                    cambiarIMGfelicitaciones(felicitaciones1);
                    btnShare.SetActive(true);
                    fondoEstrellas.SetActive(true);
                    asignarValores = false;
                    if (PlayerPrefs.GetInt("Creloj") == 0)
                    {
                        LlenarEstrellas(cantidadTiros);
                    }
                    if (tiempo >= 1f)
                    {
                        //print ("puntos de mierdecita 4");
                        imageFelicitaciones.transform.parent.gameObject.SetActive(true);
                    }
                }
                if (nextLevel)
                {
                    nextLevel = false;
                    CambioNivel();
                }
            }
        }
        else if (nivel == 5)
        {
            PlayerPrefs.SetInt("level", 5);

            if (GoalDetermine.puntajeTotal >= puntosPorNivel)
            {
                {
                    Debug.Log("Gano!!!! Mapache en " + nivel.ToString());
                    panelSoloGanar.SetActive(true);
                    //					panelSoloPerder.SetActive (true);
                    puntajesT.SetActive(true);
                    btnNext.SetActive(false);
                }

                puntosPorNivelText.text = "<color=#F5D519FF>" + puntosPorNivelText.text + "</color>";
                particulas.SetActive(true);
                PanelPerder.alpha = 0f;
                PanelGanar.alpha = 1f;
                tiempo += Time.deltaTime;
                GoalDetermine.share.PuntajeText.gameObject.SetActive(false);
                cambiarValoresPPRetos(nivel);
                cambiarIMGfelicitaciones(felicitaciones1);
                btnShare.SetActive(true);
                fondoEstrellas.SetActive(true);
                asignarValores = false;
                if (PlayerPrefs.GetInt("Creloj") == 0)
                {
                    LlenarEstrellas(cantidadTiros);
                }
                if (tiempo >= 1f)
                {
                    //print ("puntos de mierdecita 4");
                    imageFelicitaciones.transform.parent.gameObject.SetActive(true);
                }

                //				if (!nextLevel) {
                //					cambiarValoresPPRetos(nivel);
                //					cambiarIMGfelicitaciones (felicitaciones1);
                //					asignarValores = false;
                //					if (PlayerPrefs.GetInt ("Creloj") == 0) {
                //						LlenarEstrellas (cantidadTiros);
                //					}
                //					if (tiempo >= 1f) {
                //						//print ("puntos de mierdecita 4");
                //						imageFelicitaciones.transform.parent.gameObject.SetActive (true);
                //					}
                //				}
                //				if (nextLevel) {
                //					nextLevel = false;
                //					CambioNivel ();
                //				}
            }
        }
        //		else if (nivel == 5 && PlayerPrefs.GetInt ("Creloj") == 0) 
        //		{
        //			PlayerPrefs.SetInt ("level", 5);
        //			if (GoalDetermine.puntajeTotal >= puntosPorNivel && ControlLose == false) 
        //			{
        //				{
        //					Debug.Log ("Gano!!!! Mapache");
        //					panelSoloGanar.SetActive (true);
        //					panelSoloPerder.SetActive (true);
        //					puntajesT.SetActive (true);
        //				}
        //
        //				puntosPorNivelText.text = "<color=#F5D519FF>" + puntosPorNivelText.text + "</color>";
        //				particulas.SetActive (true);
        //				//  GANA EL JUEGO ( SUPERA EL 5TO NIVEL)----
        //				cambiarValoresPPRetos(nivel);
        //				ControlLose = true;
        //				PanelPerder.alpha = 1f;
        //				PanelGanar.alpha = 0f;
        //				//cambiarIMG (nivel3gol10); 
        //				tiempo = 1f;
        //				TextPuntajes.color = CYellow;
        //				GoalDetermine.share.PuntajeText.gameObject.SetActive (false);
        //				TextPuntajes.text = GoalDetermine.Puntaje.ToString ();
        //				cambiarIMGfelicitaciones (felicitaciones3);
        //				asignarValores = false;
        ////				if (PlayerPrefs.GetInt ("Creloj") == 0) {
        //					LlenarEstrellas (cantidadTiros);
        ////				}
        //				if (tiempo >= 1f) {
        //					//print ("puntos de mierdecita 5");
        //					imageFelicitaciones.transform.parent.gameObject.SetActive (true);
        //				}
        //				//FinishScore ();    
        //
        //			}
        //		}
    }

    //pp= playerprefs
    private void cambiarValoresPPRetos(int level)
    {
        if (PlayerPrefs.GetInt("Creloj") == 2)
        {
            if (level == 1)
            {
                PlayerPrefs.SetInt("r1n1", 2);
            }
            else if (level == 2)
            {
                PlayerPrefs.SetInt("r1n2", 2);
            }
            else if (level == 3)
            {
                PlayerPrefs.SetInt("r1n3", 2);
            }
            else if (level == 4)
            {
                PlayerPrefs.SetInt("r1n4", 2);
            }
        }
        else if (PlayerPrefs.GetInt("Creloj") == 3)
        {
            if (level == 1)
            {
                PlayerPrefs.SetInt("r2n1", 2);
            }
            else if (level == 2)
            {
                PlayerPrefs.SetInt("r2n2", 2);
            }
            else if (level == 3)
            {
                PlayerPrefs.SetInt("r2n3", 2);
            }
            else if (level == 4)
            {
                PlayerPrefs.SetInt("r2n4", 2);
            }
        }
        else if (PlayerPrefs.GetInt("Creloj") == 4)
        {
            if (level == 1)
            {
                PlayerPrefs.SetInt("r3n1", 2);
            }
            else if (level == 2)
            {
                PlayerPrefs.SetInt("r3n2", 2);
            }
            else if (level == 3)
            {
                PlayerPrefs.SetInt("r3n3", 2);
            }
            else if (level == 4)
            {
                PlayerPrefs.SetInt("r3n4", 2);
            }
        }
        else if (PlayerPrefs.GetInt("Creloj") == 5)
        {
            if (level == 1)
            {
                PlayerPrefs.SetInt("r4n1", 2);
            }
            else if (level == 2)
            {
                PlayerPrefs.SetInt("r4n2", 2);
            }
            else if (level == 3)
            {
                PlayerPrefs.SetInt("r4n3", 2);
            }
            else if (level == 4)
            {
                PlayerPrefs.SetInt("r4n4", 2);
            }
        }
        else if (PlayerPrefs.GetInt("Creloj") == 0)
        {
            if (level == 1)
            {
                PlayerPrefs.SetInt("tn1", 2);
            }
            else if (level == 2)
            {
                PlayerPrefs.SetInt("tn2", 2);
            }
            else if (level == 3)
            {
                PlayerPrefs.SetInt("tn3", 2);
            }
            else if (level == 4)
            {
                PlayerPrefs.SetInt("tn4", 2);
            }
            else if (level == 5)
            {
                PlayerPrefs.SetInt("tn5", 2);
            }
        }
    }

    private void ContraReloj()
    {
        if (PlayerPrefs.GetInt("TimeTrial") == 1)
            tiempoReloj -= Time.deltaTime;

        if (tiempoReloj >= 0)
        {
            float minutos = Mathf.Floor(tiempoReloj / 60);
            float segundos = Mathf.Floor(tiempoReloj % 60);
            if (tiempoReloj > 60)
            {
                textoTiempoCR.text = minutos.ToString("0") + ":" + segundos.ToString("00");
            }
            else
            {
                textoTiempoCR.text = segundos.ToString("00");
            }

            if (tiempoReloj < 11)
            {
                textoTiempoCR.text = "<color=#FF0000FF>" + textoTiempoCR.text + "</color>";
                cuentaAtras.text = segundos.ToString("0");
            }
            else
            {
                textoTiempoCR.text = "<color=#FFFFFFFF>" + textoTiempoCR.text + "</color>";
                cuentaAtras.text = "";
            }
            //Debug.Log ("tiempo " + tiempoReloj);
        }
    }


    private void LlenarEstrellas(int tiros)
    {
        Image[] stars = estrellas.GetComponentsInChildren<Image>();
        if (nivel == 1)
        {
            print("nivel 1 estrellas");
            if (tiros > 7)
            {
                //print ("tres estrellas");
                for (int i = 0; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                PlayerPrefs.SetInt("estrellasGanadasN1", 3);
            }
            else if (tiros > 4 && tiros <= 7)
            {
                //print ("dos estrellas");
                for (int i = 1; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                if (PlayerPrefs.GetInt("estrellasGanadasN1") < 3)
                    PlayerPrefs.SetInt("estrellasGanadasN1", 2);
            }
            else if (tiros >= 2 && tiros <= 4)
            {
                //print ("una estrella");
                stars[1].sprite = estrellaDorada;
                if (PlayerPrefs.GetInt("estrellasGanadasN1") < 2)
                    PlayerPrefs.SetInt("estrellasGanadasN1", 1);
            }
        }

        if (nivel == 2)
        {
            if (tiros > 6)
            {
                //print ("tres estrellas");
                for (int i = 0; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                PlayerPrefs.SetInt("estrellasGanadasN2", 3);
            }
            else if (tiros > 3 && tiros <= 6)
            {
                //print ("dos estrellas");
                for (int i = 1; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                if (PlayerPrefs.GetInt("estrellasGanadasN2") < 3)
                    PlayerPrefs.SetInt("estrellasGanadasN2", 2);
            }
            else if (tiros >= 1 && tiros <= 3)
            {
                //print ("una estrella");
                stars[1].sprite = estrellaDorada;
                if (PlayerPrefs.GetInt("estrellasGanadasN2") < 2)
                    PlayerPrefs.SetInt("estrellasGanadasN2", 1);
            }
        }
        if (nivel == 3)
        {
            if (tiros > 5)
            {
                //print ("tres estrellas");
                for (int i = 0; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                PlayerPrefs.SetInt("estrellasGanadasN3", 3);
            }
            else if (tiros > 2 && tiros <= 5)
            {
                //print ("dos estrellas");
                for (int i = 1; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                if (PlayerPrefs.GetInt("estrellasGanadasN3") < 3)
                    PlayerPrefs.SetInt("estrellasGanadasN3", 2);
            }
            else if (tiros >= 0 && tiros <= 2)
            {
                //print ("una estrella");
                stars[1].sprite = estrellaDorada;
                if (PlayerPrefs.GetInt("estrellasGanadasN3") < 2)
                    PlayerPrefs.SetInt("estrellasGanadasN3", 1);
            }
        }
        if (nivel == 4)
        {
            if (tiros > 4)
            {
                //print ("tres estrellas");
                for (int i = 0; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                PlayerPrefs.SetInt("estrellasGanadasN4", 3);
            }
            else if (tiros > 1 && tiros <= 4)
            {
                //print ("dos estrellas");
                for (int i = 1; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                if (PlayerPrefs.GetInt("estrellasGanadasN4") < 3)
                    PlayerPrefs.SetInt("estrellasGanadasN4", 2);
            }
            else if (tiros >= 0 && tiros <= 1)
            {
                //print ("una estrella");
                stars[1].sprite = estrellaDorada;
                if (PlayerPrefs.GetInt("estrellasGanadasN4") < 2)
                    PlayerPrefs.SetInt("estrellasGanadasN4", 1);
            }
        }
        if (nivel == 5)
        {
            if (tiros > 3)
            {
                //print ("tres estrellas");
                for (int i = 0; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                PlayerPrefs.SetInt("estrellasGanadasN5", 3);
            }
            else if (tiros > 1 && tiros <= 3)
            {
                //print ("dos estrellas");
                for (int i = 1; i < stars.Length; i++)
                {
                    stars[i].sprite = estrellaDorada;
                }
                if (PlayerPrefs.GetInt("estrellasGanadasN5") < 3)
                    PlayerPrefs.SetInt("estrellasGanadasN5", 2);
            }
            else if (tiros >= 0 && tiros <= 1)
            {
                //print ("una estrella");
                stars[1].sprite = estrellaDorada;
                if (PlayerPrefs.GetInt("estrellasGanadasN5") < 2)
                    PlayerPrefs.SetInt("estrellasGanadasN5", 1);
            }
        }

    }

    private void ConfiguracionProx()
    {
        if (PlayerPrefs.GetInt("pateando1") == 1)
        {
            flechasRetos.SetActive(false);
        }
        else
        {
            flechasRetos.SetActive(true);
        }

        if (GoalDetermine.share.intentarDeNuevo)
        {
            flechasRetos.SetActive(true);
        }

        if (PlayerPrefs.GetInt("Creloj") == 0)
        {
            //print ("Tiros libres");
            estrellas.SetActive(true);
            TirosLibres();
        }
        else if (PlayerPrefs.GetInt("Creloj") == 1)
        {
            //print ("contra reloj");
            ContraReloj();
        }
        else if (PlayerPrefs.GetInt("Creloj") == 2)
        {
            moneda.SetActive(false);
            if (PlayerPrefs.GetFloat("posPateadorX") > 7)
            {
                //				flechasRetos.GetComponentInChildren<Transform> ().GetChild (1).gameObject.SetActive (true);
                //				flechasRetos.GetComponentInChildren<Transform> ().GetChild (0).gameObject.SetActive (false);
                plataformaIzquierda.SetActive(true);
                plataformaDerecha.SetActive(false);
            }
            else if (PlayerPrefs.GetFloat("posPateadorX") < -3 || PlayerPrefs.GetFloat("posPateadorX") >= -3)
            {
                //				flechasRetos.GetComponentInChildren<Transform> ().GetChild (1).gameObject.SetActive (false);
                //				flechasRetos.GetComponentInChildren<Transform> ().GetChild (0).gameObject.SetActive (true);
                plataformaIzquierda.SetActive(false);
                plataformaDerecha.SetActive(true);
            }
            TirosLibres();
        }
        else if (PlayerPrefs.GetInt("Creloj") == 3)
        {
            barrera.SetActive(false);
            //Debug.Log ("aros");
            aro.SetActive(true);
            MeshRenderer[] arosMesh = new MeshRenderer[2];
            //estas posiciones se pusieron a mano porque si, si quiere hacerlo de otra manera bienvenido sea...
            if (PlayerPrefs.GetInt("pateando1") == 0)
            {
                arosMesh = aro.GetComponentsInChildren<MeshRenderer>();
                foreach (MeshRenderer mr in arosMesh)
                {
                    mr.enabled = true;
                }
            }
            if (PlayerPrefs.GetFloat("posPateadorX") > 7)
            {

                aro.transform.position = new Vector3(4.74f, 2f, -9.79f);
                aro.GetComponentInChildren<Transform>().GetChild(0).localPosition = new Vector3(-3.8f, 4f, -0.5f);
                aro.GetComponentInChildren<Transform>().GetChild(1).localPosition = new Vector3(-5f, -1f, 0f);
                aro.GetComponentInChildren<Transform>().GetChild(2).localPosition = new Vector3(-4.5f, -6f, 0f);
            }
            else if (PlayerPrefs.GetFloat("posPateadorX") < -3)
            {
                aro.transform.position = new Vector3(0f, 2f, -9.79f);
                aro.GetComponentInChildren<Transform>().GetChild(0).localPosition = new Vector3(-6.5f, 4f, -0.5f);
                aro.GetComponentInChildren<Transform>().GetChild(1).localPosition = new Vector3(-4.3f, -1f, 0f);
                aro.GetComponentInChildren<Transform>().GetChild(2).localPosition = new Vector3(-3.25f, -6f, 0f);
            }
            else if (PlayerPrefs.GetFloat("posPateadorX") >= -3 && PlayerPrefs.GetFloat("posPateadorX") < 7)
            {
                aro.transform.position = new Vector3(2f, 2f, -9.79f);
                aro.GetComponentInChildren<Transform>().GetChild(0).localPosition = new Vector3(-3f, 4f, -0.5f);
                aro.GetComponentInChildren<Transform>().GetChild(1).localPosition = new Vector3(-3.3f, -1f, 0f);
                aro.GetComponentInChildren<Transform>().GetChild(2).localPosition = new Vector3(-3.8f, -6f, -0.1f);
            }
            TirosLibres();
        }
        else if (PlayerPrefs.GetInt("Creloj") == 4)
        {
            //Debug.Log ("distancia");
            TirosLibres();
        }
        else
        {
            barrera.SetActive(false);
            //Debug.Log ("platillo");
            if (PlayerPrefs.GetFloat("posPateadorX") > 7)
            {
                if (primeraInstancia)
                {
                    primeraInstancia = false;
                    GameObject plat = Instantiate(platillo);
                    plat.transform.position = new Vector3(2.5f, 1f, 1f);
                }
            }
            else if (PlayerPrefs.GetFloat("posPateadorX") < -3)
            {
                if (primeraInstancia)
                {
                    primeraInstancia = false;
                    GameObject plat = Instantiate(platillo);
                    plat.transform.position = new Vector3(-2.5f, 1f, 1f);
                }
            }
            else if (PlayerPrefs.GetFloat("posPateadorX") >= -3 && PlayerPrefs.GetFloat("posPateadorX") < 7)
            {
                if (primeraInstancia)
                {
                    primeraInstancia = false;
                    GameObject plat = Instantiate(platillo);
                }
            }
            TirosLibres();
        }
    }

    public void FinishScore()
    {
        int ID;
        ID = DataApp2.main.GetMyID();
        userID = ID;
        puntaje = GoalDetermine.Puntaje;

        if (PlayerPrefs.HasKey("MyScore"))
        {
            if (PlayerPrefs.GetInt("MyScore") <= puntaje)
            {
                PlayerPrefs.SetInt("MyScore", puntaje);
            }
        }
        else
        {
            PlayerPrefs.SetInt("MyScore", puntaje);
        }

        if (DataApp2.main.IsRegistered() && uploadFramesCounts == 0)
        {
            StartCoroutine(ActualizarPuntaje(IsRecord =>
            {
                RecordPanel.SetActive(true);

                RecordInfo.text = (IsRecord) ? "¡NUEVO RÉCORD PERSONAL!" : "Sigue intentando superar tu récord personal";
            }));
        }

        if (PlayerPrefs.GetInt("Creloj") == 1 && PlayerPrefs.GetInt("TimeTrial") == 1)
        {
            cantidadGoles = 0;
            golesTotales = 10;
            tiempoAdicional = 10;
        }

        uploadFramesCounts++;

    }

    private void VerificarSiSeHizoGol()
    {

        if (PlayerPrefs.GetInt("gol") == 1 && PlayerPrefs.GetInt("Creloj") != 1)
        {
            if (PlayerPrefs.GetInt("Creloj") == 2)
                BarraMovil.NumeroRandom();
            cantidadGoles++;
            if (nivel != 1)
            {
                AumentarBarra(1 + (0.5f * (nivel - 1)));
            }
            else
            {
                AumentarBarra(1);
            }
            PlayerPrefs.SetInt("gol", 0);
        }

        if (PlayerPrefs.GetInt("gol") == 1 && PlayerPrefs.GetInt("Creloj") == 1 && PlayerPrefs.GetInt("TimeTrial") == 1)
        {
            if (cantidadGoles == golesTotales)
            {
                golesTotales += 10;

                if (cantidadGoles <= 40)
                {
                    tiempoAdicional -= 2;
                }
                else
                {
                    tiempoAdicional = 1;
                }
            }
            //print ("aumentar tiempo");
            cantidadGoles++;
            tiempoTotal += tiempoAdicional;
            tiempoReloj += tiempoAdicional;
            tiempoAdicionalText.text = "+" + tiempoAdicional;
            desvanecer = true;
            PlayerPrefs.SetInt("gol", 0);
        }
    }

    IEnumerator DesvanecerTiempoAdicional()
    {
        //print (tiempoAdicionalText.color);
        tiempoAdicionalText.color = Color.Lerp(tiempoAdicionalText.color, new Color(0.953f, 0.812f, 0.090f, 0f), 0.8f * Time.deltaTime);
        yield return new WaitForSeconds(2f);
        desvanecer = false;
        tiempoAdicionalText.text = "";
        tiempoAdicionalText.color = tAdicionalColorInicial;
    }

    [SerializeField]
    bool scoreUploaded = false;
    public GameObject RecordPanel;
    public Text RecordInfo;

    private IEnumerator ActualizarPuntaje(System.Action<bool> newRecord)
    {
        bool result = false;

        WWWForm formUser = new WWWForm();
        formUser.AddField("indata", "actualizarPuntaje");
        //formUser.AddField("idUser", DataApp2.main.GetMyID());
        formUser.AddField("idUser", PlayerPrefs.GetString("MyPhone"));
        formUser.AddField("nombre", "");
        formUser.AddField("puntaje", puntaje);
        WWW setData = new WWW(DataApp2.main.host + "TraerYActualizarPuntaje.php?", formUser);
        Debug.Log("URL: " + DataApp2.main.host + "TraerYActualizarPuntaje.php?" + formUser);
        yield return setData;

        scoreUploaded = true;

        if (string.IsNullOrEmpty(setData.error))
        {
            print(setData.text);
            if (setData.text == "Succesfully Updated")
            {
                result = true;
            }
        }
        else
        {
            StartCoroutine(ActualizarPuntaje(newRecord));
        }

        newRecord(result);
    }

    public void CambiarImgYTextoNivel()
    {
        int creloj = PlayerPrefs.GetInt("Creloj");
        //imageNivel.texture = imagenTemporal1;
        if (creloj == 0)
        {
            NivelText.text = "";
            switch (nivel)
            {
                case 1:
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "WILSON MORENO";
                    else
                        nombreNivel.text = "LUKA'S TRAINING";
                    //
                    imageNivel.sprite = linea;
                    break;
                case 2:
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "CAPITÁN MODRIC";
                    else
                        nombreNivel.text = "CAPTAIN MODRIC";
                    //
                    imageNivel.sprite = linea;
                    break;
                case 3:
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "THE BEST";
                    else
                        nombreNivel.text = "THE BEST";
                    //
                    imageNivel.sprite = linea;
                    break;
                case 4:
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "MI PRIMER BALÓN DE ORO";
                    else
                        nombreNivel.text = "1ST GOLDEN BALL";
                    //
                    imageNivel.sprite = linea;
                    break;
                case 5:
                    imageNivel.sprite = linea;
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "¡SOY UNA LEYENDA";
                    else
                        nombreNivel.text = "I'M A LEGEND!";
                    //
                    break;
            }
        }
        else
        {
            switch (creloj)
            {
                case 1:
                    NivelText.text = "";
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "CONTRA RELOJ";
                    else
                        nombreNivel.text = "TIME TRIAL";
                    break;
                case 2:
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "TIRO CON REBOTE";
                    else
                        nombreNivel.text = "DEFLECTED SHOT";
                    break;
                case 3:
                    if (GlobalData.idiomaActivo == Idioma.ES)
                        nombreNivel.text = "AROS";
                    else
                        nombreNivel.text = "RINGS";
                    break;
                case 4:
                    NivelText.GetComponent<RectTransform>().localPosition = new Vector3(NivelText.GetComponent<RectTransform>().localPosition.x + 10f, NivelText.GetComponent<RectTransform>().localPosition.y, NivelText.GetComponent<RectTransform>().localPosition.z);
                    imageNivel.sprite = reto3;
                    break;
                case 5:
                    NivelText.GetComponent<RectTransform>().localPosition = new Vector3(NivelText.GetComponent<RectTransform>().localPosition.x + 2f, NivelText.GetComponent<RectTransform>().localPosition.y, NivelText.GetComponent<RectTransform>().localPosition.z);
                    imageNivel.sprite = reto4;
                    break;
            }
            //
            if (GlobalData.idiomaActivo == Idioma.ES)
                NivelText.text = "Nivel " + nivel.ToString();
            else
                NivelText.text = "Level " + nivel.ToString();

        }
    }

    public void cambiarIMGfelicitaciones(Sprite imagenTemporal2)
    {
        print("cambiar imagen");
        imageFelicitaciones.sprite = imagenTemporal2;
    }

    public void reiniciarJuego()
    {
        uploadFramesCounts = 0;
        bNewRecord = false;

        GoalDetermine.Puntaje = 0;
        scoreUploaded = false;

        Loading.SetActive(true);
        if (PlayerPrefs.GetInt("Creloj") == 1)
        {
            tiempoReloj = 40f;
            tiempoTotal = tiempoReloj;
            cantidadGoles = 0;
            golesTotales = 10;
            tiempoAdicional = 10;
        }
        if (PlayerPrefs.GetInt("Creloj") == 3)
        {
            Aro.contador = 0;
        }
        PlayerPrefs.SetInt("r1p", 0);
        PlayerPrefs.SetInt("r2p", 0);
        PlayerPrefs.SetInt("r3p", 0);
        PlayerPrefs.SetInt("r4p", 0);

        puntosPorNivelText.text = "<color=#FFFFFFFF>" + puntosPorNivelText.text + "</color>";
        particulas.SetActive(false);
        //GoalDetermine.share.PuntajeText.rectTransform.anchoredPosition = textoPuntosPosicionInicial;
        rellenoBarra.GetComponent<Slider>().value = 0f;
        ControlLose = false;
        Shoot.share.reset();
        GoalKeeper.share.reset();
        CameraManager.share.reset();
        GoalDetermine.share._goalCheck = false;
        animationPateador.PosYTouch = 0;
        animationPateador.PosFinalYTouch = 0;
        collisionBall.isTouchBall = false;
        BonusMark.IsTouchBonus = false;
        nivel = PlayerPrefs.GetInt("level");
        CambiarImgYTextoNivel();
        valoresParaCadaModoDeJuego();
        cantidadGoles = 0;
        textoTiros = cantidadTiros.ToString();
        animationPateador.PosYTouch = 0;
        animationPateador.PosFinalYTouch = 0;
        StartCoroutine(LoadingReser());
        GoalDetermine.share.StopAllCoroutines();
    }

    IEnumerator LoadingReser()
    {
        yield return new WaitForSeconds(1);
        Loading.SetActive(false);
        //print ("quitar la puta imagen");
        imageFelicitaciones.transform.parent.gameObject.SetActive(false);
        GoalDetermine.Puntaje = 0;
        GoalDetermine.puntajeTotal = 0;
        GoalDetermine.share.PuntajeText.text = GoalDetermine.Puntaje.ToString();
        GoalDetermine.share.puntajeContraReloj.text = GoalDetermine.Puntaje.ToString();
        GoalDetermine.share.PuntajeText.gameObject.SetActive(false);

    }

    public void salirJuego()
    {
        StartCoroutine(WaitResponsive());
        PlayerPrefsTracking.main.LogMainMenuScreen();
    }


    IEnumerator WaitResponsive()
    {
        Loading.SetActive(true);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("MenuPrincipal");

    }

    private void CambioNivel()
    {

        if (PlayerPrefs.GetInt("Creloj") == 3)
        {
            Aro.contador = 0;
        }
        StartCoroutine(LoadingReser());
        //GoalDetermine.share.PuntajeText.rectTransform.anchoredPosition = textoPuntosPosicionInicial;
        rellenoBarra.GetComponent<Slider>().value = 0f;
        particulas.SetActive(false);
        puntosPorNivelText.text = "<color=#FFFFFFFF>" + puntosPorNivelText.text + "</color>";
        imageFelicitaciones.transform.parent.gameObject.SetActive(false);
        ControlLose = false;
        cantidadGoles = 0;
        nivel++;
        valoresParaCadaModoDeJuego();
        textoTiros = cantidadTiros.ToString();
        showTiros.text = textoTiros;
        if (PlayerPrefs.GetInt("Creloj") == 0)
        {
            puntosPorNivel += 4000;
        }
        else
        {
            puntosPorNivel += 5000;
        }
        puntosPorNivelText.text = puntosPorNivel.ToString();
        CambiarImgYTextoNivel();
        PlayerPrefs.SetInt("level", nivel);
        tiempo = 0;

        PlayerPrefsTracking.main.LogGameplayScreen();
    }

    private void valoresParaCadaModoDeJuego()
    {
        asignarValores = true;
        //solo hay playerprefs para tres retos, porque el reto 3 es igual que el de tiros libres solo que con distancia
        //		PlayerPrefs.SetInt ("r1p", 0);
        //		PlayerPrefs.SetInt ("r2p", 0);
        //		PlayerPrefs.SetInt ("r4p", 0);
        if (PlayerPrefs.GetInt("Creloj") == 0)
        {
            if (nivel == 1)
            {
                cantidadTiros = 16;
            }
            else
            {
                //	print ("tiros nivel 2");
                cantidadTiros = 16;
            }
        }
        else if (PlayerPrefs.GetInt("Creloj") == 1)
        {
            botonContinuar.SetActive(false);
            barraTiros.SetActive(false);
            barraContraReloj.SetActive(true);
            tAdicionalColorInicial = tiempoAdicionalText.color;
            tiempoReloj = 40f;
            tiempoTotal = tiempoReloj;
            cantidadTiros = 10000;
            golesTotales = 10;
        }
        else if (PlayerPrefs.GetInt("Creloj") == 2 || PlayerPrefs.GetInt("Creloj") == 3 || PlayerPrefs.GetInt("Creloj") == 4 || PlayerPrefs.GetInt("Creloj") == 5)
        {
            if (nivel <= 2)
            {
                cantidadTiros = 16;
            }
            else
            {
                //if (PlayerPrefs.GetInt ("r1n3") == 1 || PlayerPrefs.GetInt ("r2n3") == 1 || PlayerPrefs.GetInt ("r3n3") == 1 || PlayerPrefs.GetInt ("r4n3") == 1 || PlayerPrefs.GetInt ("r1n4") == 1 || PlayerPrefs.GetInt ("r2n4") == 1 || PlayerPrefs.GetInt ("r3n4") == 1 || PlayerPrefs.GetInt ("r4n4") == 1)
                cantidadTiros = 16;
            }
            if (PlayerPrefs.GetInt("r1n1") == 2)
            {
                plataformaDerecha.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
                plataformaIzquierda.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
            }
            if (PlayerPrefs.GetInt("r1n2") == 2)
            {
                plataformaDerecha.transform.localScale = new Vector3(1f, 1f, 1f);
                plataformaIzquierda.transform.localScale = new Vector3(1f, 1f, 1f);
            }
            if (PlayerPrefs.GetInt("r1n3") == 2)
            {
                plataformaDerecha.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
                plataformaIzquierda.transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            }

            if (PlayerPrefs.GetInt("r2n1") == 2)
            {
                aro.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            }
            if (PlayerPrefs.GetInt("r2n2") == 2)
            {
                aro.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
            }
            if (PlayerPrefs.GetInt("r2n3") == 2)
            {
                aro.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            }

            if (PlayerPrefs.GetInt("r4n1") == 2)
            {
                platillo.transform.localScale = new Vector3(1.8f, 1.8f, 1.8f);
            }
            if (PlayerPrefs.GetInt("r4n2") == 2)
            {
                platillo.transform.localScale = new Vector3(1.6f, 1.6f, 1.6f);
            }
            if (PlayerPrefs.GetInt("r4n3") == 2)
            {
                platillo.transform.localScale = new Vector3(1.4f, 1.4f, 1.4f);
            }
        }
    }

    private void PuntosPorNivel()
    {
        if (PlayerPrefs.GetInt("Creloj") == 0)
        {
            if (nivel == 1)
            {
                puntosPorNivel = 8000;
            }
            if (nivel == 2)
            {
                puntosPorNivel = 12000;
            }
            if (nivel == 3)
            {
                puntosPorNivel = 16000;
            }
            if (nivel == 4)
            {
                puntosPorNivel = 20000;
            }
            if (nivel == 5)
            {
                puntosPorNivel = 24000;
            }
        }
        else
        {
            if (nivel == 1)
            {
                puntosPorNivel = 5000;
            }
            if (nivel == 2)
            {
                puntosPorNivel = 10000;
            }
            if (nivel == 3)
            {
                puntosPorNivel = 15000;
            }
            if (nivel == 4)
            {
                puntosPorNivel = 20000;
            }
        }
    }

    public void Pause(float scale)
    {
        Time.timeScale = scale;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCanvasRenderMode : MonoBehaviour {

	public Canvas canvas;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void change(bool screenSpaceOverlay){
		if (screenSpaceOverlay && DataApp2.main.IsRegistered()) {
			canvas.renderMode = RenderMode.ScreenSpaceOverlay;
		} else {
			canvas.renderMode = RenderMode.ScreenSpaceCamera;
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;



public class Shoot : MonoBehaviour
{

    public bool IsBall_Persecution;
    public Vector2[] PosAleatorias;
    private Vector3 distance;
    float tempY;
    public Transform newPos;
    public static Shoot share;

    public static Action EventShoot = delegate { };
    public static Action<float> EventChangeSpeedZ = delegate { };
    public static Action<float> EventChangeBallZ = delegate { };
    public static Action<float> EventChangeBallX = delegate { };
    public static Action<float> EventChangeBallLimit = delegate { };
    public static Action<Collision> EventOnCollisionEnter = delegate { };
    public static Action EventDidPrepareNewTurn = delegate { };
    public List<Vector3> PosCurva = new List<Vector3>();

    public float _ballControlLimit;

    public Transform _goalKeeper;
    public Transform _ballTarget;
    protected Vector3 beginPos;
    protected bool _isShoot = false;

    public float minDistance = 100;     // 40f
    public float _TimerShot;

    public Rigidbody _ball;
    public float factorUp = 30f;                // 10f
    public float factorDown = 0.003f;           // 1f
    public float factorLeftRight = 0.025f;      // 2f
    public float factorLeftRightMultiply = 0.8f;        // 2f
    public float _zVelocity = 0f;

    public AnimationCurve _curve;
    protected Camera _mainCam;

    protected float factorUpConstant = 0.016f * 960f;   // 0.015f * 960f;
    protected float factorDownConstant = 0.006f * 960f; // 0.005f * 960f;
    protected float factorLeftRightConstant = 0.0235f * 640f; // 0.03f * 640f; // 0.03f * 640f;

    public Transform _ballShadow;

    public float _speedMin = 18f;   // 20f;
    public float _speedMax = 30f;   // 36f;

    public float _distanceMinZ = 16.5f;
    public float _distanceMaxZ = 18f;

    public float _distanceMinX = -25f;
    public float _distanceMaxX = 25f;

    public bool _isShooting = false;
    public bool _canControlBall = false;

    public Transform _cachedTrans;

    public bool afterShoot = false;//Indicates wherer or not we have already kicked the ball (to avoid the fly kick bug)
    public bool _enableTouch = false;
    public static bool _enableTouchSt;

    public float screenWidth;
    public float screenHeight;

    Vector3 _prePos, _curPos;
    public float angle;

    protected Transform _ballParent;

    protected RaycastHit _hit;
    public bool _isInTutorial = false;

    public Vector3 ballVelocity;
    public static Vector3 _ballVelocity;

    private float _ballPostitionZ = -22f;
    private float _ballPostitionX = 0f;


    public BoxCollider _CenterCollider;
    public float BallPositionZ
    {
        get { return _ballPostitionZ; }
        set { _ballPostitionZ = value; }
    }

    public float BallPositionX
    {
        get { return _ballPostitionX; }
        set { _ballPostitionX = value; }
    }
    public TrailRenderer _effect;
    public GameObject PastoEffect;
    public GameObject Arquero;
    public GameObject Moneda;
    public CanvasGroup PopUpTocarbalon;
    public AudioSource SourceM;
    public AudioClip ShootAudio;
    public AudioClip PitoAudioInicio, PitoAudioFinal;
    public BoxCollider ColliderCobrador;
    public bool TouchBall = true;

    int countInit;

    protected virtual void Awake()
    {
        share = this;
        _cachedTrans = transform;
        _isShooting = true;
        _ballParent = _ball.transform.parent;
        _distanceMinX = -15f;
        _distanceMaxX = 15f;
        _distanceMaxZ = 29f;
        float constante = ((Screen.width) / 1024);
    }

    // Use this for initialization
    protected virtual void Start()
    {
        _mainCam = CameraManager.share._cameraMainComponent;

#if UNITY_WP8 || UNITY_ANDROID
        Time.maximumDeltaTime = 0.2f;
        Time.fixedDeltaTime = 0.008f;
#else
		Time.maximumDeltaTime = 0.1f;
		Time.fixedDeltaTime = 0.005f;
#endif


        calculateFactors();

        //_ballControlLimit = 6f;
        EventChangeBallLimit(_ballControlLimit);
        reset();
        CameraManager.share.reset();
        GoalKeeper.share.reset();

        GoalDetermine.EventFinishShoot += goalEvent;
        LoadCombaPlayer();
    }


    void LoadCombaPlayer()
    {
        factorLeftRightMultiply = 3.0f;
        ShootAI.shareAI._curveLevel = 80f;
    }

    void OnDestroy()
    {
        GoalDetermine.EventFinishShoot -= goalEvent;
    }

    public virtual void goalEvent(bool isGoal, Area area)
    {
        PosCurva.Clear();
        _isShooting = true;
        afterShoot = false;
        _canControlBall = false;
        _isShooting = false;
    }

    public void calculateFactors()
    {
        screenHeight = Screen.height;
        screenWidth = Screen.width;

        minDistance = (100 * screenHeight) / Screen.height;
        factorUp = factorUpConstant / screenHeight;
        factorDown = factorDownConstant / screenHeight;
        factorLeftRight = factorLeftRightConstant / screenWidth;

        /*Debug.Log("Orientation : " + orientation + "\t Screen height = " + screenHeight 
            + "\t Screen width = " + screenWidth + "\t factorUp = " + factorUp + "\t factorDown = " + factorDown 
            + "\t factorLeftRight = " + factorLeftRight + "\t minDistance = " + minDistance);*/
    }

    protected void LateUpdate()
    {
        if (screenHeight != Screen.height)
        {
            //	orientation = Screen.orientation;
            calculateFactors();
            CameraManager.share.reset();
        }
    }
    void FixedUpdate()
    {
        ballVelocity = _ball.velocity;
        Vector3 pos = _ball.transform.position;
        pos.y = 0.015f;
        _ballShadow.position = pos;
    }

    protected virtual void Update()
    {
        //Debug.Log ("curPos : "+_curPos);
        animationPateador.CantidadPosCurva = PosCurva.Count;
        if (_isShooting)
        {       // neu banh chua vao luoi hoac trung thu mon, khung thanh thi banh duoc phep bay voi van toc dang co
                //Debug.Log("Should be false to kick the ball = " + afterShoot);

            if (/*!afterShoot &&*/ _enableTouch && !_isInTutorial)
            {
                if (Input.GetMouseButtonDown(0) && !afterShoot)
                {
                    //PosCurva.Clear();
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.transform.tag == "Ball")
                        {
                            StopAllCoroutines();
                            StartCoroutine(PopUpDisableTocarbalon());
                            Debug.LogError("es esto?");
                        }
                        else
                        {
                            StopAllCoroutines();
                            StartCoroutine(PopUpEnableTocarbalon());
                        }
                    }
                    // touch phase began
                    if (collisionBall.isTouchBall)
                    {
                        Debug.Log("pateando: " + collisionBall.isTouchBall);
                        //Invoke ("pateandoBalon", 0.02F);
                    }
                    mouseBegin(Input.mousePosition);
                    PlayerPrefs.SetInt("pateando", 1);
                    PlayerPrefs.SetInt("pateando1", 1);
                    animacionDefensa.primeraAnimacion = true;
                }
                else if (Input.GetMouseButtonUp(0) && !afterShoot)
                {   // touch ended
                    ShootAI._enableTouchSt = false;
                    mouseEnd();

                    if (TouchBall == true)
                    {
                        //TouchBall=false;

                        if (m_Result > 50 && PosCurva.Count > 3)
                        {
                            afterShoot = true;
                            _isShooting = false;
                        }

                        //if (!_isShooting)
                        //{
                        //    afterShoot = false;
                        //}

                    }
                }
            }

            if (_isShoot)
            {
                Vector3 speed = _ballParent.InverseTransformDirection(_ball.velocity);
                speed.z = _zVelocity;
                _ball.velocity = _ballParent.TransformDirection(speed);
            }
        }
        _enableTouchSt = _enableTouch;
        _ballVelocity = ballVelocity;


        if (Input.GetMouseButton(0) && PosCurva.Count < 200 && _isShoot == false && !afterShoot)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "Ball")
                {
                    PosCurva.Clear();
                    StopAllCoroutines();
                    StartCoroutine(PopUpDisableTocarbalon());
                    TouchBall = true;
                }
            }
            if (TouchBall)
            {
                Vector3 ballPos = Camera.main.WorldToScreenPoint(transform.position);
                Debug.Log("Ball init pos = " + ballPos.ToString());

                if (Input.mousePosition.y > ballPos.y)
                    PosCurva.Add(Input.mousePosition);

                if (PosCurva.Count > 2)
                {
                    if (PosCurva[PosCurva.Count - 1] == PosCurva[PosCurva.Count - 2])
                    {
                        PosCurva.RemoveAt(PosCurva.Count - 1);
                    }
                }
            }

            float TempPosyfinal = Input.mousePosition.y;
            animationPateador.PosFinalYTouch = TempPosyfinal;
        }
    }

    IEnumerator PopUpEnableTocarbalon()
    {
        if (PlayerPrefs.GetInt("Tutorial") == 1 && !_isShoot)
        {
            for (int i = 0; i < 100; i++)
            {
                PopUpTocarbalon.alpha += 0.03f;
                yield return new WaitForSeconds(0.01f);
            }
        }
    }

    IEnumerator PopUpDisableTocarbalon()
    {
        for (int i = 0; i < 100; i++)
        {
            PopUpTocarbalon.alpha -= 0.03f;
            yield return new WaitForSeconds(0.01f);
        }
    }

    public void mouseBegin(Vector3 pos)
    {
        if (Input.GetMouseButtonDown(0))
        {
            PosCurva.Clear();
            float TempPosy = Input.mousePosition.y;
            animationPateador.PosYTouch = TempPosy;

        }
        _prePos = _curPos = pos;
        //Debug.Log ("curPos en inicio: "+_curPos);
        beginPos = _curPos;
    }

    public float m_Result;
    public bool corner;

    public void mouseEnd()
    {
        tempY = 0;
        float result = Input.mousePosition.y - animationPateador.PosYTouch;
        m_Result = result;
        //		Debug.Log (result +" result que vale verga");
        if (PosCurva.Count <= 3 || result < 50 && animationPateador.isShot)
        {
            PosCurva.Clear();
            afterShoot = false;
        }

        if (result < 0)
        {
            print("No sirve");
        }

        afterShoot = false;
    }

    public void mouseMove(Vector3 pos)
    {
        if (_curPos != pos)
        {
            _prePos = _curPos;
            _curPos = pos;

            distance = _curPos - beginPos;
            if (_isShoot == false)
            {
                if (distance.y > 0 /*&& distance.magnitude >= minDistance*/)
                {
                    if (Physics.Raycast(_mainCam.ScreenPointToRay(_curPos), out _hit, 150f) && _hit.transform != _cachedTrans)
                    {
                        _isShoot = true;
                        Vector3 point1 = _hit.point;        // contact point
                        SourceM.PlayOneShot(ShootAudio, 1);
                        Instantiate(PastoEffect, this.transform.position, Quaternion.identity);
                        point1.y = 0;
                        point1 = _ball.transform.InverseTransformPoint(point1);
                        point1 -= Vector3.zero;
                        Vector3 diff = point1;
                        diff.Normalize();

                        float x = distance.x * factorLeftRight;
                        _ball.velocity = _ballParent.TransformDirection(new Vector3(x, distance.y * factorUp, _zVelocity));
                        _ball.angularVelocity = new Vector3(0, x, 0f);

                        if (EventShoot != null)
                        {
                            EventShoot();

                        }
                    }
                }
            }
            else
            {
                if (_canControlBall == true)
                {

                    if (_cachedTrans.position.z < -_ballControlLimit)
                    {
                        distance = _curPos - _prePos;
                        Vector3 speed = _ballParent.InverseTransformDirection(_ball.velocity);
                        speed.y += distance.y * ((distance.y > 0) ? factorUp : factorDown);
                        speed.x += distance.x * factorLeftRight * factorLeftRightMultiply;
                        _ball.velocity = _ballParent.TransformDirection(speed);
                        speed = _ball.angularVelocity;
                        speed.y += distance.x * factorLeftRight;
                        _ball.angularVelocity = speed;
                    }
                    else
                    {

                        _isShooting = true;
                        afterShoot = false;
                        _canControlBall = false;
                    }
                }
            }
        }
    }

    public IEnumerator TimeShoot(float timerShoot)
    {
        //		afterShoot = true;
        if (PosCurva.Count > 40)
        {
            countInit = 6;
        }
        else if (PosCurva.Count > 20)
        {
            countInit = 3;
        }
        else
        {
            countInit = 0;
            yield return null;
            Debug.Log("El contador es de... " + PosCurva.Count);
        }
        //		print("un numero que vale verga y no se de que sera "+countInit);
        animationPateador.isShot = true;
        if (!IsBall_Persecution && collisionBall.isTouchBall)
        {
            yield return new WaitForSeconds(timerShoot);
            for (int i = countInit; i < PosCurva.Count; i++)
            {
                mouseMove(new Vector3(PosCurva[i].x, PosCurva[i].y, PosCurva[i].z));
                yield return new WaitForEndOfFrame();
            }

            IsBall_Persecution = true;
        }
        //		PosCurva.Clear();
    }

    public void pateandoBalon()
    {
        StartCoroutine(TimeShoot(_TimerShot));
        StartCoroutine(ResetOnTimer(3.0f));
    }

    IEnumerator ResetOnTimer(float _time)
    {
        yield return new WaitForSeconds(_time);
        DemoShoot.share.Reset(true);
    }

    protected void OnCollisionEnter(Collision other)
    {
        string tag = other.gameObject.tag;
        if (tag.Equals("Player") || tag.Equals("Obstacle") || tag.Equals("Net") || tag.Equals("Wall") || tag.Equals("Portero"))
        {   // banh trung thu mon hoac khung thanh hoac da vao luoi roi thi ko cho banh bay voi van toc nua, luc nay de~ cho physics engine tinh' toan' quy~ dao bay
            _isShooting = false;
            if (tag.Equals("Net"))
            {
                _ball.velocity /= 3f;
            }
        }
        EventOnCollisionEnter(other);
    }

    private void enableEffect()
    {
        _effect.time = 1;
    }


    //Let's make a loop corotine that waits for a couple of frames, that will wait until everything is reseted for real
    private IEnumerator RestoreReset(int limit)
    {
        Debug.Log("Se prendio esta mierda");

        int i = 0;

        do
        {
            animationPateador.isShot = false;
            PosCurva.Clear();
            afterShoot = false;
            collisionBall.isTouchBall = false;

            if (collisionBall.main != null)
                collisionBall.main.m_ColliderBall.enabled = false;

            _canControlBall = true;
            _isShoot = false;
            _isShooting = true;
            //_ball.velocity = Vector3.zero;
            //_ball.angularVelocity = Vector3.zero;
            //_ball.transform.localEulerAngles = Vector3.zero;
            m_Result = 0;
            PlayerPrefs.SetInt("pateando1", 0);

            i++;
            yield return null;
        }
        while (i < limit);

    }

    public virtual void reset()
    {

        StartCoroutine(RestoreReset(6));

        Debug.Log("Reiniciando!!!");

        int tempPos = Random.Range(0, PosAleatorias.Length - 1);
        Debug.LogError("esto lo programaron con los pies");
        if (corner)
        {
            reset(newPos.position.x, newPos.position.y);

        }
        else
        {
            reset(PosAleatorias[tempPos].x, PosAleatorias[tempPos].y);

        }
        if (PlayerPrefs.GetInt("Creloj") == 2)
        {
            print("reto1");
            if (PosAleatorias[tempPos].x >= -3 && PosAleatorias[tempPos].x <= 7)
            {
                Debug.LogError("esto lo programaron con los pies");
                //esta posicion se tomo del arreglo de posiciones aleatorias por eso vale verga
                float xpos = -9.53f;
                xpos = xpos * (-1);
                reset(xpos, -27.138f);
            }
        }
        else if (PlayerPrefs.GetInt("Creloj") == 4)
        {
            Debug.Log("reto 3");
            //posiciones para el reto de distancia
            if (PlayerPrefs.GetInt("r3n1") == 2)
            {
                Debug.Log("aumentar distancia en z");
                reset(PosAleatorias[tempPos].x, -35f);
            }
            else if (PlayerPrefs.GetInt("r3n2") == 2)
            {
                Debug.LogError("esto lo programaron con los pies");
                reset(PosAleatorias[tempPos].x, -40f);
            }
            else if (PlayerPrefs.GetInt("r3n3") == 2)
            {
                Debug.LogError("esto lo programaron con los pies");
                reset(PosAleatorias[tempPos].x, -45f);
            }
        }
    }



    public virtual void reset(float x, float z)
    {
        Moneda.GetComponent<Collider>().enabled = true;
        foreach (Transform t in Moneda.transform)
        {
            t.gameObject.SetActive(true);
        }
        BonusMark.IsTouchBonus = false;
        rotacionBarrear.MoveBarrera = true;
        _CenterCollider.enabled = false;
        IsBall_Persecution = false;
        animationArquero._isCentro = false;
        animationPateador.PosYTouch = 0;
        animationPateador.PosFinalYTouch = 0;
        animationArquero.LadoSeleccionado = false;
        PlayerPrefs.SetInt("disparo", 0);
        PlayerPrefs.SetInt("pateando1", 0);
        PlayerPrefs.SetInt("restarTiro", 0);
        Arquero.SetActive(false);
        Arquero.SetActive(true);
        //Debug.Log(string.Format("<color=#c3ff55>Reset Ball Pos, x = {0}, z = {1}</color>", x, z));
        if (PlayerPrefs.GetInt("Creloj") == 3)
        {
            Aro.contador = 0;
            print("reiniciar el puto contador " + Aro.contador);
        }
        else if (PlayerPrefs.GetInt("Creloj") == 5)
        {
            respawn.primeraInstancia = true;
            DestruirPlatillos();
        }

        PlayerPrefs.SetFloat("posPateadorX", x);
        PlayerPrefs.SetFloat("posPateadorZ", z);
        _effect.time = 0;
        Invoke("enableEffect", 0.1f);
        BallPositionX = x;
        EventChangeBallX(x);
        BallPositionZ = z;
        EventChangeBallZ(z);
        collisionBall.isTouchBall = false;
        _canControlBall = true;
        _isShoot = false;
        _isShooting = true;
        _ball.velocity = Vector3.zero;
        _ball.angularVelocity = Vector3.zero;
        _ball.transform.localEulerAngles = Vector3.zero;
        Vector3 pos = new Vector3(BallPositionX, 0f, BallPositionZ);

        if (PlayerPrefs.GetInt("sonar") == 1)
            SourceM.PlayOneShot(PitoAudioFinal, 0.5f);

        Vector3 diff = -pos;
        diff.Normalize();
        float angleRadian = Mathf.Atan2(diff.z, diff.x);
        float angle = 90 - angleRadian * Mathf.Rad2Deg;
        _ball.transform.parent.localEulerAngles = new Vector3(0, angle, 0);
        _ball.transform.position = new Vector3(BallPositionX, 0.1f, BallPositionZ);
        pos = _ballTarget.position;
        pos.x = 0;
        _ballTarget.position = pos;
        float val = (Mathf.Abs(_ball.transform.localPosition.z) - _distanceMinZ) / (_distanceMaxZ - _distanceMinZ);
        _zVelocity = Mathf.Lerp(_speedMin, _speedMax, val);
        EventChangeSpeedZ(_zVelocity);
        EventDidPrepareNewTurn();
        //mouseEnd();
        PosCurva.Clear();
        afterShoot = false;
    }

    public void enableTouch()
    {
        _enableTouch = true;
    }

    public void disableTouch()
    {
        StartCoroutine(_disableTouch());
    }

    private IEnumerator _disableTouch()
    {
        yield return new WaitForEndOfFrame();
        _enableTouch = false;
    }

    private void DestruirPlatillos()
    {
        TBlanco[] platillos = GameObject.FindObjectsOfType<TBlanco>();
        foreach (TBlanco p in platillos)
        {
            Destroy(p.gameObject);
        }
    }
}

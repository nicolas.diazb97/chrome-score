﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Wall : MonoBehaviour {

	public static Wall share;
	public Transform PosBall;
	public GameObject[] _bodiesEditor;
	public List<GameObject> _bodies;

	public Transform _poleLeft;
	public Transform _poleRight;

	private Transform _sphere;
	private Transform _sphere1;

	public Transform initRightPosition;
	public Transform initLeftPosition;

    public bool _isWall = true;

	private int count;
	private int nivel = 1;

	void Start(){
		share = this;
		if (animationPateador.pelotaALaIzquierda) {
			//Debug.Log ("barrera izquierda");
			transform.localPosition = initLeftPosition.localPosition;
			//Wall.share.setWall(initLeftPosition.localPosition);
		} else if (animationPateador.pelotaALaDerecha) {
			//Debug.Log ("barrera derecha");
			transform.localPosition = initRightPosition.localPosition;
			//Wall.share.setWall(initRightPosition.localPosition); 
		} else {
			//Debug.Log ("barrera centro");
			transform.localPosition = initRightPosition.localPosition;
		}
		_bodies = new List<GameObject>();
		IsWall = true;
	}

    public bool IsWall
    {
        get { return _isWall; }
        set
        {
            _isWall = value;
            SetActiveWall(_isWall);
        }
    }

    public void OnToggle_Wall(bool val)
	{
	    SetActiveWall(val);
	}

	public void SetActiveWall(bool active) 
	{
	    foreach (GameObject go in _bodies)
	    {
	        if (go.activeInHierarchy != active)
	        {
                go.SetActive(active);
	        }
	    }
	    foreach (GameObject go in _bodiesEditor)
	    {
	        if (go.activeInHierarchy != active)
	        {
                go.SetActive(active);
	        }
	    }
	}

	void Update(){
		if (animationPateador.pelotaALaIzquierda) {
			//Debug.Log ("barrera izquierda");
			transform.localPosition = initLeftPosition.localPosition;
			//Wall.share.setWall(initLeftPosition.localPosition);
		} else if (animationPateador.pelotaALaDerecha) {
			//Debug.Log ("barrera derecha");
			transform.localPosition = initRightPosition.localPosition;
			//Wall.share.setWall(initRightPosition.localPosition); 
		} else {
			//Debug.Log ("barrera centro");
			transform.localPosition = initRightPosition.localPosition;
		}
		//this.transform.position = new Vector3(this.transform.position.x,this.transform.position.y,PosBall.position.z+3f);
		//Debug.Log(GameObject.FindObjectOfType<animationPateador>().leftHanded);
		//Debug.Log("posicion barrera: "+ transform.localPosition + " "+transform.position.y+" "+ transform.position.z);
	}

	void Awake() 
	{
        //SetActiveWall();

	}

	public Renderer[] _renderers;

	public void setWallAlpha(bool isFront) {
		float a;

		foreach(Renderer renderer in _renderers) { 
			Material mat1 = renderer.materials[0];
			Material mat2 = renderer.materials[1];
			Material mat3 = renderer.materials[2];

			if(isFront) {
				a = 1f;
				mat3.shader = Shader.Find("Diffuse");
			}
			else {
				a = 1f;
				mat3.shader = Shader.Find("Transparent/Diffuse");
			}

			Color c = mat1.color;
			c.a = a;
			mat1.color = c;

			c = mat2.color;
			c.a = a;
			mat2.color = c;

			c = mat3.color;
			c.a = a;
			mat3.color = c;
		}
	}

	public void setWallUniform(Country country) {

		foreach(Renderer renderer in _renderers) {
			Material mat1 = renderer.materials[0];
			Material mat2 = renderer.materials[1];
			Material mat3 = renderer.materials[2];
			mat3.mainTexture = (Texture2D) Resources.Load( "Uniform/UniformFootball_" + country.ToString());
			mat1.mainTexture = (Texture2D) Resources.Load( "Numbers/number" + ((int)Random.Range(2, 22)));
			mat2.mainTexture = (Texture2D) Resources.Load( "Numbers/number" + ((int)Random.Range(2, 22)));
		}
	}

	public void PosBarrera(string pos){
		
		switch(pos){
		case "p1":
			if(PlayerPrefs.GetFloat ("posPateadorX")>=8){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else if (PlayerPrefs.GetFloat ("posPateadorX")<3 && PlayerPrefs.GetFloat ("posPateadorX")>-5){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			} else if (PlayerPrefs.GetFloat ("posPateadorX")<-5 && PlayerPrefs.GetFloat ("posPateadorX")>-13){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else{
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}
			break;
		case "p2":
			if (PlayerPrefs.GetFloat ("posPateadorX")>-5 && PlayerPrefs.GetFloat ("posPateadorX")<0){
				_bodiesEditor[0].SetActive(false);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else  if (PlayerPrefs.GetFloat ("posPateadorX")>0){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}
			else{
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}
			break;
		case "p3":
			_bodiesEditor[0].SetActive(true);
			_bodiesEditor[1].SetActive(true);
			_bodiesEditor[2].SetActive(true);
			_bodiesEditor[3].SetActive(true);
			break;
		case "p4":
			_bodiesEditor[0].SetActive(true);
			_bodiesEditor[1].SetActive(true);
			_bodiesEditor[2].SetActive(true);
			_bodiesEditor[3].SetActive(true);
			break;
		case "p5":
			_bodiesEditor[0].SetActive(true);
			_bodiesEditor[1].SetActive(true);
			_bodiesEditor[2].SetActive(true);
			_bodiesEditor[3].SetActive(true);
			break;
		case "p6":
			if(PlayerPrefs.GetFloat ("posPateadorX")>5){
				_bodiesEditor[0].SetActive(false);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else{
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}

			break;
		case "p7":
			

			if(PlayerPrefs.GetFloat ("posPateadorX")>-5){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}
			else if(PlayerPrefs.GetFloat ("posPateadorX")>9 && PlayerPrefs.GetFloat ("posPateadorX")<12){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else if(PlayerPrefs.GetFloat ("posPateadorX")>13){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else if (PlayerPrefs.GetFloat ("posPateadorX")<-9){
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}else{
				_bodiesEditor[0].SetActive(true);
				_bodiesEditor[1].SetActive(true);
				_bodiesEditor[2].SetActive(true);
				_bodiesEditor[3].SetActive(true);
			}
	
			break;
		}
	//	Debug.Log(pos+"--"+PlayerPrefs.GetFloat ("posPateadorX"));
	}

	public void setWall(Vector3 ballPosition) {
		if(IsWall == false) {
			SetActiveWall(false);
			return;
		}

		Vector3 pos = -ballPosition*2;
		pos.Normalize();
		float angleRadian = Mathf.Atan2(pos.z, pos.x);		// tinh' goc' lech
		float angle = 90 - angleRadian * Mathf.Rad2Deg;		// angle in degree
		float angleAbs = Mathf.Abs(angle);

				// bao nhieu nguoi dung hang rao

		if(angleAbs < 10) {
			count = 6;
		}
		else if(angleAbs < 20) {
			count = 5;
		}
		else if(angleAbs < 30) {
			count = 4;
		}
		else if(angleAbs < 40) {
			count = 3;
		}
		else {
			count = 2;
		}

		nivel = PlayerPrefs.GetInt ("level");

		//count = 0;
			if (PlayerPrefs.GetFloat ("posPateadorX") > 3) 
			{
				PosBarrera("p1");
			}
			else if (PlayerPrefs.GetFloat ("posPateadorX") > 2 && PlayerPrefs.GetFloat ("posPateadorX") <= 3) 
			{
				PosBarrera("p2");
			}
			else if (PlayerPrefs.GetFloat ("posPateadorX") > 1 && PlayerPrefs.GetFloat ("posPateadorX") <= 2) 
			{
				PosBarrera("p3");
			}
			else if (PlayerPrefs.GetFloat ("posPateadorX") > -1 && PlayerPrefs.GetFloat ("posPateadorX") <= 1) 
			{
				PosBarrera("p4");
			}
			else if (PlayerPrefs.GetFloat ("posPateadorX") > -2 && PlayerPrefs.GetFloat ("posPateadorX") <= -1) 
			{
				PosBarrera("p5");
			}
			else if (PlayerPrefs.GetFloat ("posPateadorX") > -3 && PlayerPrefs.GetFloat ("posPateadorX") <= -2) 
			{
				PosBarrera("p6");
			}
			if (PlayerPrefs.GetFloat ("posPateadorX") <= -3) 
			{
				PosBarrera("p7");
			}
		
	

//		foreach(GameObject go in _bodies) 
//			_bodiesEditor.Add(go);
//
//			_bodies.Clear();
//
//		foreach(GameObject go in _bodiesEditor)
//			go.SetActive(false);
//
//		for(int i = 0; i < count; ++i) {		// count la so nguoi se~ dung' trong hang` rao`, add dzo _bodies
//			GameObject go = _bodiesEditor[Random.Range(0, _bodiesEditor.Count)];
//			_bodies.Add(go);
//			_bodiesEditor.Remove(go);
//			_bodiesEditor[i].SetActive(true);
//		}


		Vector3 posPole;
		if(angle > 0) {  // hang rao` se~ lay' cot doc ben trai lam chuan, nhin` tu` banh den' goal
			posPole = initLeftPosition.position;
		}
		else {			// hang rao` se~ lay' cot doc ben fai lam chuan,  nhin` tu` banh den' goal
			posPole = initRightPosition.position;
		}

		float z =  ballPosition.z + 11f;		// z cua hang rao, hang rao se dat o~ vi tri' z, * 0.55f co' nghia la hang rao se gan trai banh hon la gan khung thanh
		Vector3 lineVector = (posPole - ballPosition).normalized;		//  vector chi~ phuong tu` trai' banh den' cot doc

		Vector3 intersection;		// giao diem cua~ duong thang tu trai banh den' cot doc cat' mat phang co' z = z vua tim duoc o~ tren, cau thu thu' 2 se dat o giao diem nay
		Math3d.LinePlaneIntersection (out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));

		intersection.y = 0;

		float sign = Mathf.Sign(angle);

		//intersection.x -= (sign * 0.9f);		// cau thu dau` tien se~ bi dat lech 0.5f, tuy luc' do' la ben trai' hay fai ma` cau thu se bi lech trai hay lech fai~

		//cantidad de defensas
		for(int i = 0; i < _bodies.Count; ++i)
		{
			Debug.Log ("loop");
			GameObject go = _bodies[i];
			go.transform.position = Vector3.zero;
			//distancia entre los defensas
			intersection.x += (sign * 0.8f);// que tan pegada deben estar los jugadores de la barrera
			go.SetActive(true);
		}
	}
}

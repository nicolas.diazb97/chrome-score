﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Core;
using Holoville.HOTween.Path;
using Holoville.HOTween.Plugins;
using Holoville.HOTween.Plugins.Core;
using UnityEngine.UI;

public enum Area {
	Top,
	Left,
	Right,
	Normal,
	CornerLeft,
	CornerRight,
	None
}
public delegate void GoalEvent(bool isGoal, Area area);

public class GoalDetermine : MonoBehaviour {

	public static GoalDetermine share;
	public static int PuntajeMax,Puntaje, puntajePorGol, puntajeTotal;
	public static GoalEvent EventFinishShoot;
	public GameObject _prefabGoalSuccess;
	public GameObject loading;
	private bool _isGoal;
	public bool _goalCheck;

	private Transform _ball;

	[SerializeField] 
    private Transform _pointLeft;
    [SerializeField]
    private Transform _pointRight;
    [SerializeField]
    private Transform _pointUp;
    [SerializeField]
    private Transform _pointDown;
    [SerializeField]
    private Transform _pointBack;

	public Renderer _areaTop;
	public Renderer _areaCornerLeft;
	public Renderer _areaCornerRight;
	public Renderer _areaLeft;
	public Renderer _areaRight;

	public Renderer _effectTop;
	public Renderer _effectLeft;
	public Renderer _effectCornerLeft;
	public Renderer _effectCornerRight;
	public Renderer _effectRight;

	public Transform _poleLeft;
	public Transform _poleRight;

	public Material _matCenter;
	public Texture2D _centerNormal;
	public Texture2D _centerCritical;

	public GameObject GoalEffectAmaraillo, GoalEffectAmarailloEN, GoalEffectAzul, GoalEffectAzulEN, PointEffect, intentaloEffect, intentaloEffectEN, GoalEffectRojoEN;
	public Text PuntajeText, puntajeContraReloj;
	private Area _poleArea;

	public bool intentarDeNuevo;

	void Awake() {
		share = this;
		_prefabGoalSuccess.SetActive(false);

	}

	// Use this for initialization
	void Start () {
		_ball = Shoot.share._ball.transform;
		Shoot.EventShoot += eventShoot;
		initScores();
		PuntajeText.gameObject.SetActive (false);
	}

	public void initScores(){
		Puntaje=0;
		puntajeTotal = 0;
		puntajePorGol = 0;
		PuntajeText.text = Puntaje.ToString();	
		if (PlayerPrefs.GetInt ("Creloj") == 1) {
			puntajeContraReloj.text = Puntaje.ToString ();
		} else {
			PuntajeText.text = Puntaje.ToString();	
		}
	}

	void OnDestroy() {
		Shoot.EventShoot -= eventShoot;
	}

	private Vector3 _contactPointWithPole;

	public void hitPole(Area poleArea, Vector3 contactPoint) {
		//Debug.Log("salio");
		if(_goalCheck) {
			_contactPointWithPole = contactPoint;
			_poleArea = poleArea;
		}
	}

	private void eventShoot() {
		_count1 = _count2 = _count3 = 0;
		_goalCheck = true;
		_posPrev = _posCur = _ball.position;

		_poleArea = Area.None;
//		RepeticionGol.main.ResetRepeticion(true);
//		RepeticionGol.main.TakeScreen();
	}

	public void reset() {
		_goalCheck = false;
	}

	public void flashingHighScoreAreas() {
		_effectTop.enabled = true;
		_effectLeft.enabled = true;
		_effectRight.enabled = true;
		_effectCornerLeft.enabled = true;
		_effectCornerRight.enabled = true;

//		Material mat = _effectTop.sharedMaterial;
//		Color col = mat.GetColor("_TintColor");
//		col.a = 50f/255f;
//		mat.SetColor("_TintColor", col);

//		mat = _effectCornerLeft.sharedMaterial;
//		col = mat.GetColor("_TintColor");
//		col.a = 50f/255f;
//		mat.SetColor("_TintColor", col);

		iTween.ValueTo(gameObject, iTween.Hash("time", 1.5f
		                                       ,"delay", 0.5f
		                                       ,"from", 50f/255f
		                                       ,"to", 0f
		                                       ,"easetype", iTween.EaseType.linear
		                                       ,"onupdate", "onUpdateFlashing"
		                                       ,"oncomplete", "completeFlashing"
		                                       ));
	}

	private void onUpdateFlashing(float val) {
//		Material mat = _effectTop.sharedMaterial;
//		Color col = mat.GetColor("_TintColor");
//		col.a = val;
//		mat.SetColor("_TintColor", col);

//		mat = _effectCornerLeft.sharedMaterial;
//		col = mat.GetColor("_TintColor");
//		col.a = 50f/255f;
//		mat.SetColor("_TintColor", col);
	}

	private void completeFlashing() {
		_effectTop.enabled = false;
		_effectLeft.enabled = false;
		_effectRight.enabled = false;
		_effectCornerLeft.enabled = false;
		_effectCornerRight.enabled = false;
	}

	private Vector3 _posPrev;
	private Vector3 _posCur;

	private float _count1;
	private float _count2;
	private float _count3;

	float _distance;

	public AudioClip GoalAudio, FailAudio;
	public AudioSource SourceM;

	// Update is called once per frame
	void Update () {
		
		//print ("puntaje por gol "+puntajePorGol);

		if(_goalCheck) {

			_posPrev = _posCur;
			_posCur = _ball.position;

			if( _posCur.z >= _pointDown.position.z) 
			{       // ko check goal nua
				if(_posCur.z <= _pointBack.position.z && _posCur.x < _pointRight.position.x && _posCur.x > _pointLeft.position.x && _posCur.y < _pointUp.position.y  && _posCur.y > 0) {
					_count1 = 0;

					_goalCheck = false;

					Area area = Area.Normal;

					if(_poleArea != Area.None) {		// neu truoc do trung xa ngang hay cot doc xong roi banh vo luoi 

						area = Area.CornerLeft;
						if (PlayerPrefs.GetInt ("Creloj") == 0 || PlayerPrefs.GetInt ("Creloj") == 1 || PlayerPrefs.GetInt ("Creloj") == 4) {
							setGoalSuccesIcon(area, _contactPointWithPole);
						} else if (PlayerPrefs.GetInt ("Creloj") == 2 || PlayerPrefs.GetInt ("Creloj") == 3 || PlayerPrefs.GetInt ("Creloj") == 5){
							if (PlayerPrefs.GetInt ("r1p") == 1 || PlayerPrefs.GetInt ("r2p") == 1 || PlayerPrefs.GetInt ("r4p") == 1) {
								print ("malparido gol normal");
								setGoalSuccesIcon (area, _contactPointWithPole);
							} else {
								print ("intentalo de nube");
								intentarDeNuevo = true;
								setGoalSuccesIcon (area, _contactPointWithPole);
							}
						}
					}
					else {		// khong trung xa ngang cot doc gi het
						if( _areaTop.bounds.Contains(_posCur))
							area = Area.Top;
						else if(_areaLeft.bounds.Contains(_posCur)) {
							area = Area.Left;
						}
						else if(_areaRight.bounds.Contains(_posCur)) {
							area = Area.Right;
						}
						else if(_areaCornerLeft.bounds.Contains(_posCur)) {
							area = Area.CornerLeft;
						}
						else if(_areaCornerRight.bounds.Contains(_posCur)) {
							area = Area.CornerRight;
						}

						if (PlayerPrefs.GetInt ("Creloj") == 0 || PlayerPrefs.GetInt ("Creloj") == 1 || PlayerPrefs.GetInt ("Creloj") == 4) {
							setGoalSuccesIcon (area, _ball.transform.position);
						} else if (PlayerPrefs.GetInt ("Creloj") == 2 || PlayerPrefs.GetInt ("Creloj") == 3 || PlayerPrefs.GetInt ("Creloj") == 4 || PlayerPrefs.GetInt ("Creloj") == 5){
							if (PlayerPrefs.GetInt ("r1p") == 1 || PlayerPrefs.GetInt ("r2p") == 1 || PlayerPrefs.GetInt ("r4p") == 1) {
								print ("masdas gol");
								setGoalSuccesIcon (area, _ball.transform.position);
							}else {
								print ("intentalo de nube");
								intentarDeNuevo = true;
								setGoalSuccesIcon (area, _ball.transform.position);
							}
						}
					}


					if(EventFinishShoot != null) 
						EventFinishShoot(true, area);
				}
				else {
					_count1 += Time.deltaTime;
					if(_count1 > 1f) {
						_goalCheck = false;
						if(EventFinishShoot != null) 
							EventFinishShoot(false, Area.None);
					}
				}
			}
			else {      // tiep tuc check goal
				if(_posCur.z > _posPrev.z) {
					_count2 = 0;

					_distance = 10f;
					if(Shoot.share._ball.velocity.sqrMagnitude < 0.3f)
						_distance = 0f;
					else if(Shoot.share._ball.velocity.sqrMagnitude < 2f) 
						_distance = 4f;

					if( Mathf.Abs(_posCur.x) > _distance && (Mathf.Abs(_posCur.x) > Mathf.Abs(_posPrev.x) ||  Shoot.share._ball.velocity.sqrMagnitude < 0.3f)) {
						_count3 += Time.deltaTime;
						if(_count3 > 1f) {
							_goalCheck = false;
							if(EventFinishShoot != null) 
								EventFinishShoot(false, Area.None);
						}
					}
					else {
						_count3 = 0;
					}
				}
				else {
					_count2 += Time.deltaTime;
					if(_count2 > 1f) {
						_goalCheck = false;
						if(EventFinishShoot != null) 
							EventFinishShoot(false, Area.None);
					}
				}
			}
		}
	}


	public IEnumerator SumarPuntaje(int punt){
		//print (".l.");
		puntajePorGol = punt;
		puntajeTotal += puntajePorGol;
		for (int i = 0; i < punt; i += 3) {
			//print (".i.");
			Puntaje += 3;
			if (PlayerPrefs.GetInt ("Creloj") == 1) {
				puntajeContraReloj.text = Puntaje.ToString ();
			} else {
				PuntajeText.text = Puntaje.ToString ();
			}
			//yield return new WaitForSeconds(0.000000000000001f);
			yield return new WaitForFixedUpdate ();
		}
	}

	private void setGoalSuccesIcon(Area area, Vector3 position)
    {
		
		_prefabGoalSuccess.SetActive(true);
		_prefabGoalSuccess.transform.position = position;

				Debug.LogError("HP");
		if(area == Area.None || area == Area.Normal) 
		{
			if (PlayerPrefs.GetInt ("Tutorial") == 0) {
				TutorialCanvas.IsGoal++;
			} else if (PlayerPrefs.GetInt ("Creloj") == 0 || PlayerPrefs.GetInt ("Creloj") == 4) {
				PlayerPrefs.SetInt ("gol", 1);
				Debug.LogError("GOL HP");
				if (PlayerPrefs.GetInt ("level") == 1) {
					if (!BonusMark.IsTouchBonus) {

						MatchManager.main.SetNewScoreByIndex(MatchManager.main.currShoot, 80);
						StartCoroutine (SumarPuntaje (80));
					} else {
						Debug.Log ("Combo");
						MatchManager.main.SetNewScoreByIndex(MatchManager.main.currShoot, 100);
						StartCoroutine (SumarPuntaje (100));
					}
				} else if (PlayerPrefs.GetInt ("level") == 2) {
					if (!BonusMark.IsTouchBonus) {
						StartCoroutine (SumarPuntaje (1000));
					} else {
						Debug.Log ("Combo");
						StartCoroutine (SumarPuntaje (1100));
					}
				} else if (PlayerPrefs.GetInt ("level") == 3) {
					if (!BonusMark.IsTouchBonus) {
						Debug.Log ("Combo");
						StartCoroutine (SumarPuntaje (1200));
					} else {
						Debug.Log ("Combo");
						StartCoroutine (SumarPuntaje (1300));
					}
				} else if (PlayerPrefs.GetInt ("level") == 4) {
					if (!BonusMark.IsTouchBonus) {
						StartCoroutine (SumarPuntaje (1400));
					} else {
						Debug.Log ("Combo");
						StartCoroutine (SumarPuntaje (1500));
					}
				} else {
					if (!BonusMark.IsTouchBonus) {
						StartCoroutine (SumarPuntaje (1600));
					} else {
						Debug.Log ("Combo");
						StartCoroutine (SumarPuntaje (1700));
					}
				}
			} else if (PlayerPrefs.GetInt ("Creloj") == 1) {
				PlayerPrefs.SetInt ("gol", 1);
				StartCoroutine (SumarPuntaje (1000));
			} else if (PlayerPrefs.GetInt ("Creloj") == 2 ||  PlayerPrefs.GetInt ("Creloj") == 3 || PlayerPrefs.GetInt ("Creloj") == 5 ) {
				
				//agregar el playerpref de cada reto
				if (PlayerPrefs.GetInt ("r1p") == 1 || PlayerPrefs.GetInt ("r2p") == 1 || PlayerPrefs.GetInt ("r4p") == 1) {
					print ("enotr");
					PlayerPrefs.SetInt ("r1p", 0);
					PlayerPrefs.SetInt ("r2p", 0);
					PlayerPrefs.SetInt ("r4p", 0);
					PlayerPrefs.SetInt ("gol", 1);
					if (PlayerPrefs.GetInt ("r1n1") == 1 || PlayerPrefs.GetInt ("r2n1") == 1 || PlayerPrefs.GetInt ("r4n1") == 1 || PlayerPrefs.GetInt ("r1n1") == 2 || PlayerPrefs.GetInt ("r2n1") == 2 || PlayerPrefs.GetInt ("r4n1") == 2) {
						Debug.Log ("RACABAI");
						StartCoroutine (SumarPuntaje (1000));
					} else if (PlayerPrefs.GetInt ("r1n2") == 1 || PlayerPrefs.GetInt ("r2n2") == 1 || PlayerPrefs.GetInt ("r4n2") == 1 || PlayerPrefs.GetInt ("r1n2") == 2 || PlayerPrefs.GetInt ("r2n2") == 2 || PlayerPrefs.GetInt ("r4n2") == 2) {
						Debug.Log ("SUGAR");
						StartCoroutine (SumarPuntaje (1200));
					}else if (PlayerPrefs.GetInt ("r1n3") == 1 || PlayerPrefs.GetInt ("r2n3") == 1 || PlayerPrefs.GetInt ("r4n3") == 1 || PlayerPrefs.GetInt ("r1n3") == 2 || PlayerPrefs.GetInt ("r2n3") == 2 || PlayerPrefs.GetInt ("r4n3") == 2) {
						StartCoroutine (SumarPuntaje (900));
					}else if (PlayerPrefs.GetInt ("r1n4") == 1 || PlayerPrefs.GetInt ("r2n4") == 1 || PlayerPrefs.GetInt ("r4n4") == 1 || PlayerPrefs.GetInt ("r1n4") == 2 || PlayerPrefs.GetInt ("r2n4") == 2 || PlayerPrefs.GetInt ("r4n4") == 2) {
						StartCoroutine (SumarPuntaje (1100));
					}
				}
			}

			_matCenter.mainTexture = _centerNormal;
			if (intentarDeNuevo)
            {
				intentarDeNuevo = false;
				GameObject Effect = new GameObject ();
                if(GlobalData.idiomaActivo == Idioma.ES)
				    Effect = Instantiate (intentaloEffect, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;
                else
                    Effect = Instantiate(intentaloEffectEN, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;

                Effect.transform.SetParent (PointEffect.transform);
			}
            else
            {
				print ("GOOOLLL");
				GameObject Effect = new GameObject ();
                if(GlobalData.idiomaActivo == Idioma.ES)
				    Effect = Instantiate (GoalEffectAzul, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;
                else
                    Effect = Instantiate(GoalEffectAzulEN, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;
                //
                Effect.transform.SetParent (PointEffect.transform);
                GoalAudioManager.instance.Goal();
			}


			///// goles tutorial

		}
		else 
		{ 
			if(PlayerPrefs.GetInt("Tutorial") == 0)
            {
				TutorialCanvas.IsGoal++;
			}else if(PlayerPrefs.GetInt ("Creloj") == 0 || PlayerPrefs.GetInt ("Creloj") == 4 ){
				PlayerPrefs.SetInt ("gol", 1);
				if(PlayerPrefs.GetInt ("level") == 1){
					int tempP = Random.Range(900,1000);
					if(BonusMark.IsTouchBonus)
						tempP = tempP*3;
					StartCoroutine(SumarPuntaje(tempP));
				}else if(PlayerPrefs.GetInt ("level") == 2){
					int tempP = Random.Range(1100,1200);
					if(BonusMark.IsTouchBonus)
						tempP = tempP*3;
					StartCoroutine(SumarPuntaje(tempP));
				}else if(PlayerPrefs.GetInt ("level") == 3){
					int tempP = Random.Range(1300,1400);
					if(BonusMark.IsTouchBonus)
						tempP = tempP*3;
					StartCoroutine(SumarPuntaje(tempP));
				}else if(PlayerPrefs.GetInt ("level") == 4){
					int tempP = Random.Range(1500,1600);
					if(BonusMark.IsTouchBonus)
						tempP = tempP*3;
					StartCoroutine(SumarPuntaje(tempP));
				}else{
					int tempP = Random.Range(1700,1800);
					if(BonusMark.IsTouchBonus)
						tempP = tempP*3;
					StartCoroutine(SumarPuntaje(tempP));
				}
			}else if (PlayerPrefs.GetInt ("Creloj") == 1) {
				PlayerPrefs.SetInt ("gol", 1);
				StartCoroutine (SumarPuntaje (1200));
			} else if (PlayerPrefs.GetInt ("Creloj") == 2 ||  PlayerPrefs.GetInt ("Creloj") == 3 || PlayerPrefs.GetInt ("Creloj") == 5 ) {
				if (PlayerPrefs.GetInt ("r1p") == 1 || PlayerPrefs.GetInt ("r2p") == 1 || PlayerPrefs.GetInt ("r4p") == 1) {
					PlayerPrefs.SetInt ("r1p", 0);
					PlayerPrefs.SetInt ("r2p", 0);
					PlayerPrefs.SetInt ("r4p", 0);
					PlayerPrefs.SetInt ("gol", 1);
					if (PlayerPrefs.GetInt ("r1n1") == 1 || PlayerPrefs.GetInt ("r2n1") == 1 || PlayerPrefs.GetInt ("r4n1") == 1 || PlayerPrefs.GetInt ("r1n1") == 2 || PlayerPrefs.GetInt ("r2n1") == 2 || PlayerPrefs.GetInt ("r4n1") == 2) {
						Debug.Log ("RACABAI");
						StartCoroutine (SumarPuntaje (1000));
					} else if (PlayerPrefs.GetInt ("r1n2") == 1 || PlayerPrefs.GetInt ("r2n2") == 1 || PlayerPrefs.GetInt ("r4n2") == 1 || PlayerPrefs.GetInt ("r1n2") == 2 || PlayerPrefs.GetInt ("r2n2") == 2 || PlayerPrefs.GetInt ("r4n2") == 2) {
						Debug.Log ("SUGAR");
						StartCoroutine (SumarPuntaje (1200));
					}else if (PlayerPrefs.GetInt ("r1n3") == 1 || PlayerPrefs.GetInt ("r2n3") == 1 || PlayerPrefs.GetInt ("r4n3") == 1 || PlayerPrefs.GetInt ("r1n3") == 2 || PlayerPrefs.GetInt ("r2n3") == 2 || PlayerPrefs.GetInt ("r4n3") == 2) {
						StartCoroutine (SumarPuntaje (900));
					}else if (PlayerPrefs.GetInt ("r1n4") == 1 || PlayerPrefs.GetInt ("r2n4") == 1 || PlayerPrefs.GetInt ("r4n4") == 1 || PlayerPrefs.GetInt ("r1n4") == 2 || PlayerPrefs.GetInt ("r2n4") == 2 || PlayerPrefs.GetInt ("r4n4") == 2) {
						StartCoroutine (SumarPuntaje (1100));
					}
				}
			}

			_matCenter.mainTexture = _centerCritical;
			Debug.LogError(" SE ACABOOOO SA[P PERRO");
			print ("Tiro fallido!!!");
			SourceM.PlayOneShot(GoalAudio,2);
			if (intentarDeNuevo) {
				intentarDeNuevo = false;
				GameObject Effect = new GameObject ();
                if (GlobalData.idiomaActivo == Idioma.ES)
                    Effect = Instantiate(intentaloEffect, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;
                else
                    Effect = Instantiate(intentaloEffectEN, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;

                Effect.transform.SetParent (PointEffect.transform);
			} else {
				GameObject Effect = new GameObject ();
                if(GlobalData.idiomaActivo == Idioma.ES)
				    Effect = Instantiate (GoalEffectAmaraillo, PointEffect.transform.position, PointEffect.transform.transform.rotation)  as GameObject;
                else
                    Effect = Instantiate(GoalEffectAmarailloEN, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;
                //
                Effect.transform.SetParent (PointEffect.transform);
			}
		


			/////// tutorial goles
			if(PlayerPrefs.GetInt("Tutorial") == 0){
				TutorialCanvas.IsGoal++;
			}
		}

		_prefabGoalSuccess.transform.localScale = new Vector3(0.6f, 0.6f, 1);
		HOTween.To(_prefabGoalSuccess.transform, 1f, new TweenParms()
		           .Prop( "localScale", new Vector3(0.9f, 0.9f, 1), false)
		           .Loops(1, LoopType.Restart)
		           .Ease(EaseType.Linear)
		           .OnComplete(animationFinished)
		           );

		Material mat = _prefabGoalSuccess.GetComponent<Renderer>().sharedMaterial;
		Color col = mat.GetColor("_TintColor");
		col.a = 0.5f;
		mat.SetColor("_TintColor", col);
		
		iTween.ValueTo(gameObject, iTween.Hash("time", 0.5f
		                                       ,"from", 0.5f
		                                       ,"to", 1f
		                                       ,"easetype", iTween.EaseType.easeInCubic
		                                       ,"onupdate", "onUpdateColor"
		                                       ,"oncomplete", "complete1"
		                                       ));

	    flashingHighScoreAreas();
        Shoot.share.PosCurva.Clear();
        Shoot.share.afterShoot = false;

        collisionBall.isTouchBall = false;
        Shoot.share._canControlBall = true;
        Shoot.share._isShooting = true;
        //Shoot.share._ball.velocity = Vector3.zero;
        //Shoot.share._ball.angularVelocity = Vector3.zero;
        //Shoot.share._ball.transform.localEulerAngles = Vector3.zero;
        PlayerPrefs.SetInt("pateando1", 0);
    }

	private void onUpdateColor(float val) {
		Material mat = _prefabGoalSuccess.GetComponent<Renderer>().sharedMaterial;
		Color col = mat.GetColor("_TintColor");
		col.a = val;
		mat.SetColor("_TintColor", col);
	}
	public void EnemyGoal()
	{
		GameObject Effect = new GameObject();
		Effect = Instantiate(GoalEffectRojoEN, PointEffect.transform.position, PointEffect.transform.transform.rotation) as GameObject;
	}
	private void complete1() {
		iTween.ValueTo(gameObject, iTween.Hash("time", 0.5f
		                                       ,"from", 1f
		                                       ,"to", 0f
		                                       ,"easetype", iTween.EaseType.easeOutCubic
		                                       ,"onupdate", "onUpdateColor"
		                                       ));
	}

	private void animationFinished() {
		_prefabGoalSuccess.SetActive(false);
	}
}

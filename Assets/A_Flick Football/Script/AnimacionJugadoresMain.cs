﻿using UnityEngine;
using System.Collections;

public class AnimacionJugadoresMain : MonoBehaviour {


	public Animator anim;
	public int numerRandom;

	// Use this for initialization
	void Start () {
		AsignarAnimacionAleatoria ();
	}

	// Update is called once per frame
	void Update () {
	
	}

	private void AsignarAnimacionAleatoria(){
		numerRandom =(int) Random.Range (0, 6);
		anim.SetInteger ("random",numerRandom);
		print ("numero random que vale verga "+numerRandom);
		print ("Animacion actual " +anim.GetCurrentAnimatorClipInfo (0)[0].clip.name);
	}
}

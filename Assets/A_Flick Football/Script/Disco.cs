﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Exploder.Utils;

public class Disco : MonoBehaviour {

	public AudioClip romper;

	private GameObject exploder;
	public static bool explotar;

	// Use this for initialization
	void Start () {
		exploder = GameObject.FindObjectOfType<ExploderSingleton> ().gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		exploder.transform.position = transform.position;
		if (explotar) {
			explotar = false;
			Shoot.share.SourceM.PlayOneShot(romper,1);
			ExploderSingleton.ExploderInstance.Explode ();
		}
	}
}

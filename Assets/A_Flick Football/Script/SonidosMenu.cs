﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SonidosMenu : MonoBehaviour {

	public Button sonido;
	public Sprite off,on;
	public AudioClip boton;
	public AudioSource SourceM;
	public bool sonando;

	void Start( ){
		sonando = ( PlayerPrefs.GetInt("sonar") == 1 )? true: false;
		SetAudio( );
	}

	// Update is called once per frame
	void SetAudio ( ) {

//		Debug.Log("sonido de mierda:" + sonando + PlayerPrefs.GetInt("sonar"));

		if ( !sonando ) {
			sonido.image.sprite = off;
			SourceM.mute = true;
			PlayerPrefs.SetInt("sonar", 0);
		} else {
			sonido.image.sprite = on;
			SourceM.mute = false;
			PlayerPrefs.SetInt("sonar", 1);
		}
	}

	public void QuitarSonido(){
		sonando = !sonando;
		SetAudio( );
	}

	public void SonidoBoton(){
		//print ("sonido boton");
		SourceM.PlayOneShot (boton, 1f);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlechaReto : MonoBehaviour {

	private bool apagar;
	private Image flecha;

	// Use this for initialization
	void Start () {
		flecha = GetComponent<Image> ();
		print (flecha);
	}
	
	// Update is called once per frame
	void Update () {
		StartCoroutine (FlechaAlpha());
	}

	IEnumerator FlechaAlpha(){
		if (apagar) {
			print ("apagar");
			flecha.color = Color.Lerp (flecha.color, new Color (flecha.color.r,flecha.color.g, flecha.color.b, 0f), 2f * Time.deltaTime);
			yield return new WaitForSeconds (1);
			apagar = false;
		} else {
			print ("prender");
			flecha.color = Color.Lerp (flecha.color, new Color (flecha.color.r, flecha.color.g, flecha.color.b, 1f), 2f * Time.deltaTime);
			yield return new WaitForSeconds (1);
			apagar = true;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NivelesTirosLibres : MonoBehaviour
{

    public Sprite nivel2Activo, nivel3Activo, nivel4Activo, nivel5Activo;
    public Sprite estrellaDorada;
    public List<Image> imgsEstado = new List<Image>();
    public List<Image> estrellasN1 = new List<Image>();
    public List<Image> estrellasN2 = new List<Image>();
    public List<Image> estrellasN3 = new List<Image>();
    public List<Image> estrellasN4 = new List<Image>();
    public List<Image> estrellasN5 = new List<Image>();
    public Transform nivel1, nivel2, nivel3, nivel4, nivel5;
    List<bool> nivelesCompletados = new List<bool>();
    public List<Image> resplandores;

    private bool apagar = true;
    Color win = Color.cyan, complete = Color.yellow;

    // Use this for initialization
    void Start()
    {
        if (PlayerPrefs.GetInt("tn1") != 2)
        {
            PlayerPrefs.SetInt("tn1", 1);
        }
    }

    private void OnEnable()
    {
        cambiarImagenesDeNivel();
        win.a = complete.a = .6f;
    }

    // Update is called once per frame
    void Update()
    {
        Resplandecer();
    }

    private void cambiarImagenesDeNivel()
    {
        if (PlayerPrefs.GetInt("tn1") == 1 || PlayerPrefs.GetInt("tn1") == 2)
        {
            int estrellas = PlayerPrefs.GetInt("estrellasGanadasN1");

            for (int i = 0; i < estrellas; i++)
            {
                estrellasN1[i].color = Color.white;
                estrellasN1[i].sprite = estrellaDorada;
            }
        }
        if (PlayerPrefs.GetInt("tn1") == 2)
        {
            imgsEstado[1].sprite = nivel2Activo;
            int estrellas = PlayerPrefs.GetInt("estrellasGanadasN2");
            //
            if(estrellas >= 3)
                nivelesCompletados.Add(true);
            else
                nivelesCompletados.Add(false);
            //
            for (int i = 0; i < estrellas; i++)
            {
                estrellasN2[i].color = Color.white;
                estrellasN2[i].sprite = estrellaDorada;
            }
        }
        if (PlayerPrefs.GetInt("tn2") == 2)
        {
            imgsEstado[2].sprite = nivel3Activo;
            int estrellas = PlayerPrefs.GetInt("estrellasGanadasN3");
            //
            if (estrellas >= 3)
                nivelesCompletados.Add(true);
            else
                nivelesCompletados.Add(false);
            //
            for (int i = 0; i < estrellas; i++)
            {
                estrellasN3[i].color = Color.white;
                estrellasN3[i].sprite = estrellaDorada;
            }
        }
        if (PlayerPrefs.GetInt("tn3") == 2)
        {
            imgsEstado[3].sprite = nivel4Activo;
            int estrellas = PlayerPrefs.GetInt("estrellasGanadasN4");
            //
            if (estrellas >= 3)
                nivelesCompletados.Add(true);
            else
                nivelesCompletados.Add(false);
            //
            for (int i = 0; i < estrellas; i++)
            {
                estrellasN4[i].color = Color.white;
                estrellasN4[i].sprite = estrellaDorada;
            }
        }
        if (PlayerPrefs.GetInt("tn4") == 2)
        {
            imgsEstado[4].sprite = nivel5Activo;
            int estrellas = PlayerPrefs.GetInt("estrellasGanadasN5");
            //
            if (estrellas >= 3)
                nivelesCompletados.Add(true);
            else
                nivelesCompletados.Add(false);
            //
            for (int i = 0; i < estrellas; i++)
            {
                estrellasN5[i].color = Color.white;
                estrellasN5[i].sprite = estrellaDorada;
            }
        }
    }

    private void Resplandecer()
    {
        ResetResplandores();
        //
        if (PlayerPrefs.GetInt("tn1") == 1)
        {
            StartCoroutine(Resplandor(resplandores[0]));
        }
        else if (PlayerPrefs.GetInt("tn1") == 2)
        {
            StartCoroutine(Resplandor(resplandores[1]));
        }

        if (PlayerPrefs.GetInt("tn2") == 2)
        {
            StartCoroutine(Resplandor(resplandores[2]));
        }

        if (PlayerPrefs.GetInt("tn3") == 2)
        {
            StartCoroutine(Resplandor(resplandores[3]));
        }

        if (PlayerPrefs.GetInt("tn4") == 2)
        {
            StartCoroutine(Resplandor(resplandores[4]));
        }
    }

    void ResetResplandores()
    {
        foreach (Image resplandor in resplandores)
        {
            if (resplandores.IndexOf(resplandor) < nivelesCompletados.Count)
            {
                if (!nivelesCompletados[resplandores.IndexOf(resplandor)])
                {
                    resplandor.enabled = true;
                    resplandor.color = complete;
                }
                else
                {
                    resplandor.enabled = true;
                    resplandor.color = win;
                }
            }
            else
            {
                resplandor.enabled = false;
            }
        }
    }

    IEnumerator Resplandor(Image imageResplandor)
    {
        imageResplandor.enabled = true;
        if (apagar)
        {
            imageResplandor.color = Color.Lerp(imageResplandor.color, new Color(imageResplandor.color.r, imageResplandor.color.g, imageResplandor.color.b, 0f), 2f * Time.deltaTime);
            yield return new WaitForSeconds(1);
            apagar = false;
        }
        else
        {
            imageResplandor.color = Color.Lerp(imageResplandor.color, new Color(imageResplandor.color.r, imageResplandor.color.g, imageResplandor.color.b, 1f), 2f * Time.deltaTime);
            yield return new WaitForSeconds(1);
            apagar = true;
        }
    }
}

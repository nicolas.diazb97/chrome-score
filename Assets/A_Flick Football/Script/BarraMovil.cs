﻿using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class BarraMovil : MonoBehaviour {

	public  static int numero;

	public float tiempo;


	private Vector3 nuevaPosicionHorizontal;
	private Vector3 nuevaPosicionVertical;
	private Vector3 posicionInicial;
	private bool inicio = true;




	// Use this for initialization
	void Start () {
		posicionInicial = transform.position;
		nuevaPosicionHorizontal = new Vector3 (transform.position.x, transform.position.y, -11.85f);
		nuevaPosicionVertical = new Vector3 (transform.position.x, 4f , transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		
		if (numero >0) {
			MovimientoHorizontal ();
		} else {
			MovimientoVertical ();
		}



	}

	public static void NumeroRandom(){
		numero = (int )Random.Range (-2f,2f);
		print ("numero random "+numero);
	}

	private void MovimientoHorizontal(){
		if (inicio) {
			transform.position = Vector3.Lerp (transform.position, nuevaPosicionHorizontal, tiempo * Time.deltaTime);
			if(transform.position.z < (nuevaPosicionHorizontal.z+1f)){
				//Debug.Log ("cambiar lado");
				inicio = false;
			}
		} else {
			transform.position = Vector3.Lerp (transform.position, posicionInicial, tiempo * Time.deltaTime);
			if(transform.position.z > (posicionInicial.z-1f)){
				inicio = true;
			}
		}
	}

	private void MovimientoVertical(){
		if (inicio) {
			transform.position = Vector3.Lerp (transform.position, nuevaPosicionVertical, tiempo * Time.deltaTime);
			if(transform.position.y > (nuevaPosicionVertical.y-0.5f)){
				//Debug.Log ("cambiar lado vertical");
				inicio = false;
			}
		} else {
			transform.position = Vector3.Lerp (transform.position, posicionInicial, tiempo * Time.deltaTime);
			if(transform.position.y < (posicionInicial.y+0.1f)){
				inicio = true;
			}
		}
	}

	void OnCollisionEnter(Collision col){
		if (col.collider.tag == "Ball") {
			print ("pared");
			if (PlayerPrefs.GetInt ("r1n1") == 1 || PlayerPrefs.GetInt ("r1n1") == 2 || PlayerPrefs.GetInt ("r1n2") == 2 || PlayerPrefs.GetInt ("r1n3") == 2 || PlayerPrefs.GetInt ("r1n4") == 2) {
				PlayerPrefs.SetInt ("r1p", 1);
				print ("pared");
			} 
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Retos : MonoBehaviour {

	public GameObject bReto1, bReto2, bReto3, bReto4;
	[Header ("textos reto 1")]
	public Text tReto1;
	public Text tReto1Nivel1, tReto1Nivel2, tReto1Nivel3, tReto1Nivel4;
	[Header ("textos reto 2")]
	public Text tReto2;
	public Text tReto2Nivel1, tReto2Nivel2, tReto2Nivel3, tReto2Nivel4;
	[Header ("textos reto 3")]
	public Text tReto3;
	public Text tReto3Nivel1, tReto3Nivel2, tReto3Nivel3, tReto3Nivel4;
	[Header ("textos reto 4")]
	public Text tReto4;
	public Text tReto4Nivel1, tReto4Nivel2, tReto4Nivel3, tReto4Nivel4;


	public Color colorcito;

	private bool aumento1, resplandor1;
	private bool aumento2, resplandor2;
	private bool aumento3, resplandor3;
	private bool aumento4, resplandor4;
	private bool apagar = true;

	// Use this for initialization
	void Start () {
		tReto1Nivel1.GetComponent<Outline> ().enabled = false;
		tReto1Nivel2.GetComponent<Outline> ().enabled = false;
		tReto1Nivel3.GetComponent<Outline> ().enabled = false;
		tReto1Nivel4.GetComponent<Outline> ().enabled = false;
		tReto2Nivel1.GetComponent<Outline> ().enabled = false;
		tReto2Nivel2.GetComponent<Outline> ().enabled = false;
		tReto2Nivel3.GetComponent<Outline> ().enabled = false;
		tReto2Nivel4.GetComponent<Outline> ().enabled = false;
		tReto3Nivel1.GetComponent<Outline> ().enabled = false;
		tReto3Nivel2.GetComponent<Outline> ().enabled = false;
		tReto3Nivel3.GetComponent<Outline> ().enabled = false;
		tReto3Nivel4.GetComponent<Outline> ().enabled = false;
		tReto4Nivel1.GetComponent<Outline> ().enabled = false;
		tReto4Nivel2.GetComponent<Outline> ().enabled = false;
		tReto4Nivel3.GetComponent<Outline> ().enabled = false;
		tReto4Nivel4.GetComponent<Outline> ().enabled = false;

	}
	
	// Update is called once per frame
	void Update () {

		Resplandecer ();


		if ((PlayerPrefs.GetInt ("r1n1") == 2 || PlayerPrefs.GetInt ("r1n2") == 2 || PlayerPrefs.GetInt ("r1n3") == 2 || PlayerPrefs.GetInt ("r1n4") == 2) && !aumento1) {
			bReto1.GetComponent<Image> ().fillAmount += 0.25f;

			if (PlayerPrefs.GetInt ("r1n1") == 2) {
				
				tReto1Nivel1.text = "<color=#F5D519FF>" + tReto1Nivel1.text + "</color>"; 
				tReto1Nivel1.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto1Nivel1.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r1n2") == 2) {
				
				tReto1Nivel2.text = "<color=#F5D519FF>" + tReto1Nivel2.text + "</color>"; 
				tReto1Nivel2.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto1Nivel2.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r1n3") == 2) {
				
				tReto1Nivel3.text = "<color=#F5D519FF>" + tReto1Nivel3.text + "</color>"; 
				tReto1Nivel3.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto1Nivel3.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r1n4") == 2) {
				tReto1Nivel4.text = "<color=#F5D519FF>" + tReto1Nivel4.text + "</color>"; 
				tReto1Nivel4.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto1Nivel4.GetComponentsInChildren<Image> () [2].color = colorcito;
				tReto1.text = "<color=#00000000>" + tReto1Nivel4.text + "</color>" ;
			}
			aumento1 = true;
		}
		if ((PlayerPrefs.GetInt ("r2n1") == 2 || PlayerPrefs.GetInt ("r2n2") == 2 || PlayerPrefs.GetInt ("r2n3") == 2 || PlayerPrefs.GetInt ("r2n4") == 2) && !aumento2) {
			bReto2.GetComponent<Image> ().fillAmount += 0.25f;

			if (PlayerPrefs.GetInt ("r2n1") == 2) {
				
				tReto2Nivel1.text = "<color=#F5D519FF>" + tReto2Nivel1.text + "</color>"; 
				tReto2Nivel1.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto2Nivel1.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r2n2") == 2) {
				
				tReto2Nivel2.text = "<color=#F5D519FF>" + tReto2Nivel2.text + "</color>"; 
				tReto2Nivel2.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto2Nivel2.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r2n3") == 2) {
				
				tReto2Nivel3.text = "<color=#F5D519FF>" + tReto2Nivel3.text + "</color>"; 
				tReto2Nivel3.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto2Nivel3.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r2n4") == 2) {
				tReto2Nivel4.text = "<color=#F5D519FF>" + tReto2Nivel4.text + "</color>"; 
				tReto2Nivel4.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto2Nivel4.GetComponentsInChildren<Image> () [2].color = colorcito;
				tReto2.text = "<color=#00000000>" + tReto1Nivel4.text + "</color>" ;
			}
			aumento2 = true;
		}
		if ((PlayerPrefs.GetInt ("r3n1") == 2 || PlayerPrefs.GetInt ("r3n2") == 2 || PlayerPrefs.GetInt ("r3n3") == 2 || PlayerPrefs.GetInt ("r3n4") == 2) && !aumento3) {
			bReto3.GetComponent<Image> ().fillAmount += 0.25f;

			if (PlayerPrefs.GetInt ("r3n1") == 2) {
				
				tReto3Nivel1.text = "<color=#F5D519FF>" + tReto3Nivel1.text + "</color>"; 
				tReto3Nivel1.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto3Nivel1.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r3n2") == 2) {
				
				tReto3Nivel2.text = "<color=#F5D519FF>" + tReto3Nivel2.text + "</color>"; 
				tReto3Nivel2.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto3Nivel2.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r3n3") == 2) {
				
				tReto3Nivel3.text = "<color=#F5D519FF>" + tReto3Nivel3.text + "</color>"; 
				tReto3Nivel3.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto3Nivel3.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r3n4") == 2) {
				tReto3Nivel4.text = "<color=#F5D519FF>" + tReto3Nivel4.text + "</color>"; 
				tReto3Nivel4.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto3Nivel4.GetComponentsInChildren<Image> () [2].color = colorcito;
				tReto3.text = "<color=#00000000>" + tReto1Nivel4.text + "</color>" ;
			}
			aumento3 = true;
		}
		if ((PlayerPrefs.GetInt ("r4n1") == 2 || PlayerPrefs.GetInt ("r4n2") == 2 || PlayerPrefs.GetInt ("r4n3") == 2 || PlayerPrefs.GetInt ("r4n4") == 2) && !aumento4) {

			bReto4.GetComponent<Image> ().fillAmount += 0.25f;
			if (PlayerPrefs.GetInt ("r4n1") == 2) {
				
				tReto4Nivel1.text = "<color=#F5D519FF>" + tReto4Nivel1.text + "</color>"; 
				tReto4Nivel1.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto4Nivel1.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r4n2") == 2) {
				
				tReto4Nivel2.text = "<color=#F5D519FF>" + tReto4Nivel2.text + "</color>"; 
				tReto4Nivel2.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto4Nivel2.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r4n3") == 2) {
				
				tReto4Nivel3.text = "<color=#F5D519FF>" + tReto4Nivel3.text + "</color>"; 
				tReto4Nivel3.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto4Nivel3.GetComponentsInChildren<Image> () [2].color = colorcito;
			}
			if (PlayerPrefs.GetInt ("r4n4") == 2) {
				tReto4Nivel4.text = "<color=#F5D519FF>" + tReto4Nivel4.text + "</color>"; 
				tReto4Nivel4.GetComponentsInChildren<Image> () [0].color = colorcito;
				tReto4Nivel4.GetComponentsInChildren<Image> () [2].color = colorcito;
				tReto4.text = "<color=#00000000>" + tReto1Nivel4.text + "</color>" ;
			}
			aumento4 = true;
		}
	}




	IEnumerator Resplandor(Text text)
    {
		if (apagar) {
			//print ("apagar");
			text.color = Color.Lerp (text.color, new Color (colorcito.r, colorcito.g, colorcito.b, 0f), 3f * Time.deltaTime);
			yield return new WaitForSeconds (1);
			apagar = false;
		} else {
			//print ("prender");
			text.color = Color.Lerp (text.color, new Color (colorcito.r, colorcito.g, colorcito.b, 1f), 3f * Time.deltaTime);
			yield return new WaitForSeconds (1);
			apagar = true;
		}
	}

	IEnumerator ResplandorLinea(Image image){
		if (apagar) {
			//print ("apagar");
			image.color = Color.Lerp (image.color, new Color (colorcito.r, colorcito.g, colorcito.b, 0f), 3f * Time.deltaTime);
			yield return new WaitForSeconds (1);
			apagar = false;
		} else {
			//print ("prender");
			image.color = Color.Lerp (image.color, new Color (colorcito.r, colorcito.g, colorcito.b, 1f), 3f * Time.deltaTime);
			yield return new WaitForSeconds (1);
			apagar = true;
		}
	}

	private void Resplandecer(){
		
		if ((PlayerPrefs.GetInt ("r1n1") == 1 || PlayerPrefs.GetInt ("r1n1") == 0) && PlayerPrefs.GetInt ("r1n1") != 2) {

			StartCoroutine (Resplandor (tReto1Nivel1));
			StartCoroutine (ResplandorLinea (tReto1Nivel1.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r1n2") == 0 || PlayerPrefs.GetInt ("r1n2") == 1)&&  PlayerPrefs.GetInt ("r1n1") == 2) {
			StartCoroutine (Resplandor (tReto1Nivel2));
			StartCoroutine (ResplandorLinea (tReto1Nivel2.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r1n3") == 0 || PlayerPrefs.GetInt ("r1n3") == 1)&&  PlayerPrefs.GetInt ("r1n2") == 2) {
			StartCoroutine (Resplandor (tReto1Nivel3));
			StartCoroutine (ResplandorLinea (tReto1Nivel3.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r1n4") == 0 || PlayerPrefs.GetInt ("r1n4") == 1)&&  PlayerPrefs.GetInt ("r1n3") == 2) {
			StartCoroutine (Resplandor (tReto1Nivel4));
			StartCoroutine (ResplandorLinea (tReto1Nivel4.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r2n1") == 1 || PlayerPrefs.GetInt ("r2n1") == 0) && PlayerPrefs.GetInt ("r2n1") != 2) {
			StartCoroutine (Resplandor (tReto2Nivel1));
			StartCoroutine (ResplandorLinea (tReto2Nivel1.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r2n2") == 0 || PlayerPrefs.GetInt ("r2n2") == 1)&&  PlayerPrefs.GetInt ("r2n1") == 2) {
			StartCoroutine (Resplandor (tReto2Nivel2));
			StartCoroutine (ResplandorLinea (tReto2Nivel2.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r2n3") == 0 || PlayerPrefs.GetInt ("r2n3") == 1)&&  PlayerPrefs.GetInt ("r2n2") == 2) {
			StartCoroutine (Resplandor (tReto2Nivel3));
			StartCoroutine (ResplandorLinea (tReto2Nivel3.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r2n4") == 0 || PlayerPrefs.GetInt ("r2n4") == 1)&&  PlayerPrefs.GetInt ("r2n3") == 2) {
			StartCoroutine (Resplandor (tReto2Nivel4));
			StartCoroutine (ResplandorLinea (tReto2Nivel4.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r3n1") == 1 || PlayerPrefs.GetInt ("r3n1") == 0) &&  PlayerPrefs.GetInt ("r3n1") != 2) {
			StartCoroutine (Resplandor (tReto3Nivel1));
			StartCoroutine (ResplandorLinea (tReto3Nivel1.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r3n2") == 0 || PlayerPrefs.GetInt ("r3n2") == 1)&&  PlayerPrefs.GetInt ("r3n1") == 2) {
			StartCoroutine (Resplandor (tReto3Nivel2));
			StartCoroutine (ResplandorLinea (tReto3Nivel2.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r3n3") == 0 || PlayerPrefs.GetInt ("r3n3") == 1)&&  PlayerPrefs.GetInt ("r3n2") == 2) {
			StartCoroutine (Resplandor (tReto3Nivel3));
			StartCoroutine (ResplandorLinea (tReto3Nivel3.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r3n4") == 0 || PlayerPrefs.GetInt ("r3n4") == 1)&&  PlayerPrefs.GetInt ("r3n3") == 2) {
			StartCoroutine (Resplandor (tReto3Nivel4));
			StartCoroutine (ResplandorLinea (tReto3Nivel4.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r4n1") == 1 || PlayerPrefs.GetInt ("r4n1") == 0) &&  PlayerPrefs.GetInt ("r4n1") != 2) {
			StartCoroutine (Resplandor (tReto4Nivel1));
			StartCoroutine (ResplandorLinea (tReto4Nivel1.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r4n2") == 0 || PlayerPrefs.GetInt ("r4n2") == 1)&&  PlayerPrefs.GetInt ("r4n1") == 2) {
			StartCoroutine (Resplandor (tReto4Nivel2));
			StartCoroutine (ResplandorLinea (tReto4Nivel2.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r4n3") == 0 || PlayerPrefs.GetInt ("r4n3") == 1)&&  PlayerPrefs.GetInt ("r4n2") == 2) {
			StartCoroutine (Resplandor (tReto4Nivel3));
			StartCoroutine (ResplandorLinea (tReto4Nivel3.GetComponentsInChildren<Image> () [0]));
		}
		if ((PlayerPrefs.GetInt ("r4n4") == 0 || PlayerPrefs.GetInt ("r4n4") == 1)&&  PlayerPrefs.GetInt ("r4n3") == 2) {
			StartCoroutine (Resplandor (tReto4Nivel4));
			StartCoroutine (ResplandorLinea (tReto4Nivel4.GetComponentsInChildren<Image> () [0]));
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class uniformesGame : MonoBehaviour 
{
	private int nivel = 1;
	public Texture uniforme1;
	public Texture uniforme2;
	public Texture uniforme3;
	public Renderer rend;

	private bool unaVez1=false;
	private bool unaVez2=false;
	private bool unaVez3=false;

	// Use this for initialization
	void Start () 
	{
		nivel = 1;
		rend = GetComponent<Renderer>();

		unaVez1=false;
		unaVez2=false;
		unaVez3=false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		nivel = PlayerPrefs.GetInt ("level");

		if (nivel == 1 && unaVez1==false) 
		{
			rend.material.mainTexture = uniforme1;
//			unaVez1 = true;
		}
		else if (nivel == 2 && unaVez2==false) 
		{
			rend.material.mainTexture = uniforme2;
//			unaVez2 = true;
		}
		else if (nivel == 3 && unaVez3==false) 
		{
			rend.material.mainTexture = uniforme3;
//			unaVez3 = true;
		}
	}
}

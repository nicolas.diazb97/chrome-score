﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoginScreen : MonoBehaviour
{
    public UnityEvent OnSignuP;
    // Start is called before the first frame update
    void Start()
    {
        DatabaseManager.main.OnSignUp.AddListener((Firebase.Auth.FirebaseUser newUser) =>
        {
            OnSignuP.Invoke();
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}

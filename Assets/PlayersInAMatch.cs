using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayersInAMatch 
{
    public string[] players = new string[2];
    public PlayersInAMatch(string p1, string p2)
    {
        players[0] = p1;
        players[1] = p2;
    }
}

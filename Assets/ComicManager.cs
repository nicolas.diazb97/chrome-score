﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComicManager : MonoBehaviour
{
    public RectTransform[] comics;
    ScrollRect manager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void SetActiveComic(int _comic)
    {
        GetComponent<ScrollRect>().content = comics[_comic];
    }
}

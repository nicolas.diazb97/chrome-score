using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchManager : Singleton<MatchManager>
{
    public int currShoot = 0;
    public MatchData data;
    public string currPlayerNumber;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public string GetResultByIndex(int resultIndex)
    {
        if (currPlayerNumber == "P1")
        {
            return data.resultsP1[resultIndex-1];
        }
        else
        {
            return data.resultsP2[resultIndex-1];
        }
    }
    public void SetNewScoreByIndex(int resultIndex, int score)
    {
        if (currPlayerNumber == "P1")
        {
            data.resultsP1[resultIndex-1] = score.ToString();
        }
        else
        {
            data.resultsP2[resultIndex-1] = score.ToString();
        }
    }
}

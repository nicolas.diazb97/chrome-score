﻿using UnityEngine;
using System.Collections;

public class ValidEmailRegister : MonoBehaviour {

	private string getUrlActive = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/DownloadUserNivelClub.php";

	public GameObject verificacionPanel, verificacionFail, verificacionSuccess;
	public GameObject panelRegister;


	public void verificationEmailProcess ( ){
		this.gameObject.SetActive( true );
		StartCoroutine( validVerification( ) );
	}


	public void backVerification ( ){
		//PlayerPrefs.DeleteAll( );
		panelRegister.SetActive(true);
		verificacionPanel.SetActive( false );
	}

	IEnumerator validVerification( ){
		string urlString = getUrlActive + "?opc=IsVerficationEmail&userID="+ DataApp.main.GetMyID( ); 
		print("recibiendo respuesta de verificacion: "+urlString);
		WWW idReturnv = new WWW (urlString);
		yield return idReturnv;
		if( string.IsNullOrEmpty( idReturnv.error ) ){
			if( idReturnv.text == "0" ){
//				verificacionPanel.SetActive( true );
				verificacionFail.SetActive( true );
			}else{
				verificacionSuccess.SetActive( true );
				verificacionPanel.SetActive( false );
			}
		}
	}


	public void postverificationEmailProcess ( ){
		this.gameObject.SetActive( true );
		StartCoroutine( postvalidVerification( ) );
	}

	IEnumerator postvalidVerification( ){
		string urlString = getUrlActive + "?opc=IsVerficationEmail&userID="+ DataApp.main.GetMyID( ); 
		print("recibiendo respuesta de verificacion: "+urlString);
		WWW idReturnv = new WWW (urlString);
		yield return idReturnv;
		if( string.IsNullOrEmpty( idReturnv.error ) ){
			if( idReturnv.text == "0" ){
				verificacionPanel.SetActive( true );
			}
		}
	}

}

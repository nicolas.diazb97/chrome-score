﻿///////////////////////////////////////////////////////////////////////////////
// Copyright 2016 (C) Wise In Media SAS.
// File:             Votes.cs
// Created on:       21-01-2016
// Last Changed:    21-01-2016
// Author:           Fabian
////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


/**
 * @brief Clase que define los datos guardados de la app * .
 */
public class DataApp : Singleton<DataApp> {
	

	public void DeleteVar( string _name){
		PlayerPrefs.DeleteKey (_name);
	}

	public void DeleteAll(){
		PlayerPrefs.DeleteAll();
	}


// DATA USER	
	public void SetActivedHNC( int act){
		PlayerPrefs.SetInt ("ImHnc", act);
	}
	public int GetActivedHNC (){
		return PlayerPrefs.HasKey("ImHnc") ? PlayerPrefs.GetInt ("ImHnc"): 0;
	}

	public bool IsHinchaNivelClub(){
		return PlayerPrefs.HasKey("ImHnc");
	}

	public void SetEmail( string email){
		PlayerPrefs.SetString ("MyEmail",email);
	}

	public string GetEmail( ){
		return PlayerPrefs.GetString ("MyEmail");
	}

//VOTE ID 

	public void SetMyID (string id){
		PlayerPrefs.SetInt ("MyID", int.Parse(id));
	}

	public int GetMyID (){
		// print("MY ID ACTUAL "+(PlayerPrefs.HasKey("MyID") ? PlayerPrefs.GetInt ("MyID"): 0));
		return PlayerPrefs.HasKey("MyID") ? PlayerPrefs.GetInt ("MyID"): 0;
	}

	public bool IsRegistered(){
		return PlayerPrefs.HasKey("MyID");
	}

	// TASA REFRESCO CHAT 

	public void SetRateRefreshChat (string id){
		PlayerPrefs.SetInt ("RateRefreshChat", int.Parse(id));
	}

	public int GetRateRefreshChat (){
		// print("MY ID ACTUAL "+(PlayerPrefs.HasKey("RateRefreshChat") ? PlayerPrefs.GetInt ("RateRefreshChat"): 0));
		return PlayerPrefs.HasKey("RateRefreshChat") ? PlayerPrefs.GetInt ("RateRefreshChat"): 0;
	}

	public bool ExistRateRefreshChat(){
		return PlayerPrefs.HasKey("RateRefreshChat");
	}




//JURAMENTO
	public void SetMyOATH (string id){
		PlayerPrefs.SetInt ("MyOATH", int.Parse(id));
	}

	public int GetMyOATH (){
		// print("MY ID ACTUAL "+(PlayerPrefs.HasKey("MyOATH") ? PlayerPrefs.GetInt ("MyOATH"): 0));
		return PlayerPrefs.HasKey("MyOATH") ? PlayerPrefs.GetInt ("MyOATH"): 0;
	}

	public bool ExistMyOATH(){
		return PlayerPrefs.HasKey("MyOATH");
	}


//link tiro amarillo
	public void SetLinkStoreTiroAmarillo (string link){
		PlayerPrefs.SetString ("LinkStoreTiroAmarillo", link);
	}

	public string GetLinkStoreTiroAmarillo (){
		// print("MY ID ACTUAL "+(PlayerPrefs.HasKey("LinkStoreTiroAmarillo") ? PlayerPrefs.GetInt ("LinkStoreTiroAmarillo"): 0));
		return  PlayerPrefs.GetString ("LinkStoreTiroAmarillo");
	}

	public bool ExistLinkStoreTiroAmarillo(){
		return PlayerPrefs.HasKey("LinkStoreTiroAmarillo");
	}


// Link de notificaciones
	public void SetAddressOfNotificationServer (string link){
		PlayerPrefs.SetString ("AddressOfNotificationServer", link);
	}

	public string GetAddressOfNotificationServer (){
		// print("MY ID ACTUAL "+(PlayerPrefs.HasKey("AddressOfNotificationServer") ? PlayerPrefs.GetInt ("AddressOfNotificationServer"): 0));
		return  PlayerPrefs.GetString ("AddressOfNotificationServer");
	}

	public bool ExistAddressOfNotificationServer(){
		return PlayerPrefs.HasKey("AddressOfNotificationServer");
	}


//VOTE ITEM 

	public string GetTotalVotesItems (){
		return PlayerPrefs.GetString ("TotalVotesItems");
	}

	public void SetTotalVotesItems (string _tv){
		PlayerPrefs.SetString ("TotalVotesItems", _tv);
	}
	
	public bool ExistTotalVotesItems (){
		return PlayerPrefs.HasKey("TotalVotesItems");
	}




	public void SetDaysTabs (int ds){
		PlayerPrefs.SetInt ("DaysTabs", ds);
	}

	public int GetDaysTabs (){
		return PlayerPrefs.HasKey("DaysTabs") ? PlayerPrefs.GetInt ("DaysTabs"): 0;
	}


//VOTE DATE CIERRE  

	public string GetDTCierreItems (){
		return PlayerPrefs.GetString ("DTCierreItems");
	}

	public void SetDTCierreItems (string _dt){
		PlayerPrefs.SetString ("DTCierreItems", _dt);
	}
	
	public bool ExistDTCierreItems (){
		return PlayerPrefs.HasKey("DTCierreItems");
	}


//Months to show in Girls Year  

	public string GetMonthToShowInGirlsYear (){
		return PlayerPrefs.GetString ("MonthToShowInGirlsYear");
	}

	public void SetMonthToShowInGirlsYear (string _s){
		PlayerPrefs.SetString ("MonthToShowInGirlsYear", _s);
	}
	
	public bool ExistMonthToShowInGirlsYear (){
		return PlayerPrefs.HasKey("MonthToShowInGirlsYear");
	}

//VOTE DATE BestP  

	public string GetEndVoteBestPDate (){
		return PlayerPrefs.GetString ("EndVoteBestPDate");
	}

	public void SetEndVoteBestPDate (string _dt){
		PlayerPrefs.SetString ("EndVoteBestPDate", _dt);
	}
	
	public bool ExistEndVoteBestPDate (){
		return PlayerPrefs.HasKey("EndVoteBestPDate");
	}



//VOTE DATE APERTURA  

	public string GetMatchDate (){
		return PlayerPrefs.GetString ("MatchDate");
	}

	public void SetMatchDate (string _dt){
		PlayerPrefs.SetString ("MatchDate", _dt);
	}
	
	public bool ExistMatchDate (){
		return PlayerPrefs.HasKey("MatchDate");
	}

	//VOTE DATE MONUMENTALES  

	public string GetGirlsDate (){
		return PlayerPrefs.GetString ("GirlsDate");
	}

	public void SetGirlsDate (string _dt){
		PlayerPrefs.SetString ("GirlsDate", _dt);
	}
	
	public bool ExistGirlsDate (){
		return PlayerPrefs.HasKey("GirlsDate");
	}


//VOTE BESTP 


	public bool ExistVoteGirl(string monthYear ){
		return PlayerPrefs.HasKey("VoteGirl"+monthYear);
	}

	public bool GetVoteGirl (string monthYear){
		return   PlayerPrefs.GetInt ("VoteGirl"+monthYear) ==1?true:false;
	}
	
	public void SetVoteGirl (  bool val, string monthYear ){
		PlayerPrefs.SetInt ("VoteGirl"+monthYear, val?1:0);
	}


//BARCELONA GANADOR PARTIDO

	public bool ExistEnableByWinner( ){
		return PlayerPrefs.HasKey("EnableByWinner");
	}

	public bool IsEnableByWinner (){
		return   PlayerPrefs.GetInt ("EnableByWinner") ==1?true:false;
	}
	
	public void SetEnableByWinner (  bool val ){
		PlayerPrefs.SetInt ("EnableByWinner", val?1:0);
	}


// DATE  Enemy team
// 
	public string GetEnemyTeam (){
		return PlayerPrefs.GetString ("EnemyTeam");
	}

	public void SetEnemyTeam (string _dt){
		PlayerPrefs.SetString ("EnemyTeam", _dt);
	}
	
	public bool ExistEnemyTeam (){
		return PlayerPrefs.HasKey("EnemyTeam");
	}

// DATE  LAST MATCH

	public string GetDateLastMatch (){
		return PlayerPrefs.GetString ("DateLastMatch");
	}

	public void SetDateLastMatch (string _dt){
		PlayerPrefs.SetString ("DateLastMatch", _dt);
	}
	
	public bool ExistDateLastMatch (){
		return PlayerPrefs.HasKey("DateLastMatch");
	}


//VOTE FEATURE  

	public bool ExistPlayerValueOfFeature( int id, int fet){
		return PlayerPrefs.HasKey("PlayerValueOfFeature"+id+fet);
	}

	public int GetPlayerValueOfFeature (int id, int fet){
		return   PlayerPrefs.GetInt ("PlayerValueOfFeature"+id+fet) ;
	}
	
	public void SetPlayerValueOfFeature ( int id, int fet ,int val){
		PlayerPrefs.SetInt ("PlayerValueOfFeature"+id+fet,val);
	}




//VOTE FEATURE AVERAGE 

	public bool ExistPlayerAverageOfFeature( int id, int fet){
		return PlayerPrefs.HasKey("PlayerAverageOfFeature"+id+fet);
	}

	public float GetPlayerAverageOfFeature (int id, int fet){
		return   PlayerPrefs.GetFloat ("PlayerAverageOfFeature"+id+fet) ;
	}
	
	public void SetPlayerAverageOfFeature ( int id, int fet ,float val){
		PlayerPrefs.SetFloat ("PlayerAverageOfFeature"+id+fet,val);
	}


//VOTE POPULARITY  

	public bool ExistVotePopularityToPlayer( int id){
		return PlayerPrefs.HasKey("VotePopularityToPlayer"+id);
	}

	public bool GetVotePopularityToPlayer (int id){
		return   PlayerPrefs.GetInt ("VotePopularityToPlayer"+id) ==1?true:false;
	}
	
	public void SetVotePopularityToPlayer ( int id, bool val ){
		PlayerPrefs.SetInt ("VotePopularityToPlayer"+id, val?1:0);
	}

//VOTE BestP Month  

	public bool ExistRankingMonthPlayer( int id){
		return PlayerPrefs.HasKey("RankingMonthPlayer"+id);
	}

	public int GetRankingMonthPlayer (int id){
		return   PlayerPrefs.GetInt ("RankingMonthPlayer"+id) ;
	}
	
	public void SetRankingMonthPlayer ( int id, int val ){
		PlayerPrefs.SetInt ("RankingMonthPlayer"+id, val);
	}


//VOTE POPULARITY GLOBAL POSITIVES

	public bool ExistGlobalPositivesPopularityToPlayer( int id){
		return PlayerPrefs.HasKey("GlobalPositivesPopularityToPlayer"+id);
	}

	public int GetGlobalPositivesPopularityToPlayer (int id){
		return   PlayerPrefs.GetInt ("GlobalPositivesPopularityToPlayer"+id) ;
	}
	
	public void SetGlobalPositivesPopularityToPlayer ( int id, int val ){
		PlayerPrefs.SetInt ("GlobalPositivesPopularityToPlayer"+id, val);
	}


	//VOTE POPULARITY GLOBAL TOTAL


	public bool ExistGlobalTotalPopularityToPlayer( int id){
		return PlayerPrefs.HasKey("GlobalTotalPopularityToPlayer"+id);
	}

	public int GetGlobalTotalPopularityToPlayer (int id){
		return   PlayerPrefs.GetInt ("GlobalTotalPopularityToPlayer"+id) ;
	}
	
	public void SetGlobalTotalPopularityToPlayer ( int id, int val ){
		PlayerPrefs.SetInt ("GlobalTotalPopularityToPlayer"+id, val);
	}

//VOTE POPULARITY TOTAL 


	public bool ExistTotalVotePopularityToPlayer( int id){
		return PlayerPrefs.HasKey("TotalVotePopularityToPlayer"+id);
	}

	public int GetTotalVotePopularityToPlayer (int id){
		return   PlayerPrefs.GetInt ("TotalVotePopularityToPlayer"+id) ;
	}
	
	public void SetTotalVotePopularityToPlayer ( int id, int val ){
		PlayerPrefs.SetInt ("TotalVotePopularityToPlayer"+id, val);
	}



//VOTE POPULARITY POSITIVES 


	public bool ExistPositivesVotePopularityToPlayer( int id){
		return PlayerPrefs.HasKey("PositivesVotePopularityToPlayer"+id);
	}

	public int GetPositivesVotePopularityToPlayer (int id){
		return   PlayerPrefs.GetInt ("PositivesVotePopularityToPlayer"+id) ;
	}
	
	public void SetPositivesVotePopularityToPlayer ( int id, int val ){
		PlayerPrefs.SetInt ("PositivesVotePopularityToPlayer"+id, val);
	}




//VOTE BESTP TOTAL 


	public bool ExistTotalVoteToBestP( int id){
		return PlayerPrefs.HasKey("TotalVoteToBestP"+id);
	}

	public int GetTotalVoteToBestP (int id){
		return   PlayerPrefs.GetInt ("TotalVoteToBestP"+id) ;
	}
	
	public void SetTotalVoteToBestP ( int id, int val ){
		PlayerPrefs.SetInt ("TotalVoteToBestP"+id, val);
	}



//VOTE BESTP 

	public bool ExistPositivesVoteToBestP( int id){
		return PlayerPrefs.HasKey("PositivesVoteToBestP"+id);
	}

	public int GetPositivesVoteToBestP (int id){
		return   PlayerPrefs.GetInt ("PositivesVoteToBestP"+id) ;
	}
	
	public void SetPositivesVoteToBestP ( int id, int val ){
		PlayerPrefs.SetInt ("PositivesVoteToBestP"+id, val);
	}




//VOTE BESTP 

	public bool ExistVoteToBestP( int id){
		return PlayerPrefs.HasKey("VoteToBestP"+id);
	}

	public bool GetVoteToBestP (int id){
		return   PlayerPrefs.GetInt ("VoteToBestP"+id) ==1?true:false;
	}
	
	public void SetVoteToBestP ( int id, bool val ){
		PlayerPrefs.SetInt ("VoteToBestP"+id, val?1:0);
	}


//VOTE ITEM

	public bool ExistMyItemVote( int id){
		return PlayerPrefs.HasKey("MyItemVote"+id);
	}

	public bool GetMyItemVote (int id){
		return   PlayerPrefs.GetInt ("MyItemVote"+id) ==1?true:false;
	}
	
	public void SetMyItemVote (int id, bool val){
		PlayerPrefs.SetInt ("MyItemVote"+id, val?1:0);
	}

	public string GetViewChatInstructions (){//new
		return PlayerPrefs.GetString ("ViewChatInstructions");//new
	}//new
	//new
	public void SetViewChatInstructions (string _tv){//new
		PlayerPrefs.SetString ("ViewChatInstructions", _tv);//new
	}//new
	//new
	public bool AlreadyViewChatInstructions (){//new
		return PlayerPrefs.HasKey("ViewChatInstructions");//new
	}//new
}

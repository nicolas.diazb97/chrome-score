﻿using UnityEngine;
using System.Collections;

public class analytics : MonoBehaviour {
	//url dode se comunica con php para adicionar el click del menu
	private string getUrlPos = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/Analytics/PostSelectJugar.php";
	private int itemID = 0;
	private int userID = 0;
	public string pos="";

	public void SelectIntem(int temp ) 
	{
		userID = DataApp.main.GetMyID();
		//print (userID);
		itemID = temp;
		StartCoroutine ("sendItem");
	}

	private IEnumerator sendItem() 
	{
		WWWForm form = new WWWForm ();
		form.AddField ("userID",userID);
		form.AddField ("itemID",itemID);
		WWW www = new WWW (getUrlPos,form);
		yield return www;
		pos = www.text;
		itemID = 0;
		userID = 0;
		pos="";
	}
	
}

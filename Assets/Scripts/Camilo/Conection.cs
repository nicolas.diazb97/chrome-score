﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Conection : MonoBehaviour 
{
	//url dode se comunica con php para leer los datos
	private string readURLMail = "http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/MostrarMail.php";
	string uRLFechaApertura = "http://2waysports.com/2waysports/Ecuador/Barcelona/Fecha/getFechaApetura.php";
	string reloadPass =  "http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/RecuperarContrasena/SendEmailPass.php";
	

	//variable para asociar el nombre al input field 
	public InputField mailInput;
	//Variable con el mail del usuario
	private string userMail = "";
	//Variable que recibe la respuesta del php para saber si tiene o no ese mail registrado
	private string answerMail = "";
	// variable del paswoord de usuarios nc
	
	//muestra un texto avisando que el mail no existe
	public GameObject warningMessageRegister;
	public Text warningText;
	RegexUtilities util = new RegexUtilities();
	public GameObject checkInternet;
	public GameObject CheckPassword;
	public Registration register;
	public static bool isLog;
	public static string pass = "";

	public GameObject EmailGO;//Eduardo

	public static string tempId;
	public static string tempEmail;
	//funcion cuando oprimen el boton
	public void postRegistration() {
		StartCoroutine ("confirmInternet");
	}

	public void SetEmailFromFB(string _s){
		mailInput.text = _s;
	}

	private IEnumerator confirmInternet() {
		WWW getFechaApertura = new WWW (uRLFechaApertura+ "?" + "id=" +1);
		yield return getFechaApertura;
		if(string.IsNullOrEmpty(getFechaApertura.text)){
			checkInternet.SetActive(true);
		}else{
			DataApp2.main.EnableLoading();
			//InternetManager.main.loading.SetActive(true);
			userMail = mailInput.text;
			
			if(util.IsValidEmail(userMail) ){
				warningText.text = " ";
				StartCoroutine ("ConfirmMail");
			}else{
				DataApp2.main.DisableLoading();
				warningMessageRegister.SetActive(true);//Nicolas
				warningText.text = "Por favor ingresa un email valido.";
			}
		}
	}

	//funcion que pregunta si el mail puesto en el input text ya esta registrado
	private IEnumerator ConfirmMail()
	{
		userMail = mailInput.text;
		string urlString = readURLMail + "?" + "correo=" + WWW.EscapeURL (userMail); 
		urlString = urlString.Trim();
		WWW getMail = new WWW (urlString);
		yield return getMail;
		print(getMail); 
		print ("MyId: " +getMail.text);
		tempId = getMail.text;
		answerMail = getMail.text;
		answerMail = answerMail.Trim();
		//si no existe el mail notifique al usuario
		if (answerMail.Equals ("") == true) {
			warningMessageRegister.SetActive(true);//Nicolas
			warningText.text = "El mail no existe por favor registrate";
			DataApp2.main.DisableLoading();
		} 
		else {
			StartCoroutine( TransactionalProcess.thisTransaction.ShowActiveNivelC( ) );
			warningText.text = "";
			DataApp.main.SetEmail(userMail);
			Debug.Log(DataApp.main.GetEmail());
			yield return new WaitForSeconds(1);

			EmailGO.SetActive(false);//Eduardo

			if(TransactionalProcess.isActive == 0){
				DataApp2.main.DisableLoading();
				register.DoneRegister(answerMail);
				isLog = true;
			}else{
				DataApp2.main.DisableLoading();
				CheckPassword.SetActive(true);
			}
		}
		 
	}

	public void sendPasstoEmail ( ){
		StartCoroutine(RandomGenerationCode( ));
	}




	public void SendValidPassword( InputField pss ){
		if ( pass != pss.text ||  pss.text == "" ) {
			warningMessageRegister.SetActive(true);//Nicolas
			warningText.text = "Contraseña Incorrecta";
			DataApp2.main.DisableLoading();
		} else {
			register.DoneRegister(answerMail);
			PersonalizacionPersonaje.ThisCustom.GuardarCambios();
			warningMessageRegister.SetActive(true);//Nicolas
			warningText.text = "Ingreso Exitoso";
			isLog = true;
		}
	}

	public IEnumerator RandomGenerationCode( ){ 
		string _newpass = "";
		for ( int i =0; i<9;i++){
			int type = Random.Range(0,2);
			if(type == 1){				
				_newpass += Random.Range(1,9);
			}else{
				char c = (char)Random.Range(65,90); _newpass += c;
			}
		}
		string constr = reloadPass +"?to="+ mailInput.text +"&newpass="+ _newpass;
		WWW newPass = new WWW(constr);
		yield return newPass;
		if(string.IsNullOrEmpty( newPass.error) ){
			warningMessageRegister.SetActive(true);//Nicolas
			warningText.text = "Se ha enviado una nueva contraseña a tu Correo Electronico, Revisalo en unos minutos.";
			DataApp2.main.DisableLoading();
		}else{
			warningMessageRegister.SetActive(true);//Nicolas
			warningText.text = "No se ha podido cambiar la contraseña.";
			DataApp2.main.DisableLoading();
		}
	}
	



}
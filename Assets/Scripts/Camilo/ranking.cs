﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ranking : MonoBehaviour
{
	//variables para la comunicacion PHP
	private string getUrlName = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarTablaNombre.php";
	private string getUrlPts = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarTablaPuntaje.php";

	public GameObject GirarPantalla;
	//varible de recibir los datos
	public Text resultName;
	public Text resultPts;
	public Text InfoInternetFail;
	public GameObject RankingPanel;
	public GameObject checkInternet;
	public GameObject loading,PanelRegsitrar;
	public GameObject PanelTransacionalInit;
	public bool hasInternet;
	WWW  getVotosTotalCamisetas;
	//internet


	public void LoadTable(bool enable)
	{
		StartCoroutine ("CheckInternet");	
	}

	//internet
	private IEnumerator CheckInternet() {
		loading.SetActive(true);
		getVotosTotalCamisetas = new WWW ("http://2waysports.com/2waysports/Ecuador/Barcelona/Camiseta/getVotosTotalCamisetas.php");
		yield return getVotosTotalCamisetas;
		hasInternet = !string.IsNullOrEmpty(getVotosTotalCamisetas.text);
		loading.SetActive(false);
		if(hasInternet && DataApp.main.IsRegistered()){
			GirarPantalla.SetActive(true);
			yield return new WaitForSeconds(2);
			GirarPantalla.SetActive(false);
			if(TransactionalProcess.isActive == 1){
				StartCoroutine("showTable");
				RankingPanel.SetActive(true);
			}else{
				PanelTransacionalInit.SetActive(true);
			}
		}else{
			if(!hasInternet){
				InfoInternetFail.text = "Error en la conexión, Intenta de nuevo.";
			//	Debug.Log("no hay internet");
				checkInternet.SetActive(true);
			}else if(!DataApp.main.IsRegistered()) {
			//	InfoInternetFail.text = "Por favor registrarse para poder jugar.";
			//	Debug.Log("no esta registado");
				PanelRegsitrar.SetActive(true);
			}
			RankingPanel.SetActive(false);
		}
	}

	private IEnumerator showTable()
	{
		loading.SetActive(true);
		WWW getName = new WWW (getUrlName);
		yield return getName;
		if (getName.isDone) 
		{
			print (getName.text);
			resultName.text = getName.text;
		}

		WWW getPts = new WWW (getUrlPts);
		yield return getPts;
		if (getPts.isDone) 
		{
			resultPts.text = getPts.text;
			loading.SetActive(false);
		}

	}


	void OnDisable() 
	{
		ClearScreen();
	}

	void ClearScreen()
	{		
		//print ("sisisisisi2222222222222");
		resultName.text = "";
		resultPts.text = "";
	}

}

﻿	///////////////////////////////////////////////////////////////////////////////
// Copyright 2016 (C) Wise In Media SAS.
// File:             Registration.cs
// Created on:       21-01-2016
// Last Changed:    21-01-2016
// Author:           Camilo
////////////////////////////////////////////////////////////////////////////

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections.Generic;


[System.Serializable]
public class GameObjectEvent : UnityEvent<GameObject> {} //empty class; just needs to exist
 
public class Registration : MonoBehaviour {
	public UnityEvent onRegisterDone = new UnityEvent(); 
	// public GameObjectEvent additionalEvent = new GameObjectEvent(); 
	//url dode se comunica con php para guardar datos
	private string postURL = "http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/PostNombreCorreoCelInsert.php";
	private string readURLMail = "http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/MostrarMail.php";
	string uRLFechaApertura = "http://2waysports.com/2waysports/Ecuador/Barcelona/Fecha/getFechaApetura.php";
	public static int isActive;
	private string getUrlActive = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/DownloadUserNivelClub.php";
	//variable para asociar el nombre al input field 
	public InputField nameInput;
	public Text nameFromFB;
	public Toggle gen;
	public string Genero;
	private string userLastName = "";
	public InputField lastnameInput;
	//Variable con el nombre del usuario
	private string userName = "";
	//variable para asociar el celular al input field 
	public InputField celInput;
	//Variable con el celular del usuario
	private string userCel = "";
	private string userCelInd = "";
	//variable para asociar el nombre al input field 
	public InputField mailInput;
	//Variable con el mail del usuario
	public InputField indcelinput;
	private string userMail = "";
	//Variable que recibe la respuesta del php para saber si tiene o no ese mail registrado
	private string answerMail = "";
	//muestra un texto avisando que el mail ya existe
	private int Platform,app=1;
	public GameObject warningMessageRegister;
	public Text warningText;
	public GameObject menu;
	public GameObject loading;
	public GameObject checkInternet;
	public Conection conection;
	public GameObject verificacionPanel;
	public GameObject accedercuenta;
	 GameObject objAdditional;
	Toggle[] screens;
	Toggle screenToGo;
	RegexUtilities util = new RegexUtilities();
	string typeRegister ="";

	public void LoadScreens(){
		screens =  menu.GetComponentsInChildren<Toggle>();
	}

	//funcion cuando oprimen el boton
	public void readRegistration() {
		StartCoroutine ("confirmInternet");
	}

	public void SetTypeRegister(string type){
		typeRegister = type;
	}

	private IEnumerator confirmInternet() {
		loading.SetActive(true);
		WWW getFechaApertura = new WWW (uRLFechaApertura+ "?" + "id=" +1);
		yield return getFechaApertura;
		if(string.IsNullOrEmpty(getFechaApertura.text)){
			checkInternet.SetActive(true);
		}else{
			userName = nameInput.text;
			userMail = mailInput.text;
			userCel  = celInput.text;
			userLastName = lastnameInput.text;
			userCelInd = indcelinput.text;
			string badField="";
			bool allValid = util.IsValidEmail(userMail) && !string.IsNullOrEmpty(userCel) && !string.IsNullOrEmpty(userName)  && !string.IsNullOrEmpty(userLastName)  && !string.IsNullOrEmpty(userCelInd);
//			Debug.Log( userMail + "-"+ userCelInd + "-"+ userCel + "-" + userName +"-"+ userLastName );
			if( allValid ){
				warningText.text = " ";
				StartCoroutine ("confirmMail");
			}else{
	        	loading.SetActive(false);
				badField = string.IsNullOrEmpty(userCel) ?"teléfono":"";
				badField = string.IsNullOrEmpty(userCelInd) ?"teléfono":"";
				badField = util.IsValidEmail(userMail) ?badField:"email";
				badField = string.IsNullOrEmpty(userName) ?"nombre":"";
				badField = string.IsNullOrEmpty(userLastName) ?"apellido":"";
				warningMessageRegister.SetActive(true);//Nicolas
				warningText.text = "Llena todos los datos del registro";
			}
		}
	}



	public void changeGenero( ){
		if( gen.isOn )	
			Genero = "Masculino";
		else
			Genero = "Femenino";
	}

	public void SetEmailFromFB(string _s){
		mailInput.text = _s;
	}
	
	public void SetPhoneFromFB(string _s){
		celInput.text = _s;
	}

	public void SetIndPhoneFromFB(string _s){
		indcelinput.text = _s;
	}

	public void SetNameFromFB(string _s){
		nameInput.text = _s;
////		nameInput.gameObject.SetActive(false);
//		nameInput.interactable = false;
//		mailInput.interactable = false;
		nameFromFB.text = _s;
	}

	public void SetLastNameFromFB(string _s){
		lastnameInput.text = _s;
	}

	public void SetScreenToGo(int _i){
		screenToGo = screens[_i-1];
		if(DataApp.main.IsRegistered()){
			onRegisterDone.Invoke();
		}else{
			this.gameObject.SetActive(true);
		}
	}

	public void ContinueToScreen(){
		screenToGo.isOn= true;
	}

	public void SetAditionalConfig(GameObject _g){
		objAdditional = _g;
		// additionalEvent.AddListener(CompleteAdditionalConfig);
		// additionalEvent.AddListener(CompleteAdditionalConfig);
		onRegisterDone.AddListener(CompleteAdditionalConfig);
	}

	public void CompleteAdditionalConfig(){
		objAdditional.SetActive(true);
		print("CORRE>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

		onRegisterDone.RemoveListener(CompleteAdditionalConfig);
		// onRegisterDone.RemoveListener(AdditionalConfig);
		// onadditional.Invoke();
	}


	public void RemoveAditionalConfig(){
		onRegisterDone.RemoveListener(CompleteAdditionalConfig);
	}

	//funcion que pregunta si el mail puesto en el input text ya esta registrado
	private IEnumerator confirmMail() {
		userMail = mailInput.text;
		string urlString = readURLMail + "?" + "correo=" + WWW.EscapeURL (userMail); 
		WWW getMail = new WWW (urlString);
		yield return getMail;

		answerMail = getMail.text.Trim( );
		print ( answerMail );
		//si no existe el mail guarde los datos en la base de datos
		if ( answerMail.Equals("") == true ) {
			StartCoroutine ("PostNameMail");
			warningText.text = " ";
		}
		//si existe el mail notifiquecelo al usuario
		else {
			if(typeRegister == "FB"){
				this.gameObject.SetActive(false);
				conection.gameObject.SetActive(true);
				conection.SetEmailFromFB(mailInput.text);
				conection.postRegistration();
			}else{
				warningMessageRegister.SetActive(true);//Nicolas
				warningText.text = "Esta cuenta ya existe";
//				accedercuenta.SetActive(true);
	        	loading.SetActive(false);
			}
		} 
	}


	// guarde los datos en la base de datos
	private IEnumerator PostNameMail() {
		userName = nameInput.text;
		userMail = mailInput.text;
		userCel = celInput.text;
		userCelInd = indcelinput.text;

		#if UNITY_ANDROID
			Platform=1;
		#elif UNITY_IOS
			Platform=2;
		#endif
		//de esta forma se suma un nombre, el correo y el cel al php para guardarle en la DB
		string urlString = postURL + "?" + "nombre=" + WWW.EscapeURL (userName)  + "&apellido=" + WWW.EscapeURL (userLastName) + "&" + "correo=" + WWW.EscapeURL (userMail)+ "&"+ "genero=" + WWW.EscapeURL (Genero)+ "&" + "celular=" + WWW.EscapeURL (userCel)+ "&" +  "indcelular=" + WWW.EscapeURL (userCelInd)+ "&"+ "app=" + WWW.EscapeURL(app.ToString()) + "&" + "plataforma=" + WWW.EscapeURL(Platform.ToString()); 
		print("eviando: "+urlString);
		//se carga la url
		WWW idReturn = new WWW (urlString);
		//se espera a que se envie
		yield return idReturn;
		warningMessageRegister.SetActive(true);
		warningText.text = "Registro Completo";
			DoneRegister(idReturn.text);
	}


	public void DoneRegister(string _s){
		print(_s);
		Debug.Log("entro"+_s);
    	loading.SetActive(false);
		DataApp.main.SetMyID(_s);
		onRegisterDone.Invoke();
		Conection.tempId = DataApp.main.GetMyID( ).ToString( );
		DataApp2.main.DisableLoading( );
		TransactionalProcess.thisTransaction.CerrarSesion.interactable = true;
		//verificacionPanel.SetActive( true );     CAMBIAR  PARA VERIFICAR EN MAIL 
	}

}
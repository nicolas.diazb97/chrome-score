﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PosrankingUsuario : MonoBehaviour 
{

	//variables para la comunicacion PHP
	private string getUrlName = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarNombreXUsuario.php";
	private string getUrlPts = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarPuntajeXUsuario.php";
	private string getUrlPos = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarPosXUsuario.php";
	//varible de recibir los datos
	public Text resultName;
	public Text resultPts;
	public Text resultPos;

	private int userID = 0;
	public Text InfoInternetFail;
	public GameObject RankingPanel;
	public GameObject checkInternet;
	public GameObject loading;
	public bool hasInternet;
	WWW  getVotosTotalCamisetas;
	//internet

	public void LoadTable(bool enable)
	{
		//print ("sisisisisisi");
		userID = DataApp.main.GetMyID();
		StartCoroutine ("CheckInternet");	
	}

	private IEnumerator CheckInternet( ){
		loading.SetActive(true);
		getVotosTotalCamisetas = new WWW ("http://2waysports.com/2waysports/Ecuador/Barcelona/Camiseta/getVotosTotalCamisetas.php");
		yield return getVotosTotalCamisetas;
		hasInternet = !string.IsNullOrEmpty(getVotosTotalCamisetas.text);
		loading.SetActive(false);
		if(hasInternet && DataApp.main.IsRegistered() ){
			StartCoroutine("showTable");
			Debug.Log("hay internet y ya esta registrado");
			RankingPanel.SetActive(true);
		}else{
			if(!hasInternet){
				InfoInternetFail.text = "Error en la conexión, Intenta de nuevo.";
				Debug.Log("no hay internet");
				checkInternet.SetActive(true);
			}
			RankingPanel.SetActive(false);
		}
	}

	private IEnumerator showTable()
	{
		WWWForm form = new WWWForm ();
		form.AddField ("userID",userID);
		WWW www = new WWW (getUrlName,form);
		yield return www;
		if(www.isDone)
		resultName.text = www.text;

		WWWForm form1 = new WWWForm ();
		form1.AddField ("userID",userID);
		WWW www1 = new WWW (getUrlPts,form1);
		yield return www1;
		if(www1.isDone)
		resultPts.text = www1.text;

		WWWForm form2 = new WWWForm ();
		form2.AddField ("userID",userID);
		WWW www2 = new WWW (getUrlPos,form2);
		yield return www2;
		if(www2.isDone)
			resultPos.text = www2.text;
	}


	void OnDisable() 
	{
		ClearScreen();
	}

	void ClearScreen()
	{		
		resultName.text = "";
		resultPts.text = "";
		resultPos.text = "";
	}
}

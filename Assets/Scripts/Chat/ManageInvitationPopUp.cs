﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ManageInvitationPopUp : MonoBehaviour {
	public Button acceptButton;
	public Button rejectButton;
	public Button blockButton;
	public Button closeButton;

	bool userChoose;
	string result;


	void Start(){
		acceptButton.onClick.AddListener(()=> {
			userChoose =true;
			result ="Accept";
		});
		rejectButton.onClick.AddListener(()=> {
			userChoose = true;
			result ="Reject";
		});
		blockButton.onClick.AddListener(()=> {
			userChoose = true;
			result ="Block";
		});
		closeButton.onClick.AddListener(()=> {
			userChoose = true;
			result ="Close";
		});
	}

	public IEnumerator AskToUser(System.Action<string> callback){
		yield return new WaitUntil(() => userChoose);
		userChoose = false;
		callback(result);
		result = "";
	}
}

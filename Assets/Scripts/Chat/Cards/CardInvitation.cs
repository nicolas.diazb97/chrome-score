﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardInvitation : MonoBehaviour {
	public Text nameMemberRemote;
	public Text nameChat;
	public Button buttonView;
	public ManageInvitationPopUp invP;
	public Text nameRemoteInPopUp;
	public Text nameChatInPopUp;
	public Image userPhotoInPopUp; // DIEGO 
	public Image userPhoto; // DIEGO 
	public InvitationsView view;
	Invitations inv;
	public string id;
	string ImageProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";

	// Use this for initialization
	public void SetValues (Invitations _inv, string _nameChat , int idUserInv) {
		inv = _inv;
		nameMemberRemote.text = _nameChat;
		nameChat.text = _inv.Chats.DisplayName;
		id= idUserInv.ToString( );
		Debug.Log( "el hijo de puta que me invito se llama :" + nameMemberRemote.text +  "y su puto ID es : " + id);
		StartCoroutine( getUserId( resultid => { 	
			if( resultid != "" ){
//				Debug.Log ( "TAMAÑO DE MI LISTA: "+EditProfile.myUser.dwnlSpriteUser.Count  );
				if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
					for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
//						Debug.Log("i img : "+ EditProfile.myUser.dwnlSpriteUser[i].name +" result: "+ resultid );
						if( EditProfile.myUser.dwnlSpriteUser[i].name == resultid){
							userPhoto.sprite = EditProfile.myUser.dwnlSpriteUser[i];
							break;
						}else{
							Debug.Log(" acabo "+EditProfile.myUser.dwnlSpriteUser.Count );
							if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
								userPhoto.sprite = EditProfile.myUser.dwnlSpriteUser[0];
								StartCoroutine( DownloadSpriteUser ( resultid) ) ;
								break;
							}else{
//								Debug.Log( "SIGUIENTE ");
							}
						}	
					}
				}else{
					StartCoroutine( DownloadSpriteUser ( resultid) ) ;
				}
			}
		}));
		RegisterListener();

		// idChat = _id;
	}

	void DeleteInvitation(){
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				view.Reload();
				//Recargar pantalla de lista
			}else{
				ChatController.main.SetWarningMessage("Experimentamos problemas para completar la acción, por favor intenta nuevamente.(22)");
			}
		} ,PCI.INVITATIONS, inv.InvitationId.ToString() ));
	}

	void LoginToChat(){
		string json = JsonUtility.ToJson (new ChatsMembersCreator( inv.Chats.ChatId , ChatController.main.myUserIDchat));
		ApiChat.main.StartCoroutine(ApiChat.main.POST<ChatsMembersList>( cmThisChat => {
			if(cmThisChat == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para registrarte en "+ inv.Chats.DisplayName+".(12)");
			}else{
		      	DeleteInvitation();
			}
		} ,PCI.CHATS_MEMBERS,json ));
	}


	void BlockMember(){
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				//Recargar pantalla de lista
				string json = JsonUtility.ToJson (new BlockCreator(ChatController.main.myUserIDchat , inv.Chats.Owner  ));
				ApiChat.main.StartCoroutine(ApiChat.main.POST<BlockList>( block => {
					if(block == null){
						ChatController.main.SetWarningMessage("Experimentamos problemas para completar la acción.(21)");
					}else{
						view.Reload();
						ChatController.main.SetWarningMessage("Este usuario ha sido bloqueado para enviarte mensajes privados.");
					}
				} ,PCI.BLOCKS,json ));
			}else{
				ChatController.main.SetWarningMessage("Experimentamos problemas para completar la acción, por favor intenta nuevamente.(23)");
			}
		} ,PCI.INVITATIONS, inv.InvitationId.ToString() ));

		
	}


	IEnumerator getUserId( System.Action<string> result ){
		string c = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/userSearch.php?opc=userImage&mUserIDchat="+ id;
//		Debug.Log( "PUERCO ID : " + inv.Members.);
		WWW dimg = new WWW( c );
		yield return dimg;
		if( string.IsNullOrEmpty(dimg.error) ){
			if( !string.IsNullOrEmpty(dimg.text ) ){
				id = dimg.text;
			}
		}
		result ( id );
	}


	IEnumerator DownloadSpriteUser  (  string id){
		//		yield return new WaitForEndOfFrame();
		Sprite result = null;
		string urlimg = ImageProfile+"/ImagenesComprimidasRanking/"+id+".jpg";
		Debug.Log("la foto es]: " +urlimg );
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		if(string.IsNullOrEmpty(d_img.error) ){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			result = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			userPhoto.sprite = result;
			EditProfile.myUser.dwnlSpriteUser.Add( result );
			EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = id;
		}
	}

	void RegisterListener(){
		buttonView.onClick.AddListener(() =>{ 
			nameRemoteInPopUp.text = nameMemberRemote.text;
			userPhotoInPopUp.sprite = userPhoto.sprite;
			nameChatInPopUp.text =" Te ha invitado a participar en el \n chat <b>"+nameChat.text+"</b>";
			invP.gameObject.SetActive(true);

			StartCoroutine(invP.AskToUser(result =>{
				switch (result) {
					    case "Accept":
					    	LoginToChat();
					      break;
					    case "Reject":
					      DeleteInvitation();
					      break;
					    case "Block":
					      // Bloquea al usuario
					      BlockMember();
					      break;
					    case "Close":
					      break;
					}	

				invP.gameObject.SetActive(false);
			} ));
		});
	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class CardChat : MonoBehaviour {
	public Text txtInvisible;
	public Text txtVisible;
    public Text userName;//Eduardo
    public Button viewProfileBtn;
    public Button blockMemberBtn;
	public Image userPhoto; // DIEGO 
    Member thisMember;
	public string id;
	string ImageProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";


	IEnumerator getUserId( System.Action<string> result ){
		string c = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/userSearch.php?opc=userImage&mUserIDchat="+ thisMember.MemberId;
		WWW dimg = new WWW( c );
		yield return dimg;
		if( string.IsNullOrEmpty(dimg.error) ){
			if( !string.IsNullOrEmpty(dimg.text ) ){
				id = dimg.text;
			}
		}
		result ( id );
	}
	// Use this for initialization
	public void SetTextMessage (string _msg, Member _member) {
		thisMember = _member;
		txtInvisible.text = _msg;
		txtVisible.text = _msg;
		userName.text = _member.DisplayName;
		StartCoroutine( getUserId( resultid => { 
			if( resultid != "" ){
				if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
					for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){

						if( EditProfile.myUser.dwnlSpriteUser[i].name == id){
							Debug.Log( "ENCONTRE LA FOTO EN LA LISTA" + EditProfile.myUser.dwnlSpriteUser[i].name);
							userPhoto.sprite = EditProfile.myUser.dwnlSpriteUser[i];
							break;
						}else{
							
							if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
								Debug.Log( "NO LA ENCOTNRE DESCARGUELA" );
								userPhoto.sprite = EditProfile.myUser.dwnlSpriteUser[0];
								StartCoroutine( DownloadSpriteUser ( id) ) ;
								break;
							}
						}	
					}
				}else{
					Debug.Log( "LA PRIMERA DESCARGUELA" );
					StartCoroutine( DownloadSpriteUser ( id) ) ;
				}
			}
		}));
	
	}

	IEnumerator DownloadSpriteUser  (  string id){
//		yield return new WaitForEndOfFrame();
		Sprite result = null;
		string urlimg = ImageProfile+"/ImagenesComprimidasRanking/"+id+".jpg";
		Debug.Log("la foto es]: " +urlimg );
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		if(string.IsNullOrEmpty(d_img.error) ){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			result = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			userPhoto.sprite = result;
			EditProfile.myUser.dwnlSpriteUser.Add( result );
			EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = id;
		}
	}


	public void OnviewProfileBtn(){
		ChatController.main.ShowProfileOf(thisMember.MemberId);
	}

	public void OnblockMemberBtn(){
		CheckIfAlreadyBlocked();
	}

	void AskToBlock(){
		ChatController.main.cp.gameObject.SetActive(true);
		ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
			if(result == "ok"){
				BlockMember();
			}else{
			}	
			ChatController.main.cp.gameObject.SetActive(false);
		},"¿Desea bloquear a "+thisMember.DisplayName+"?" ));
	}


	void CheckIfAlreadyBlocked(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<BlockList>( blocks  => {
			if(blocks != null) {
				bool canBeBlocked =true;
				foreach(Block blk in blocks.list) {
					if(thisMember.MemberId == blk.BlockedMemberId ){
						canBeBlocked = false;
					}
				}
				if(canBeBlocked){
					AskToBlock();
				}else{
					ChatController.main.SetWarningMessage("Este usuario ya ha sido bloqueado.");
				}
			}
		} ,PCI.BLOCKS,parameters));
		
	}


	void BlockMember(){
		string json = JsonUtility.ToJson (new BlockCreator(ChatController.main.myUserIDchat , thisMember.MemberId  ));
		ApiChat.main.StartCoroutine(ApiChat.main.POST<BlockList>( block => {
			if(block == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para completar la acción.(15)");
			}else{
				BlockSucess();
			}
		} ,PCI.BLOCKS,json ));
		
	}

	void BlockSucess(){
		ChatController.main.SetWarningMessage(thisMember.DisplayName+" ha sido bloqueado para enviar mensajes privados.");
	}



	

}

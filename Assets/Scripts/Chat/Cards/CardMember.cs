﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CardMember : MonoBehaviour {
	public Text nameMember;
	public Image photoUser;
	public Button buttonView;
	public Button buttonDelete;
	ChatsMembers chatsMember;
	Invitations invitation;
	public string idMember;
	string ImageProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";
	// Use this for initialization


	public void SetValues (ChatsMembers _cm) {
		chatsMember = _cm;
		nameMember.text = chatsMember.Members.DisplayName;
		idMember ="";
		StartCoroutine( getUserIdMember( resultid => { 
			if( resultid != "" ){
				if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
					for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
						if( EditProfile.myUser.dwnlSpriteUser[i].name == idMember){
							Debug.Log( "ENCONTRE LA FOTO EN LA LISTA" + EditProfile.myUser.dwnlSpriteUser[i].name);
							photoUser.sprite = EditProfile.myUser.dwnlSpriteUser[i];
							break;
						}else{
							if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
								Debug.Log( "NO LA ENCOTNRE DESCARGUELA" );
								photoUser.sprite = EditProfile.myUser.dwnlSpriteUser[0];
								StartCoroutine( DownloadSpriteUser ( idMember) ) ;
								break;
							}
						}	
					}
				}else{
					Debug.Log( "LA PRIMERA DESCARGUELA" );
					StartCoroutine( DownloadSpriteUser ( idMember) ) ;
				}
			}
		}));


		if( ChatController.main.myUserIDchat == chatsMember.Chats.Owner){
			RegisterDeleteListener();
		}else{
			buttonDelete.gameObject.SetActive(false);
		}
	}


	IEnumerator DownloadSpriteUser  (  string id){
		//		yield return new WaitForEndOfFrame();
		Sprite result = null;
		string urlimg = ImageProfile+"/ImagenesComprimidasRanking/"+id+".jpg";
		Debug.Log("la foto es]: " +urlimg );
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		if(string.IsNullOrEmpty(d_img.error) ){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			result = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			photoUser.sprite = result;
			EditProfile.myUser.dwnlSpriteUser.Add( result );
			EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = id;
		}
	}

	public void SetValues (Invitations _iv) {
		invitation = _iv;
		nameMember.text = invitation.Members.DisplayName;
		idMember ="";
		StartCoroutine( getUserIdInv( resultid => { 
			if( resultid != "" ){
				if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
					for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
						if( EditProfile.myUser.dwnlSpriteUser[i].name == idMember){
							Debug.Log( "ENCONTRE LA FOTO EN LA LISTA" + EditProfile.myUser.dwnlSpriteUser[i].name);
							photoUser.sprite = EditProfile.myUser.dwnlSpriteUser[i];
							break;
						}else{
							if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
								Debug.Log( "NO LA ENCOTNRE DESCARGUELA" );
								photoUser.sprite = EditProfile.myUser.dwnlSpriteUser[0];
								StartCoroutine( DownloadSpriteUser ( idMember) ) ;
								break;
							}
						}	
					}
				}else{
					Debug.Log( "LA PRIMERA DESCARGUELA" );
					StartCoroutine( DownloadSpriteUser ( idMember) ) ;
				}
			}
		}));
		buttonDelete.gameObject.SetActive(false);
	}


	IEnumerator getUserIdMember( System.Action<string> result ){
		string c = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/userSearch.php?opc=userImage&mUserIDchat="+ chatsMember.MemberId;
		Debug.Log ("img card memeber: " + c);
		WWW dimg = new WWW( c );
		yield return dimg;
		if( string.IsNullOrEmpty(dimg.error) ){
			if( !string.IsNullOrEmpty(dimg.text ) ){
				idMember = dimg.text;
			}
		}
		result ( idMember );
	}

	IEnumerator getUserIdInv( System.Action<string> result ){
		string c = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/userSearch.php?opc=userImage&mUserIDchat="+ invitation.MemberId;
		Debug.Log ("img card memeber: " + c);
		WWW dimg = new WWW( c );
		yield return dimg;
		if( string.IsNullOrEmpty(dimg.error) ){
			if( !string.IsNullOrEmpty(dimg.text ) ){
				idMember = dimg.text;
			}
		}
		result ( idMember );
	}


	void RegisterDeleteListener(){
		buttonDelete.onClick.AddListener(()=> { 
			ChatController.main.cp.gameObject.SetActive(true);
			ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					DeleteThisMember();
					print ("ELIGIO OK");
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea eliminar a "+nameMember.text+" de este ranking?" ));
		});
	}

	void DeleteThisMember(){
		Parameter p1 = new Parameter("ChatId",chatsMember.Chats.ChatId);
		Parameter p2 = new Parameter("MemberId",chatsMember.Members.MemberId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		parameters.Add(p2);

		ApiChat.main.StartCoroutine(ApiChat.main.DELETE( deletes => {
			if( deletes ){
				this.gameObject.SetActive(false);
			}else{
				ChatController.main.SetWarningMessage("No fue posible eliminar este usuario. (19)");
			}
		} ,PCI.CHATS_MEMBERS,parameters ));
	}
	

}

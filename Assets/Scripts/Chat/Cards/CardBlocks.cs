﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardBlocks : MonoBehaviour {
	public Text nameMemberRemote;
	public Button buttonUnblock;
	public BlocksView view;
	public Image photoUser;
	Block blk;
	string ImageProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";
	string idMember; 


	public void SetValues (Block _blk, string _nameRemote, int _idRemote) {
		blk = _blk;
		idMember = _idRemote.ToString( );
		nameMemberRemote.text = _nameRemote;
		StartCoroutine( getUserId( resultid => { 
			if( resultid != "" ){
				if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
					for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
						if( EditProfile.myUser.dwnlSpriteUser[i].name == idMember){
							Debug.Log( "ENCONTRE LA FOTO EN LA LISTA" + EditProfile.myUser.dwnlSpriteUser[i].name);
							photoUser.sprite = EditProfile.myUser.dwnlSpriteUser[i];
							break;
						}else{
							if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
								Debug.Log( "NO LA ENCOTNRE DESCARGUELA" );
								photoUser.sprite = EditProfile.myUser.dwnlSpriteUser[0];
								StartCoroutine( DownloadSpriteUser ( idMember) ) ;
								break;
							}
						}	
					}
				}else{
					Debug.Log( "LA PRIMERA DESCARGUELA" );
					StartCoroutine( DownloadSpriteUser ( idMember) ) ;
				}
			}
		}));
		RegisterListener();
	}


	IEnumerator DownloadSpriteUser  (  string id){
		//		yield return new WaitForEndOfFrame();
		Sprite result = null;
		string urlimg = ImageProfile+"/ImagenesComprimidasRanking/"+id+".jpg";
		Debug.Log("la foto es]: " +urlimg );
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		if(string.IsNullOrEmpty(d_img.error) ){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			result = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			photoUser.sprite = result;
			EditProfile.myUser.dwnlSpriteUser.Add( result );
			EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = id;
		}
	}

	IEnumerator getUserId( System.Action<string> result ){
		string c = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/userSearch.php?opc=userImage&mUserIDchat="+ idMember;
		Debug.Log ("img card memeber: " + c);
		WWW dimg = new WWW( c );
		yield return dimg;
		if( string.IsNullOrEmpty(dimg.error) ){
			if( !string.IsNullOrEmpty(dimg.text ) ){
				idMember = dimg.text;
			}
		}
		result ( idMember );
	}


	void UnblockThisMember(){						
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				view.Reload();
				//Recargar pantalla de lista
			}else{
			}
		} ,PCI.BLOCKS, blk.BlockId.ToString() ));
	}

	void RegisterListener(){
		buttonUnblock.onClick.AddListener(() =>{ 
			ChatController.main.cp.gameObject.SetActive(true);
			ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					UnblockThisMember();
					print ("ELIGIO OK");
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea desbloquear a "+blk.BLockedMembers.DisplayName+"?" ));
		});
	}

}

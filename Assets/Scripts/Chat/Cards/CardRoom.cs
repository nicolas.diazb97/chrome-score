﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CardRoom : MonoBehaviour {
	public Text nameChat;
	public Button buttonView;
	public Button buttonViewRank;
	public Button buttonDelete;
	Rooms room;
	public bool openView;
	int size;

	public ScrollRankingGrupoScript panelRanking;


	// Use this for initialization
	public void SetValues ( Rooms _r, int _size) {
		room = _r;
		nameChat.text = room.DisplayName;
		RegisterViewListeners();
		size = _size;
		print("trace where this is called");
		if(size >1){
			buttonDelete.gameObject.SetActive(false);
		}else{
			RegisterDeleteListeners();
		}
	}
	
	void RegisterDeleteListeners(){
		buttonDelete.onClick.AddListener(()=>{
			ChatController.main.cp.gameObject.SetActive(true);
			StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					DeleteThisRoom();
					print ("ELIGIO OK");
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea eliminar el grupo <b>"+nameChat.text+"</b>?" ));
		} );
	}
	
	void DeleteThisRoom(){
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				this.gameObject.SetActive(false);
			}else{
			}
		} ,PCI.CHATS,room.ChatId.ToString() ));
	}

	void RegisterViewListeners(){

	
		buttonView.onClick.AddListener( () => {
			ChatController.main.SetChatRoom (room);
			ChatController.main.ActiveView(VIEW_CHAT.ROOM);
		} ) ;


		// buttonSend.onClick.AddListener( inputF.Select);
		// inputF.onEndEdit.AddListener( (x) => {PutLocalMessageUI();} );
		// inputF.onEndEdit.AddListener( (x) => {inputF.Select();} );

		buttonViewRank.onClick.AddListener( () => {
			//panelRanking.groupName = room.DisplayName;

			ShowRankingToChat();
		} ) ;
	}

	/// PROGRAMACION DE MOSTRAR EL RANKING:

	public void ShowRankingToChat(){
		DataApp2.main.EnableLoading();
		ChatController.main.SetChatRoom(room);
		panelRanking.GroupTitleTxt.text =  room.DisplayName;
		panelRanking.groupName = room.DisplayName;
		panelRanking.ShowRankingGroupID(room.ChatId);
	}

}

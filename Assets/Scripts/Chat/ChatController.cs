﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
/*GENERAL_*/
public enum VIEW_CHAT : int{ ROOM =0,LISTS =1, EDIT=2, INVITES =3 , MEMBERS = 4 ,BLOCKS = 5,  SEARCH_MEM = 6 };

public class ChatController :  Singleton<ChatController> {

	public GameObject popUpWarning;
	public IChatView actualView { get; private set;}
	public  int myUserIDchat   { get; private set;}
	public Rooms muroAmarillo { get; private set;}
	public 	Rooms actualRoom { get; private set;}
	public 	Member me { get; private set;}
	public ConfirmationPopUp cp;
	public int tries  =  0;
	public UnityEvent onOpenMuroAmarillo;
	public Button homebtn;
	public Button rankisngsbtn;
	public CardViewUserNC cvu;
	public Image photoRemoteMember;
	public bool shouldGoTo;
	List<IChatView>  views = new List<IChatView>();
	public int actualIdView ;
	public int previousIdView ;

	public override void MyAwake(){
		Invoke("DisableMe",0.1f);
	}

	void DisableMe(){
		this.gameObject.SetActive(false);
	}

	public void SetMyIDChat(int _id){
		myUserIDchat = _id;
	}

	public void SetMyMember(Member _m){
		me = _m;
	}

	public void ShowProfileOf( int _ourId ){

		ApiChat.main.StartCoroutine(ApiChat.main.GetInfoOfMember( info => {
			print("RECIBIDA ESTA INFO:\n "+info);
			if(info.Split(',').Length == 9){
				print("logro split de 9 items ");
				GetPhotoProfile( info);
			}else{
				ChatController.main.SetWarningMessage("No fue posible encontrar el perfil de este usuario.(16)");
			}
		} , _ourId.ToString() ));
	}

	void GetPhotoProfile(string _info){
		ApiChat.main.StartCoroutine(ApiChat.main.DownloadImageProfile( sprite => {
			print("RECIBIDA ESTA INFO:\n "+sprite);
			int actualuser = 0;
			if(sprite != null && int.TryParse(_info.Split(',')[1],out actualuser ) ){
				cvu.gameObject.SetActive(true);

				string _id = _info.Split(',')[0] ;
				string nickname = _info.Split(',')[2] ;
				string fecha_ac = _info.Split(',')[3] ;
				string scoreTa = _info.Split(',')[4] ;
				string posTa = _info.Split(',')[5] ;
				string scorePronos = _info.Split(',')[6] ;
				string posPronos = _info.Split(',')[7] ;
				string desc = _info.Split(',')[8] ;
				photoRemoteMember.sprite = sprite;

			 	cvu.SetDatas(_id, actualuser, nickname, fecha_ac, scoreTa, posTa, scorePronos, posPronos, desc, photoRemoteMember);
			}else {
				ChatController.main.SetWarningMessage("No fue posible encontrar el perfil de este usuario.(17)");
			}
		} , _info.Split(',')[0] ));
		
	}


	public void SetMuroAmarillo(Rooms _r){
		muroAmarillo = _r;
	}

	public void OpenInMuroAmarilloFromMenu( bool go){
		
		// if(DataApp.main.IsRegistered() ){
		if(TransactionalProcess.isActive == 1){
			shouldGoTo = go;
			tries =0;
			LoadViews();
			LoginController.main.InitLogin();
		}else{
			//TransactionalProcess.thisTransaction.InitTransaction.SetActive(true);
			Debug.Log(" no esta registrado!!!");
		}
	}

	public void CreateUserDUMMY(string _id){
		DataApp.main.SetMyID(_id);
	}

	public void SetChatRoom(Rooms _r){
		print("con SetChatRoom"+_r);
		actualRoom =_r;
	}

	public void OpenMuroAmarillo(){
		actualRoom = muroAmarillo;
		tries = 0;
		onOpenMuroAmarillo.Invoke();
		ActiveView(VIEW_CHAT.ROOM);
		DataApp2.main.DisableLoading();
//		InternetManager.main.loading.SetActive(false);
	}

	public void OpenListRankings(){
		actualRoom = muroAmarillo;
		tries = 0;
		onOpenMuroAmarillo.Invoke();
		ActiveView(VIEW_CHAT.LISTS);
		DataApp2.main.DisableLoading();
		//		InternetManager.main.loading.SetActive(false);
	}

	public void ActiveView(int _v){
		ActiveView((VIEW_CHAT) _v );
	}


	public void ActiveView(VIEW_CHAT _vc){
		if(actualView != null){
			actualView.DisposeView();
		}
		previousIdView = actualIdView;
		actualIdView = (int)_vc;
		actualView = views[actualIdView];
		actualView.SetupView();
		print("..............Se pide a  "+actualIdView+" y se viene de "+previousIdView);
	}

	public void ReturnToPreviousView(){
		print(".............retorno a "+previousIdView);
		ActiveView(previousIdView);
	}

	public void SetLikeEdit(Rooms _r){
		New_EditChatView mView = (New_EditChatView) views[(int)VIEW_CHAT.EDIT];
		mView.SetLikeEditView(_r);
	}


	void LoadViews(){
		for(int i=0; i<this.transform.childCount; i++){
			views.Add( this.transform.GetChild(i).gameObject.GetComponent<IChatView>() );
			// print("//////// AGREGADO: "+this.transform.GetChild(i).gameObject.name+ "  CON ID: "+i );
		}
	}

	public void SetWarningMessage(string _msj){
		popUpWarning.SetActive(true);
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		popUpWarning.GetComponentInChildren<Text>().text = _msj;
	}
	
	public void SetWarningLoginMessage(string _msj){
		Debug.LogWarning(" intento fallido "+tries);
		if(tries<2){
//			InternetManager.main.loading.SetActive(true);
			DataApp2.main.EnableLoading();
			Invoke("InitLogin",0.8f);
			tries++;
		}else if(!popUpWarning.activeSelf) {
			Debug.Log(" FALLA ");
			SetWarningMessage(_msj);
		}
	}

	void OnDisable(){
		if(actualView != null)
			actualView.DisposeView();
	}


}

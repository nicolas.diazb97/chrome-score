﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoginController :  Singleton<LoginController> {

	public 	void InitLogin(){
		ApiChat.main.StartCoroutine(ApiChat.main.GetHostOfChatAndIdMuroAmarillo( msj => {
			print("con InitLogin: "+msj);
			if(string.IsNullOrEmpty( msj)) {
				GetMyChatIdInServer();
			}else{
				ChatController.main.SetWarningLoginMessage(msj);
			}
		} ));
	}

	void GetMyChatIdInServer(){
		ApiChat.main.StartCoroutine(ApiChat.main.GetChatIdInServer( _id => {
			print("con ProcessFromGetIdInServer: "+_id);
			if(_id > 0){
				ChatController.main.SetMyIDChat( _id);
				CheckMyIDinServer();
			}else{
				ChatController.main.SetWarningLoginMessage("Experimentamos problemas para conectar con el Muro Amarillo.(1)");
			}
		} , DataApp.main.GetMyID().ToString() ));
	}


	void CheckMyIDinServer(){
		ApiChat.main.StartCoroutine(ApiChat.main.GETraw<MembersList>( users => {
			print("con CheckMyIDinServer: "+users);
			if(users == null){
				ChatController.main.SetWarningLoginMessage("Experimentamos problemas para conectar con el Muro Amarillo.(2)");
			}else{
				ChatController.main.SetMyMember(users.list[0]);
				CheckitIsInMuroAmarillo();
			}
			DataApp2.main.EnableLoading();
//			InternetManager.main.loading.SetActive(true);
		} ,PCI.MEMBERS,ChatController.main.myUserIDchat.ToString() ));
	}

	void CheckitIsInMuroAmarillo(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( myChatsMembers => {
			print("con CheckitIsInMuroAmarillo: "+myChatsMembers);
			if(myChatsMembers != null){
				if(InMuroAmarillo(myChatsMembers) ){
					if( ChatController.main.shouldGoTo)
						ChatController.main.OpenMuroAmarillo();
					else
						ChatController.main.OpenListRankings();
				}else{ 
					RegisterInMuroAmarilloAndBack();
				}
			}else{
				ChatController.main.SetWarningLoginMessage("No hemos podido procesar la solicitud.(3)");
			}
//			InternetManager.main.loading.SetActive(true);
			DataApp2.main.EnableLoading();
		} ,PCI.CHATS_MEMBERS,parameters ));
	}

	void RegisterInMuroAmarilloAndBack(){
		int idMA;
		if( int.TryParse(ApiChat.main.idMuroAmarillo, out idMA)){
			string json = JsonUtility.ToJson (new ChatsMembersCreator( idMA , ChatController.main.myUserIDchat));
			ApiChat.main.StartCoroutine(ApiChat.main.POST<ChatsMembersList>( cmMuroAm => {
				print("con RegisterInMuroAmarilloAndBack: "+cmMuroAm);
				if(cmMuroAm == null){
					ChatController.main.SetWarningLoginMessage("Experimentamos problemas para registrarte en el Muro Amarillo.(4)");
				}else{
					ChatController.main.SetMuroAmarillo( cmMuroAm.list[0].Chats);
					ChatController.main.OpenMuroAmarillo();
				}
			} ,PCI.CHATS_MEMBERS,json ));
		}else{
			ChatController.main.SetWarningLoginMessage("Experimentamos problemas para encontrar nuestro Muro Amarillo.(5)");
		}
	}

	bool InMuroAmarillo(ChatsMembersList _myChatsMembers){
		bool result = false;
		foreach (ChatsMembers cm in _myChatsMembers.list) {
			if(cm.Chats.ChatId.ToString() == ApiChat.main.idMuroAmarillo ){
				result = true;
				ChatController.main.SetMuroAmarillo( cm.Chats);
			}
		}
		return result;
	}

}

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class New_EditChatView : MonoBehaviour,IChatView {
	public CardMember  cardMemberPrefab;
	public CardMember  cardInvitedPrefab;
	public CardMember  separator;
	public Button editName;
	public Button setName;

	public Button buttonDelete;
	public Button buttonSave;
	public Button buttonInvite;
	public InputField nameField;
	public Text nameChat;
	public Text title;
	public Rooms room { get; private set;}

	List<CardMember> cards = new List<CardMember>();

	public bool isInEdit;

	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		RegisterListeners();
	}

	public void SetLikeEditView(Rooms _r){

		isInEdit = true;
		room = _r;
		title.text ="EDITAR CHAT";
		nameChat.text = room.DisplayName;

		nameChat.gameObject.SetActive(true);
		nameField.gameObject.SetActive(false);
		buttonDelete.gameObject.SetActive(false);
		buttonSave.gameObject.SetActive(true);
		
		bool isOwn = room.Owner == ChatController.main.myUserIDchat  ;
		buttonInvite.gameObject.SetActive(isOwn);
		DecideShowDeleteButton(room);
		ShowMembersInRoom();

	}

	void DecideShowDeleteButton(Rooms _r){
		Parameter p1 = new Parameter("ChatId",_r.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			bool canDelete = chatmbs.list.Count < 2;
			buttonDelete.gameObject.SetActive(canDelete);
		} ,PCI.CHATS_MEMBERS,parameters));
	}
	

	public void SetLikeNewView(){

		isInEdit = false;
		nameChat.gameObject.SetActive(false);
		nameField.gameObject.SetActive(true);

		buttonDelete.gameObject.SetActive(false);
		// buttonCreate.gameObject.SetActive(false);
		buttonSave.gameObject.SetActive(false);
		buttonInvite.gameObject.SetActive(false);

		nameChat.text = "";
		title.text ="NUEVO CHAT";
	}

	void ShowMembersInRoom(){
		Parameter p1 = new Parameter("ChatId",room.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			PutMembersInRoom(chatmbs);
			ShowInvitesToRoom();
		} ,PCI.CHATS_MEMBERS,parameters));
	}


	void ShowInvitesToRoom(){
		Parameter p1 = new Parameter("ChatId",room.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<InvitationsList>( invites  => {
			// if(invites != nul){
			if(invites.list.Count > 0){
				InstantiateCard(separator);
			}
			PutInvitesInRoom(invites);
		} ,PCI.INVITATIONS,parameters));
	}

	void RegisterListeners(){

		buttonDelete.onClick.AddListener( ()=>{
			ChatController.main.cp.gameObject.SetActive(true);
			ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
				if(result == "ok"){
					DeleteThisRoom();
					print ("ELIGIO OK");
				}else{
					print ("ELIGIO "+result);
				}	
				ChatController.main.cp.gameObject.SetActive(false);
			},"¿Desea eliminar el grupo "+nameChat.text+"?" ));
		} );

		buttonInvite.onClick.AddListener( ()=> { 
			ChatController.main.ActiveView(VIEW_CHAT.MEMBERS);
				// CreateNewChat();
		} );

		setName.onClick.AddListener( ()=> { 

			if(! string.IsNullOrEmpty(nameField.text) ){

				if(isInEdit){
					EditChatName();
				}else{
					CreateNewChat();

				}
			}
		} );

		//Eduardo

		editName.onClick.AddListener(() => {

			nameField.text = room.DisplayName;
			setName.transform.parent.gameObject.SetActive(true);
			editName.transform.parent.gameObject.SetActive(false);
		});
		//*-**************
	}

#region Eduardo
	void EditChatName() {
		string json = JsonUtility.ToJson (new ChatsEditCreator(room.ChatId, room.Owner, room.CreatedDate, room.ChatStateId, nameField.text));
		
		string idParam = room.ChatId.ToString();

		ApiChat.main.StartCoroutine(ApiChat.main.PUTraw( response => {
			if(response == "ok"){
				Reload();
			}else if(response == "error"){
				ChatController.main.SetWarningMessage("Experimentamos problemas para  modificar este chat. (13)");
			}
		} ,PCI.CHATS, idParam,json ));
	}

	void Reload(){
		DisposeView();
		SetupView();
		ReloadThisRoom();
	}

	void ReloadThisRoom() {
		ApiChat.main.StartCoroutine(ApiChat.main.GETraw<RoomsList>( thisRoom => {
			if(thisRoom == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para completar la accion. (14)");
			}else{
				ChatController.main.SetChatRoom(thisRoom.list[0]);
				SetLikeEditView(thisRoom.list[0]);
			}
			DataApp2.main.EnableLoading();
//			InternetManager.main.loading.SetActive(true);
		} ,PCI.CHATS,room.ChatId.ToString() ));
	}

#endregion

	void DeleteThisRoom(){
		ApiChat.main.StartCoroutine(ApiChat.main.DELETEraw( deletes => {
			if( deletes ){
				//SetLikeNewView();
				ChatController.main.ActiveView( VIEW_CHAT.LISTS);
				nameField.text = "";
			}else{
			}
		} ,PCI.CHATS,room.ChatId.ToString() ));
	}

	void CreateNewChat(){
		string json = JsonUtility.ToJson (new ChatsCreator( ChatController.main.myUserIDchat, nameField.text));
		print("ESTO ES: "+json);
		ApiChat.main.StartCoroutine(ApiChat.main.POST<RoomsList>( newRoom => {
			if(newRoom == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para crear "+nameField.text+" (6)");
			}else{
				SetLikeEditView(newRoom.list[0]);
			}
		} ,PCI.CHATS,json ));
	}



	void PutMembersInRoom(ChatsMembersList chatmbs){
		foreach (ChatsMembers cm in chatmbs.list) {
			if(cm.Members.MemberId != ChatController.main.myUserIDchat ){
				InstantiateCard(cardMemberPrefab   ,cm );
			}
		}
	}


	void PutInvitesInRoom(InvitationsList invites){
		foreach (Invitations iv in invites.list) {
			if(iv.Members.MemberId != ChatController.main.myUserIDchat ){
				InstantiateCard(cardInvitedPrefab   ,iv );
			}
		}
	}

	void InstantiateCard(CardMember _c, ChatsMembers _t){
        CardMember card = Instantiate(_c ) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
        card.SetValues(_t );
	}

	void InstantiateCard(CardMember _c, Invitations _i){
        CardMember card = Instantiate(_c ) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
        card.SetValues(_i);
	}
	void InstantiateCard(CardMember _c){
        CardMember card = Instantiate(_c ) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
	}

	public void DisposeView(){
		foreach (CardMember c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);
		setName.onClick.RemoveAllListeners();
		buttonDelete.onClick.RemoveAllListeners();
		buttonInvite.onClick.RemoveAllListeners();

		editName.onClick.RemoveAllListeners();//Eduyardo

		nameField.text = "";
	}
}

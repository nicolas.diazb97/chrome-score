﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class InvitationsView : MonoBehaviour,IChatView {

	public CardInvitation cardPrefab;

	List<CardInvitation> cards = new List<CardInvitation>();


	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		ShowMyInvitations();
	}

	void ShowMyInvitations(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<InvitationsList>( invites  => {
			if(invites != null) {
				foreach(Invitations inv in invites.list) {
					ConsultInvitationInfo(inv);
				}
			}
		} ,PCI.INVITATIONS,parameters));

		//foreach(Member)
	}

	public void Reload(){
		DisposeView();
		SetupView();
	}

	void ConsultInvitationInfo(Invitations _inv){
		ApiChat.main.StartCoroutine(ApiChat.main.GETraw<MembersList>( users => {
			print("con CheckMyIDinServer: "+users);
			if(users == null){
				ChatController.main.SetWarningMessage("Experimentamos problemas para completar la acción.(9)");
			}else{
				InstantiateCard(cardPrefab, _inv, users.list[0].DisplayName, users.list[0].MemberId ) ;
			}
		} ,PCI.MEMBERS,_inv.Chats.Owner.ToString() ));
		
	}


	void InstantiateCard(CardInvitation _c, Invitations _inv, string _nameRemote, int _idRemote){
        CardInvitation card = Instantiate(_c) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
		card.SetValues(_inv,_nameRemote,_idRemote);
	}

	public void DisposeView(){
		foreach (CardInvitation c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);
		// setName.onClick.RemoveAllListeners();
		// buttonDelete.onClick.RemoveAllListeners();
		// nameField.text = "";
	}
}

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;

public class RoomView : MonoBehaviour,IChatView {
	public CardChat  cardLocalPrefab;
	public CardChat  cardRemotePrefab;
	public MyInputField  inputF;
	public Button  buttonSend;
	public Text title;

	public Button btnBlocks;
	public Button btnInvites;
	public Button btnEdit;
	public Button btnExit;
	public GameObject instructions;
	public TitleBtns titleBtns;

	List<CardChat> cards = new List<CardChat>();
	List<Member> remoteMembers = new List<Member>();
	Rooms thisRomm;
	DirectoryInfo folder;

	// Use this for initialization
	public void SetupView () {
		CancelInvoke();
		if( ! DataApp.main.AlreadyViewChatInstructions()){//new
			instructions.SetActive(true);//new
		}
		thisRomm = ChatController.main.actualRoom;
		CreateDirectoryToHistory();
		this.gameObject.SetActive(true);
		RegisterListeners();
		// CLEARView();
		SetTitleRoom();
		GetRemoteMembers();

	}

	public void ViewedInstructions(){//new
		DataApp.main.SetViewChatInstructions("1");//new
	}//new



	void RSTINSTRUCTIONS(){//new
		DataApp.main.DeleteVar("ViewChatInstructions");//new
	}//new


	void GetRemoteMembers(){
		Parameter p1 = new Parameter("ChatId",thisRomm.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( myChatsMembers => {
			if(myChatsMembers != null){
				foreach (ChatsMembers cm in myChatsMembers.list ) {
					remoteMembers.Add(cm.Members);
				}
				ShowPastMessages();
				InvokeRepeating ("ShowNewMessages",0f,2f);
			}
		} ,PCI.CHATS_MEMBERS,parameters ));
	}

	void CreateDirectoryToHistory(){
		folder = new DirectoryInfo(Application.persistentDataPath +"/"+thisRomm.ChatId);
		if(!folder.Exists){
			folder.Create();
			print(" LA CARPETA; "+folder.ToString());
		}
	}

	public void SetTitleRoom(){
		
		bool isMA = thisRomm.ChatId == ChatController.main.muroAmarillo.ChatId;
		title.text = (isMA) ? thisRomm.DisplayName + " Tiro Amarillo" : thisRomm.DisplayName;
		title.text = (isMA) ? thisRomm.DisplayName + " Tiro Amarillo" : thisRomm.DisplayName;
		//titleBtns.SetStates(!isMA,true,!isMA);
		titleBtns.SetStates(!isMA, true, true);//Eduardo
		btnEdit.gameObject.SetActive(!isMA);
		btnExit.gameObject.SetActive(!isMA);
	}

	void ShowNewMessages(){
		Parameter p1 = new Parameter("ChatId",thisRomm.ChatId);
		Parameter p2 = new Parameter("Receiver",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		parameters.Add(p2);

		ApiChat.main.StartCoroutine(ApiChat.main.PUT<MessagesList>( msjs => {
			if(msjs != null){
				// CheckRemoteMembersAndThenPutMessage(msjs, true);
				PutMessages(msjs, true);
			}else{

			}
		} ,PCI.MESSAGES,parameters ));
	}
	
	void ShowPastMessages(){
		if(File.Exists(folder.ToString()+"/history.json")){
			string json = "{\"list\":["+File.ReadAllText(  folder.ToString()+"/history.json"  )+"]}";
			PutMessages(  JsonUtility.FromJson<MessagesList>(json), false );
		}else{
			File.WriteAllText(folder.ToString() +"/history.json", "{}" );
		}
	}

	// void CheckRemoteMembersAndThenPutMessage(MessagesList msjs, bool isNew){
	void CheckRemoteMembersAndThenInstantiateCard(Messages _msj){
		CardChat cc = _msj.Sender == ChatController.main.myUserIDchat ? cardLocalPrefab :cardRemotePrefab;
		// string nameToShow = "";
		Member remoteMember = null;

		foreach (Member m in remoteMembers) {
			if(m.MemberId == _msj.Sender){
				remoteMember = m;
				// nameToShow = m.DisplayName;
			}
		}
		if(remoteMember == null){
			ApiChat.main.StartCoroutine(ApiChat.main.GETraw<MembersList>( users => {
				if(users == null){
					// nameToShow ="Unknown User";
				}else{
					remoteMember = users.list[0];
					remoteMembers.Add( remoteMember);
					// nameToShow = users.list[0].DisplayName;
					InstantiateCard(cc,_msj.Text, remoteMember);//GUARDAR
				}
				DataApp2.main.EnableLoading();
//				InternetManager.main.loading.SetActive(true);
			} ,PCI.MEMBERS,_msj.Sender.ToString() ));
			
		}else{
			InstantiateCard(cc,_msj.Text, remoteMember);//GUARDAR
		}
	}

	void PutMessages(MessagesList msjs, bool isNewMessages){
		foreach (Messages m in msjs.list) {
			CheckRemoteMembersAndThenInstantiateCard(m);
			if(isNewMessages){
				File.AppendAllText(folder.ToString() +"/history.json", CreateObjectRemotoAndTRansformToJson(m) );
				CheckSystemCommand(m);
			}
		}
	}

	//NUEVOS
	void CheckSystemCommand(Messages _m){
		if( _m.Sender == 1 || _m.Sender == 1004  ){
			string[] command = _m.Text.Split(','); 
			if(command.Length > 1 ){
				if(command[0] == ChatController.main.myUserIDchat.ToString()){
					Notif(command[1]);
				}
			}else{
				Invoke(_m.Text,0);
			}
		}
	}
	void CLCH(){
		File.Delete(folder.ToString() +"/history.json");
		DisposeView();
		SetupView();
	}

	void Notif(string _m){
		ChatController.main.SetWarningMessage(_m);
	}

//	void PutMessages(MessagesList msjs, bool isNewMessages){
//		foreach (Messages m in msjs.list) {
//			CheckRemoteMembersAndThenInstantiateCard(m);
//			if(isNewMessages){
//				File.AppendAllText(folder.ToString() +"/history.json", CreateObjectRemotoAndTRansformToJson(m) );
//			}
//		}
//	}

	public void PutLocalMessageUI(){
		buttonSend.interactable = false;
		string tmpMsg = inputF.text;
		inputF.text = "";
		if(!string.IsNullOrEmpty(tmpMsg) ){
			PutLocalMessageToServer(tmpMsg);
		}else{ 
		
			buttonSend.interactable = true;
		}
	}

	void PutLocalMessageToServer(string _text){
		Parameter p1 = new Parameter("SenderMemberId",ChatController.main.myUserIDchat);
		Parameter p2 = new Parameter("ChatId",ChatController.main.actualRoom.ChatId);
		Parameter p3 = new Parameter("text",System.Uri.EscapeUriString(_text));
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1); parameters.Add(p2); parameters.Add(p3);

		ApiChat.main.StartCoroutine(ApiChat.main.POST( result => {
			if(result == "\"Sucessfully\""){
				InstantiateCard(cardLocalPrefab,_text,ChatController.main.me);
				File.AppendAllText(folder.ToString() +"/history.json", CreateObjectLocalAndTRansformToJson(_text) );

        		inputF.text ="";

				buttonSend.interactable = true;
			}else{

				buttonSend.interactable = true;
			}
		} ,PCI.MESSAGES,parameters ));
	}

	void InstantiateCard(CardChat _c, string _msj, Member _member){
		// print("$$$$$$$$$ llega este name"+_name);
		if(!string.IsNullOrEmpty(_msj))
		{
	        CardChat card = Instantiate(_c) ;
	        card.transform.SetParent (_c.transform.parent, false);
	        cards.Add(card);
	        card.gameObject.SetActive(true);
	        card.SetTextMessage(_msj,_member);
	        // card.userName.text = _mem;//Eduardo afecta todos incluso los remotos con mi nombre de usuario
		}
	}


	string CreateObjectRemotoAndTRansformToJson(Messages _m){
		string result = "";
		MessagesCreator myMsj =  new MessagesCreator(_m.Receivers, _m.Sender, _m.Text, _m.IsFile );
		result = JsonUtility.ToJson( myMsj);
		return ","+result;
	}

	string CreateObjectLocalAndTRansformToJson(string _m){
		string result = "";
		MessagesCreator myMsj =  new MessagesCreator(new ChatsMembers(ChatController.main.actualRoom,ChatController.main.me) , ChatController.main.myUserIDchat , _m, false );
		result = JsonUtility.ToJson( myMsj);
		return ","+result;
	}

	
	string NameOfThisID(int _id){
		string result = "";
		foreach (Member m in remoteMembers) {
			if(m.MemberId == _id){
				result = m.DisplayName;
			}
		}
		if(string.IsNullOrEmpty(result)){
			ApiChat.main.StartCoroutine(ApiChat.main.GETraw<MembersList>( users => {
				if(users == null){
					result ="Unknown User";
				}else{
					remoteMembers.Add( users.list[0]);
					result = users.list[0].DisplayName;
				}
//				InternetManager.main.loading.SetActive(true);
				DataApp2.main.EnableLoading();
			} ,PCI.MEMBERS,_id.ToString() ));
		}
		return result;
	}

	void DeleteMeFromThisRoom(){
		Parameter p1 = new Parameter("ChatId",thisRomm.ChatId);
		Parameter p2 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		parameters.Add(p2);

		ApiChat.main.StartCoroutine(ApiChat.main.DELETE( deletes => {
			if( deletes ){
				ChatController.main.ActiveView(VIEW_CHAT.LISTS);
			}else{
				ChatController.main.SetWarningMessage("No fue posible salir del grupo. (18)");
			}
		} ,PCI.CHATS_MEMBERS,parameters ));
	}

	public void DisposeView(){
		foreach (CardChat c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);

		buttonSend.onClick.RemoveAllListeners();
		btnBlocks.onClick.RemoveAllListeners();
		btnInvites.onClick.RemoveAllListeners();
		btnEdit.onClick.RemoveAllListeners();
		btnExit.onClick.RemoveAllListeners();
		titleBtns.addMemberBtn.onClick.RemoveAllListeners();
		titleBtns.backBtn.onClick.RemoveAllListeners();
		remoteMembers.Clear();
		CancelInvoke();
	}

	void RegisterListeners(){
		if(buttonSend.onClick.GetPersistentEventCount() <1){
			btnBlocks.onClick.AddListener(()=>{
				ChatController.main.ActiveView(VIEW_CHAT.BLOCKS);
				titleBtns.SettingsBtn.isOn = false;

			});
			
			btnInvites.onClick.AddListener(()=>{
				ChatController.main.ActiveView(VIEW_CHAT.INVITES);
				titleBtns.SettingsBtn.isOn = false;
			});


			btnEdit.onClick.AddListener(()=>{
				ChatController.main.ActiveView(VIEW_CHAT.EDIT);
				ChatController.main.SetLikeEdit(thisRomm );
				titleBtns.SettingsBtn.isOn = false;
			});

			btnExit.onClick.AddListener(()=>{
				ChatController.main.cp.gameObject.SetActive(true);
				ApiChat.main.StartCoroutine(ChatController.main.cp.AskToUser(result =>{
					if(result == "ok"){
						print ("ELIGIO OK");
						DeleteMeFromThisRoom();
					}else{
						print ("ELIGIO "+result);
					}	
					ChatController.main.cp.gameObject.SetActive(false);
				},"¿Desea salir del grupo <b>"+thisRomm.DisplayName+"</b>?" ));
			});

			titleBtns.addMemberBtn.onClick.AddListener(()=>{
	//			btnEdit.onClick.Invoke();
				ChatController.main.ActiveView(VIEW_CHAT.EDIT);
	//			ChatController.main.ActiveView(VIEW_CHAT.MEMBERS);
				ChatController.main.SetLikeEdit(thisRomm );
				print("JUEPUTA AQUI DEBE SALIR");
			});

			titleBtns.backBtn.onClick.AddListener(()=>{
				ChatController.main.ActiveView(VIEW_CHAT.LISTS);
			});

			buttonSend.onClick.AddListener( PutLocalMessageUI);
		}
	}
}

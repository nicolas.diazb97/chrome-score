﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChatListsView : MonoBehaviour,IChatView {
	public CardRoom  cardPrefab;
	public Button newChat;
	public TitleBtns titleBtns;
	List<CardRoom> cards = new List<CardRoom>();
	public Button blocksBtn;
	public Button invitesBtn;
	List<int> actualMembersToAvoid = new List<int>();
	int lenghtOfRooms;

	public ScrollRankingGrupoScript objPanelRanking;


//	void OnEnable( ){
//		SetupView();
//	}

	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		RegisterListeners();
		ShowOwnRooms();
		titleBtns.SetStates(false, true, true);
		print("Aca se ejecuta el borrado");

	}

	void ShowOwnRooms(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			lenghtOfRooms = chatmbs.list.Count;
			CheckBlocksByMe(chatmbs);
		} ,PCI.CHATS_MEMBERS,parameters));
	}

	void CheckBlocksByMe(ChatsMembersList chatmbs) {
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		StartCoroutine(ApiChat.main.GET<BlockList>( myBlocks  => {
			foreach (Block b in myBlocks.list ) {
				actualMembersToAvoid.Add( b.BlockedMemberId );
			}
			PutOwnRooms(chatmbs);
		} , PCI.BLOCKS , parameters));
	}



	void PutOwnRooms(ChatsMembersList chatmbs){
		foreach (ChatsMembers cm in chatmbs.list) {
			if(cm.ChatId.ToString() !=  ApiChat.main.idMuroAmarillo){
				CheckSizeOfRoomAndInstantiate(cm.Chats);

			}
		}
	}


	void CheckSizeOfRoomAndInstantiate(Rooms _r){
		Parameter p1 = new Parameter("ChatId",_r.ChatId);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatmbs  => {
			// bool aviableRoom = true;
			int idOther = 0;
			if(chatmbs.list.Count == 2 ){
				foreach (ChatsMembers cm in chatmbs.list ) {
					if(cm.MemberId != ChatController.main.myUserIDchat  ){
						idOther = cm.MemberId;
					}
				}
				// print	("------- En "+_r.DisplayName+"  SOMOS: " + chatmbs.list[0].Members.DisplayName+ "  y  "+chatmbs.list[1].Members.DisplayName);
			}
			InstantiateCard(cardPrefab , _r, chatmbs.list.Count ,idOther);

			// print("______ OCURRE: "+cards.Count+"  y  "+lenghtOfRooms);
			// if(cards.Count == lenghtOfRooms){
			// 	foreach (CardRoom c in cards ) {
			//       		c.gameObject.SetActive(true);
			// 	}
			// }
		} ,PCI.CHATS_MEMBERS,parameters));
	}



	void InstantiateCard(CardRoom _c , Rooms _cid, int size, int _idOther){
		bool aviableRoom = true;
		if(size == 2 ){
			if(actualMembersToAvoid.Contains ( _idOther )){
				aviableRoom = false;
			}
		}

		if(aviableRoom){
			CardRoom card = Instantiate(_c) ;
			card.transform.SetParent (_c.transform.parent, false);
			cards.Add(card);
			card.gameObject.SetActive(true);
			card.SetValues( _cid, size);
			card.GetComponent<CardRoom>().panelRanking = objPanelRanking;
		}
	}

	public void DisposeView(){
		foreach (CardRoom c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		lenghtOfRooms =0;
		actualMembersToAvoid.Clear();
		cards.Clear();
		this.gameObject.SetActive(false);
		newChat.onClick.RemoveAllListeners();
		blocksBtn.onClick.RemoveAllListeners();
		invitesBtn.onClick.RemoveAllListeners();
	}

//	void OnDisable(){
//		DisposeView();
//	}

	void RegisterListeners(){
		newChat.onClick.AddListener( ()=> { 
			Debug.Log("new");
			ChatController.main.ActiveView(VIEW_CHAT.EDIT) ;
			New_EditChatView newView = (New_EditChatView)ChatController.main.actualView;
			newView.SetLikeNewView();
		} );


		blocksBtn.onClick.AddListener( ()=> { 
			ChatController.main.ActiveView(VIEW_CHAT.BLOCKS);
			titleBtns.SettingsBtn.isOn = false;
		} );

				
		invitesBtn.onClick.AddListener( ()=> { 
			ChatController.main.ActiveView(VIEW_CHAT.INVITES) ;
			titleBtns.SettingsBtn.isOn = false;

		} );

	}
}

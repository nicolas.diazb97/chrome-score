﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MembersView : MonoBehaviour,IChatView {

	public CardViewUserNC cardMember;
	public New_EditChatView viewEdit;
	public Button inviteButton;


	public void SetupView(){
		this.gameObject.SetActive(true);
		RegisterListenners();
	}

	void RegisterListenners(){
		inviteButton.onClick.AddListener(()=>{
			GetHisChatIdInServer();
		});	
	}


	void GetHisChatIdInServer(){
		ApiChat.main.StartCoroutine(ApiChat.main.GetChatIdInServer( _id => {
			print("con ProcessFrom GetIdInServer: "+_id);
			if(_id > 0){
				CheckIfIsBlockedByMe(_id);
			}else{
				ChatController.main.SetWarningMessage("Experimentamos problemas para enviar tu invitación.(10)");
			}
		} , cardMember.idUser ));
	}


	void CheckIfIsBlockedByMe(int _id) {
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		StartCoroutine(ApiChat.main.GET<BlockList>( myBlocks  => {
			print("con ProcessFrom CheckIfIsBlockedByMe: "+myBlocks.list.Count);

			bool isBlocked = false;
			foreach (Block b in myBlocks.list ) {
				print("b.BlockedMemberId:  "+b.BlockedMemberId  +" , _id: "+_id);
				if(b.BlockedMemberId == _id)
					isBlocked = true;
			}

			if(isBlocked){
				ChatController.main.SetWarningMessage("Este usuario fue anteriormente bloqueado, puedes desbloquearlo en el menú de Usuarios bloqueados.(20)");
			}else{
				CheckIfImBlockedByOther(_id);
			}
		} ,PCI.BLOCKS,parameters));
	}


	void CheckIfImBlockedByOther(int _id) {
		Parameter p1 = new Parameter("MemberId",_id);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);
		StartCoroutine(ApiChat.main.GET<BlockList>( hisBlocks  => {
			bool isBlocked = false;
			string otherName= "";
			foreach (Block b in hisBlocks.list ) {
				if(b.BlockedMemberId == ChatController.main.myUserIDchat)
					isBlocked = true;
			}
			if(isBlocked){
				ChatController.main.SetWarningMessage( otherName+ " no está disponible para recibir invitaciones.(21)");
			}else{
				CheckIfIsActualMember(_id);
			}
		} ,PCI.BLOCKS,parameters));
	}


	void CheckIfIsActualMember(int _id){
		Parameter p1 = new Parameter("MemberId",_id);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<ChatsMembersList>( chatsMems  => {
			print("Responde si es actual miembro: "+chatsMems.list.Count);
			if(chatsMems != null) {
				bool canBeInvited =true;
				foreach(ChatsMembers cm in chatsMems.list) {
					print("%%%%%%% COMPARA: " +cm.ChatId+" con "+ viewEdit.room.ChatId+"   %%%%%%%%%%");
					if(cm.ChatId == viewEdit.room.ChatId ){
						canBeInvited = false;
						print("ENCONTRADOOOO!!! (1)");
					}
				}
				if(canBeInvited){
					CheckIfAlreadyInvited(_id);
				}else{
					print("______ LLAma popUp (1)");
					ChatController.main.SetWarningMessage("Este usuario ya hace parte del grupo "+viewEdit.room.DisplayName+".");
				}
			}
		} ,PCI.CHATS_MEMBERS,parameters));
	}

	void CheckIfAlreadyInvited(int _id){
		Parameter p1 = new Parameter("MemberId",_id);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		ApiChat.main.StartCoroutine(ApiChat.main.GET<InvitationsList>( invites  => {
			print("Responde sus invitaciones con: "+invites.list.Count);
			if(invites != null) {
				bool canBeInvited =true;
				foreach(Invitations inv in invites.list) {
					print("//////////// COMPARA: " +inv.ChatId+" con "+ viewEdit.room.ChatId+"   ////////////%%%");
					if(inv.ChatId == viewEdit.room.ChatId ){
						canBeInvited = false;
						print("ENCONTRADOOOO!!! (2)");
					}
				}
				if(canBeInvited){
					SendInvitation(_id);
				}else{
					print("______ LLAma popUp (2)");
					ChatController.main.SetWarningMessage("Este usuario ya ha sido invitado al grupo "+viewEdit.room.DisplayName+".");
				}
			}
		} ,PCI.INVITATIONS,parameters));

	}

	void SendInvitation( int _id){
		print("Al menos alcanza a llegar hasta aqui");
		string json = JsonUtility.ToJson (new InvitationsCreator(  viewEdit.room.ChatId , _id  ));
		ApiChat.main.StartCoroutine(ApiChat.main.POST<InvitationsList>( invite => {
			if(invite == null){
				ChatController.main.SetWarningMessage("No es posible enviar una invitación a este usuario.");
			}else{
				cardMember.gameObject.SetActive(false);
				ChatController.main.ActiveView(VIEW_CHAT.EDIT);
				viewEdit.SetLikeEditView(viewEdit.room);
			}
		} ,PCI.INVITATIONS,json ));
	}

	public void DisposeView(){
		this.gameObject.SetActive(false);
		inviteButton.onClick.RemoveAllListeners();
	}

}


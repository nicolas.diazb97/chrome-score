﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class BlocksView : MonoBehaviour,IChatView {
	public CardBlocks cardPrefab;
	List<CardBlocks> cards = new List<CardBlocks>();

	// Use this for initialization
	public void SetupView () {
		this.gameObject.SetActive(true);
		ShowMyBlockeds();
	}

	void ShowMyBlockeds(){
		Parameter p1 = new Parameter("MemberId",ChatController.main.myUserIDchat);
		List <Parameter> parameters = new List<Parameter>();
		parameters.Add(p1);

		StartCoroutine(ApiChat.main.GET<BlockList>( blocks  => {
			if(blocks != null) {
				print(" USUARIO BLOQUEADO LLEGA: "+blocks.list.Count);
				PutMyBlockeds(blocks);
			}else{
			}
		} ,PCI.BLOCKS,parameters));
	}

	public void Reload(){
		DisposeView();
		SetupView();
	}

	void PutMyBlockeds(BlockList blocks) {
		foreach (Block blk in blocks.list) {
			InstantiateCard(cardPrefab, blk, blk.BLockedMembers.DisplayName, blk.BLockedMembers.MemberId);
		}
	}

	void InstantiateCard(CardBlocks _c, Block _block, string _nameRemote , int _idRemote){
        CardBlocks card = Instantiate(_c) ;
        card.transform.SetParent (_c.transform.parent, false);
        cards.Add(card);
        card.gameObject.SetActive(true);
		card.SetValues(_block,_nameRemote, _idRemote);
	}


	public void DisposeView(){
		foreach (CardBlocks c in cards) {
			Destroy(c.gameObject,0.03f);
		}
		cards.Clear();
		this.gameObject.SetActive(false);
	}
}

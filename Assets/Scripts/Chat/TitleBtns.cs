﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TitleBtns : MonoBehaviour {
	[SerializeField]
	public Button addMemberBtn;
	public Toggle SettingsBtn;
	public Button backBtn;
	
	public void SetStates(bool _b1, bool _b2, bool _b3){
		addMemberBtn.interactable = _b1;
		SettingsBtn.interactable = _b2;
		backBtn.interactable = _b3;
	}




}

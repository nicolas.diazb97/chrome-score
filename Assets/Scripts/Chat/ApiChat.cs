﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class Wrapper<T> {
	public T[] array;
}

public enum PCI : int{ 
	CHATS = 0,     CHATS_MEMBERS = 1,    MEMBERS= 2,   MESSAGES = 3, BLOCKS = 4 ,    INVITATIONS = 5 };

public class ApiChat :  Singleton<ApiChat>  {
	static string[] paths = new string [6]{
	"/api/Chats", "/api/ChatsMembers", "/api/Members", "/api/Messages", "/api/Blocks" , "/api/Invitations"
	};

	public  string host;
	public  string idMuroAmarillo;
	public GameObject cc;

	public override void MyAwake(){
		cc.SetActive(true);
	}


/////////////*********************GETS***********************/////////////////////////

	public IEnumerator GET<T>(System.Action<T> callback, PCI _pci, List<Parameter> _params) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		// print("api GET");
		string url = host+paths[(int)_pci]+BuildParams(_params);

		WWW consult = new WWW(url);
		yield return consult;
		T result =default (T);
		if (!string.IsNullOrEmpty(consult.error)) {
		} else {
    		result = JsonUtility.FromJson< T > ("{\"list\":"+consult.text+"}");
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(result);
	}

	public IEnumerator GETraw<T>(System.Action<T> callback, PCI _pci, string _param) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		// print("api GETraw");
		string url = host+paths[(int)_pci]+"/"+_param;

		WWW consult = new WWW(url);
		yield return consult;
		T result =default (T);
		if (!string.IsNullOrEmpty(consult.error)) {
		} else {
    		result =  JsonUtility.FromJson< T > ("{\"list\":["+consult.text+"]}");
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(result);
	}

	public IEnumerator GetHostOfChatAndIdMuroAmarillo(System.Action<string> callback){
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		print("api GetHostOfChatAndIdMuroAmarillo");
		WWW hs_getH = new WWW (ToAntiCache("http://www.2waysports.com/2waysports/Ecuador/Barcelona/Links/linksBarcelona.php?idLink=6"));
		yield return hs_getH;
		string result = "Experimentamos problemas de Conexión al Muro Amarillo!(6)";
		if (!string.IsNullOrEmpty(hs_getH.error)) {
		} else {
			if(!string.IsNullOrEmpty(hs_getH.text)){
				host = hs_getH.text.Split(',')[0];
				int number;
				if (hs_getH.text.Split(',').Length >1 ){
					if( int.TryParse(hs_getH.text.Split(',')[1], out number)){
						idMuroAmarillo = hs_getH.text.Split(',')[1];
						result = "";
					}else{
						idMuroAmarillo ="";
					}
				}else{
					result = "Nos encontramos en mantenimiento de nuestro Muro Amarillo!(7)";
				}
			} 
		}
		callback(result);
	}

	public IEnumerator GetChatIdInServer(System.Action<int> callback, string _id) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		WWWForm serverForm = new WWWForm();
		serverForm.AddField("userID", _id );
		// serverForm.AddField("userID", DataApp.main.GetMyID() );
		WWW serverId = new WWW("http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/UpadateIdServer.php", serverForm); 
		yield return serverId;
		int id = 0;
		if(string.IsNullOrEmpty(serverId.text)){
		}else{
			if( int.TryParse(serverId.text , out id)){
				Debug.Log("EL ID DEL USUARIO EN SERVIDOR ES: "+serverId.text);
			}else{
			}
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(id);
	}
	
	public IEnumerator GetInfoOfMember(System.Action<string> callback, string _id) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		WWWForm serverForm = new WWWForm();
		serverForm.AddField("userID", _id );
		// serverForm.AddField("userID", DataApp.main.GetMyID() );
		WWW serverId = new WWW("http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/InfoUser.php", serverForm); 
		yield return serverId;
		string result ="";

		if(string.IsNullOrEmpty(serverId.text)){

		}else{
			result = serverId.text;
		}
		callback(result);
	}

	public IEnumerator DownloadImageProfile ( System.Action<Sprite> callback, string _id ){
		//InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		string urlimg = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/" +_id+".jpg";

		WWW d_img = new WWW(urlimg);
		yield return d_img;
		Sprite memberSprite = null;
		Rect dim ;
		if(string.IsNullOrEmpty(d_img.error) && d_img.bytes.Length > 1699){
			dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			memberSprite = Sprite.Create(d_img.texture,dim, Vector2.zero);
		}else{
			urlimg = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/siluet.jpg";
			WWW d_imgSiluet = new WWW(urlimg);
			yield return d_imgSiluet;
			dim = new Rect(0,0,d_imgSiluet.texture.width,d_imgSiluet.texture.height);
			memberSprite = Sprite.Create(d_imgSiluet.texture,dim, Vector2.zero);
		}
		// InternetManager.main.loading.SetActive(false);
		callback(memberSprite);

	}


	

/////////////*********************POST***********************/////////////////////////

	public IEnumerator POST(System.Action<string> callback, PCI _pci, List<Parameter> _params)
    {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		print("api POST");
		WWWForm form = new WWWForm();
		foreach (Parameter p in _params) {
			form.AddField( p.name, p.value );
		}
		Dictionary<string,string> headers = form.headers;
		byte[] rawData = form.data;
		headers["Content-Type"] = "application/json" ;
		string url = host+paths[(int)_pci]+BuildParams(_params);

		WWW consult = new WWW(url,rawData,headers);
		yield return consult;
		string result = "";
		if (!string.IsNullOrEmpty(consult.error)) {
			Debug.LogError(consult.error+" de: "+consult.text);
		} else {
			result = consult.text;
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback( result);
	}

    
	public IEnumerator POST<T>(System.Action<T> callback, PCI _pci, string _json) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		print("api POST<T>");
		WWWForm form = new WWWForm();
		System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
		Dictionary<string,string> headers = form.headers;
		headers["Content-Type"] = "application/json" ;
		string url = host+paths[(int)_pci];

		WWW consult = new WWW(url,encoding.GetBytes(_json),headers);
		yield return consult;
		if (!string.IsNullOrEmpty(consult.error)) {
			// Debug.LogError("un pinche error de: "+consult.error);
			callback (default (T));
		} else {
			// Debug.LogWarning("LLEGA ESTA CHIMBADA \n "+consult.text);
    		callback( JsonUtility.FromJson< T > ("{\"list\":["+consult.text+"]}"));
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
	}

////////////////////////////////// ***************PUT**************************///////////////////////////////

	public IEnumerator PUT<T>(System.Action<T> callback, PCI _pci, List<Parameter> _params) {
		if(typeof(T) != typeof(MessagesList)){
//			InternetManager.main.loading.SetActive(true);
			DataApp2.main.EnableLoading();
		}
		// print("api PUT");
		string url = host+paths[(int)_pci]+BuildParams(_params);
		UnityWebRequest  consult = UnityWebRequest.Put(url,"1");
		// WWW consult = new WWW(url);
		yield return consult.Send();
		
		T result =default (T);
		if (!string.IsNullOrEmpty(consult.error)) {
		} else {
    		result = JsonUtility.FromJson< T > ("{\"list\":"+consult.downloadHandler.text+"}");
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(result);
	}

	public IEnumerator PUTraw(System.Action<string> callback, PCI _pci, string _params, string _json) {
	
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
	
		print("api PUT");
		string url = host+paths[(int)_pci]+"/"+_params;
		UnityWebRequest  consult = UnityWebRequest.Put(url,encoding.GetBytes(_json));
		consult.SetRequestHeader("Content-Type","application/json");
		// WWW consult = new WWW(url);
		yield return consult.Send();
		
		string result = "";

		if (!string.IsNullOrEmpty(consult.error)) {
			result = "error";
		} else {
    		result = "ok";
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(result);
	}
////////////////////////////////// ***************DELETE**************************///////////////////////////////

	public IEnumerator DELETEraw(System.Action<bool> callback, PCI _pci, string _params) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		print("api DELETE");
		string url = host+paths[(int)_pci]+"/"+_params;
		UnityWebRequest  consult = UnityWebRequest.Delete(url);
		// WWW consult = new WWW(url);
		yield return consult.Send();
		
		bool result =false;
		print(consult.responseCode);
		if(! (consult.responseCode >= 300 )){
			result = true ;
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(result);
	}

	public IEnumerator DELETE(System.Action<bool> callback, PCI _pci,  List<Parameter> _params) {
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		print("api DELETE");
		string url = host+paths[(int)_pci]+BuildParams(_params);;
		UnityWebRequest  consult = UnityWebRequest.Delete(url);
		// WWW consult = new WWW(url);
		yield return consult.Send();
		
		bool result =false;
		print(consult.responseCode);
		if(! (consult.responseCode >= 300 )){
			result = true ;
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
		callback(result);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	string BuildParams(List<Parameter> _params){
		string result="?";
		foreach (Parameter p in _params) {
			result += p.name+"="+p.value+"&";
		}
		result = result.Remove(result.Length - 1);
		return result;
	}

	public  string ToAntiCache( string url) {
		string r = "";
		r += UnityEngine.Random.Range(1000000,9000000).ToString();
		r += UnityEngine.Random.Range(1000000,9000000).ToString();
		string result="";
		if(url.Substring(url.Length -4,4   ) == ".php" || url.Substring(url.Length -4,4   ) == ".png"){
			result = url + "?key=" + r;
		}else{
			result = url + "&key=" + r;
		}
		return result;
	}

}


public class Parameter{
	public string name;
	public string value;
	public Parameter(string _n,string _v){
		name = _n;
		value = _v;
	}
	public Parameter(string _n,int _v){
		name = _n;
		value = _v.ToString();
	}

}

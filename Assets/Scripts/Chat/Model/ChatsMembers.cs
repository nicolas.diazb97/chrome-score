﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ChatsMembers  {
	public Rooms Chats;
	public Member Members;
	public int ChatMemberId;
	public int ChatId;
	public int MemberId;
	public string CreatedDate;

	public ChatsMembers(Rooms _r, Member _m ){
		Chats = _r;
		Members = _m;
		ChatId = _r.ChatId;
		MemberId = _m.MemberId;
	}
	// public ChatsMembersCreator GetCreator(){
	// 	return new ChatsMembersCreator(ChatId, MemberId);
	// }
	// public string SubmitedDate;
	// public static ChatsMembers CreateFromJSON(string jsonString) {
	// 	return JsonUtility.FromJson<ChatsMembers>(jsonString);
	// }
}

[System.Serializable]
public class ChatsMembersCreator  {
	public int ChatId;
	public int MemberId;

	public ChatsMembersCreator( int _cid, int _mid){
		ChatId = _cid;
		MemberId = _mid;
	}
}

[System.Serializable]
public class ChatsMembersList  {
	public List<ChatsMembers> list;
}

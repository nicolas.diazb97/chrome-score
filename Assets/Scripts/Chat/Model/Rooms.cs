using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Rooms  {

	public int ChatId;
	public int Owner;
	public string CreatedDate;
	public int ChatStateId;
	public string DisplayName;
	// public int Receiver;
	// public bool IsFile;
	// public string SubmitedDate;
	// public static Rooms CreateFromJSON(string jsonString) {
	// 	return JsonUtility.FromJson<Rooms>(jsonString);
	// }

}

[System.Serializable]
public class ChatsCreator  {
	public int Owner;
	public string DisplayName;

	public ChatsCreator( int _own, string _name){
		Owner = _own;
		DisplayName = _name;
	}


}

[System.Serializable]
public class ChatsEditCreator  {
	public int Owner;
	public string DisplayName;

	public int ChatId;
	public string CreatedDate;
	public int ChatStateId;


	public ChatsEditCreator(int _chtId, int _own, string _crDate, int _chtState, string _name) {
	  ChatId = _chtId;
	  Owner = _own;
	  CreatedDate = _crDate;
	  ChatStateId = _chtState;
	  DisplayName = _name;
	}
}

[System.Serializable]
public class RoomsList  {
	public List<Rooms> list;
}

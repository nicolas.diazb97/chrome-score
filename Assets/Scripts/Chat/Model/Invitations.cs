﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Invitations  {

	public Rooms Chats;
	public Member Members;
	public int InvitationId;
	public int ChatId;
	public int MemberId;
	public string CreatedDate;
	// public string SubmitedDate;
	public static Invitations CreateFromJSON(string jsonString) {
		return JsonUtility.FromJson<Invitations>(jsonString);
	}
}

[System.Serializable]
public class InvitationsCreator  {
	public int ChatId;
	public int MemberId;

	public InvitationsCreator( int _cid, int _mid){
		ChatId = _cid;
		MemberId = _mid;
	}
}

[System.Serializable]
public class InvitationsList  {
	public List<Invitations> list;
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[System.Serializable]
public class Member  {

	public int MemberId;
	public string DisplayName;
	public string CreatedDate;
	public int MemberStateId;
	// public string SubmitedDate;
	// public static Member CreateFromJSON(string jsonString) {
	// 	Debug.Log("para Vladimir: "+jsonString);
	// 	return JsonUtility.FromJson<Member>(jsonString);
	// }
}

[System.Serializable]
public class MembersList  {
	public List<Member> list;
}

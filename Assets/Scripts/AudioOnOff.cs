﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AudioOnOff : MonoBehaviour {

	public List <AudioSource> Sources;
	public static bool mute;
	public Toggle tt;
	public RectTransform circleToogle;
	public Sprite on, off;


	void OnEnable ( )
    {
		foreach ( AudioSource sc in Sources )
        {
			sc.mute = mute;
		}
        //
		tt.isOn = mute;
		changeAudio( tt );
	}

	public void changeAudio (Toggle ison )
    {
		mute = ison.isOn;
		foreach ( AudioSource sc in Sources )
        {
			sc.mute = mute;
		}

        if (on && off)
        {
            if (mute)
            {
                circleToogle.anchoredPosition = new Vector3(58.9f, 0, 0);
                circleToogle.GetComponent<Image>().sprite = off;
            }
            else
            {
                circleToogle.anchoredPosition = new Vector3(-55.9f, 0, 0);
                circleToogle.GetComponent<Image>().sprite = on;
            }
        }
	}
}

﻿using UnityEngine;
using System.Collections;

public class BonusMark : MonoBehaviour
{
	public GameObject ParticulasTocadas;
	public GoalDetermine _goal;
	public static bool IsTouchBonus;
	public AudioClip succes;
	// Use this for initialization
	
	void OnTriggerEnter(Collider col)
    {
		if(col.gameObject.tag == "Ball" && PlayerPrefs.GetInt("Tutorial") == 1){
			IsTouchBonus = true;
			Instantiate(ParticulasTocadas, transform.position,Quaternion.identity);
			StartCoroutine(_goal.SumarPuntaje(100));
			GetComponent<Collider>().enabled = false;
            foreach(Transform t in transform)
            {
                t.gameObject.SetActive(false);
            }
			ShootAI.shareAI.SourceM.PlayOneShot (succes,0.8f);
		} 
		
	}

	void OnTriggerExit(Collider col)
    {
		if(col.gameObject.tag == "Ball")
        {
			IsTouchBonus = false;	
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerLuka : MonoBehaviour
{
    public void EmpezarContrarreloj()
    {
        GlobalDataManager.instance.SeleccionaModalidad(ModalidadDeJuego.Contrarreloj);
    }

    public void EmpezarTirosLibres()
    {
        GlobalDataManager.instance.SeleccionaModalidad(ModalidadDeJuego.TirosLibres);
    }

    public void EmpezarRetoLuka(int modo)
    {
        switch (modo)
        {
            case 1:
                GlobalDataManager.instance.SeleccionaModalidad(ModalidadDeJuego.RetoLukaAros);
                break;
            case 2:
                GlobalDataManager.instance.SeleccionaModalidad(ModalidadDeJuego.RetoLukaBarrera);
                break;
        }
    }
}

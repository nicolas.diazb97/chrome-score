﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalData
{
    public static ModalidadDeJuego modalidadDeJuego = ModalidadDeJuego.Contrarreloj;
    public static Idioma idiomaActivo = Idioma.ES;
    public static string nickJugador = "";
    public static int puntajeDePartida = 0;
    public static bool tutorialTerminado = false;
}

public enum ModalidadDeJuego
{
    Contrarreloj,
    TirosLibres,
    RetoLukaAros,
    RetoLukaBarrera
}

public enum Idioma
{
    ES,
    EN
}

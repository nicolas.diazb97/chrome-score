﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class IntroManager : MonoBehaviour
{
    public VideoPlayer player;

    // Start is called before the first frame update
    void Start()
    {
        if (!player)
            player = GetComponent<VideoPlayer>();

        player.loopPointReached += EndReached;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) || Input.touchCount > 0)
        {
            EndReached();
        }
    }

    void EndReached()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuPrincipal");
    }

    void EndReached(VideoPlayer vp)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MenuPrincipal");
    }
}

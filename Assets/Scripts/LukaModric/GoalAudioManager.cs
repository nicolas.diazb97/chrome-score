﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalAudioManager : MonoBehaviour
{
    public List<AudioClip> audiosGoal = new List<AudioClip>();
    public List<AudioClip> audiosMiss = new List<AudioClip>();
    public List<AudioClip> audiosPost = new List<AudioClip>();
    AudioSource asource;

    #region Singleton

    public static GoalAudioManager instance;
    private void Awake()
    {
        if (instance)
            return;
        //
        instance = this;
    }

    private void Start()
    {
        asource = GetComponent<AudioSource>();
    }

    #endregion

    public void Goal()
    {
        asource.PlayOneShot(audiosGoal[Random.Range(0, audiosGoal.Count)]);
    }

    public void Miss()
    {
        asource.PlayOneShot(audiosMiss[Random.Range(0, audiosMiss.Count)]);
    }

    public void Post()
    {
        asource.PlayOneShot(audiosPost[Random.Range(0, audiosPost.Count)]);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    [Header("Paneles")]
    public int panelSeleccionado = 0;
    public List<GameObject> paneles = new List<GameObject>();

    // Update is called once per frame
    void Update()
    {
        if(panelSeleccionado != 0)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ShowPanel(0);
            }
        }
    }

    #region Paneles de Menu

    public void ShowPanel(int index)
    {
        foreach(GameObject p in paneles)
        {
            p.SetActive(false);
        }
        //
        paneles[index].SetActive(true);
        //
        panelSeleccionado = index;
    }

    #endregion
}

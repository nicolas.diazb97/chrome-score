﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalDataManager : MonoBehaviour
{
    #region Singleton

    public static GlobalDataManager instance = null;

    // Start is called before the first frame update
    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
            return;
        }
        //
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    #endregion
    
    #region Global Data

    public void SeleccionaModalidad(ModalidadDeJuego modalidadDeJuego)
    {
        GlobalData.modalidadDeJuego = modalidadDeJuego;
    }

    public void AsignarNombreUsuario(string nombreUsuario)
    {
        GlobalData.nickJugador = nombreUsuario;
    }

    public void ActualizarPuntaje(int puntaje)
    {
        GlobalData.puntajeDePartida = puntaje;
    }
    public void SumarPuntaje(int valor)
    {
        GlobalData.puntajeDePartida += valor;
    }
    public void RestarPuntaje(int valor)
    {
        GlobalData.puntajeDePartida -= valor;
    }
    public void ResetearPuntaje()
    {
        GlobalData.puntajeDePartida = 0;
    }

    #endregion
}

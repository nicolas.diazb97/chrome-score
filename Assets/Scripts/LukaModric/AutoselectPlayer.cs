﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoselectPlayer : MonoBehaviour
{
    public bool selectOnEnable = true;
    Animator anim;

    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        if(selectOnEnable)
        {
            anim.SetBool("JugadorSelec", true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

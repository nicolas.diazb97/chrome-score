﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LanguageSelector : MonoBehaviour
{
    public bool autoDetectSystemLanguage = false;
    public UnityEngine.UI.Text txtIdioma;
    string selectedLang = "ES";
    public Localization_SOURCE localization;

    // Start is called before the first frame update
    void Start()
    {
        selectedLang = GlobalData.idiomaActivo.ToString();
        if(autoDetectSystemLanguage)
        {
            if(Application.systemLanguage == SystemLanguage.Spanish)
            {
                LoadLanguage(Idioma.ES);
            }
            else
            {
                LoadLanguage(Idioma.EN);
            }
            return;
        }

        switch(selectedLang)
        {
            case "ES":
                LoadLanguage(Idioma.ES);
                break;
            case "EN":
                LoadLanguage(Idioma.EN);
                break;
        }
    }
    
    public void ToggleLanguaje()
    {
        if(selectedLang == "ES")
        {
            LoadLanguage(Idioma.EN);
        }
        else
        {
            LoadLanguage(Idioma.ES);
        }
    }

    void LoadLanguage(Idioma lang)
    {
        switch (lang)
        {
            case Idioma.ES:
                localization.PUBLIC_LoadLanguage(0);
                break;
            case Idioma.EN:
                localization.PUBLIC_LoadLanguage(1);
                break;
        }
        //
        GlobalData.idiomaActivo = lang;
        selectedLang = GlobalData.idiomaActivo.ToString();
        if (txtIdioma)
            txtIdioma.text = selectedLang;
        else
            Debug.LogWarning("No existe boton de cambio de idioma o no está asignado");
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class asignarNombreChatsAmisGrupos : MonoBehaviour {

	public Text _input;


	IEnumerator _asignarNombre ( Text input  ){
		yield return new WaitForEndOfFrame( );	
		input.text = _input.text;
	}

	public void AsignarNombreGrupo ( Text input ){
		StartCoroutine( _asignarNombre ( input )); 
	}
}

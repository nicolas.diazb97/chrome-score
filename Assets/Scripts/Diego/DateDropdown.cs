﻿using UnityEngine;
using System.Collections;
using MaterialUI;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class DateDropdown : MonoBehaviour {


	public MaterialDropdown d_day, d_year;
	public int initAge;

	int finalAge;
	int finalDay;


	void Start( ){
		CompleteYearDropdown ();
		CompleteDayDropdown ();
	}

	private void CompleteYearDropdown () {
		finalAge = DateTime.Now.Year;
		for(int i = finalAge; i >= initAge; i--){
			d_year.AddData( new OptionData( i.ToString(),null, () => {
//				Debug.Log("Agregado " + i);
			}));
		}
	}
		
	private void CompleteDayDropdown () {
		finalDay = 31;
		for(int i = 1; i <= finalDay; i++){
			d_day.AddData( new OptionData( i.ToString(),null, () => {
				//				Debug.Log("Agregado " + i);
			}));
		}
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class popUpInfo : MonoBehaviour {

	public Text titlePopup;
	public Text msgPopUp;


	void OnEnable( ){

		DataApp2.main.IsActivePopUpInfo = true;
	}

	void OnDisable( )
    {
		
		DataApp2.main.IsActivePopUpInfo = false;
	}

    public void ActivatePlayers()
    {
        if (PersonalizacionPersonaje.ThisCustom.tryJugador)
        {
            PersonalizacionPersonaje.ThisCustom.baseJugadores.SetActive(true);
            PersonalizacionPersonaje.ThisCustom.tryJugador = false;
        }
            
    }
}

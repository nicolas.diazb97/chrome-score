﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public enum idActualizacion
{

    tablas = 1,
    calendario = 2,
    noticias = 3,
    patrocinadores = 4,
    convocados = 5,
    alineacion = 6,

    userRanking = 7,
    userPrediction = 8,

    jugadores = 9,
    tuElijes = 10,
    infopopuop = 11,
    splash = 12
}

public class DataApp2 : MonoBehaviour
{

    public enum DateState : int { WAITING_DATE = 1, WAITING_TIME = 2, WAITING_DAY_MATCH = 3, IMMINENT_MATCH = 4, IN_MATCH = 5 };

    public static DataApp2 main;
    public string host = "";
    public string urlUserMetadata = "";
    public string urlActualizarInfo = "";
    public popUpInfo _PopUpInformative;
    public GameObject loadingPrincipal, popUpRegistroInfo, imgBienvenido;
    public bool IsActivePopUpInfo;
    public bool pago = false;
    public List<string> Directorys;

    public int versionApp;

    /**Eduardo Bedoya*/
    public Button LogOutBtn;
    /****************/


    void Awake()
    {
        print(Application.persistentDataPath);
        CreateDirectorys();
        //if( IsRegistered())
        //{
        //          LogOutBtn.interactable = true;
        //	Debug.Log("USER NUMBER -----> "+ GetMyID());
        //	StartCoroutine(setMyVersion(Application.version));
        //	StartCoroutine (GetPago());
        //}

        if (IsPhoneLog())
        {
            Debug.Log("Is Registered" + IsRegistered());
            LogOutBtn.interactable = true;
            Debug.Log("USER NUMBER -----> " + GetMyID());
            Debug.Log("USER PHONE NUMBER -----> " + PlayerPrefs.GetString("MyPhone"));
            StartCoroutine(setMyVersion(Application.version));
            StartCoroutine(GetThePayment());
        }
    }

    void Start()
    {
        if (IsRegistered())
        {
            LogOutBtn.interactable = true;
            User.main.reloadDataInUser();
        }
    }

    void Update()
    {
        if (IsRegistered() && !LogOutBtn.interactable)
        {
            LogOutBtn.interactable = true;
        }
    }

    IEnumerator setMyVersion(string version)
    {
        WWW getVersion = new WWW(DataApp2.main.host + "/ReadVersion.php?idUser=" + GetMyID());
        yield return getVersion;

        //si la version de la bd es diferente a la del app entonces actualice la version en la bd
        if (getVersion.text != version)
        {
            WWW v = new WWW(DataApp2.main.host + "/RegisterLogin.php?indata=changeVersion&iduser=" + GetMyID() + "&version=" + version);
            yield return v;
        }


        Gender sexo;
        if (User.main.GetMyGender() == "Femenino")
        {
            sexo = Gender.Female;
        }
        else
        {
            sexo = Gender.Male;
        }
        Analytics.SetUserGender(sexo);

    }

    // CREAR LOS DIRECTORIOS NECESARIOS EN LA APP PARA EL CACHE
    public void CreateDirectorys()
    {


        foreach (string dir in Directorys)
        {
            if (!Directory.Exists(Application.persistentDataPath + "/" + dir))
                Directory.CreateDirectory(Application.persistentDataPath + "/" + dir);
        }
    }


    #region Pagos
    public IEnumerator GetPago()
    {
        WWW urlPago = new WWW(DataApp2.main.host + "RegisterLogin.php?indata=consultarPago&idUser=" + GetMyID());

        yield return urlPago;
        Debug.Log("metodo pagar");
        if (urlPago.text != "no existe")
        {
            if (int.Parse(urlPago.text) == 1)
            {
                pago = true;
                // 1 para pago y 0 para no pago
                PlayerPrefs.SetInt("pago", int.Parse(urlPago.text));
                Debug.Log("El man esta suscrito");
                PersonalizacionPersonaje.ThisCustom.JugadorUnlock();
            }
            else
            {
                PlayerPrefs.SetInt("pago", int.Parse(urlPago.text));
                Debug.Log("no pago");

            }
        }
        else
        {
            Debug.Log("el usuario no existe");

        }

        yield return null;
    }

    public void GetPay(string _state)
    {

        if (_state == "TRUE")
        {
            pago = true;
            // 1 para pago y 0 para no pago
            PlayerPrefs.SetInt("pago", 1);
            Debug.Log("El man esta suscrito");
            PersonalizacionPersonaje.ThisCustom.JugadorUnlock();
        }
        else if (_state == "FALSE")
        {
            PlayerPrefs.SetInt("pago", 0);
            Debug.Log("no pago");

        }

    }

    public IEnumerator GetThePayment()
    {
        WWW urlPago = new WWW(DataApp2.main.host + "RegisterLogin.php?indata=ValidarNumero&numero=" + PlayerPrefs.GetString("MyPhone"));

        yield return urlPago;

        PhoneRegister _tempPhone = JsonUtility.FromJson<PhoneRegister>(urlPago.text);

        if (_tempPhone.dataList[0].pago == "TRUE")
        {
            pago = true;
            // 1 para pago y 0 para no pago
            PlayerPrefs.SetInt("pago", 1);
            Debug.Log("El man esta suscrito");
            PersonalizacionPersonaje.ThisCustom.JugadorUnlock();
        }
        else if (_tempPhone.dataList[0].pago == "FALSE")
        {
            PlayerPrefs.SetInt("pago", 0);
            Debug.Log("no pago");

        }

        yield return null;
    }
    #endregion


    // DATA DEL ID DEL USUARIO
    public void SetMyID(string id)
    {
        PlayerPrefs.SetInt("MyID", int.Parse(id));
    }

    public int GetMyID()
    {
        return PlayerPrefs.HasKey("MyID") ? PlayerPrefs.GetInt("MyID") : 0;
    }

    public bool IsRegistered()
    {
        int id = PlayerPrefs.GetInt("MyID");

        return PlayerPrefs.HasKey("MyID") && id > 0;
    }

    public bool IsPhoneLog()
    {
        return PlayerPrefs.HasKey("MyPhone");
    }

    /// DATA PARA GUARDARRRR ACTUALIZACION Y LLAVESS  GET -----  SET

    //variableType = 1 INT, 2 FLOAT, 3 String 
    public void SetMyInfo(string nameKey, string value, int variableType)
    {
        if (variableType == 1)
        {
            PlayerPrefs.SetInt(nameKey, int.Parse(value));
        }

        if (variableType == 2)
        {
            PlayerPrefs.SetFloat(nameKey, float.Parse(value));
        }

        if (variableType == 3)
        {
            PlayerPrefs.SetString(nameKey, value);
        }
    }

    public int GetMyInfoInt(string nameKey)
    {
        return PlayerPrefs.HasKey(nameKey) ? PlayerPrefs.GetInt(nameKey) : 0;
    }

    public float GetMyInfoFloat(string nameKey)
    {
        return PlayerPrefs.HasKey(nameKey) ? PlayerPrefs.GetFloat(nameKey) : 0;
    }

    public string GetMyInfoString(string nameKey)
    {
        return PlayerPrefs.HasKey(nameKey) ? PlayerPrefs.GetString(nameKey) : " ";
    }



    /// CHEQUEARR SI HAY INTERNETTT EN LA APP


    public IEnumerator CheckInternet(System.Action<bool> hasInternet)
    {
        bool result = false;

        string chcInternet = (host + "IsConnection.php");

        WWW getData = new WWW(chcInternet);
        yield return getData;

        Debug.Log("Internet State = " + getData.text);

        if (string.IsNullOrEmpty(getData.error))
        {
            result = true;
        }

        hasInternet(result);
    }



    public IEnumerator Consult(string link, Dictionary<string, string> addFields, System.Action<string> Rresult)
    {

        string result = null;
        string url = link;
        WWWForm form = new WWWForm();
        foreach (var field in addFields)
        {
            form.AddField(field.Key.Trim(), field.Value.Trim());
        }

        WWW newConsult = new WWW(WWW.UnEscapeURL(url), form);
        yield return newConsult;

        if (!string.IsNullOrEmpty(newConsult.error))
            result = newConsult.error;
        else
            result = newConsult.text;
        Rresult(result);

    }



    #region VERSION APP
    public void SetMyversion(string v)
    {
        PlayerPrefs.SetString("MyVersion", v);
    }

    public string GetMyversion()
    {
        return PlayerPrefs.HasKey("MyVersion") ? PlayerPrefs.GetString("MyVersion") : "";
    }

    public bool IsVersionRegister()
    {
        return PlayerPrefs.HasKey("MyVersion");
    }
    #endregion



    public void DeleteVar(string _name)
    {
        PlayerPrefs.DeleteKey(_name);
    }

    public void FinalSesion()
    {
        EnableLoading();
        StartCoroutine(_Finalsesion());
        PlayerPrefs.DeleteAll();
    }

    IEnumerator _Finalsesion()
    {
        EnableLoading();
        yield return new WaitForEndOfFrame();
        SceneManager.LoadSceneAsync("SeleccionColombia");
        //		NavigatorManager.main.saveIndex( 0,0,"Inicio" );
        //		DisableLoading();
    }




    public void SimpleActivePopUpInformative(bool act)
    {
        _PopUpInformative.gameObject.SetActive(act);
        IsActivePopUpInfo = act;
    }


    public void popUpInformative(bool active, string title, string msg)
    {
        Debug.LogWarning("Falla Aquie");
        _PopUpInformative.gameObject.SetActive(active);
        if (active)
        {
            _PopUpInformative.titlePopup.text = title;
            _PopUpInformative.msgPopUp.text = msg;
        }

        DisableLoading();
    }


    public IEnumerator _DisbleLoading()
    {
        yield return new WaitForSeconds(1);
        loadingPrincipal.SetActive(false);
    }



    public void ActivePopUpRegistroInfo(bool act)
    {
        popUpRegistroInfo.SetActive(act);
        IsActivePopUpInfo = act;
    }


    public void EnableLoading()
    {
        loadingPrincipal.SetActive(true);
    }


    public void DisableLoading()
    {
        loadingPrincipal.SetActive(false);
    }

}


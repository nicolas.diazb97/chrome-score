﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using MaterialUI;
using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Mail;
using System.Net;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
//using Facebook.Unity;

using UnityEngine.SocialPlatforms;
using System.Security.Principal;
using UnityEngine.SceneManagement;

[System.Serializable]
public class TempRegister
{
	public string pago;
	public int ID;
}

[System.Serializable]
public class PhoneRegister
{
	public TempRegister[] dataList;
}

enum optionSendEmail
{

	nosend = 0,
	send = 1,
	error = 2,
	cancel = 3
}

public enum EGender
{
	Masculino,
	Femenino
}

public class RegisterLoginManager : MonoBehaviour
{


	public static int panel;


	string urlRegisterAndLogin;
	//	[HideInInspector]
	public InputField t_name, t_lastname, t_email, t_indcel, t_cel, t_pass, t_confirmpass;
	//	[HideInInspector]
	public MaterialDropdown d_day, d_month, d_year;
	//	[HideInInspector]
	public MaterialDropdown d_country, d_city;
	//	[HideInInspector]
	public string gender;


	public GameObject panelRegister, PanelLogin, cityImage;

	public InputField login_email, login_pass, login_phone, index_phone;
	public Button LoginBtn, PhoneBtn;

	public Texture[] mailtextures = new Texture[3];
	optionSendEmail optMail = optionSendEmail.nosend;

	RegexUtilities utilmail = new RegexUtilities();
	public RawImage mailRaw;

	private string getUrlPos = "Analytics/PostInstalacionesDiarias.php";
	private int userID = 0;
	public EGender UserGender;

	void Awake()
	{

		//		if(DataApp.main.IsRegistered())
		//			FinalSesion.interactable = true;

		/*Facebook
		if (!FB.IsInitialized)
		{
			FB.Init(InitCallback, OnHideUnity);
		}
		else
		{
			FB.ActivateApp();
		}
		*/
	}



	void Start()
	{
		//		urlRegisterAndLogin = "http://localhost/GolOficial/PHP/RegisterLogin.php";
		urlRegisterAndLogin = "http://fcf.2waysports.com/goloficial/RegisterLogin.php";
		Debug.Log("url " + urlRegisterAndLogin);

		LoginBtn.onClick.AddListener(() => LoginUser(login_email, login_pass));
		PhoneBtn.onClick.AddListener(() => PhoneLogin(login_phone, index_phone));
		downloadImgMailing();
	}

	void Update()
	{
		if (d_country.currentlySelected != 0)
		{
			d_city.gameObject.SetActive(false);
			cityImage.SetActive(false);
		}
		else
		{
			d_city.gameObject.SetActive(true);
			cityImage.SetActive(true);
		}
	}

	#region FB METHODS
	private void InitCallback()
	{
		/*Facebook
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
			//DebugConsole.Log("Facebook SDK Activation");
			Debug.Log("Facebook SDK Activation");
		}
		else
		{
			//DebugConsole.Log("Could not initalize");
			Debug.LogError("Could not initalize");
		}
		*/
	}

	private void OnHideUnity(bool isGameShown)
	{
		if (!isGameShown)
		{
			Time.timeScale = 0;
		}
		else
		{
			Time.timeScale = 1;
		}
	}


	public void CallFBLogin()
	{
		/*Facebook
		var perms = new List<string>() { "public_profile", "email" };
		FB.LogInWithReadPermissions(perms, HandleResult);
		DataApp2.main.EnableLoading();
		*/
	}


	private void FetchFBProfile()
	{
		/*Facebook
		FB.API("/me?fields=first_name,last_name,email", HttpMethod.GET, FetchProfileCallback);
		*/
	}

	/*Facebook
	private void HandleResult(ILoginResult result)
	{

		Debug.Log("logeado? :  " + FB.IsLoggedIn);
		if (FB.IsLoggedIn)
		{
			AccessToken aToken = AccessToken.CurrentAccessToken;

			FetchFBProfile();
			//DataApp2.main.popUpInformative(true, result.RawResult , "Logeando");
		}
		else
		{
			DataApp2.main.DisableLoading();
			Debug.Log("User cancelled login HANDLE RESULT");
			//			DataApp2.main.popUpInformative(true, result.Error , "User cancelled login HANDLE RESULT");
			DataApp2.main.popUpInformative(true, "Esta cuenta de Facebook ya ha sido ragistrada", "");
			//  return;
		}
	}
	*/

    /*Facebook
    private void FetchProfileCallback(IResult result)
    {

    string mailResponse = "";
    string nameResponse = "";
    string lnameResponse = "";

    if (result.Error == null)
    {

	    Debug.Log("Obtain: " + result.RawResult);
	    //DataApp2.main.popUpInformative(true, result.RawResult , "");

	    Dictionary<string, object> FBUserDetails = (Dictionary<string, object>)result.ResultDictionary;
	    mailResponse = FBUserDetails["email"].ToString();
	    nameResponse = FBUserDetails["first_name"].ToString();
	    lnameResponse = FBUserDetails["last_name"].ToString();
	    StartCoroutine(confirmDatasAndMailFb(true, mailResponse, nameResponse, lnameResponse));
    }
    else
    {
	    Debug.Log("Error FetchProfileCallback");
    }
    }
    */
    #endregion
    IEnumerator PostNewUser()
    {

    int plataform = 0;

    #if UNITY_ANDROID
	    plataform = 1;
    #elif UNITY_IOS
    plataform = 2;
    #endif

    DateTime nt = DateTime.Now;
    string fechaInit = nt.Year + "-" + nt.Month + "-" + nt.Day;

    string tk = TokenGenerator();

    int month = parseMonth(d_month.buttonTextContent.text);

    int device = 0;

    if (Application.platform == RuntimePlatform.Android)
	    device = 1;
    else if (Application.platform == RuntimePlatform.IPhonePlayer)
	    device = 2;
    else
	    device = 0;

    StartCoroutine(DataApp2.main.Consult(urlRegisterAndLogin, new Dictionary<string, string>{
	    {"indata","nuevoUsuario"},
	    {"nombre",t_name.text},
	    {"apellido",t_lastname.text},
	    {"correo",t_email.text},
	    {"pass",t_pass.text},
	    {"indcelular",t_indcel.text},
	    {"celular",t_cel.text},
	    {"paisid",d_country.currentlySelected.ToString()},
	    {"pais",d_country.buttonTextContent.text},
	    {"ciudadid",d_city.currentlySelected.ToString()},
	    {"ciudad",d_city.buttonTextContent.text},
	    {"fechaNacimiento",d_year.buttonTextContent.text+"-"+month+"-"+d_day.buttonTextContent.text},
	    {"plataforma", device.ToString()},
	    {"app",1.ToString()},
	    {"genero",UserGender.ToString()},
	    {"version",Application.version},
	    {"token",tk},
	    {"fechainit",fechaInit},

    }, result => {
	    Debug.Log("PUTO R: " + result);
	    int number;
	    bool result_ = Int32.TryParse(result, out number);
	    if (!string.IsNullOrEmpty(result) && result_)
	    {
		    Debug.Log("REGISTRADO");
		    DataApp2.main.SetMyID(result);
		    User.main.SetMyFechaIncial(fechaInit);
		    User.main.SetMyNumH(result);
		    User.main.SetMyBirthday(d_year.buttonTextContent.text + "-" + month + "-" + d_day.buttonTextContent.text);
		    //SendEmailto( t_email.text, tk, false );
		    mailSent = true;
		    StartCoroutine(OpenOptionEmailVerification(mailSent));
		    SetDatesUser(t_name.text, t_lastname.text, t_email.text, t_pass.text, t_indcel.text, t_cel.text, d_country.currentlySelected, d_city.currentlySelected, d_day.currentlySelected, d_month.currentlySelected, d_year.currentlySelected, plataform, 1, result);
		    User.main.SetMyToken(tk);
		    panelRegister.SetActive(false);
		    //EnviarA();
		    //guardar en la tabla puntajes
		    StartCoroutine(GuardarEnTablaPuntajes(DataApp2.main.GetMyID(), t_name.text));
		    StartCoroutine(DataApp2.main.GetPago());
	    }
	    else
	    {
		    //ToastManager.Show("Error al Registrarte, Intentalo de nuevo.",5f,null);
		    DataApp2.main.DisableLoading();
	    }
    }));
    yield return new WaitForEndOfFrame();


    }

    private IEnumerator GuardarEnTablaPuntajes(int idUsuario, String nombre)
    {
        WWWForm formUser = new WWWForm();
        formUser.AddField("indata", "nuevoUsuario");
        formUser.AddField("idUser", idUsuario);
        formUser.AddField("nombre", nombre);
        formUser.AddField("puntaje", 0);
        WWW getData = new WWW(DataApp2.main.host + "TraerYActualizarPuntaje.php?", formUser);
        Debug.Log("URL: " + DataApp2.main.host + "TraerYActualizarPuntaje.php?" + formUser);
        yield return getData;
        Debug.Log("GETDATA: " + getData.text);
        if (string.IsNullOrEmpty(getData.error))
        {
	        if (getData.text != "Usuario ya registrado")
	        {
		        Debug.Log("usuario agregado a la tabla puntajes");
	        }
        }
        else
        {
	        DataApp2.main.popUpInformative(true, "Fallo en la conexíon.", "Revisa tu conexión a internet.");
        }
    }

    int parseMonth(string month)
    {
        int rMonth = 0;
        rMonth = (month == "ENE") ? 1 : (month == "FEB") ? 2 : (month == "MAR") ? 3 : (month == "ABR") ? 4 : (month == "MAY") ? 5 : (month == "JUN") ? 6 : (month == "JUL") ? 7 : (month == "AGOS") ? 8 : (month == "SEP") ? 9 : (month == "OCT") ? 10 : (month == "NOV") ? 11 : 12;
        return rMonth;
    }

    string saveFechaInicial()
    {
        DateTime dt = new DateTime();
        dt = DateTime.Now;
        string date = (dt.Month == 1) ? "ENE" : (dt.Month == 2) ? "FEB" : (dt.Month == 3) ? "MAR" : (dt.Month == 4) ? "ABRIL" : (dt.Month == 5) ? "MAY" : (dt.Month == 6) ? "JUN" : (dt.Month == 7) ? "JUL" : (dt.Month == 8) ? "AGOS" : (dt.Month == 9) ? "SEPT" : (dt.Month == 10) ? "OCT" : (dt.Month == 11) ? "NOV" : "DIC";
        return date + " " + dt.Year;
    }

    void SetDatesUser(string name, string lname, string email, string pass, string indcel, string cel, int country, int city, int day, int month, int year, int plataform, int app, string gender)
    {

        User.main.SetMyName(name);
        User.main.SetMyLastName(lname);
        User.main.SetMyEmail(email);
        User.main.SetMyPass(pass);
        User.main.SetMyIndcel(indcel);
        User.main.SetMyCel(cel);
        User.main.SetMyCountry(country);
        User.main.SetMyCity(city);
        User.main.SetMyDayId(day);
        User.main.SetMyMonthId(month);
        User.main.SetMyYearId(year);
        User.main.SetMyGender(gender);
        User.main.reloadDataInUser();

        StartCoroutine(analyticsInstalacionesDiarias(name, email));
    }

    private IEnumerator analyticsInstalacionesDiarias(string name, string email)
    {
        userID = DataApp2.main.GetMyID();
        WWWForm form = new WWWForm();
        form.AddField("userID", userID);
        form.AddField("correo", email);
        form.AddField("name", name);
        form.AddField("version", DataApp2.main.versionApp);
        WWW www = new WWW(DataApp2.main.host + getUrlPos, form);
        yield return www;

        print(www.text);
    }

    void SendEmailto(string emailreceiver, string tk, bool rv)
    {
        string sender = "programacion.wise@wiseinmedia.com";
        string smtpPassword = "wise1234";
        string smtpHost = "smtp.gmail.com";


        //string pathHeader = Application.dataPath +"/Mail/header.png";
        string[] attachmentPath = new string[1];
        attachmentPath[0] = Application.dataPath + "/Mail/body.jpg";

        MailMessage mail = new MailMessage();
        mail.IsBodyHtml = true;
        //Debug.Log("LINK DE LA IMAGEN" +  attachmentPath);
        mail.AlternateViews.Add(getImageBody(null, attachmentPath, tk));
        mail.From = new MailAddress(sender);
        mail.Subject = "Selección Colombia App";
        mail.Body = "Welcome to Selección Colombia Oficial";
        mail.BodyEncoding = System.Text.Encoding.UTF8;
        mail.To.Add(emailreceiver);

        var smtpServer = new SmtpClient()
        {
	        Host = "smtp.gmail.com",
	        EnableSsl = true,
	        Port = 25,
	        DeliveryMethod = SmtpDeliveryMethod.Network,
	        Credentials = (ICredentialsByHost)new NetworkCredential(sender, smtpPassword)
        };

        smtpServer.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate ICertificatePolicy, X509Chain chain, SslPolicyErrors ssPolicyErrors) { return true; };
        string userState = "..." + t_email.text;
        smtpServer.SendAsync(mail, userState);
        mail.Dispose();
        StartCoroutine(asynFailEmail());
        StartCoroutine(OpenOptionEmailVerification(rv));
    }

    void downloadImgMailing()
    {
        //print(ImgLoadManager.main.name);
        mailtextures[0] = ImgLoadManager.main.MailImg(mailRaw, "body", false);
        //mailtextures[1] = ImgLoadManager.main.MailImg(mailRaw,"body",false);
    }

    bool mailSent = false;
    private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        String token = (string)e.UserState;
        if (e.Cancelled)
        {
	        Debug.Log("Send canceled to" + token);
        }
        optMail = optionSendEmail.cancel;
        if (e.Error != null)
        {
	        Debug.Log("msg error to" + token);
	        optMail = optionSendEmail.error;
        }
        else
        {
	        Debug.Log("msg sent to" + token);
	        optMail = optionSendEmail.send;
        }
        mailSent = true;
    }

    IEnumerator asynFailEmail()
    {
        for (int i = 0; i < 10; i++)
        {
	        Debug.Log(": " + mailSent + " en seg:  " + i);
	        if (mailSent)
		        break;
	        if (i == 6)
	        {
		        SnackbarManager.Show("El registro esta demorando más de lo normal, Por favor Espere", 5f, "Continuar", () =>
		        {
			        SnackbarManager.OnSnackbarCompleted();
		        });
	        }
	        yield return new WaitForSecondsRealtime(1);
        }

        if (!mailSent)
        {
	        //DataApp.main.popUpInformative (true, "Error al registrar su correo", "Intenta más tarde re-enviar su correo de activación.");
	        yield return new WaitForSecondsRealtime(1);
	        mailSent = true;
        }

    }

    IEnumerator OpenOptionEmailVerification(bool reenviar)
    {
        yield return new WaitUntil(() => mailSent);
        //mailSent = false;
        if (DataApp2.main.IsRegistered())
        {
	        if (reenviar)
		        StartCoroutine(User.main.getDatasUser(DataApp2.main.host + "UserData.php?indata=userDatas&idUser=" + DataApp2.main.GetMyID(), "registro"));
	        //				StartCoroutine( User.main.getDatasUser(DataApp2.main.host + "Registro/userDatas.php?indata=userDatas&idUser=" + DataApp2.main.GetMyID(), "registro"));

	        if (optMail == optionSendEmail.error)
		        ToastManager.Show("Se ha producido un Error al enviar el correo electronico", 5f, null);
	        else if (optMail == optionSendEmail.send && !reenviar)
		        DataApp2.main.ActivePopUpRegistroInfo(true);

	        ToastManager.Show("Registro éxitoso. Bienvenido", 5f, null);
	        StartCoroutine(EnablBtnVerif());


        }
        DataApp2.main.DisableLoading();
    }

    IEnumerator EnablBtnVerif()
    {
        yield return new WaitForSecondsRealtime(8f);
        for (float i = 0.0f; i <= 1.0f; i += 0.01f)
        {
	        yield return new WaitForEndOfFrame();
	        //verificarButton.alpha = i ;
        }
    }

    public void OpenMail()
    {
        bool isSave = false;
        string stmp = null;
        string mail = User.main.GetMyEmail();
        for (int i = 0; i < mail.Length; i++)
        {
	        if (isSave == true)
		        stmp += mail[i];
	        if (mail[i] == '@')
		        isSave = true;
        }
        Application.OpenURL("https://www." + stmp);
        Debug.Log("EMAIL DE : " + stmp);
    }

    private AlternateView getImageBody(string pathHeader, string[] filePath, string token)
    {
        List<LinkedResource> inlines = new List<LinkedResource>(0);
        LinkedResource inline;
        //BODY INIT

        //		string linkValid = DataApp2.main.host + "Registro/registerandlogin.php?indata=changeMail&iduser=" + DataApp2.main.GetMyID () + "&token=" + token;
        string linkValid = DataApp2.main.host + "registro/registerandlogin.php?indata=changeMail&iduser=" + DataApp2.main.GetMyID() + "&token=" + token;

        string htmlBody = null;
        if (!string.IsNullOrEmpty(pathHeader))
        {
	        inline = new LinkedResource(pathHeader);
	        inline.ContentId = Guid.NewGuid().ToString();
	        inlines.Add(inline);
	        htmlBody += @"<body style=""background-color:white;""</body> <center > < a href = "" //www.fcf.com.co/""><img  src='cid:" + inline.ContentId + @"'/></a></center><center > < br > < font face = ""verdana"" color = ""black"" > < h2 > Bienvenido, < b > " + t_name.text + " " + t_lastname.text + @" < /b> </font > < br / > < /h2></center >";
        }
        else
        {
	        htmlBody += @"<body style=""background-color:white;""</body> <center > < br > < font face = ""verdana"" color = ""black"" > < h2 > Bienvenido, < b > " + t_name.text + " " + t_lastname.text + @" < /b> </font > < br / > < /h2></center > ";
        }

        foreach (string path in filePath)
        {
	        inline = new LinkedResource(path);
	        inline.ContentId = Guid.NewGuid().ToString();
	        inlines.Add(inline);
        }

        int countInLines = 0;
        foreach (LinkedResource inl in inlines)
        {
	        countInLines++;
	        if ((!string.IsNullOrEmpty(pathHeader) && countInLines == 1))
	        { }
	        else
	        {
		        htmlBody += @"<center><a href=" + linkValid + @"><img  src='cid:" + inl.ContentId + @"'/></a></center>";
		        htmlBody += @"<center><p><h5> </h5></p></center>";
	        }
        }

        htmlBody += @"<br><br/> <
	        center > < p > < h5 > < font face = ""verdana"" color = ""gray"" > < a href = "" //www.fcf.com.co/""> Políticas de Privacidad </a> </h5></p></center>
	        <
	        center > < p > < h5 > < font face = ""verdana"" color = ""gray"" > Federación Colombiana de Futbol, Inc.·Bogotá, Colombia < /font></h
        5 > < /p></center > ";

        AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);

        //		<center><b> <h2>Para terminar tu proceso de registro, Haz click<a href=+ linkValid +@> AQUI </a></h2></b></center>
        foreach (LinkedResource inl in inlines)
        {
	        alternateView.LinkedResources.Add(inl);
        }
        return alternateView;
    }



    public void PostUser()
    {
        DataApp2.main.EnableLoading();
        _validDatas();
    }


    void _validDatas()
    {
        bool validDatas = false;

        if (string.IsNullOrEmpty(t_name.text))
        {
	        DataApp2.main.popUpInformative(true, "Nombre no válido", "Ingresa tu nombre.");
        }
        else if (string.IsNullOrEmpty(t_lastname.text))
        {
	        DataApp2.main.popUpInformative(true, "Apellido no válido", "Ingresa tu apellido.");
        }
        else if (!utilmail.IsValidEmail(t_email.text))
        {
	        DataApp2.main.popUpInformative(true, "Correo no válido", "Tu correo electrónico no existe o no es correcto.");
        }
        else if (string.IsNullOrEmpty(t_pass.text))
        {
	        DataApp2.main.popUpInformative(true, "Contraseña no válida", "Ingresa tu contraseña.");
        }
        else if (string.IsNullOrEmpty(t_confirmpass.text) || t_confirmpass.text != t_pass.text)
        {
	        DataApp2.main.popUpInformative(true, "Contraseña no válida", "Las contraseñas no coincide.");
        }
        else if (t_pass.text.Length < 6)
        {
	        DataApp2.main.popUpInformative(true, "Contraseña no válida", "La contraseña debe tener minimo 6 digitos.");
        }
        else if (string.IsNullOrEmpty(t_indcel.text))
        {
	        DataApp2.main.popUpInformative(true, "Indicativo no válido", "Ingresa el número indicativo de tu país.");
        }
        else if (string.IsNullOrEmpty(t_cel.text))
        {
	        DataApp2.main.popUpInformative(true, "Celular no válido", "Ingresa tu número de celular.");
        }
        else if (d_country.currentlySelected == -1)
        {
	        DataApp2.main.popUpInformative(true, "País no válido", "Selecciona tu país de residencia.");
        }
        else if (d_city.currentlySelected == -1 && d_country.currentlySelected == 0)
        {
	        DataApp2.main.popUpInformative(true, "Ciudad no válida", "Selecciona tu ciudad de nacimiento.");
        }
        else if (d_day.currentlySelected == -1)
        {
	        DataApp2.main.popUpInformative(true, "Día no válido", "Ingresa el día de tu nacimiento.");
        }
        else if (d_month.currentlySelected == -1)
        {
	        DataApp2.main.popUpInformative(true, "Mes no válido", "Ingresa el mes de tu nacimiento.");
        }
        else if (d_year.currentlySelected == -1)
        {
	        DataApp2.main.popUpInformative(true, "Año no válido", "Ingresa el año de tu nacimiento.");
        }
        else if (string.IsNullOrEmpty(gender))
        {
	        DataApp2.main.popUpInformative(true, "Género no válido", "Selecciona tu género.");
        }
        else
        {
	        validDatas = true;
	        StartCoroutine(DataApp2.main.CheckInternet(internet =>
	        {
		        if (internet)
			        StartCoroutine(confirmDatasAndMail(validDatas));
		        else
			        DataApp2.main.popUpInformative(true, "Fallo en la conexíon.", "Revisa tu conexión a internet.");
	        }));
        }
    }


    public void ChangeGender(Toggle g)
    {
        //		if(g.isOn )
        //		{
        //			gender = g.name;
        //
        //		}

        //		UserGender = (g.isOn) ? EGender._Masculino : EGender._Femenino;
        //		gender = (g.isOn)?"Masculino":"Femenino";
        }

        public void LoginUser(InputField mail, InputField pass)
        {
        DataApp2.main.EnableLoading();
        StartCoroutine(DataApp2.main.CheckInternet(internet =>
        {

	        if (internet)
	        {
		        Debug.Log("Hasta aca hay internet");

		        if (utilmail.IsValidEmail(mail.text))
			        StartCoroutine(_loginUser(mail.text, pass.text));
		        else
			        DataApp2.main.popUpInformative(true, "Correo no válido", "Tu correo electrónico no es correcto.");
	        }
	        else
	        {
		        Debug.Log("Aca ya fallo internet");
		        DataApp2.main.popUpInformative(true, "Fallo en la conexíon.", "Revisa tu conexión a internet.");
	        }

        }));
    }

    IEnumerator _loginUser(string mail, string pass)
    {
        WWW getId = new WWW(urlRegisterAndLogin + "?indata=login&correo=" + mail + "&pass=" + pass);
        yield return getId;

        print(getId.text);
        string answerMail = getId.text.Trim();

        if (string.IsNullOrEmpty(getId.error))
        {
	        Debug.Log("Hasta aca hay internet");

	        if (answerMail.Equals("notFound"))
	        {
		        DataApp2.main.popUpInformative(true, "Correo o contraseña incorrecta", "Por favor verifique");
	        }
	        else
	        {
		        yield return StartCoroutine(User.main.getDatasUser(DataApp2.main.host + "UserData.php?indata=userDatas&idUser=" + answerMail, "login"));
		        Debug.Log("termino de consultar datos");
		        DataApp2.main.SetMyID(answerMail);

		        Debug.Log("nombre usuario " + User.main.GetMyName() + " id usuario " + DataApp2.main.GetMyID());
		        StartCoroutine(DataApp2.main.GetPago());
		        StartCoroutine(GuardarEnTablaPuntajes(DataApp2.main.GetMyID(), User.main.GetMyName()));

		        DataApp2.main.DisableLoading();
		        PanelLogin.SetActive(false);
		        //enviar hacia el respectivo lugar ctm
		        //EnviarA();
	        }
        }
        else
        {
	        Debug.Log("Aca ya fallo internet");
	        DataApp2.main.popUpInformative(true, "Fallo en la conexíon.", "Revisa tu conexión a internet.");
        }
        //DataApp.main.DisableLoading();
    }

    private void EnviarA()
    {
        if (panel == 1)
        {
	        SceneManager.LoadScene(1);
        }

        if (panel == 2)
        {
	        PersonalizacionPersonaje.ThisCustom.panelTiros.SetActive(true);
        }

        if (panel == 3)
        {
	        PersonalizacionPersonaje.ThisCustom.panelRetos.SetActive(true);
        }

        if (panel == 4)
        {
			
	        TransactionalProcess.thisTransaction.Ranking.SetActive(true);
	        t_cel.text = PlayerPrefs.GetString("MyPhone");
        }

        if (panel == 5)
        {
	        SceneManager.LoadScene("TiroTriColor");
        }
    }

    private IEnumerator confirmDatasAndMailFb(bool validDts, string mail, string name, string lname)
    {
        if (validDts)
        {
	        DataApp2.main.EnableLoading();
	        WWW getMail = new WWW(urlRegisterAndLogin + "?indata=validarMail&correo=" + mail);
	        yield return getMail;
	        print("correo fb" + getMail.text);
	        string answerMail = getMail.text.Trim();
	        //si no existe el mail guarde los datos en la base de datos
	        if (string.IsNullOrEmpty(getMail.error))
	        {
		        if (answerMail.Equals("") == true)
		        {
			        t_email.text = mail;
			        t_name.text = name;
			        t_lastname.text = lname;
			        DataApp2.main.popUpInformative(true, "Rellena los datos faltantes.", "");
			        //						ActivatePanelLogin(true);
		        }
		        else
		        {
			        login_email.text = mail;
			        //						NavigatorManager.main.saveIndex( 14, 1, "");
			        //						NavigatorManager.main.panelsPrincipales[NavigatorManager.main.actualPanel]._enablePopUpInfoPanel( 0 );
		        }
	        }
	        else
	        {
		        DataApp2.main.popUpInformative(true, "Fallo en la conexíon.", "Revisa tu conexión a internet.");
	        }
	        DataApp2.main.DisableLoading();
        }
    }

    public void ContinueApp()
    {
        SnackbarManager.OnSnackbarCompleted();
        NavigatorManager.main.HistorialClear();
        NavigatorManager.main.saveIndex(0, 0, "Inicio");
        }

        public string TokenGenerator()
        {
        string tk = "";
        for (int i = 0; i < 9; i++)
        {
	        int type = UnityEngine.Random.Range(0, 2);
	        if (type == 1)
	        {
		        tk += UnityEngine.Random.Range(1, 9);
	        }
	        else
	        {
		        char c = (char)UnityEngine.Random.Range(65, 90); tk += c;
	        }
        }
        return DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Minute + "-" + tk;
    }

    private IEnumerator confirmDatasAndMail(bool validDts)
    {
        if (validDts)
        {

	        DataApp2.main.EnableLoading();
	        string userMail = t_email.text;
	        Debug.Log(userMail);
	        WWW getMail = new WWW(urlRegisterAndLogin + "?indata=validarMail&correo=" + userMail);
	        yield return getMail;
	        print(getMail.text);
	        string answerMail = getMail.text.Trim();
	        //si no existe el mail guarde los datos en la base de datos
	        if (answerMail.Equals("") == true)
	        {
		        StartCoroutine(PostNewUser());
	        }
	        //si existe el mail notifiquecelo al usuario
	        else
	        {
		        DataApp2.main.popUpInformative(true, "Este correo ya esta registrado", "Intenta registrarte con otro correo.");
		        t_email.text = "";
		        DataApp2.main.DisableLoading();
	        }

        }
    }

    IEnumerator validEmailVerification()
    {
        if (DataApp2.main.IsRegistered() && User.main.GetMyEmailVerif() == 0)
        {
	        string userMail = t_email.text;
	        WWW emailV = new WWW(urlRegisterAndLogin + "?indata=verificarMail&idUser=" + DataApp2.main.GetMyID());
	        yield return emailV;
	        if (string.IsNullOrEmpty(emailV.error))
	        {
		        if (!string.IsNullOrEmpty(emailV.text) && emailV.text != "fail")
		        {
			        User.main.SetMyEmailVerif(int.Parse(emailV.text));
			        User.main._emailV = User.main.GetMyEmailVerif();
			        if (User.main.GetMyEmailVerif() == 1)
			        {
				        // DESACTIVAR LOS BOTONES(VERIFICAR Y CONTINUAR) CUANDO REGRESA A FOCO; 
				        ToastManager.Show("Tu cuenta ha sido verificado exitosamente\nBienvenido a Selección Colombia Oficial");
				        yield return new WaitForSecondsRealtime(3);
				        DataApp2.main.EnableLoading();
				        DataApp2.main.ActivePopUpRegistroInfo(false);
				        yield return new WaitForSecondsRealtime(2);
				        DataApp2.main.DisableLoading();
			        }
		        }
	        }
        }
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus && DataApp2.main.IsRegistered() && User.main.GetMyEmailVerif() == 0)
        {
	        // HACER EL LLAMADO PARA VERIFICAR SI VALIDO LA CUENTA O NO;
	        StartCoroutine(validEmailVerification());
	        Debug.Log("SALIO DE PAUSA");
        }
        else
        {
	        Debug.Log("EN PAUSA");
        }
    }

    bool bGender = false;

    public void GenderMale(Toggle _toggle)
    {
        UserGender = (_toggle.isOn) ? EGender.Masculino : EGender.Femenino;
        }

        public void GenderFemale(Toggle _toggle)
        {
        UserGender = (_toggle.isOn) ? EGender.Femenino : EGender.Masculino;
        }

        #region PhoneLogin
        [Header("Phone Registration")]
        public PhoneRegister m_PhoneRegister;

        public void PhoneLogin(InputField _phoneField, InputField _indexField)
        {
        DataApp2.main.EnableLoading();
        StartCoroutine(DataApp2.main.CheckInternet(internet =>
        {

	        if (internet)
	        {
		        Debug.Log("Hasta aca hay internet");
		        string numberPhone = _phoneField.text.Trim();
		        Debug.Log(numberPhone);

		        if (!string.IsNullOrEmpty(_indexField.text))
		        {
			        if (!string.IsNullOrEmpty(numberPhone))
				        StartCoroutine(LoginByPhone(numberPhone));
			        else
				        DataApp2.main.popUpInformative(true, "Teléfono no valido", "Por favor digite un número de celular valido.");
		        }
		        else
		        {
			        DataApp2.main.popUpInformative(true, "Inidice no valido", "Por favor digite el indice del país donde reside.");
		        }
	        }
	        else
	        {
		        Debug.Log("Aca ya fallo internet");
		        DataApp2.main.popUpInformative(true, "Fallo en la conexíon.", "Revisa tu conexión a internet.");
	        }

        }));
    }

    IEnumerator LoginByPhone(string _phoneNumber)
    {
        PlayerPrefs.SetString("MyPhone", _phoneNumber);

        WWWForm form = new WWWForm();

        form.AddField("indata", "ValidarNumero");
        form.AddField("numero", _phoneNumber);

        WWW www = new WWW(urlRegisterAndLogin, form);

        yield return www;

        if (string.IsNullOrEmpty(www.error))
        {
	        Debug.Log("The phone does exist? " + www.text);
	        m_PhoneRegister = JsonUtility.FromJson<PhoneRegister>(www.text);

	        if (m_PhoneRegister.dataList[0].ID == 0)
	        {
		        DataApp2.main.popUpInformative(true, "Lo sentimos", "Este número no tiene plan activo");
	        }
	        else
	        {

		        string MyID = m_PhoneRegister.dataList[0].ID.ToString();

		        yield return StartCoroutine(User.main.getDatasUser(DataApp2.main.host + "UserData.php?indata=userDatas&idUser=" + MyID, "login"));
		        //yield return StartCoroutine(User.main.getDatasUser(DataApp2.main.host + "UserData.php?indata=userDatas&idUser=7955", "login"));
		        Debug.Log("termino de consultar datos");
		        DataApp2.main.SetMyID(MyID);

		        Debug.Log("nombre usuario " + User.main.GetMyName() + " id usuario " + DataApp2.main.GetMyID());
		        //StartCoroutine(DataApp2.main.GetPago());
		        DataApp2.main.GetPay(m_PhoneRegister.dataList[0].pago);
		        StartCoroutine(GuardarEnTablaPuntajes(DataApp2.main.GetMyID(), User.main.GetMyName()));

		        DataApp2.main.DisableLoading();
		        PanelLogin.SetActive(false);
                //enviar hacia el respectivo lugar ctm
                //EnviarA();
            }

        }
        else
        {
	        Debug.Log("Fallo el internet");
	        DataApp2.main.popUpInformative(true, "Fallo de conexión", "Por favor verifique su conexión o intente más tarde");
        }

    }
    #endregion
}

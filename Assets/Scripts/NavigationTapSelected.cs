﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class NavigationTapSelected : MonoBehaviour {

	public static NavigationTapSelected mynavTab;
	[Header("INIT")]

	public int  initab;
	[Header("CURRENT")]
	public int CurrentTab;
	[Header("SELECT")]
	public int SelectTab;

	public GameObject hncPanel;
	public GameObject rankignPanel;

	void Awake (){
		mynavTab = this;
	}


	public void _SelectTab (int tab){
		CurrentTab = SelectTab;
		SelectTab = tab;
	}



	void Update () {
		if( SelectTab != initab && ( !DataApp2.main.gameObject.activeSelf) ){
			if( Input.GetKeyDown(KeyCode.Escape)&& (SelectTab != CurrentTab ) ){
				SelectTab = CurrentTab;
				ChatController.main.ActiveView( SelectTab );
			}else if ( Input.GetKeyDown(KeyCode.Escape) && (SelectTab == CurrentTab ) && SelectTab == 1 ) {
				Debug.Log( "desactivar panel 1" );
				SelectTab = 0;
				rankignPanel.SetActive(false);
			}else if ( Input.GetKeyDown(KeyCode.Escape) && (SelectTab == CurrentTab ) && SelectTab != 0 ) {
				Debug.Log( "desactivar panel 2" );
				SelectTab = 0;
			}else if ( Input.GetKeyDown(KeyCode.Escape) && SelectTab == 0 && CurrentTab != 0 ){
				Debug.Log( "desactivar panel 3" );
				SelectTab = initab;
			}else if ( Input.GetKeyDown(KeyCode.Escape)) {
				Debug.Log( "desactivar panel 4" );
				hncPanel.SetActive(false);
			}
		}else if( SelectTab == initab && SelectTab ==0 && ( !DataApp2.main.gameObject.activeSelf ) && Input.GetKeyDown(KeyCode.Escape) ){
			Debug.Log( "desactivar panel 5" );
			SelectTab = initab;
			rankignPanel.SetActive(false);
		}
	
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TerminosCaller : MonoBehaviour 
{
    public Text TermsText;

    void OnEnable()
    {
        StartCoroutine(DownloadTerminos());
    }

    IEnumerator DownloadTerminos()
    {
        WWW consult = new WWW("http://fcf.2waysports.com/goloficial/terminos_gol_oficial.php");

        yield return consult;

        if (string.IsNullOrEmpty(consult.error))
        {
            if (!string.IsNullOrEmpty(consult.text))
            {
                Debug.Log(consult.text);
                TermsText.text = consult.text;
            }
        }

    }
}

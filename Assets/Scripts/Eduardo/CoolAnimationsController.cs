﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PhantomPlayer
{
    public Animator MainAnimator;
    public Animator HiddenAnimator;

    public void PlayCoolAnim()
    {
        MainAnimator.SetTrigger("Jugada");
        HiddenAnimator.enabled = false;
        HiddenAnimator.Rebind();
        HiddenAnimator.enabled = true;
    }
}

/**DEPRECATED*/
public class CoolAnimationsController : MonoBehaviour
{
    public PhantomPlayer AnimationsOnPlayer;
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        RaycastHit Hit;

        if (Input.GetMouseButtonDown(0) /*&& Waiting for anim to finish*/)
        {
            if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out Hit, Mathf.Infinity, 1 << 8))
            {
                Debug.Log("Detecto al jugador");
                if (!AnimationsOnPlayer.MainAnimator || !AnimationsOnPlayer.HiddenAnimator)
                {
                    Debug.Log("Asigne las animaciones");
                }
                else
                {
                    AnimationsOnPlayer.PlayCoolAnim();
                }
            }
        }

    }
}

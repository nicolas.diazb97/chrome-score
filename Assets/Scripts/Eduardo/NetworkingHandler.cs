﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Serializable]
public class RecoverStatus
{
    public string status;
    public string data;
    public string[] error;
    
}

public class NetworkingHandler : MonoBehaviour
{
    public InputField EmailInput;
    public string host;
    public RecoverStatus CurStatus;

    #region Password
    public void RecoverPasswordByEmail()
    {
        DataApp2.main.EnableLoading();

        if (!string.IsNullOrEmpty(EmailInput.text))
        {
            StartCoroutine(POST("{\"email\":\""+ EmailInput.text + "\"}"));
        }
        else
        {
            DataApp2.main.popUpInformative(true, "Correo no valido", "Porfavor introduzca un correo valido.");
            DataApp2.main.DisableLoading();
        }
    }


    public IEnumerator POST(string _json)
    {
        print("api POST<T>");
        WWWForm form = new WWWForm();
        System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
        Dictionary<string, string> headers = form.headers;
        headers["Content-Type"] = "application/json";
        string url = host;

        WWW consult = new WWW(url, encoding.GetBytes(_json), headers);
        yield return consult;
        if (!string.IsNullOrEmpty(consult.error))
        {
            Debug.LogError("un pinche error de: " + consult.error);
        }
        else
        {
            Debug.LogWarning("LLEGA ESTA CHIMBADA \n " + consult.text);
            CurStatus = JsonUtility.FromJson<RecoverStatus>(consult.text);
            DataApp2.main.DisableLoading();
            DataApp2.main.popUpInformative(true, CurStatus.status, CurStatus.data);
            this.gameObject.SetActive(false);

        }

    }

    string BuildParams(List<Parameter> _params)
    {
        string result = "?";
        foreach (Parameter p in _params)
        {
            result += p.name + "=" + p.value + "&";
        }
        result = result.Remove(result.Length - 1);
        return result;
    }
    #endregion
}

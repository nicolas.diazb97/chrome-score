﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PaymentLinks : MonoBehaviour 
{
    public GameObject LoginPanel;
    public GameObject RetosPanel, FKPanel;
    public GameObject BuyPlayersPanel, BuyLevels;
    public GameObject Jugadores;

    public void BuySuscription(string _panel)
    {
        StartCoroutine(DataApp2.main.CheckInternet(internet =>
            {
                if(!DataApp2.main.IsRegistered( ))
                {
                    LoginPanel.SetActive (true);
                    RegisterLoginManager.panel =5;

                    if (_panel == "Jugadores")
                    {
                        BuyPlayersPanel.SetActive(false);
                    }
                    else if (_panel == "Niveles")
                    {
                        RetosPanel.SetActive(false);
                        FKPanel.SetActive(false);
                        BuyLevels.SetActive(false);
                    }
                }
                else if(DataApp2.main.IsRegistered( ) && PlayerPrefs.GetInt("pago")==0)
                {
                  Debug.Log("compra");
                    //TODO: Mandelo al link de compra
                    DataApp2.main.popUpInformative(true, "Fallo de Conexión", "No se ha podido conectar a la compra, porfavor intente más tarde");
                 } 
            })); 

      
    }

}

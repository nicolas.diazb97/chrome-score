﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerPrefsTracking : Singleton<PlayerPrefsTracking> 
{
	[Header("Player Prefs Keys")]
	public string Creloj = "Creloj";

	public string tn1 = "tn1";
	public string estrellasGanadasN1 = "estrellasGanadasN1";

	public string tn2 = "tn2";
	public string estrellasGanadasN2 = "estrellasGanadasN2";

	public string tn3 = "tn3";
	public string estrellasGanadasN3 = "estrellasGanadasN3";

	public string tn4 = "tn4";
	public string estrellasGanadasN4 = "estrellasGanadasN4";

	public string tn5 = "tn5";
	public string estrellasGanadasN5 = "estrellasGanadasN5";

	public string level = "level";

    public string Tutorial = "Tutorial";

    [Header ("Player Prefs Integers")]
	public int v_Creloj;
	public int TirosLibresNivel1;
	public int EarnedStars1;

	public int TirosLibresNivel2;
	public int EarnedStars2;

	public int TirosLibresNivel3;
	public int EarnedStars3;

	public int TirosLibresNivel4;
	public int EarnedStars4;

	public int TirosLibresNivel5;
	public int EarnedStars5;

	public int levelValue;
    public int tutorialFlag;

    //public GoogleAnalyticsV4 googleAnalytics;

    // Use this for initialization
    void Start()
    {
        //googleAnalytics.StartSession();
        LogMainMenuScreen();

        int defaultValue = EventSystem.current.pixelDragThreshold;
        EventSystem.current.pixelDragThreshold =
                Mathf.Max(
                     defaultValue,
                     (int)(defaultValue * Screen.dpi / 160f));
    }

    // Update is called once per frame
    void Update () 
	{
		#region Getting Player Prefs Integers
		GetPrefValue (Creloj, out v_Creloj);
		GetPrefValue (tn1, out TirosLibresNivel1);
		GetPrefValue (estrellasGanadasN1, out EarnedStars1);
		GetPrefValue (tn1, out TirosLibresNivel2);
		GetPrefValue (estrellasGanadasN1, out EarnedStars2);
		GetPrefValue (tn1, out TirosLibresNivel3);
		GetPrefValue (estrellasGanadasN1, out EarnedStars3);
		GetPrefValue (tn1, out TirosLibresNivel4);
		GetPrefValue (estrellasGanadasN1, out EarnedStars4);
		GetPrefValue (tn1, out TirosLibresNivel5);
		GetPrefValue (estrellasGanadasN1, out EarnedStars5);
		GetPrefValue (level, out levelValue);
        GetPrefValue (Tutorial, out tutorialFlag);
        #endregion

    }

	void GetPrefValue(string _key, out int _value)
	{
		_value = 0;

		if (PlayerPrefs.HasKey (_key)) 
		{
			_value = PlayerPrefs.GetInt (_key);
		}

	}

    void OnApplicationQuit()
    {
        //googleAnalytics.StopSession();
    }

    public void LogMainMenuScreen()
    {
        //if (googleAnalytics != null)
        //{
        //    //googleAnalytics.LogScreen("Main Menu Gol Oficial");
        //    //Firebase.Analytics.FirebaseAnalytics.LogEvent("Main Menu Gol Oficial");
        //}
    }

    public void LogRankingScreen()
    {
        //if (googleAnalytics != null)
        //{
        //    //googleAnalytics.LogScreen("Ranking Menu Gol Oficial");
        //    //Firebase.Analytics.FirebaseAnalytics.LogEvent("Ranking Menu Gol Oficial");
        //}
    }

    public void LogGameplayScreen()
    {
        //if (googleAnalytics != null)
        //{
        //    //Call this on the respawn
        //    if (v_Creloj == 0)
        //    {
        //        Debug.Log("Tiros Libres Nivel " + levelValue);
        //        //googleAnalytics.LogScreen("Tiros Libres Nivel " + levelValue);
        //        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Tiros Libres Nivel " + levelValue);
        //    }
        //    else if (v_Creloj == 1)
        //    {
        //        Debug.Log("Contrareloj");
        //        //googleAnalytics.LogScreen("Contrareloj");
        //        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Contrareloj");
        //    }
        //    else if (v_Creloj == 2)
        //    {
        //        Debug.Log("Reto Oficial: Rebote Nivel " + levelValue);
        //        //googleAnalytics.LogScreen("Reto Oficial: Rebote Nivel " + levelValue);
        //        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Reto Oficial: Rebote Nivel " + levelValue);
        //    }
        //    else if (v_Creloj == 3)
        //    {
        //        Debug.Log("Reto Oficial: Aros Nivel " + levelValue);
        //        //googleAnalytics.LogScreen("Reto Oficial: Aros Nivel " + levelValue);
        //        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Reto Oficial: Aros Nivel " + levelValue);
        //    }
        //    else if (v_Creloj == 4)
        //    {
        //        Debug.Log("Reto Oficial: Distancia Nivel " + levelValue);
        //        //googleAnalytics.LogScreen("Reto Oficial: Distancia Nivel " + levelValue);
        //        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Reto Oficial: Distancia Nivel " + levelValue);
        //    }
        //    else if (v_Creloj == 5)
        //    {
        //        Debug.Log("Reto Oficial: Platillos Nivel " + levelValue);
        //        //googleAnalytics.LogScreen("Reto Oficial: Platillos Nivel " + levelValue);
        //        //Firebase.Analytics.FirebaseAnalytics.LogEvent("Reto Oficial: Platillos Nivel " + levelValue);
        //    }
        //}
        
    }
}

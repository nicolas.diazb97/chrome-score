﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTexture : MonoBehaviour 
{
	private Material m_Material;
	private Material[] m_AllMaterials;

	[SerializeField]
	private Material materialAsset ;

	[SerializeField]
	private Texture texDefault = null;
	[SerializeField]
	private Texture[] texPerLevel = null;

	public void Start()
	{
		m_Material = GetComponent<Renderer> ().material;
		m_AllMaterials = GetComponent<Renderer> ().materials;
	}

	public void SetTextureByLevel(int _level)
	{
		if (texPerLevel.Length > 0) {
			if (_level >= 0 && _level < texPerLevel.Length)
				m_Material.mainTexture = texPerLevel [_level];
			else
				m_Material.mainTexture = texDefault;
		} 
		else 
		{
			m_Material.mainTexture = texDefault;
		}

	}

	public void SetTextureAssetByLevel(int _level)
	{
		if (texPerLevel.Length > 0) 
		{
			if (_level >= 0 && _level < texPerLevel.Length)
				materialAsset.mainTexture = texPerLevel [_level];
			else
				materialAsset.mainTexture = texDefault;
		} 
		else 
		{
			materialAsset.mainTexture = texDefault;
		}

	}

	public void SetTextureArrayByLevel(int _level)
	{
		foreach (Material mat in m_AllMaterials) {
			if (texPerLevel.Length > 0) {
				if (_level >= 0 && _level < texPerLevel.Length)
					mat.mainTexture = texPerLevel [_level];
				else
					mat.mainTexture = texDefault;
			} else {
				mat.mainTexture = texDefault;
			}
				
		}
	}
}

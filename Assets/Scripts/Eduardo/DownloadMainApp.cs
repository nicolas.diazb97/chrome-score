﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownloadMainApp : MonoBehaviour
{

    public void DownloadApp()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.Wise.SeleccionColombiaOficial");
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            Application.OpenURL("https://itunes.apple.com/co/app/seleccion-colombia-oficial/id1210353281?mt=8");
        }

        #if UNITY_ANDROID
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.Wise.SeleccionColombiaOficial");
        #elif UNITY_IOS
         Application.OpenURL("https://itunes.apple.com/co/app/seleccion-colombia-oficial/id1210353281?mt=8");
        #endif
    }
}

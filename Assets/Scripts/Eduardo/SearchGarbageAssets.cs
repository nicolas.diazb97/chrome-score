﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SearchGarbageAssets : MonoBehaviour 
{
    public GameObject obj_Root;//This is the root object from wich the garbage is going to be collected
    public List<Transform> RootChilds;//This list store all the children's inside the root object
    public Transform GarbageHolder;//This holder house the children component, if we wich to move them from the root
    public List<Texture> AllTextures;

//    public Mesh 
    /// <summary>
    /// Gets all the childrens from the Root object.
    /// </summary>
    public void GetRootChildrens<T>()
    {
        if (obj_Root != null)
        {
            Debug.Log("");
            RootChilds = obj_Root.GetComponentsInChildren<Transform>().ToList();
        }
    }
}

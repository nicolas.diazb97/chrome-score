﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TestList : MonoBehaviour 
{
    [SerializeField]
    public List<Transform> AllChilds;

    public void GetChildrens()
    {
        AllChilds = this.GetComponentsInChildren<Transform>().ToList();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneDummy : MonoBehaviour 
{
    public GameObject CanvasObj;

    public void LoadLevel(string _levelName)
    {
        CanvasObj.SetActive(false);
        SceneManager.LoadSceneAsync(_levelName);
    }  
}

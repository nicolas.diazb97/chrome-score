﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SponsorsOnlineManager : MonoBehaviour
{
    public Texture2D defaultTex;
    public string onlineTexUrl;
    public List<MeshRenderer> additionalMeshes = new List<MeshRenderer>();
    Texture2D getTex;
    Material sponsorsMaterial;

    // Start is called before the first frame update
    void Start()
    {
        sponsorsMaterial = GetComponent<MeshRenderer>().material;
        StartCoroutine(GetOnlineTexture());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator GetOnlineTexture()
    {
        UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(onlineTexUrl);
        yield return webRequest.SendWebRequest();
        if(webRequest.isHttpError || webRequest.isNetworkError)
        {
            getTex = defaultTex;
        }
        else
        {
            getTex = ((DownloadHandlerTexture)webRequest.downloadHandler).texture;
        }
        //
        sponsorsMaterial.SetTexture("_MainTex", getTex);
        //
        if (additionalMeshes.Count > 0)
        {
            foreach (MeshRenderer m in additionalMeshes)
            {
                m.material.SetTexture("_MainTex", getTex);
            }
        }
    }
}

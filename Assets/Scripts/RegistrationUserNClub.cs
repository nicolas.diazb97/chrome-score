﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;

public class RegistrationUserNClub : MonoBehaviour {

	public static RegistrationUserNClub MyResgUser;
	public GameObject MyUser;
	public GameObject MyRegistrationPanel;
	string readURLMail = "http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/ValidarMail.php";
	string InfoProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/EditDataProfiel.php?";
	public InputField name;
	public InputField lastname;
	public InputField email_t;
	public InputField pass_t;
	public Dropdown country_t;
	public Dropdown provincia_t;
	public InputField ind_cel_t;
	public InputField cel_t;
	[Header("Fecha de naciemiento")]
	public InputField day;
	public InputField month;
	public InputField year;
	public Toggle gen;
	public Toggle Masculino,Femenino;
	public string Genero;
	public GameObject ConfirmPopup;
	public GameObject PopUpInfo;
	public GameObject CompletePopUpInfo;
	public Text CompletePopUpInfoMsg;
	public Text PopUpInfoMsg;
	public static string Metodo;

	private DateTime date;


	//public GameObject TransactionModule;

	void Awake( ){
		MyResgUser = this;
		Metodo = PlayerPrefs.GetString("Metodo");
		EditProfile.myUser = MyUser.GetComponent<EditProfile>( );
		ProfileUser.MyUser = MyUser.GetComponent<ProfileUser>( );
		GetRegisterHNC( );
		 validInternet( );
//		StartCoroutine( DownloadIsRegistroComplete ( DataApp.main.GetMyID().ToString()));
	}


	public void validInternet (  ) {
//		StartCoroutine( LoadingDummy.Loading.CheckInternet( internet => { 
//			if( internet ){
				StartCoroutine( DownloadIsRegistroComplete ( DataApp.main.GetMyID().ToString()));
//			}
//		}));
	}
		

	public IEnumerator DownloadIsRegistroComplete( string id) {
		if(DataApp.main.IsRegistered( )  && TransactionalProcess.isActive == 1){
			string downInfoUser = InfoProfile+"opc=isCompleteRegistered&userID="+id;
			WWW url_downdload = new WWW(downInfoUser);
			yield return url_downdload;

			if( string.IsNullOrEmpty(url_downdload.text) ){
				if(DataApp.main.GetMyID( ) != 0){
					MyRegistrationPanel.SetActive( true );
					email_t.text = DataApp.main.GetEmail( );
					MyUser.SetActive( true );
					StartCoroutine( getDatas( ) ); }
				//TransactionalProcess.thisTransaction.debug.text = "<<<<<----- ACA NO ERA ---->>> DATA APP IS REGISTERD: "+ DataApp.main.IsRegistered() +  DataApp.main.GetMyID() + " is active : " + TransactionalProcess.isActive; 
			}else{
				
			}
		}
	}



	public void changeGenero( ){
		if( gen.isOn )	
			Genero = "Masculino";
		else
			Genero = "Femenino";
	}


	IEnumerator DownloadGenero (string id ){
		string downInfoUser = InfoProfile+"opc=LoadGen&userID="+id;
		WWW url_downdload = new WWW(downInfoUser);
		yield return url_downdload;
		Debug.Log ( "pinche genero: "+downInfoUser );
		if( !string.IsNullOrEmpty(url_downdload.text)){
			Genero = url_downdload.text.Trim ();
			if( Genero == "[Masculino]"){
				Masculino.isOn = true;
				Femenino.isOn = false;
			}else{
				Femenino.isOn = true;
				Masculino.isOn = false;
			}
		}
	}

	IEnumerator getDatas ( ){
		string MyDatas = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/Registro/getInfo.php?IDuser="+DataApp.main.GetMyID( );
		StartCoroutine( DownloadGenero(DataApp.main.GetMyID( ).ToString( ) ) );
		WWW getData = new WWW( MyDatas );
		yield return getData;
		string  [] datas= new string[5];
		string temp="";
		int c=0;
		Debug.Log(getData.text);
		for (int i = 0;i<getData.text.Length; i++){
			if ( string.IsNullOrEmpty( getData.error )){
				if( getData.text[i] == ','){
					datas[c] = temp;
					c++;
					temp="";
				}else{
					temp+= getData.text[i];
				}
			}
		}
		datas[c] = temp;
		Debug.Log("1: "+ datas[0] + "2: "+datas[1]+" 3: "+ datas[2] +" 2: "+ datas[3]) ;
		MyUser.GetComponent<RegistrationUserNClub>().email_t.text = DataApp.main.GetEmail();
		MyUser.GetComponent<RegistrationUserNClub>().name.text = datas[0];
		MyUser.GetComponent<RegistrationUserNClub>().lastname.text = datas[1];
		MyUser.GetComponent<RegistrationUserNClub>().cel_t.text = datas[2];
		MyUser.GetComponent<RegistrationUserNClub>().ind_cel_t.text = datas[3];
		MyUser.GetComponent<RegistrationUserNClub>().email_t.text = datas[4];

	}
	public void SetRegisterHNC( int act){
		PlayerPrefs.SetInt ("ImHnc", act);
	}

	public int GetRegisterHNC ( ){
		return PlayerPrefs.HasKey("ImHnc") ? PlayerPrefs.GetInt ("ImHnc"): 0;
	}

	public void SendInfoDataRegistrer( ){  // Actualiza los datos nuevos del usuario 
		ProfileUser.MyUser.setNameUser(name.text);
		ProfileUser.MyUser.setLastName(lastname.text);
		ProfileUser.MyUser.setEmailUser(email_t.text);
		ProfileUser.MyUser.setPassUser(pass_t.text);
		ProfileUser.MyUser.setCelUser(cel_t.text);
		ProfileUser.MyUser.setCountryuser(country_t.captionText.text);
		ProfileUser.MyUser.setProvuser(provincia_t.captionText.text);
		ProfileUser.MyUser.setBirthdayUser(day.text+"/"+month.text+"/"+year.text);
		ProfileUser.MyUser.setCountryIdUser(country_t.value);
		ProfileUser.MyUser.setProvinciaId(provincia_t.value);
		if( email_t.text != "" && pass_t.text !="" && name.text !="" && lastname.text !=""  && cel_t.text !="" && day.text !="" && month.text !="" && year.text !=""){
			ConfirmDate ();
		}else{
			PopUpInfo.SetActive(true);
			PopUpInfoMsg.text = "Ingresa tus datos completos.";
		}
	}

	public void ConfirmDataRegistrer( ){
			RegistrationNew();
	}

	private void ConfirmDate(){
		if (Int32.Parse (year.text) > 1900 && Int32.Parse (year.text) <= DateTime.Now.Year && Int32.Parse (month.text) <= 12 && Int32.Parse (day.text) <= 31) {
			StartCoroutine (confirmMail ());
			Debug.Log (day.text + "-" + month.text + "-" + year.text);
		}else {
			PopUpInfo.SetActive(true);
			PopUpInfoMsg.text = "Ingresa una fecha valida.";
		}
 	
	}



	private IEnumerator confirmMail() {
		string answerMail = "";
		string miMail = "";
		string urlStringMiMail = readURLMail + "?opc=mimail"+"&id=" +DataApp.main.GetMyID() ; 
		WWW getMail = new WWW (urlStringMiMail);
		yield return getMail;
		miMail = getMail.text;
		Debug.Log("my email: " + miMail);
		string urlStringMails = readURLMail + "?opc=mails" + "&correo=" + WWW.EscapeURL (email_t.text); 
		WWW getMails = new WWW (urlStringMails);
		yield return getMails;
		answerMail = getMails.text;
		Debug.Log("OTRO email: " + answerMail);
		if(email_t.text != ""){
			if( string.IsNullOrEmpty(getMails.error)){
				if ( answerMail == "" ) {
					ConfirmPopup.SetActive(true);
					RegistrationNew();
				}else if( miMail == answerMail ){
					ConfirmPopup.SetActive(true);
					RegistrationNew();
				}else{
					PopUpInfo.SetActive(true);
					PopUpInfoMsg.text  = "Ese correo ya esta Registrado.\nIntenta ingresando otro.";
				}
			}
		}else{
			PopUpInfo.SetActive(true);
			PopUpInfoMsg.text  = "Ingresa tu correo antiguo o uno nuevo.";
		}
		DataApp2.main.DisableLoading();
	}

	void RegistrationNew( ){
		DataApp2.main.EnableLoading();
		string temp_name = ProfileUser.MyUser.getNameUser( );
//		temp_name = temp_name.Replace(" ","%20");
		string temp_email = ProfileUser.MyUser.getEmailUser( );
//		temp_email = temp_email.Replace(" ","%20");
		string temp_pass = ProfileUser.MyUser.getPassUser( );
//		temp_pass = temp_pass.Replace(" ","%20");
		string temp_country = ProfileUser.MyUser.getCountryuser( );
//		temp_country = temp_country.Replace(" ","%20");
		string temp_provincia = ProfileUser.MyUser.getProvuser( );
//		temp_provincia = temp_provincia.Replace(" ","%20");
		string temp_birthday = ProfileUser.MyUser.getBirthdayUser( );
//		temp_birthday = temp_birthday.Replace(" ","%20");
		string temp_cel = ProfileUser.MyUser.getCelUser( );
//		temp_cel = temp_cel.Replace(" ","%20");
		string temp_ind = ind_cel_t.text;
		int pais_id = ProfileUser.MyUser.getCountryIdUser();
		int provincia_id = ProfileUser.MyUser.getProvIdUser();
		string fechaSucr = MonthString(null);
		string fechaFinSucr = MonthString("FinalSuscripcion");
		string temp_lastname = ProfileUser.MyUser.getLastName( );
		string temp_diavencido = DateTime.Now.Day.ToString( );
//		temp_lastname = temp_lastname.Replace(" ","%20");

		ApiChat.main.StartCoroutine(ApiChat.main.GetChatIdInServer( _id => {
			print("con ProcessFromGetIdInServer: "+_id);
			if(_id > 0){
				StartCoroutine(_SaveDataProfile(temp_name, temp_email, temp_pass, temp_ind, temp_cel, pais_id.ToString(), temp_country, temp_birthday, fechaSucr, fechaFinSucr, Metodo, temp_lastname,provincia_id.ToString( ), temp_provincia, temp_diavencido ));
			}else{
				ChatController.main.SetWarningLoginMessage("Experimentamos problemas para conectar con el Muro Amarillo.(1)");
			}
		} , DataApp.main.GetMyID().ToString() ));
	}

	public string MonthString(string opc){
		int mes = DateTime.Now.Month;	
		int año = DateTime.Now.Year;
		string mes_;
		if(opc == "FinalSuscripcion"){
			año = 2017;
			mes = 3; }
		mes_ = (mes ==1) ?mes_="ENE":(mes==2)?mes_="FEB":(mes ==3)?mes_="MAR":(mes==4)?mes_="ABRIL":(mes==5)?mes_="MAYO":(mes==6)?mes_="JUNIO":(mes==7)?mes_="JULIO":(mes==8)?mes_="AGOS":(mes==9)?mes_="SEPT":(mes==10)?mes_="OCT":(mes==11)?mes_="NOV": (mes==12)?mes_="DIC":"";
		mes_ +=" "+año;
		Debug.Log(mes_);
		return mes_;
	}

	public void disableProvincia( ){
		if(country_t.value != 0) {
			provincia_t.gameObject.SetActive(false);
		}else{
			provincia_t.gameObject.SetActive(true);
		}
	} 

	public void Indicativo( ){
		ind_cel_t.text = "";
		ProfileUser.MyUser.setCountryIdUser( country_t.value );
		if(ProfileUser.MyUser.getCountryIdUser( ) == 0){
			ind_cel_t.text = "+593 ";
		}else if(ProfileUser.MyUser.getCountryIdUser( ) == 1){
			ind_cel_t.text = "+34 ";
		}else if(ProfileUser.MyUser.getCountryIdUser( ) == 2){
			ind_cel_t.text = "+1 ";
		}
	}

	public IEnumerator _SaveDataProfile(string name, string email, string pass, string ind, string cel, string country_id, string country, string birth, string fechaSuc, string fechaFinsus, string metodo, string lastname, string provinciaid, string provincia, string diavencido){
		if(DataApp.main.IsRegistered( ) ){    // y agregar si el usuario es HINCHA NIVEL CLUB // 
			int _id = DataApp.main.GetMyID();
			string cons = InfoProfile;
			if(country_id != "0"){
				provinciaid = "0";
				provincia = "null";
			}
			Debug.Log("opc=UpdateData"+"&userID="+_id+"&name="+name+"&email="+email+"&pass="+pass+"&genero="+Genero+"&ind="+ind+"&cel="+cel+"&paisid="+country_id+"&pais="+country+"&fechan="+birth +"&fechaSuscr="+fechaSuc +"&fechaFinSuscr="+fechaFinsus+ "&Metodo="+metodo +"&apellido="+ lastname  +"&provinciaid="+ provinciaid  +"&provincia="+ provincia + "&diaVencido="+diavencido);
//			WWW newInfoUser = new WWW (form);	

			WWWForm form = new WWWForm();

			form.AddField("opc", "UpdateData");
			form.AddField("userID", _id);
			form.AddField("name", name );
			form.AddField("email" , email );
			form.AddField("pass" , pass );
			form.AddField("genero" , Genero );
			form.AddField("ind" , ind );
			form.AddField("cel" , cel );
			form.AddField("paisid" , country_id );
			form.AddField("pais" , country );
			form.AddField("fechan" , birth );
			form.AddField("Metodo" , metodo );
			form.AddField("fechaSuscr" , fechaSuc );
			form.AddField("fechaFinSuscr" , fechaFinsus );
			form.AddField("apellido" , lastname );
			form.AddField("provinciaid" , provinciaid );
			form.AddField("provincia" , provincia );
			form.AddField("diaVencido" , diavencido );

			WWW newInfoUser = new WWW (cons,form);	

			Debug.Log(form);
			yield return newInfoUser;	
			if(!string.IsNullOrEmpty(newInfoUser.text)){
				PlayerPrefs.SetInt("MyCountry",country_t.value);
				Debug.Log("RE LLAME TODO");
				PersonalizacionPersonaje.ThisCustom.GuardarCambios();
//				CompletePopUpInfo.SetActive(true);
//				CompletePopUpInfoMsg.text = "Has sido registrado con éxito.";
				SetRegisterHNC(1);
				DataApp.main.SetEmail(email);
				EditProfile.myUser.InitValidation( );
				MyRegistrationPanel.SetActive(false);
				Debug.Log ("hacer el cambio aqui");
			}else{
				ProfileUser.MyUser.EnablePopUp("Fallo en la conexión a internet. \nIntentelo de nuevo.");
				DataApp2.main.EnableLoading();
			}
		}else{
			Debug.Log("ID VACIO");
			// lanzar el POP DE REGISTRARSE Y/O el de Pagar HINCHA NIVEL CLUB
		}
		DataApp2.main.DisableLoading();
		//		LoadingDummy.Loading.DisableLoading();
	}

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class CodigoPromocional : MonoBehaviour {


	string urlCodes = "http://2waysports.com/2waysports/Ecuador/Barcelona/Admin/CodigosPromocionales.php";
	public InputField CodigoInput;
	public GameObject ManagerInfo;
	public UnityEvent CorrectCodigo = new UnityEvent( );
	public UnityEvent IncorrectCodigo = new UnityEvent( );

	public void SendCodePromo( ){
		DataApp2.main.EnableLoading();
		StartCoroutine(CodeActive());
	}

	//"com.barcelonasc.barcelonaapp.hinchanivelclub"
	public IEnumerator CodeActive( ){
		string construct = urlCodes + "?opc=IsCodigo&userID="+DataApp.main.GetMyID()+"&codigo="+CodigoInput.text;
		WWW CodeAct = new WWW( construct);
		yield return CodeAct;
		Debug.Log(construct);
		if( string.IsNullOrEmpty(CodeAct.error)){
			if( !string.IsNullOrEmpty(CodeAct.text)){
				print(CodeAct.text);
				CorrectCodigo.AddListener(OnActiveHinchanivelClub);
				CorrectCodigo.Invoke( );
			}else{
				print("ala mierda");
				IncorrectCodigo.Invoke( );
			}
		}
		DataApp2.main.DisableLoading ();
	}

	public void OnActiveHinchanivelClub( ){
		DataApp2.main.EnableLoading();
		StartCoroutine( getDatas( ) );
	}



	IEnumerator getDatas ( ){
		string MyDatas = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/Registro/getInfo.php?IDuser="+DataApp.main.GetMyID( );
		StartCoroutine( RegistrationUserNClub.MyResgUser.DownloadIsRegistroComplete( DataApp.main.GetMyID( ).ToString( ) ) ); 
		Debug.Log ("id: "+DataApp.main.GetMyID( ).ToString( ) );
		WWW getData = new WWW( MyDatas );
		yield return getData;
		string  [] datas= new string[5];
		string temp="";
		int c=0;
		Debug.Log(getData.text);
		for (int i = 0;i<getData.text.Length; i++){
			if ( string.IsNullOrEmpty( getData.error )){
				if( getData.text[i] == ','){
					datas[c] = temp;
					c++;
					temp="";
				}else{
					temp+= getData.text[i];
				}
			}
		}
		datas[c] = temp;
		Debug.Log("1: "+ datas[0] + "2: "+datas[1]+" 3: "+ datas[2]);
		Debug.Log("Compra exitosa");
		TransactionalProcess.isActive =1;
		RegistrationUserNClub.Metodo = "Codigo"; 
		PlayerPrefs.SetString("Metodo",RegistrationUserNClub.Metodo); 
		ManagerInfo.GetComponent<RegistrationUserNClub>().email_t.text = DataApp.main.GetEmail();
		ManagerInfo.GetComponent<RegistrationUserNClub>().name.text = datas[0];
		ManagerInfo.GetComponent<RegistrationUserNClub>().lastname.text = datas[1];
		ManagerInfo.GetComponent<RegistrationUserNClub>().cel_t.text = datas[2];
		ManagerInfo.GetComponent<RegistrationUserNClub>().ind_cel_t.text = datas[3];
		ManagerInfo.GetComponent<RegistrationUserNClub>().email_t.text = datas[4];
		StartCoroutine( ManagerInfo.GetComponent<RegistrationUserNClub> ().DownloadIsRegistroComplete (DataApp.main.GetMyID ().ToString()));
		Debug.Log("CODIGO PROMOCIONAL ACTIVADO");
		DataApp2.main.DisableLoading ();

	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ContentSearch {

	public int userID;

	public string nameText;

	public string nickName;

	public string descrp;

	public string mail;

	public string fechaSus;

	public string fechaFin;
}

[System.Serializable]
public class SearchUserList {

	public List<ContentSearch> dataList;

	public SearchUserList (){

		dataList = new List<ContentSearch> ();
	}
}
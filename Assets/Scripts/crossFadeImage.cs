﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class crossFadeImage : MonoBehaviour
{
	public Image target;

	public Sprite imagen1;
	public Sprite imagen2;

	public Color color1;

	public bool reversa;

	// Use this for initialization
	void Start ()
	{
		target.sprite = imagen1;
	}
	
	// Update is called once per frame
	void Update ()
	{
		target.color = color1;

		if (reversa == false)
		{
			if (target.color.a < 1)
			{
				color1.a += Time.deltaTime*0.3f;
			}
			else
			{
				reversa = true;
			}
		}
		else
		{
			if (color1.a > 0)
			{
				color1.a -= Time.deltaTime*0.3f;
			}
			else
			{
				if (target.sprite == imagen1)
				{
					target.sprite = imagen2;
				}
				else
				{
					target.sprite = imagen1;
				}
				reversa = false;
			}
		}
	}

	void Cambio ()
	{
		
	}
}

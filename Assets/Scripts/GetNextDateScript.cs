﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

[System.Serializable]
public class DateClass
{
	public string fecha;
	public string hora;
}

[System.Serializable]
public class DateClassList {

	public List<DateClass> dataList;
}

public class GetNextDateScript : MonoBehaviour {

	public Text days;
	public Text hours;
	public Text minutes;

	[SerializeField]
	public DateClassList fechaList;

	private bool isLoadNextDate = false;

	private string urlDatoHoraApertura = "http://www.2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/GetDate.php?";

	private IEnumerator ValidateStatusNextData (){

		WWW getData = new WWW (urlDatoHoraApertura + "indata=getNextDate" + "&idDate=" + 3);
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {

			if (getData.text != "PredictingUncreated") {

				fechaList = JsonUtility.FromJson<DateClassList> (getData.text);
				StartCoroutine (ShowFecha ());
			} else {
				
				Debug.Log("¡No hay datos!");
			}
		} else {

			Debug.Log("¡CONNECTION FAILED!");
		}

		Debug.Log ("GETDATA: " + getData.text);
	}

	private IEnumerator ShowFecha(){

		DateTime nextDate = DateTime.Parse (fechaList.dataList [0].fecha + " " + fechaList.dataList [0].hora);

		DateTime currentDate = DateTime.Now;

		TimeSpan diferencia = nextDate - currentDate;

		days.text = diferencia.Days.ToString();
		hours.text = diferencia.Hours.ToString();
		minutes.text = diferencia.Minutes.ToString();

//		Debug.Log ("Futuro " + nextDate.ToString());
//		Debug.Log ("Actual " + currentDate.ToString ());
//		Debug.Log ("Dif " + diferencia.ToString ());

		yield return null;
	}

	void OnEnable() {

		if (isLoadNextDate == false) {

			//ShowUpdatePopup ();

			StartCoroutine(ValidateStatusNextData ());
		}
	}

	public void EndUpdateData(string state){

		isLoadNextDate = true;
		//HideUpdatePopup ();
	}
}

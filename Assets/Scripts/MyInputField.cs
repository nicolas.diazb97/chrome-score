﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class MyInputField : InputField {
	[SerializeField]
	public SubmitEvent onSubmit = new SubmitEvent();


	public override void OnSubmit(BaseEventData eventData){
		base.OnSubmit(eventData);
		if(onSubmit != null){
			onSubmit.Invoke(text);
		}
	}
}

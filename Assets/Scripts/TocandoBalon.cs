﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TocandoBalon : MonoBehaviour {

	public ShootAI ManagerBall;
	public bool unavez;
	public AutoResetAfterShootFinish FinishFail;
	// Use this for initialization
	void Start ()
	{
		
	}

	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator ResetPosotion(){
		yield return new WaitForEndOfFrame();
	//	ManagerBall.reset();
		FinishFail.OnShootFinished(false,Area.Normal);
		unavez=false;
		this.gameObject.SetActive(false);
	}


	void OnTriggerEnter(Collider col){
		if(col.gameObject.tag == "Ball" && !unavez){
			unavez= true;
			StartCoroutine(ResetPosotion());
		}
	}
}

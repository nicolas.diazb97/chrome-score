﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
using System.Reflection;


public class ProfileUser : MonoBehaviour {


	#region phps rankings
	private string getUrlScoreTiroAmarillo = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarPuntajeXUsuario.php";
	private string getUrlPosTiroAmarillo = "http://2waysports.com/2waysports/Ecuador/Barcelona/rankingTiroAmarillo/MostrarPosXUsuario.php";
	private string getUrlScorePronostico = "http://2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/MostrarPuntajeXUsuario.php";
	private string getUrlPosPronostico = "http://2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/MostrarPosXUsuario.php";	
	#endregion
	public static ProfileUser MyUser;
//	public Image MyPhoto;
	private int _myid;
	private int _countryid;
	private int _provid;
	private string _name;
	private string _lastname;
	private string _email;
	private string _pass;
	private string _country;
	private string _provincia;
	private string _cel;
	private string _birthday;
	private string _nickname;
	private string _descripcion;
	private string _fechaInit;
	private string _scoreTa;
	private string _posTa;
	private string _scorePronos;
	private string _posPronos;

	[HideInInspector] public Text[] DataMyCard;
	[Space(15)]
	public Text msgPopUp;
	public GameObject PopUp;

	void Awake( ){ 
		_myid = DataApp.main.GetMyID();
		MyUser = this;
		StartCoroutine(DownloadDataTiroAmarillo(_myid.ToString()));
		StartCoroutine(DownloadDataPronostico(_myid.ToString()));
	}

	public void getDatasMyCard ( Image photoProfile){
		DataMyCard[0].text = getNickUser();
		DataMyCard[1].text = getFechaInit();
		Debug.Log(" pero que mierdas" + getFechaInit());
		DataMyCard[2].text = getScoreTa();
		DataMyCard[3].text = getPosTa();
		DataMyCard[4].text = getScorePro();
		DataMyCard[5].text = getPosPro();
		DataMyCard[6].text = getDesUser();
		//MyPhoto.sprite = photoProfile.sprite;
		DataApp2.main.DisableLoading( );
	}

	IEnumerator DownloadDataTiroAmarillo (string id ){
		//PUNTAJE TIRO AMARILLO
		string scoreTa = getUrlScoreTiroAmarillo+"?userID="+id;
		WWW scoreTa_c = new WWW(scoreTa);
		yield return scoreTa_c;
		if( !string.IsNullOrEmpty(scoreTa_c.text)){
			_scoreTa = scoreTa_c.text.Trim();
			setScoreTa(_scoreTa);
		}
		//POSICION TIRO AMARILLO
		string posTa = getUrlPosTiroAmarillo+"?userID="+id;
		WWW posTa_c = new WWW(posTa);
		yield return posTa_c;
		if( !string.IsNullOrEmpty(posTa_c.text)){
			_posTa = posTa_c.text.Trim();
			setPosTa(_posTa);
		}
	}

	IEnumerator DownloadDataPronostico (string id ){
		//PUNTAJE PRONOSTICO
		string scoreTa = getUrlScorePronostico+"?userID="+id;
		WWW scoreTa_c = new WWW(scoreTa);
		yield return scoreTa_c;
		if( !string.IsNullOrEmpty(scoreTa_c.text)){
			_scorePronos = scoreTa_c.text.Trim();
			setScorePro(_scorePronos);
		}
		//POSICION PRONOSTICO
		string posTa = getUrlPosPronostico+"?userID="+id;
		WWW posTa_c = new WWW(posTa);
		yield return posTa_c;
		if( !string.IsNullOrEmpty(posTa_c.text)){
			_posPronos = posTa_c.text.Trim();
			setPosPro(_posPronos);
		}
	}

	#region POPUP
	public void EnablePopUp (string msg ) {
		PopUp.SetActive(true);
		msgPopUp.text = msg;
	}

	public void DisablePopUp ( ) {
		PopUp.SetActive(false);
		msgPopUp.text = string.Empty;
	}

	#endregion


	#region GETS 
	public string getNameUser ( ){
		return _name;
	}
	public string getEmailUser( ){
		return _email;
	}
	public string getPassUser ( ){
		return _pass;
	}
	public string getCountryuser ( ){
		return _country;
	}
	public string getProvuser ( ){
		return _provincia;
	}
	public string getCelUser ( ){
		return _cel;		 
	}
	public string getBirthdayUser ( ){
		return _birthday;
	}
	public string getNickUser ( ){
		return _nickname;
	}
	public string getDesUser ( ){
		return _descripcion;
	}
	public int getCountryIdUser ( ){
		return _countryid;
	}
	public int getProvIdUser ( ){
		return _provid;
	}
	public string getFechaInit ( ){
		return _fechaInit;
	}
	public string getPosTa ( ){
		return _posTa;
	}
	public string getScoreTa ( ){
		return _scoreTa;
	}
	public string getPosPro ( ){
		return _posPronos;
	}
	public string getScorePro ( ){
		return _scorePronos;
	}

	public string getLastName ( ){
		return _lastname;
	}
	#endregion


	#region SETS
	public void setNameUser ( string newName ){
		_name = newName;
	}
	public void setEmailUser( string newmail ){
		_email =  newmail;
	}
	public void setPassUser ( string newPass ){
		_pass = newPass;
	}
	public void setCountryuser ( string newCountry ){
		_country = newCountry;
	}
	public void setProvuser ( string newProvicia ){
		_provincia = newProvicia;
	}
	public void setCelUser ( string newCel ){
			_cel = newCel;
	}
	public void setBirthdayUser ( string newBirth ){
		_birthday = newBirth; 
	}
	public void setNickUser ( string newNick ){
		_nickname = newNick;
	}
	public void setDesUser ( string newDesc ){
		_descripcion = newDesc;
	}
	public void setCountryIdUser ( int newCountryId ){
		_countryid = newCountryId;
	}
	public void setProvinciaId ( int newProvId ){
		_provid = newProvId;
	}
	public void setFechaInit ( string newFechaInit ){
		_fechaInit = newFechaInit;
		Debug.Log(_fechaInit);
	}
	public void setPosTa ( string newPosTa ){
		_posTa = newPosTa;
	}
	public void setScoreTa ( string newScoreTa ){
		_scoreTa = newScoreTa;
	}
	public void setPosPro ( string newPosPro ){
		_posPronos = newPosPro;
	}
	public void setScorePro ( string newScorePro ){
		_scorePronos = newScorePro;
	}
	public void setLastName ( string nesLastName ){
		_lastname = nesLastName;
	}
	#endregion
}

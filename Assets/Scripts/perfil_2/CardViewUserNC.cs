﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.ComponentModel.Design;
using System.Collections.Generic;

public class CardViewUserNC : MonoBehaviour {

	public static CardViewUserNC CardView;
	private string padrinoDownload = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/publicidad/oro";
	string ImageProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";
	public Image photProfile_ac;
	public Text[] DataCardView;
	public Button Back, Next;

	public bool activedPublic;
	[HideInInspector] public Image padrinoOro1, padrinoOro2;
	public int indexUser;
	public string idUser;

	void Start(){
		CardView = this;
		if(activedPublic){
			StartCoroutine(DownloadPadrino("1",padrinoOro1));
			StartCoroutine(DownloadPadrino("2",padrinoOro2));
		}
	}


	public void SetDatas ( string _id, int actualuser, string nickname, string fecha_ac, string scoreTa, string posTa, string scorePronos, string posPronos, string desc, Image photoProfile){
		idUser = _id;
		CardView.gameObject.SetActive(true);
		DataCardView[0].text = nickname;
		DataCardView[1].text = "DESDE "+fecha_ac;
		DataCardView[2].text = scoreTa;
		DataCardView[3].text = posTa;
		DataCardView[4].text = scorePronos;
		DataCardView[5].text = posPronos;
		DataCardView[6].text = desc;
		if ( EditProfile.myUser.dwnlSpriteUserProfile.Count > 0){
			for ( int i =0; i < EditProfile.myUser.dwnlSpriteUserProfile.Count;i ++){
				Debug.Log("i: "+ i  );
				if( EditProfile.myUser.dwnlSpriteUserProfile[i].name == idUser){
					photProfile_ac.sprite = EditProfile.myUser.dwnlSpriteUserProfile[i];
					break;
				}else{
					Debug.Log("i: "+ i + " c: "+EditProfile.myUser.dwnlSpriteUserProfile.Count );
					if( i == EditProfile.myUser.dwnlSpriteUserProfile.Count -1 ){
						photProfile_ac.sprite = EditProfile.myUser.dwnlSpriteUserProfile[0];
						StartCoroutine( DownloadImageProfile (_id) ) ;
						break;
					}
				}	
			}
		}else{
			StartCoroutine( DownloadImageProfile (_id) ) ;
		}


		DataApp2.main.DisableLoading();
		if(indexUser == 0){
			Back.interactable = false;
			Next.interactable = true;
		}else if (indexUser == PanelUsersNivelClub.MyMang.UserNC.Count-1){
			Back.interactable = true;
			Next.interactable = false;
		}else{
			Back.interactable = true;
			Next.interactable = true;
		}
	}

	IEnumerator  DownloadImageProfile (string id){
		string urlimg = ImageProfile+id+".jpg";
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		if(string.IsNullOrEmpty(d_img.error) ){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			photProfile_ac.sprite = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			photProfile_ac.color = new Color(255,255,255, alpha);
			Debug.Log(EditProfile.myUser.dwnlSpriteUser.Count);
			EditProfile.myUser.dwnlSpriteUserProfile.Add( photProfile_ac.sprite );
			EditProfile.myUser.dwnlSpriteUserProfile[EditProfile.myUser.dwnlSpriteUserProfile.Count-1].name = id;
		}else{
			float black = 0f;
			photProfile_ac.color = new Color(photProfile_ac.color.r, photProfile_ac.color.g, photProfile_ac.color.b, 225);
		}
	}

	IEnumerator DownloadPadrino( string index, Image img){
		string constr = padrinoDownload+index+".jpg";
		WWW consult = new WWW(constr);
		yield return consult;
		if( string.IsNullOrEmpty(consult.error)){
			Rect dim = new Rect(0,0,consult.texture.width,consult.texture.height);
			img.sprite = Sprite.Create(consult.texture,dim, Vector2.zero);
		}else{
			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión.\nIntentelo de nuevo.");
			DataApp2.main.DisableLoading();
		}
	}
}

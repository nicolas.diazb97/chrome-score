﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Security.Cryptography;

public class UsuarioNivelClub : MonoBehaviour {

	public static UsuarioNivelClub userNC;
	string urlDownloadActives 				= "Barcelona/ImagenesPerfilBarcelona/DownloadUserNivelClub.php?";
	string ImageProfile 					= "Barcelona/ImagenesPerfilBarcelona/ImagenesComprimidasRanking/";
	private string getUrlScoreTiroAmarillo  = "Barcelona/rankingTiroAmarillo/MostrarPuntajeXUsuario.php";
	private string getUrlPosTiroAmarillo    = "Barcelona/rankingTiroAmarillo/MostrarPosXUsuario.php";
	private string getUrlScorePronostico    = "Barcelona/Pronostico/Php/MostrarPuntajeXUsuario.php";
	private string getUrlPosPronostico      = "Barcelona/Pronostico/Php/MostrarPosXUsuario.php";
	public string Id;
	public int cantidad_Datos_Pedidos = 5;
	public Image profileImage;
	public Text [] Datas;
	public string PosTAmarillo, ScoreTAmarillo;
	public string PosPronostico, ScorePronostico;

	private IEnumerator currentRunCorutine;

	Coroutine c;

	int flag = 0;

	void Start ( ) {

		flag = 0;
		
		userNC = GetComponent<UsuarioNivelClub> ( );
		//StartCoroutine(DownloadDataUser(Id));
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		//StartCoroutine(DownloadDataTiroAmarillo(Id));
		//StartCoroutine(DownloadDataPronostico(Id));
	}

	public void sendCardView ( ) {
//		LoadingDummy.Loading.EnableLoading();
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		CardViewUserNC.CardView.indexUser = int.Parse(this.name);
		int indexSelect = CardViewUserNC.CardView.indexUser;
		CardViewUserNC.CardView.SetDatas(Id, /*FABIAN_CHANGE*/ indexSelect, Datas[1].text,Datas[4].text,ScoreTAmarillo,PosTAmarillo,ScorePronostico,PosPronostico,Datas[2].text, profileImage);
	}

	IEnumerator DownloadImageProfile ( ){
//		LoadingDummy.Loading.EnableLoading( );
		//InternetManager.main.loading.SetActive(true);
		string urlimg = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + ImageProfile+Id+".jpg";
		WWW d_img = new WWW(urlimg);
		yield return d_img;
//		LoadingDummy.Loading.DisableLoading( );
		//InternetManager.main.loading.SetActive(false);
		if(string.IsNullOrEmpty(d_img.error) && d_img.bytes.Length > 1699){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			profileImage.sprite = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			profileImage.color = new Color(255,255,255, alpha);
			EditProfile.myUser.dwnlSpriteUser.Add( profileImage.sprite );
			EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = Id;
		}else{
			// float black = 0f;
			profileImage.color = new Color(profileImage.color.r, profileImage.color.g, profileImage.color.b, 225);
		}
		
	}

	public void StopC(){
		if( c != null)
		StopCoroutine (c);
	}

	public void loadRankings (){

		StartCoroutine(DownloadDataTiroAmarillo(Id));
		StartCoroutine(DownloadDataPronostico(Id));
	}

	public void loadImg (){
		//currentRunCorutine = _DownloadImageProfile();

//		if (c == _DownloadImageProfile()){
//			StopCoroutine (c);
//		}

		flag++;

		profileImage.sprite = EditProfile.myUser.dwnlSpriteUser [0];
		//c = StartCoroutine (_DownloadImageProfile());
		LoadImageSel();
//		StartCoroutine(_DownloadImageProfile (flag));
	}


	void LoadImageSel(){
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
						for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
						//	Debug.Log("i: "+ i  );
							if( EditProfile.myUser.dwnlSpriteUser[i].name == Id){
								profileImage.sprite = EditProfile.myUser.dwnlSpriteUser[i];
//								InternetManager.main.loading.SetActive(false);
								break;
							}else{
								if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
									Debug.Log( "RECORRIDO Y NO ENCONTRADA" );
									profileImage.sprite = EditProfile.myUser.dwnlSpriteUser[0];
									c = StartCoroutine( _DownloadImageProfile (flag) ) ;
								//	break;
								}
							}	
						}
					}else{
				c = StartCoroutine( _DownloadImageProfile (flag) ) ;
			}

	}

	public  string ToAntiCache( string url) {
		string result="";
		if(url.Substring(url.Length -4,4   ) == ".php" || url.Substring(url.Length -4,4   ) == ".png"){
			result = url;
		}else{
			result = url;
		}
		return result;
	}


	IEnumerator _DownloadImageProfile (int f){
		string urlimg = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + ImageProfile+Id+".jpg";
		WWW d_img = new WWW( ToAntiCache( urlimg ));
	//	Debug.Log (ToAntiCache (urlimg));
		yield return d_img;

		if(string.IsNullOrEmpty(d_img.error) && d_img.bytes.Length > 1699){

			if(flag == f){

				Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
				profileImage.sprite = Sprite.Create(d_img.texture,dim, Vector2.zero);
				float alpha = 255f;
				profileImage.color = new Color(255,255,255, alpha);
				EditProfile.myUser.dwnlSpriteUser.Add( profileImage.sprite );
				EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = Id;
				//InternetManager.main.loading.SetActive(false);
			}

		}else{
			
			profileImage.color = new Color(profileImage.color.r, profileImage.color.g, profileImage.color.b, 225);
		}
		
//		if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
//			for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
//				Debug.Log("i: "+ i  );
//				if( EditProfile.myUser.dwnlSpriteUser[i].name == Id){
//					profileImage.sprite = EditProfile.myUser.dwnlSpriteUser[i];
//					break;
//				}else{
//					Debug.Log("i: "+ i + " c: "+EditProfile.myUser.dwnlSpriteUser.Count );
//					if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
//						profileImage.sprite = EditProfile.myUser.dwnlSpriteUser[0];
//						StartCoroutine( DownloadImageProfile () ) ;
//						break;
//					}
//				}	
//			}
//		}else{
//			StartCoroutine( DownloadImageProfile () ) ;
//		}

	}

	IEnumerator DownloadDataTiroAmarillo (string id ){
		//PUNTAJE TIRO AMARILLO
		string scoreTa = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + getUrlScoreTiroAmarillo+"?userID="+id;
		WWW scoreTa_c = new WWW(scoreTa);
		yield return scoreTa_c;
		if( !string.IsNullOrEmpty(scoreTa_c.text)){
			ScoreTAmarillo = scoreTa_c.text.Trim();
		}
		//POSICION TIRO AMARILLO
		string posTa = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + getUrlPosTiroAmarillo+"?userID="+id;
		WWW posTa_c = new WWW(posTa);
		yield return posTa_c;
		if( !string.IsNullOrEmpty(posTa_c.text)){
			PosTAmarillo = posTa_c.text.Trim();
		}
	}

	IEnumerator DownloadDataPronostico (string id ){
		//PUNTAJE PRONOSTICO
		string scoreTa = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + getUrlScorePronostico+"?userID="+id;
		WWW scoreTa_c = new WWW(scoreTa);
		yield return scoreTa_c;
		if( !string.IsNullOrEmpty(scoreTa_c.text)){
			ScorePronostico = scoreTa_c.text.Trim();
		}
		//POSICION PRONOSTICO
		string posTa = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + getUrlPosPronostico+"?userID="+id;
		WWW posTa_c = new WWW(posTa);
		yield return posTa_c;
		if( !string.IsNullOrEmpty(posTa_c.text)){
			PosPronostico = posTa_c.text.Trim();
		}
	}

	IEnumerator DownloadDataUser(string id) {
//		LoadingDummy.Loading.EnableLoading();
		int count =0;
		string c = "http://2WAYSPORTS.COM/2waysports/Ecuador/" + urlDownloadActives + "opc=LoadData&userID="+id;
		WWW consult = new WWW(c);
		yield return consult;
		if( !string.IsNullOrEmpty(consult.text)){
			StartCoroutine(DownloadImageProfile());
			string data = consult.text;
			string temp= null;
			for(int i = 0; i < data.Length; i++){
				if(count < cantidad_Datos_Pedidos){
					if(data[i] != '[' && data[i] != ']'){
						temp = temp+ data[i];
					}else{
						if(data[i] == ']'){ 
							Datas[count].text = temp;
							count++;
							temp=null;
						}
					}
				}
			}
//			LoadingDummy.Loading.DisableLoading();
		}

		PanelUsersNivelClub.MyMang.downloadUserComplete++;
		if( PanelUsersNivelClub.MyMang.downloadUserComplete == 20 || ( PanelUsersNivelClub.MyMang.totalUsers - PanelUsersNivelClub.MyMang.initUsers == PanelUsersNivelClub.MyMang.downloadUserComplete ) ){
			//InternetManager.main.loading.SetActive(false);
		}
	}

	void OnEnable(){
		flag = 0;
	}
}

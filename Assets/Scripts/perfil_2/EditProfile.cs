﻿using UnityEngine;
using System.Collections;
//using ImageVideoContactPicker;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Security.Cryptography;
using UnityEngine.EventSystems;
using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;
using System.Configuration;


public class EditProfile : MonoBehaviour {

	public static EditProfile myUser;
	[HideInInspector]public GameObject SecondPlane_Video;
	public GameObject BlockPanel;
//	public LoadCarnet MyCarnet;
	string rutaDelArchivo;
	#region URLS PHPS
	[HideInInspector]
	public string UploadImage_Profile = "http://roguez.us/ImagenesPerfilBarcelonaUnity/SubirImagen.php";
	string ImageProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/";
	string uploadVideoProfile = "ftp://ftp.roguez.us/public_html/2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/"; 
	string InfoProfile = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/EditDataProfiel.php?";
	string readURLMail = "http://2waysports.com/2waysports/Ecuador/Barcelona/Registro/ValidarMail.php";
	#endregion	
	[HideInInspector]
	public Text nicknameView;
	[HideInInspector]
	public float sizeload;	
	[Header("Vista - Foto Perfil")]
	public Image ProfilePhotoView;
	[Header("Foto Perfil")]
	public Image ProfilePhoto;
	[Header("INPUTS DATA USER")]
	//	[HideInInspector]
	public InputField [] InfosUser;
	[HideInInspector]
	public Text [] InfosView;
	[Space(20f)]
	public InputField name;
	public InputField last_name;
	public InputField email_t;
	public InputField pass_t;
	public Dropdown country_t;
	public Dropdown Provincia_t;
	public InputField ind_cel_t;
	public InputField cel_t;
	public InputField nickName;
	public InputField descripcion;
	[Header("Fecha de naciemiento")]
	public InputField day;
	public InputField month;
	public InputField year;
	public Toggle gen;
	public Toggle Masculino,Femenino;
	public string Genero;
	[Space(20f)]
	public List <Sprite> dwnlSpriteUser;
	public List <Sprite> dwnlSpriteUserProfile;
	[HideInInspector]public GameObject PopUpInitValidation;
	[HideInInspector]public GameObject InitTransaccional;
	[HideInInspector]public GameObject ConfirmUpdate;
	#region   UNITY EVENTOS QUE CONTROLAN EL PROCESO DE EDITAR PERFIL ----
	public UnityEvent finalEdit = new UnityEvent ( );
	public UnityEvent InEditProcess  = new UnityEvent ( );
	#endregion

	public string NumeracionHincha;
	Texture2D texture;
	bool newPhoto, isviewpass; 
	int _id;
	string type = "";
	bool DownloadComplete;
	bool isComplete;
	delegate void Delegado(WWW www);
	bool IsNext;

	/// METHODS -----------------------

	public void changeGenero( ){
		if( gen.isOn )	
			Genero = "Masculino";
		else
			Genero = "Femenino";
	}

	public void InitValidation ( ){ 
		Debug.Log( "llmamando validacion" );
		RegistrationUserNClub.Metodo = PlayerPrefs.GetString("Metodo");
		StartCoroutine( DataApp2.main.CheckInternet( internet => { 
			if( internet ){
				if(DataApp.main.IsRegistered( ) ){   
					_id = DataApp.main.GetMyID( );
					if(TransactionalProcess.isActive == 1 ){
						
						//StartCoroutine(DownloadDataUser(_id.ToString()));
						StartCoroutine(DownloadHinchaNumero(_id.ToString()));
						StartCoroutine(RegistrationUserNClub.MyResgUser.DownloadIsRegistroComplete(_id.ToString()) );
						StartCoroutine(DownloadFechaCaducidad(_id.ToString()));	
						PopUpInitValidation.SetActive(false);
					}else{
						BlockPanel.SetActive(true);
						Debug.Log("ya no me lo chupen x2"); 
						//InitTransaccional.SetActive(true);
					}
				}else{
					PopUpInitValidation.SetActive(true);
				}
				Debug.Log("ya no me lo chupen "); 
			}else{
				Debug.Log("CHUPEME ESTE PENCO"); 	
			}
		}));
		DataApp2.main.DisableLoading();
	}

	public void SelectIdImage ( string id){
		selectedSprite( id);
	}


	IEnumerator DownloadGenero (string id ){

		string downInfoUser = InfoProfile+"?opc=LoadGen&userID="+id;
		WWW url_downdload = new WWW(downInfoUser);
		yield return url_downdload;
		//Debug.Log ( url_downdload.text );
		if( !string.IsNullOrEmpty(url_downdload.text)){
			Genero = url_downdload.text.Trim ();
			if( Genero == "[Masculino]"){
				Masculino.isOn = true;
				Femenino.isOn = false;
			}else{
				Femenino.isOn = true;
				Masculino.isOn = false;
			}
		}
	}

	public Sprite selectedSprite( string Id ){
		Sprite spriteRet = null;
		if ( EditProfile.myUser.dwnlSpriteUser.Count > 0){
			for ( int i =0; i < EditProfile.myUser.dwnlSpriteUser.Count;i ++){
				Debug.Log("i: "+ i  );
				if( EditProfile.myUser.dwnlSpriteUser[i].name == Id){
					spriteRet = EditProfile.myUser.dwnlSpriteUser[i];
					return spriteRet;
					break;
				}else{
					Debug.Log("i: "+ i + " c: "+EditProfile.myUser.dwnlSpriteUser.Count );
					if( i == EditProfile.myUser.dwnlSpriteUser.Count -1 ){
						spriteRet = EditProfile.myUser.dwnlSpriteUser[0];
						StartCoroutine( DownloadSpriteUser( newSprite=> {  
							spriteRet =  newSprite;
						}, Id ));
						return spriteRet;
						break;
					}
				}	
			}
		}else{
			Debug.Log("perro");
			StartCoroutine( DownloadSpriteUser( newSprite =>{
				spriteRet =  newSprite;
			}, Id ));

		}
		return spriteRet;
	}

	IEnumerator DownloadSpriteUser  ( System.Action<Sprite> newSprite , string id){
		yield return new WaitForSeconds( 2 );
		Sprite result = null;
		string urlimg = ImageProfile+"/ImagenesComprimidasRanking/"+id+".jpg";
		Debug.Log("la foto es]: " +urlimg );
		WWW d_img = new WWW(urlimg);
		yield return d_img;
		if(string.IsNullOrEmpty(d_img.error) ){
			Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
			result = Sprite.Create(d_img.texture,dim, Vector2.zero);
			float alpha = 255f;
			EditProfile.myUser.dwnlSpriteUser.Add( result );
			Debug.Log("c--: "+ EditProfile.myUser.dwnlSpriteUser.Count );
			EditProfile.myUser.dwnlSpriteUser[EditProfile.myUser.dwnlSpriteUser.Count-1].name = id;
		}else{
			result = null;
		}
		newSprite( result );
	}




	public IEnumerator DownloadFechaCaducidad(string id ){
		Debug.Log("fc");
		int Mes = 0;
		int año = 0;
		string constr = InfoProfile + "opc=loadFechaCaducidad&userID="+ id;
		WWW fvencido = new WWW (constr);
		yield return fvencido;
		if ( !string.IsNullOrEmpty( fvencido.text ) && fvencido.text != " " ){
			Mes = (fvencido.text.Contains("ENE")) ?Mes=1 :(fvencido.text.Contains("FEB"))?Mes=2:(fvencido.text.Contains("MAR"))?Mes=3:(fvencido.text.Contains("ABRIL"))?Mes=4:(fvencido.text.Contains("MAYO"))?Mes=5:(fvencido.text.Contains("JUNIO"))?Mes=6:(fvencido.text.Contains("JULIO"))?Mes=7:(fvencido.text.Contains("AGOS"))?Mes=8:(fvencido.text.Contains("SEPT"))?Mes=9:(fvencido.text.Contains("OCT"))?Mes=10:(fvencido.text.Contains("NOV"))?Mes=11: (fvencido.text.Contains("DIC"))?Mes=12:0;
			año = int.Parse(fvencido.text.Trim( new char[] {' ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'}));
			int nowMes = DateTime.Now.Month;
			int nowAño = DateTime.Now.Year;
			if(Mes == nowMes ){
				 if (Mes < nowMes ){
					if( año  <= nowAño){
						StartCoroutine(FinishSuscription(id));
						ProfileUser.MyUser.EnablePopUp("Su suscripción como Hincha Nivel Club ha terminado.");
					}
				}else{
					if( año  <= nowAño){
						StartCoroutine(FinishSuscription(id));
						ProfileUser.MyUser.EnablePopUp("Su suscripción como Hincha Nivel Club ha terminado.");
					}
				}
			}
		}
	}

	public IEnumerator FinishSuscription( string id){
		string cancelSus = InfoProfile + "opc=reactiveHnc&userID="+ id;
		WWW v = new WWW (cancelSus);
		yield return v;
		if ( string.IsNullOrEmpty( v.error ) ){ 
			Debug.Log("TIEMPO TERMIANDO AMIGO");
			TransactionalProcess.isActive = 0;
			RegistrationUserNClub.MyResgUser.SetRegisterHNC(0);
			InitValidation( );
		}
	}

//	public void SendInfoCarnet( ){
//		LoadingDummy.Loading.EnableLoading( );
//		MyCarnet.LoadData((name.text +" "+last_name.text), InfosView[2].text , ("Hincha # "+ NumeracionHincha ), ProfilePhoto);
//	}


	public void ChangePhotoProfile ( ) {  ///  abre galeria y cambia la imagen de perfil
		DataApp2.main.EnableLoading();
		type = "img";
		InEditProcess.Invoke ( ); // evento donde edito mi información...
		newPhoto = true;
//		PickerEventListener.onImageSelect += OnImageSelect;
//		PickerEventListener.onError += RespuestaError;
//		PickerEventListener.onCancel += OnCancelSelect;
//		#if UNITY_ANDROID
//		AndroidPicker.BrowseImage(true);
//		#elif UNITY_IOS 
//		IOSPicker.BrowseImage(true); 
//		#endif
	}

	public void ChangeVideo ( ) {  ///  abre galeria y elije video a subir
		DataApp2.main.EnableLoading();
		type = "vid";
		InEditProcess.Invoke ( ); // evento donde edito mi información...
//		PickerEventListener.onVideoSelect += OnVideoSelect;
//		PickerEventListener.onError += RespuestaError;
//		PickerEventListener.onCancel += OnCancelSelect;
//		#if UNITY_ANDROID
//		AndroidPicker.BrowseVideo();
//		#elif UNITY_IOS
//		IOSPicker.BrowseVideo(); 
//		#endif
	}

	void OnImageSelect( string rutaImagen ) { // Selecciona la ruta de la imagen en el dispositivo
		rutaDelArchivo = rutaImagen;
		StartCoroutine( TraerContenidoWeb( "file://" + rutaDelArchivo , AsignarImagen ) );
	}

	IEnumerator TraerContenidoWeb ( string enlaceRecibido, Delegado delegado ) { 
		WWW www = new WWW (enlaceRecibido);
		yield return www;
		if(delegado != null){   
			delegado(www); 
		}
	}


	void OnCancelSelect( ) {
		ConfirmUpdate.SetActive(false);
		finalEdit.Invoke( );
		newPhoto= false;
		DataApp2.main.DisableLoading( );
		ProfileUser.MyUser.EnablePopUp("Error al actualizar los datos.");

	}

	void RespuestaError( string errorMsg ) {
		ConfirmUpdate.SetActive(false);
		finalEdit.Invoke( );
		newPhoto= false;
		DataApp2.main.DisableLoading( );
		ProfileUser.MyUser.EnablePopUp("Error al actualizar los datos.");
	}

	/// SUBIR ARCHIVO A LA BASE DE DATOS Y ASIGNARLA EN PERFIL ----------------------------------------------- 
	public void SaveDataProfile ( ) {
		// activamos aca el loading ---------------- 
		if(newPhoto){ // variable true si vas a guardar nueva iamgen
			StartCoroutine(CorutinaSubirArchivo(  rutaDelArchivo ,  UploadImage_Profile));  
			newPhoto = false;
			ProfilePhoto.sprite = ProfilePhotoView.sprite;  // asigno la iamgen de perfil de vista previa a la vista seleccionada   
			//			StartCoroutine(DownloadImageProfile( ));
		}
		SendInfoDataView( );
	}

	public void SendInfoDataView ( ){
		ProfileUser.MyUser.setFechaInit(InfosView[2].text);
		ProfileUser.MyUser.setNickUser(nickName.text);
		ProfileUser.MyUser.setDesUser(descripcion.text);
		string temp_nick = ProfileUser.MyUser.getNickUser( );
		temp_nick = temp_nick.Replace(" ","%20");
		string temp_des = ProfileUser.MyUser.getDesUser( );
		temp_des = temp_des.Replace(" ","%20");
		StartCoroutine(_SaveDataView(temp_nick, temp_des ));
	}

	IEnumerator _SaveDataView(string nick, string desc){
		if(DataApp.main.IsRegistered( ) ){    // y agregar si el usuario es HINCHA NIVEL CLUB // 
			_id = DataApp.main.GetMyID();
			string cons = InfoProfile;
			string form = cons+"opc=UpdateView"+"&userID="+_id+"&nick="+nick+"&des="+desc;
			WWW newInfoView = new WWW (form);	
			yield return newInfoView;	
			if(!string.IsNullOrEmpty(newInfoView.text)){
				InfosView[0].text = nickName.text;
				InfosView[1].text = descripcion.text;
				finalEdit.Invoke();
				StartCoroutine(DownloadDataView(_id.ToString()));
				ProfileUser.MyUser.getDatasMyCard(ProfilePhoto);
			}else{
				ProfileUser.MyUser.EnablePopUp("Error al actualizar los datos.\n\nRevisa tu conexión a internet.");
			}
		}else{
			Debug.Log("ID VACIO");
		}
		DataApp2.main.DisableLoading();
	}

	public void SendInfoData( ){  // Actualiza los datos nuevos del usuario 
		ProfileUser.MyUser.setNameUser(name.text);
		ProfileUser.MyUser.setEmailUser(email_t.text);
		ProfileUser.MyUser.setPassUser(pass_t.text);
		ProfileUser.MyUser.setCelUser(cel_t.text);
		ProfileUser.MyUser.setCountryuser(country_t.captionText.text);
		ProfileUser.MyUser.setProvuser(Provincia_t.captionText.text);
		ProfileUser.MyUser.setBirthdayUser(day.text+"/"+month.text+"/"+year.text);
		ProfileUser.MyUser.setCountryIdUser(country_t.value);
		ProfileUser.MyUser.setProvinciaId(Provincia_t.value);
		ProfileUser.MyUser.setLastName(last_name.text);
		StartCoroutine( confirmMail ( ));
	}

	public void Indicativo( ){
		ProfileUser.MyUser.setCountryIdUser( country_t.value );
		if(ProfileUser.MyUser.getCountryIdUser( ) == 0){
			ind_cel_t.text = "+593 ";
		}else if(ProfileUser.MyUser.getCountryIdUser( ) == 1){
			ind_cel_t.text = "+34 ";
		}else if(ProfileUser.MyUser.getCountryIdUser( ) == 2){
			ind_cel_t.text = "+1 ";
		}
	}

	public void disableProvincia( ){
		if(country_t.value != 0) {
			Provincia_t.gameObject.SetActive(false);
		}else{
			Provincia_t.gameObject.SetActive(true);
		}
	} 

	IEnumerator _SaveDataProfile(string name, string email, string pass,string ind, string cel, string country_id, string country, string birth, string metodo, string lastname,  string provincia_id , string provincia){
		DataApp2.main.EnableLoading();
		if(DataApp.main.IsRegistered( ) ){    // y agregar si el usuario es HINCHA NIVEL CLUB // 
			_id = DataApp.main.GetMyID();
			if(country_id != "0"){
				provincia_id = "0";
				provincia = "null";
			}
			string cons = InfoProfile;
			string form = cons+"opc=UpdateData"+"&userID="+_id+"&name="+name+"&email="+email+"&pass="+pass+"&ind="+ind+"&cel="+cel+"&paisid="+country_id+"&pais="+country+"&fechan="+birth+ "&Metodo="+metodo+ "&apellido="+lastname +"&provinciaid="+provincia_id + "&provincia="+provincia;
			WWW newInfoUser = new WWW (form);	
			Debug.Log(form);
			yield return newInfoUser;	
			if(!string.IsNullOrEmpty(newInfoUser.text)){
				PlayerPrefs.SetInt("MyCountry",country_t.value);
				PlayerPrefs.SetInt("MyProv",Provincia_t.value);
				finalEdit.Invoke();
				ProfileUser.MyUser.EnablePopUp("Su actualización ha sido un éxito.");
			}else{
				ProfileUser.MyUser.EnablePopUp("Fallo en la conexión a internet. \nIntentelo de nuevo.");
			}
		}else{
			Debug.Log("ID VACIO");
			// lanzar el POP DE REGISTRARSE Y/O el de Pagar HINCHA NIVEL CLUB
		}
		DataApp2.main.DisableLoading();
	}

	IEnumerator DownloadDataView( string id ){
		DataApp2.main.EnableLoading( );
		string downloadInfoView = InfoProfile+"opc=LoadView&userID="+id;
		WWW url_downloadView = new WWW(downloadInfoView);
		yield return url_downloadView;
		//		Debug.Log("Datos Descargados VIEW del usuario: "+ url_downloadView.text);
		int input_select = 0;
		if( !string.IsNullOrEmpty(url_downloadView.text )){
			string temp_down = url_downloadView.text;
			InfosView[0].text ="";
			InfosView[1].text ="";
			InfosView[2].text ="DESDE ";
			for(int i = 0; i< temp_down.Length ;i++){
				if(temp_down[i] == ']' || temp_down[i] == ']') {
					input_select++;
				}else{
					if(temp_down[i] != '[')
						InfosView[input_select].text = InfosView[input_select].text + temp_down[i];
				}
			}
		}
		if( string.IsNullOrEmpty(InfosView[0].text )){
			nicknameView.text ="User"+id;
		}else{
			nicknameView.text =InfosView[0].text;
		}
		// Desactivamos aca el loading ---------------- 
	}

	public void ClearDataUser(){
		for(int i =0; i < InfosUser.Length;i++){
			InfosUser[i].text = "";
		}
	}

	IEnumerator DownloadHinchaNumero( string id) {
		string downInfoUser = InfoProfile+"opc=hinchaN&userID="+id;
		Debug.Log(downInfoUser);
		WWW url_downdload = new WWW(downInfoUser);
		yield return url_downdload;
		if( !string.IsNullOrEmpty(url_downdload.text)){
			NumeracionHincha = url_downdload.text;
		}
	}

	IEnumerator  DownloadDataUser (string id){
		DataApp2.main.EnableLoading();
		foreach( InputField f in InfosUser){
			f.text = "";
		}
		string downInfoUser = InfoProfile+"opc=LoadData&userID="+id;
		WWW url_downdload = new WWW(downInfoUser);
		yield return url_downdload;
		Debug.Log("my id es :"+ id +"-"+url_downdload.text);
		int input_select = 0;
		if( !string.IsNullOrEmpty(url_downdload.text)){
			string temp_down = url_downdload.text;
			temp_down = temp_down.Replace("/","][");
			//ClearDataUser();
			StartCoroutine(DownloadDataView(id));
			for (int i =0; i < temp_down.Length; i++){
				if(temp_down[i] == ']' || temp_down[i] == ']') {
					input_select++;
				}else{
					if(temp_down[i] != '[')
						InfosUser[input_select].text = InfosUser[input_select].text + temp_down[i];
				}
			}
			if(!string.IsNullOrEmpty(InfosUser[5].text))
				country_t.value = int.Parse(InfosUser[5].text);
			if(!string.IsNullOrEmpty(InfosUser[10].text))
				Provincia_t.value = int.Parse(InfosUser[10].text);
		}else{
			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión a internet. \nIntentelo de nuevo.");
		}
		this.GetComponent<RegistrationUserNClub>().email_t.text = DataApp.main.GetEmail();
		StartCoroutine(DownloadImageProfile());
	}



	IEnumerator UploadImgCompress( ){
		string nameImg = _id+".jpg";

		Texture2D tComprimida = new Texture2D(4, 4, TextureFormat.ARGB32, false);
		WWW www = new WWW(	ImageProfile + nameImg );
		yield return www;
		www.LoadImageIntoTexture( tComprimida );
//		TextureScale.Bilinear (tComprimida, 80, 80);
		Rect size = new Rect (0, 0, tComprimida.width, tComprimida.height);

		string ftpurl;
		string ftpusername = "roguez"; // e.g. username
		string ftppassword = "Wisepassword1!"; // e.g. password
		ftpurl = "ftp://ftp.roguez.us/public_html/2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/ImagenesComprimidasRanking/" + nameImg;

		FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpurl);
		request.Method = WebRequestMethods.Ftp.UploadFile;
		request.Credentials = new NetworkCredential(ftpusername, ftppassword);

		using (Stream request_stream = request.GetRequestStream())	{
			byte[] bytes = tComprimida.EncodeToJPG();
			request_stream.Write(bytes, 0, bytes.Length);
			request_stream.Close();
			yield return request_stream;
		}
	}


	IEnumerator CorutinaSubirArchivo (string localFileName, string uploadURL) { // Guarda la IMAGEN Seleccionada en la base de datos 
		DataApp2.main.EnableLoading();
		WWW localFile = new WWW("file://" + localFileName);
		yield return localFile;
		DownloadComplete = false;
		if (localFile.error == null){
			finalEdit.Invoke();
		} else {
			finalEdit.Invoke();
			yield break;  	
		}

		if( type == "img"){
			WWWForm postForm = new WWWForm( );
			string nameImg = _id+".jpg";
			postForm.AddBinaryData("theFile",localFile.bytes,nameImg,"text/plain");
			WWW upload = new WWW(uploadURL,postForm);
			yield return upload;
			if (upload.error == null){
				finalEdit.Invoke();
				StartCoroutine(UploadImgCompress());
			} else {
				StartCoroutine(DownloadImageProfile());
				finalEdit.Invoke();
			}
		}else{
			DataApp2.main.DisableLoading( );
			if(localFile.bytes.Length <= 14000000){
				sizeload = localFile.bytes.Length;
				SecondPlane_Video.SetActive(true);
				WWWForm postForm = new WWWForm( );
				string nameImg = _id+".mov";
				postForm.AddBinaryData("theFile",localFile.bytes,nameImg,"video/quicktime");
				WWW upload = new WWW( uploadURL , postForm );
				yield return upload;
				if (upload.error == null){
					finalEdit.Invoke();
					DataApp2.main.DisableLoading( );
					ProfileUser.MyUser.EnablePopUp("Video subidó con éxito.");
					StopAllCoroutines();
				} else {
					finalEdit.Invoke();
					DataApp2.main.DisableLoading( );
					ProfileUser.MyUser.EnablePopUp("No se pudo subir el video, \nIntentelo de nuevo.");
				}
				SecondPlane_Video.SetActive(false);
			}else{
				DataApp2.main.DisableLoading( );
				ProfileUser.MyUser.EnablePopUp("Archivo demasiado pesado.");
			}
		}
		DataApp2.main.DisableLoading();
	}



	IEnumerator DownloadImageProfile ( ){
		if(!DownloadComplete){
			string urlimg = ImageProfile+_id+".jpg";
			WWW d_img = new WWW(urlimg);
			yield return d_img;
			if(string.IsNullOrEmpty(d_img.error) && d_img.bytes.Length > 1699){
				Rect dim = new Rect(0,0,d_img.texture.width,d_img.texture.height);
				ProfilePhoto.sprite = Sprite.Create(d_img.texture,dim, Vector2.zero);
				float alpha = 255f;
				ProfilePhoto.color = new Color(255,255,255, alpha);
				DownloadComplete = true;
			}else{
				float black = 0f;
				ProfilePhoto.color = new Color(ProfilePhoto.color.r, ProfilePhoto.color.g, ProfilePhoto.color.b, 225);
			}
		}
		DataApp2.main.DisableLoading();
	}

	void AsignarImagen( WWW wwwRecibido ){  // asigan la imagen Descargada del servidor
		Texture2D texturaWeb = wwwRecibido.texture;
		Sprite spriteWeb = Sprite.Create(texturaWeb,new Rect(0,0,texturaWeb.width,texturaWeb.height),new Vector2(0.5f,0.5f));
		ProfilePhotoView.sprite = spriteWeb;
//		PickerEventListener.onImageSelect -= OnImageSelect;
//		PickerEventListener.onError -= RespuestaError ;
		DataApp2.main.DisableLoading();
	}

	void OnImageLoad(string imgPath, Texture2D tex) { // Asignar la imagen de vista  seleccionda Desde la app
		Debug.Log ("Image Location : "+imgPath);
		texture = tex;
		ProfilePhotoView.sprite = Sprite.Create(texture, new Rect(0,0,texture.width,texture.height),new Vector2(0.5f,0.5f) );
		DataApp2.main.DisableLoading();
	}

	public void contentTypePass( ){
		isviewpass =! isviewpass;
		//		if( isviewpass ){Confirmpass_t.contentType = InputField.ContentType.Standard;} else{ Confirmpass_t.contentType = InputField.ContentType.Password;}
	}

	public void NotEraserInfoView(){
		nickName.text = InfosView[0].text;
		descripcion.text = InfosView[1].text;
	}

	public void UpdateData (){
		// activamos aca el loading ---------------- 
		string temp_name = ProfileUser.MyUser.getNameUser( );
		temp_name = temp_name.Replace(" ","%20");
		string temp_apellido = ProfileUser.MyUser.getLastName( );
		temp_apellido = temp_apellido.Replace(" ","%20");
		string temp_email = ProfileUser.MyUser.getEmailUser( );
		temp_email = temp_email.Replace(" ","%20");
		string temp_pass = ProfileUser.MyUser.getPassUser( );
		temp_pass = temp_pass.Replace(" ","%20");
		string temp_country = ProfileUser.MyUser.getCountryuser( );
		temp_country = temp_country.Replace(" ","%20");
		string temp_provincia = ProfileUser.MyUser.getProvuser( );
		temp_provincia = temp_provincia.Replace(" ","%20");
		string temp_birthday = ProfileUser.MyUser.getBirthdayUser( );
		temp_birthday = temp_birthday.Replace(" ","%20");
		string temp_cel = ProfileUser.MyUser.getCelUser( );
		string temp_ind = ind_cel_t.text;
		temp_cel = temp_cel.Replace(" ","%20");
		int pais_id = ProfileUser.MyUser.getCountryIdUser();
		int provincia_id = ProfileUser.MyUser.getProvIdUser();
		StartCoroutine(_SaveDataProfile(temp_name, temp_email, temp_pass, temp_ind, temp_cel, pais_id.ToString(), temp_country, temp_birthday, RegistrationUserNClub.Metodo,temp_apellido,provincia_id.ToString(), temp_provincia));
	}



	private IEnumerator confirmMail() {
		string answerMail = "";
		string miMail = "";
		string urlStringMiMail = readURLMail + "?opc=mimail"+"&id=" +DataApp.main.GetMyID() ; 
		WWW getMail = new WWW (urlStringMiMail);
		yield return getMail;
		miMail = getMail.text;
		Debug.Log("my email: " + miMail);
		string urlStringMails = readURLMail + "?opc=mails" + "&correo=" + WWW.EscapeURL (email_t.text); 
		WWW getMails = new WWW (urlStringMails);
		yield return getMails;
		answerMail = getMails.text;
		Debug.Log("OTRO email: " + answerMail);
		if(email_t.text != ""){
			if( string.IsNullOrEmpty(getMails.error)){
				if ( answerMail == "" ) {
					ConfirmUpdate.SetActive(true);
				}else if( miMail == answerMail ){
					ConfirmUpdate.SetActive(true);
				}else{
					ProfileUser.MyUser.EnablePopUp("Ese correo ya esta Registrado.\nIntenta ingresando otro.");
				}
			}
		}else{
			ProfileUser.MyUser.EnablePopUp("Ingresa tu correo antiguo o uno nuevo.");
		}
	}

	void OnApplicationPause( bool pauseStatus  ){
		if(pauseStatus){
			//			LoadingDummy.Loading.DisableLoading();
			//			finalEdit.Invoke ();
		}
	}

	#region FUNCIONES DEL PLUGIN!!! -----

	void OnVideoSelect(string vidPaht) {
		rutaDelArchivo = vidPaht;
		StartCoroutine(CorutinaSubirArchivo( rutaDelArchivo ,  UploadImage_Profile));  
	}


	void OnEnable()
	{
//		PickerEventListener.onImageSelect += OnImageSelect;
//		PickerEventListener.onImageLoad += OnImageLoad;
//		PickerEventListener.onVideoSelect += OnVideoSelect;
//		PickerEventListener.onCancel += OnCancelSelect;
//		InitValidation();
//		PickerEventListener.onError += RespuestaError;
		//		PickerEventListener.onCancel += OnCancel;

	}

	void OnDisable() {
//		PickerEventListener.onImageSelect -= OnImageSelect;
//		PickerEventListener.onImageLoad -= OnImageLoad;
//		PickerEventListener.onVideoSelect -= OnVideoSelect;
//		PickerEventListener.onCancel -= OnCancelSelect;
//		PickerEventListener.onError -= RespuestaError;
		DataApp2.main.DisableLoading();
		//		PickerEventListener.onCancel -= OnCancel;
	}



	#endregion


}

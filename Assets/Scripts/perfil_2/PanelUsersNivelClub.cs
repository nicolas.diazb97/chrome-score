﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class PanelUsersNivelClub : MonoBehaviour {

	public static PanelUsersNivelClub MyMang;
	string urlDownloadActives = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/DownloadUserNivelClub.php?";
	string urlPhpUsers = "http://2waysports.com/2waysports/Ecuador/Barcelona/ImagenesPerfilBarcelona/userSearch.php?";
	public CardViewUserNC cardViewUSER;
//	public GameObject [] UserNC;
	public List<GameObject> UserNC;
	public GameObject  ReloadUsers;
	public Text [] Letters;
	 public GameObject User_NivelClub;
	 public GameObject Padre_User;
	int actualpos, afterpos;
	 public Text Nofound;
	 public Text letterSelected;
	public float minSwipeDistY;
	public float minSwipeDistX;
	private Vector2 startPos;
	public int downloadUserComplete;
	public int initUsers;
	public int finalUsers;
	public int totalUsers;
	public RectTransform content;
	public InputField getInput_;
	string typeDownload;

	private Text currentLetter;
	private int currentInitUsers = 0;
	private int currentFinalUsers = 0;

	private bool enableUptadePanel = false;
	public GameObject ScrollContent;
	bool isRefresh;

	[SerializeField]
	SearchUserList userSearchList = new SearchUserList();
	// Use this for initialization
	void Start () {
		MyMang = this;
		enableUptadePanel = true;
		StartCoroutine( DownloadInfo( initUsers , finalUsers ));
		//StartCoroutine (TestUserSearchs ());
	}

	void OnEnable( ){
		CardViewUserNC.CardView = cardViewUSER;
	}

	void Update(){
		#if UNITY_ANDROID || UNITY_IOS
		// SwipeChangePos( );
		#endif

		if (enableUptadePanel && ! DataApp2.main.gameObject.activeSelf) {
			DataApp2.main.EnableLoading();
//			InternetManager.main.loading.SetActive (true);
		}


		if(ScrollContent.GetComponent<RectTransform>().anchoredPosition.y < -50 && !isRefresh&& !Input.GetMouseButton(0)){
			isRefresh = true;
			restDownLoadUsers( 20 );
		}else if(ScrollContent.GetComponent<RectTransform>().anchoredPosition.y > 1900 && !isRefresh && !Input.GetMouseButton(0)){
			isRefresh = true;
			plusDownLoadUsers( 20 );
		}
	}

	IEnumerator StringUserSearchs (string searchName){

//		InternetManager.main.loading.SetActive (true);
		DataApp2.main.EnableLoading();
		//initUsers = 0;
		//finalUsers = 20;

		string _name = searchName.Replace (" ", "%20");

		WWW getData = new WWW (urlPhpUsers + "opc=Search&inputSearch=" + _name + "&mUserID=" + DataApp.main.GetMyID ());
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {
			if (getData.text != "Uncreated") {
				userSearchList.dataList.Clear ();
				initUsers = 0;
				finalUsers = 20;
				userSearchList = JsonUtility.FromJson<SearchUserList> (getData.text);
				totalUsers = userSearchList.dataList.Count;
				Debug.Log ("count: " + userSearchList.dataList.Count);
				Debug.Log (getData.text);
				sizeLetter(false);
				StartCoroutine(MakeSearchPanels (initUsers, finalUsers));

			} else {
				ProfileUser.MyUser.EnablePopUp("No se encontraron usuarios que contengan <b>"+searchName+"</b>.");
//				InternetManager.main.loading.SetActive (false);
				DataApp2.main.DisableLoading();
			}
		} else {
			
			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión.\\nIntentelo de nuevo.");
//			InternetManager.main.loading.SetActive (false);
			DataApp2.main.DisableLoading();
			Debug.Log ("No hay datos");
		}

		Debug.Log (getData.text);
	}

	IEnumerator CharUserSearchs (string LetterName){

//		InternetManager.main.loading.SetActive (true);
		DataApp2.main.EnableLoading();
		foreach (GameObject obj in UserNC) {

			obj.GetComponent<UsuarioNivelClub> ().StopC ();
		}

		string _name = LetterName.Replace (" ", "%20");

		WWW getData = new WWW (urlPhpUsers + "opc=LetterSearch&inputSearch=" + _name + "&mUserID=" + DataApp.main.GetMyID () );
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {
			if (getData.text != "Uncreated") {

				userSearchList.dataList.Clear ();

				initUsers = 0;
				finalUsers = 20;

				currentLetter = letterSelected;

				userSearchList = JsonUtility.FromJson<SearchUserList> (getData.text);
				totalUsers = userSearchList.dataList.Count;
				Debug.Log ("count: " + userSearchList.dataList.Count);
				Debug.Log (getData.text);
				StartCoroutine(MakeSearchPanels (initUsers, finalUsers));

			} else {
				
				selectCurrentLetter (currentLetter);
				ProfileUser.MyUser.EnablePopUp("No se encontraron usuarios que contengan <b>"+LetterName+"</b>.");
//				InternetManager.main.loading.SetActive (false);
				DataApp2.main.DisableLoading();
			}
		} else {

			selectCurrentLetter (currentLetter);
			ProfileUser.MyUser.EnablePopUp ("Fallo en la conexión.\nIntentelo de nuevo.");
//			InternetManager.main.loading.SetActive (false);
			DataApp2.main.DisableLoading();
			Debug.Log ("No hay datos");
		}

		Debug.Log (getData.text);
	}


	public void plusDownLoadUsers ( int p ){

		if(finalUsers < totalUsers ){
//			InternetManager.main.loading.SetActive (true);
			DataApp2.main.EnableLoading();
			foreach (GameObject obj in UserNC) {
				obj.GetComponent<UsuarioNivelClub> ().StopC ();
			}
			initUsers = finalUsers;
			finalUsers += p;
			if( typeDownload == "1" ){
				content.anchoredPosition = new Vector3(0f, -3, 0f );
				//StartCoroutine( DownloadInfo( initUsers , finalUsers ));
				StartCoroutine( MakeSearchPanels( initUsers , finalUsers ));
			}else if (  typeDownload == "2" ){
				content.anchoredPosition = new Vector3(0f, -3, 0f );
				StartCoroutine( DownloadInfoSearchs( getInput_ ));
			}else{
				StartCoroutine( DownloadInfoSearchsAlpha( letterSelected ));
			}
		}
		isRefresh = false;
	}

	public void restDownLoadUsers ( int p ){



		content.anchoredPosition  = new Vector3(0f, -3, 0f );
		if(initUsers != 0 ){
//			InternetManager.main.loading.SetActive (true);
			DataApp2.main.EnableLoading();
			foreach (GameObject obj in UserNC) {
				obj.GetComponent<UsuarioNivelClub> ().StopC ();
			}
			finalUsers -= p;
			initUsers = finalUsers - p;
			if( typeDownload == "1" ){
				//StartCoroutine( DownloadInfo( initUsers , finalUsers ));
				StartCoroutine( MakeSearchPanels( initUsers , finalUsers ));
			}else if (  typeDownload == "2" ){
				StartCoroutine( DownloadInfoSearchs( getInput_ ));
			}else{
				StartCoroutine( DownloadInfoSearchsAlpha( letterSelected ));
			}
		}
		isRefresh = false;
	}

	public void Download_InfoUsers(){
		Nofound.text = "";
		ViewAllUser( );
		initUsers = 0;
		finalUsers = 20;
		StartCoroutine( DownloadInfo( initUsers , finalUsers ));
	}
		
	public void selectLetter( Text s){
		getInput_.text = "";
		letterSelected = s;

		currentInitUsers = initUsers;
		currentFinalUsers = finalUsers;

		initUsers = 0;
		finalUsers = 20;
	}

	public void selectCurrentLetter( Text s){
		getInput_.text = "";
		letterSelected = s;

		initUsers = currentInitUsers;
		finalUsers = currentFinalUsers;

		sizeLetter(true);

	}

	public void sizeLetter( bool state){
		
		int initSize = 40;
		float PosX = 2.482847f;
		for(int i =0 ; i < Letters.Length; i++){
			Letters[i].fontSize = initSize;
	//		Letters[i].rectTransform.position  = new Vector3(PosX,Letters[i].rectTransform.position.y,Letters[i].rectTransform.position.z);
		}
		letterSelected.fontSize = (state) ? 100 : initSize;
		PosX = (state) ? PosX+1f :  PosX;
	//	letterSelected.rectTransform.position = new Vector3(PosX+1f,letterSelected.rectTransform.position.y,letterSelected.rectTransform.position.z);
	}

	public IEnumerator DownloadInfo ( int initcant, int finalcant ){ // 1

//		InternetManager.main.loading.SetActive (true);
		DataApp2.main.EnableLoading();
		Nofound.text = "";
		downloadUserComplete =0;
		typeDownload = "1";
		ClearUsersView();
		int users = 0;


		userSearchList.dataList.Clear ();

		WWW getData = new WWW (urlPhpUsers + "opc=AllSearch" + "&mUserID=" + DataApp.main.GetMyID ());
		yield return getData;

		if (string.IsNullOrEmpty (getData.error)) {
			if (getData.text != "Uncreated") {
				userSearchList = JsonUtility.FromJson<SearchUserList> (getData.text);
				totalUsers = userSearchList.dataList.Count;
				Debug.Log ("count: " + userSearchList.dataList.Count);
				Debug.Log (getData.text);
				StartCoroutine(MakeSearchPanels (0, 20));
			} else {

				ProfileUser.MyUser.EnablePopUp("No se encontraron usuarios.");
//				InternetManager.main.loading.SetActive (false);
				DataApp2.main.DisableLoading();
				enableUptadePanel = false;

			}
		} else {
			
			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión.\nIntentelo de nuevo.");
//			InternetManager.main.loading.SetActive (false);
			DataApp2.main.DisableLoading();
			enableUptadePanel = false;

			Debug.Log ("No hay datos");
		}

		Debug.Log (getData.text);
	}

	public IEnumerator MakeSearchPanels ( int initcant, int finalcant ){ //IEnumerator MakeSearchPanels(){

//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		Debug.Log ("Init: " + initcant + ", final: " + finalcant);

		int index = initcant;

		if (UserNC.Count > 0) {

			foreach (GameObject obj in UserNC) {
									
				obj.gameObject.SetActive(true);
			
				if (index < finalcant && index < userSearchList.dataList.Count ) {
					obj.GetComponent<UsuarioNivelClub> ().Id = userSearchList.dataList [index].userID.ToString ();
					obj.GetComponent<UsuarioNivelClub> ().Datas [0].text = userSearchList.dataList [index].nameText;
					obj.GetComponent<UsuarioNivelClub> ().Datas [1].text = userSearchList.dataList [index].nickName;
					obj.GetComponent<UsuarioNivelClub> ().Datas [2].text = userSearchList.dataList [index].descrp;
					obj.GetComponent<UsuarioNivelClub> ().Datas [3].text = userSearchList.dataList [index].mail;
					obj.GetComponent<UsuarioNivelClub> ().Datas [4].text = userSearchList.dataList [index].fechaSus;
					obj.GetComponent<UsuarioNivelClub> ().Datas [5].text = userSearchList.dataList [index].fechaFin;

					obj.GetComponent<UsuarioNivelClub> ().loadRankings ();
					obj.GetComponent<UsuarioNivelClub> ().loadImg ();

					index++;

				} else {

					obj.gameObject.SetActive(false);
				}
			}

			yield return new WaitForSeconds (1f);
//			InternetManager.main.loading.SetActive (false);

			DataApp2.main.DisableLoading();
			enableUptadePanel = false;
			
		} else {

			foreach(ContentSearch obj in userSearchList.dataList){
			
					GameObject newUser = Instantiate( User_NivelClub);
					newUser.name = index.ToString( );
					newUser.transform.SetParent(Padre_User.transform, false);

					yield return new WaitForEndOfFrame ( );

					UserNC.Add( newUser );
					newUser.gameObject.SetActive(true);

					newUser.GetComponent<UsuarioNivelClub>().Id = obj.userID.ToString();
					newUser.GetComponent<UsuarioNivelClub> ().Datas [0].text = obj.nameText;
					newUser.GetComponent<UsuarioNivelClub> ().Datas [1].text = obj.nickName;
					newUser.GetComponent<UsuarioNivelClub> ().Datas [2].text = obj.descrp;
					newUser.GetComponent<UsuarioNivelClub> ().Datas [3].text = obj.mail;
					newUser.GetComponent<UsuarioNivelClub> ().Datas [4].text = obj.fechaSus;
					newUser.GetComponent<UsuarioNivelClub> ().Datas [5].text = obj.fechaFin;

					newUser.GetComponent<UsuarioNivelClub> ().loadRankings ();
					newUser.GetComponent<UsuarioNivelClub> ().loadImg ();

					index++;

					if (index > finalcant) {

						break;
					}
				
			}

//			InternetManager.main.loading.SetActive (false);
			DataApp2.main.DisableLoading();
			enableUptadePanel = false;
		
		}
		ScrollContent.GetComponent<RectTransform>().anchoredPosition = new Vector2( ScrollContent.GetComponent<RectTransform>().anchoredPosition.x ,0);
	}

//	public IEnumerator DownloadInfo ( int initcant, int finalcant ){ // 1
//		Nofound.text = "";
//		downloadUserComplete =0;
//		typeDownload = "1";
//		InternetManager.main.loading.SetActive(true);
//		ClearUsersView();
//		int users = 0;
//		string constr = urlDownloadActives + "opc=LoadActives";
//		WWW consult = new WWW(constr);
//		yield return consult;
//		string consult_temp = consult.text;
//		if( !string.IsNullOrEmpty(consult_temp) ){
//			string temp = null;
//			for(int i =0; i < consult_temp.Length ;i++){
//				if(consult_temp[i] != '[' && consult_temp[i] != ']'){
//					temp = temp + consult_temp[i];	
//				}else{
//					if(consult_temp[i] == ']'){ 	
//						if(temp != DataApp.main.GetMyID().ToString()){
//							if( users < finalcant && users >= initcant ){
////								UserNC = new GameObject[UserNC.Length+1];
//								GameObject newUser = Instantiate( User_NivelClub);
//								newUser.name = users.ToString( );
//								newUser.transform.SetParent(Padre_User.transform, false);
//								newUser.GetComponent<UsuarioNivelClub>().Id = temp;
//								UserNC.Add( newUser );
//								newUser.gameObject.SetActive(true);
//								yield return new WaitForEndOfFrame ( );
//								yield return new WaitForSeconds(.05f );
//							}
//							users++;
//						}
//					}
//					temp = null;
//				}
//			}
//			for(int i = 0; i<UserNC.Count;i++){
//				UserNC[i] = Padre_User.transform.GetChild(i).gameObject;
//			}
//		}else{
//			ProfileUser.MyUser.EnablePopUp("Fallo en la conexión a internet. \nIntentelo de nuevo.");
//			StartCoroutine(DownloadInfo( initUsers ,finalUsers ));
//		}
//		totalUsers = users;
//
//		//LoadingDummy.Loading.DisableLoading();
//	}

	public IEnumerator DownloadInfoSearchs ( InputField getInput ){
		Nofound.text = "";
		downloadUserComplete =0;
		typeDownload = "2";
//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		ClearUsersView( );
		initUsers = 0;
		finalUsers = 20;
		int users = 0;
		string constr = urlDownloadActives + "opc=Search&inputSearch=" + getInput.text ;
		WWW consult = new WWW(constr);
		yield return consult;
		string consult_temp = consult.text;
		if( !string.IsNullOrEmpty(consult_temp) ){
			string temp = null;
			for(int i =0; i < consult_temp.Length ;i++){
				if(consult_temp[i] != '[' && consult_temp[i] != ']'){
					temp = temp + consult_temp[i];	
				}else{
					if(consult_temp[i] == ']'){ 	
						if(temp != DataApp.main.GetMyID().ToString()){
							if( users < finalUsers && users >= initUsers ){
//								UserNC = new GameObject[UserNC.Length+1];
								GameObject newUser = Instantiate( User_NivelClub);
								newUser.name = users.ToString( );
								newUser.transform.SetParent(Padre_User.transform, false);
								newUser.GetComponent<UsuarioNivelClub>().Id = temp;
								UserNC.Add( newUser );
								newUser.gameObject.SetActive(true);
								yield return new WaitForEndOfFrame ( );
								yield return new WaitForSeconds(.1f );
							}
							users++;
						}
					}
					temp = null;
				}
			}
			for(int i = 0; i<UserNC.Count;i++){
				UserNC[i] = Padre_User.transform.GetChild(i).gameObject;
			}
		}else{
			ProfileUser.MyUser.EnablePopUp("No se encontraron usuarios que contengan <b>"+getInput.text+"</b>.");
		}
		totalUsers = users;
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
	}

	public void DownloadInfoSearchsAlpha_( Text input ){
		initUsers=0;
		finalUsers = 20;
		if( input.text.Length > 0 && input.text != "" ){
			//StartCoroutine( DownloadInfoSearchsAlpha( input ) ); 
			StartCoroutine( CharUserSearchs( input.text ) ); 
		}else{
			ProfileUser.MyUser.EnablePopUp("Fallo en la busqueda.");
			StartCoroutine(DownloadInfo( initUsers ,finalUsers ));
		}
	} 

	public void plusUserLetter( string pl ){
		typeDownload = pl;
		plusDownLoadUsers( 20 );
	}

	public IEnumerator DownloadInfoSearchsAlpha (  Text alphab ){
		Nofound.text = "";
		downloadUserComplete =0;
		typeDownload = "reload";
		letterSelected = alphab;

//		InternetManager.main.loading.SetActive(true);
		DataApp2.main.EnableLoading();
		if( initUsers == 0 ){
			content.anchoredPosition  = new Vector3(0f, -3, 0f );
			ClearUsersView( );
		}else{
			ClearUsersViewPlus( );
		}
		int users = 0;
		string constr = urlDownloadActives + "opc=SearchLetter&inputSearch=" + alphab.text;
		WWW consult = new WWW(constr);
		yield return consult;
		string consult_temp = consult.text;
		if( !string.IsNullOrEmpty(consult_temp) ){
			string temp = null;
			for(int i =0; i < consult_temp.Length ;i++){
				if(consult_temp[i] != '[' && consult_temp[i] != ']'){
					temp = temp + consult_temp[i];	
				}else{
					if(consult_temp[i] == ']'){ 	
						if(temp != DataApp.main.GetMyID().ToString()){
							if( users < finalUsers && users >= initUsers ){
//								UserNC = new GameObject[UserNC.Length+1];
								GameObject newUser = Instantiate( User_NivelClub);
								newUser.name = users.ToString( );
								newUser.transform.SetParent(Padre_User.transform, false);
								newUser.GetComponent<UsuarioNivelClub>().Id = temp;
								UserNC.Add( newUser );
								newUser.gameObject.SetActive(true);
								yield return new WaitForEndOfFrame ( );
								yield return new WaitForSeconds(.1f );
							}
							users++;
						}
					}
					temp = null;
				}
			}
			totalUsers = users;
			if( totalUsers > finalUsers){
				GameObject vermasbutton = Instantiate( ReloadUsers);
				UserNC.Add( vermasbutton );
				vermasbutton.transform.SetParent(Padre_User.transform, false);
				vermasbutton.GetComponent<Button>().onClick.AddListener ( () => plusUserLetter( "reload" ) );
			}
			for(int i = 0; i<UserNC.Count;i++){
				UserNC[i] = Padre_User.transform.GetChild(i).gameObject;
			}
		}else{
			ProfileUser.MyUser.EnablePopUp("No se encontraron usuarios que contengan <b>"+alphab.text+"</b>.");
			StartCoroutine(DownloadInfo( initUsers ,finalUsers ));
		}




	//	LoadingDummy.Loading.DisableLoading();
	}

	public void SwipeChangePos(){
		if (Input.touchCount > 0 && typeDownload == "3")  {
			Touch touch = Input.touches[0];
			switch (touch.phase) 	{
			case TouchPhase.Began:
				startPos = touch.position;
				break;
			case TouchPhase.Ended:
				float swipeDistVertical= (new Vector3(0,touch.position.y, 0) - new Vector3(0, startPos.y, 0)).magnitude;  /// Moviemiento lateral Swipe
				if (swipeDistVertical > minSwipeDistX) 	{
					float swipeValue = Mathf.Sign(touch.position.y - startPos.y);
					if (swipeValue > 0) {       //right swipe
							//ChangeViewUser(-1);
							plusDownLoadUsers(20);
					}else if (swipeValue < 0) {     //left swipe
							//ChangeViewUser(1);
							restDownLoadUsers(20);
					}
				}
				break;
			}
		}
	}


	void ClearUsersViewPlus(){
			Destroy(UserNC[UserNC.Count-1]);
			UserNC.RemoveAt( UserNC.Count -1 );
	}

	void ClearUsersView(){
		for(int i = 0; i<UserNC.Count;i++){
			Destroy(UserNC[i]);
		}
//		UserNC = new GameObject[0];
		UserNC.Clear();
	}

	public void ChangeViewUser ( int index){
		if( cardViewUSER.gameObject.activeSelf ){
			actualpos = CardViewUserNC.CardView.indexUser;
			actualpos += index;
			if( actualpos >= 0 && actualpos <= UserNC.Count-1){
				afterpos = actualpos;
				UserNC[actualpos].GetComponent<UsuarioNivelClub>().sendCardView();
			}else{
				actualpos = afterpos;
			}	
		}
	}

	public void SearchsUser(InputField inputSearch ){
		int actives = 0;
		string input = inputSearch.text.ToUpper();
		string nicknameCompare = null;
		string nameCompare = null;
		ViewAllUser( );
		foreach ( GameObject user in UserNC){
			if( user != UserNC[UserNC.Count-1]){
				nicknameCompare = user.GetComponent<UsuarioNivelClub>( ).Datas[1].text.ToUpper();
				nameCompare = user.GetComponent<UsuarioNivelClub>( ).Datas[0].text.ToUpper();
				Debug.Log(" email: "+  nameCompare);
			}
			if(nicknameCompare.Contains(input)){
				user.gameObject.SetActive(true);
				actives++;
			}else if (nameCompare.Contains(input)){
				user.gameObject.SetActive(true);
				actives++;
			}else{
				user.gameObject.SetActive(false);
			}
		}
		if(actives<=0){
			Nofound.text = "No se han encontrado resultados de su busqueda.";
		//	StartCoroutine( DownloadInfoSearchs( ) ); 
		}else{
			Nofound.text = "";
		}
//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
	}


	public void OpenCardView( ){
		cardViewUSER.gameObject.SetActive(true);
	}

	public void DownloadInfoSearchs_( InputField input ){
		if( input.text.Length > 0 && input.text != "" ){
			//StartCoroutine( DownloadInfoSearchs( input ) ); 
			StartCoroutine (StringUserSearchs (input.text));
		}else{
			ProfileUser.MyUser.EnablePopUp("La busqueda debe ser con minimo 1 caracter.");
			//StartCoroutine(DownloadInfo( initUsers ,finalUsers ));
		}
	}

	public void ViewAllUser( ){
		Nofound.text = "";
		foreach ( GameObject user in UserNC){
				user.gameObject.SetActive(true);
		}
		for(int i =0 ; i < Letters.Length; i++){
			Letters[i].fontSize = 40;
		}
	}

	public void SearchsUserAlphab( Text alphab ){
		int actives = 0;
		letterSelected = alphab;
		string input = alphab.text.ToUpper();
		string dataCompare = null;
		foreach ( GameObject user in UserNC){
			dataCompare = user.GetComponent<UsuarioNivelClub>().Datas[1].text.ToUpper();
			if(dataCompare.StartsWith(alphab.text)){
				user.gameObject.SetActive(true); 
				actives++;
			}else{
				user.gameObject.SetActive(false);
			}
		}
		if(actives<=0)
			Nofound.text = "No se han encontrado resultados de su busqueda.";
		else
			Nofound.text = "";

//		InternetManager.main.loading.SetActive(false);
		DataApp2.main.DisableLoading();
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class LoadingDummy : MonoBehaviour {

	public static LoadingDummy Loading;
	public bool InternetActive;
	public GameObject  PopUpInternetFailedLandscape;
	public UnityEvent failConection;
	// Use this for initialization
	void Awake () {
		Loading = this;
	}

	public IEnumerator CheckInternet  ( System.Action<bool> hasInternet ){
		bool result = false;
		string chcInternet = "http://2WAYSPORTS.COM/2waysports/Ecuador/Barcelona/ConexionInternet/isConection.php?conection=validarConexion";
		WWW getData = new WWW( chcInternet );
		yield return getData;
		if ( getData.text == "ConexionEstablecida") {
			Debug.Log( getData.text );
			result = true;
		}else{
			DisableLoading();
			failConection.Invoke( );
			PopUpInternetFailedLandscape.SetActive(true);
		}
		hasInternet( result );
	}
		

	public void EnableLoading( ){
		Loading.gameObject.SetActive(true);
		//gameObject.GetComponent<UpdateDataScript> ().RunUpdatePanel ();
	}

	public void DisableLoading( ){
		Loading.gameObject.SetActive(false);
		//gameObject.GetComponent<UpdateDataScript> ().StopUpdatePanel ();
	}


	void OnDisable ( ){
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExampleUseScript : MonoBehaviour {

	[SerializeField]
	private DataRowList rankingList;

	void Start () {

		StartCoroutine(ValidateStatusRanking());
	}

	private IEnumerator ValidateStatusRanking (){

		string url = "http://2waysports.com/2waysports/Ecuador/Barcelona/Pronostico/Php/UserPronosticoSColombia.php?indata=buscarRankingPronostico";
		
		yield return StartCoroutine (GetJsonDataScript.getJson.GetPhpData (url));

		if (GetJsonDataScript.getJson._state == "Successful") {

			rankingList = GetJsonDataScript.getJson.GetData (rankingList, "userID", "nameText", "points");

			VerDatosSolicitados();

		} else if (GetJsonDataScript.getJson._state == "Warning_01") {

			Debug.Log("Mensaje: No se descargaron datos.");
		
		} else if (GetJsonDataScript.getJson._state == "Warning_02") {

			Debug.Log("Mensaje: Fallo en la conexión. Intentelo de nuevo.");
		}
	}

	private void VerDatosSolicitados (){

		foreach (DataRow obj in rankingList.dataList) {

			Debug.Log ("El Id del usuario es: " + obj.GetValueToKey("userID"));
			Debug.Log ("El Nombre del usuario es: " + obj.GetValueToKey("nameText"));
			Debug.Log ("El Puntaje del usuario es: " + obj.GetValueToKey("points"));

			Debug.Log("\n");
		}
	}

}
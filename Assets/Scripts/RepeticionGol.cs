﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepeticionGol : MonoBehaviour {

	public static RepeticionGol main;
	public Button ActivarRepeticion;
	public Camera MainCamera,RepeticionCamera;
	public GameObject RawImages;
	public Transform PadreDeRaws;
	public List<RawImage> Fotos;


	void Start(){
		main = GetComponent<RepeticionGol>();
	}

	public void VerRepeticion(){
		RepeticionCamera.enabled = true;
		MainCamera.enabled = false;
		StopAllCoroutines();
		StartCoroutine(VerRepeticionCorutina());
	}

	public void ResetRepeticion(bool eraser){
		if(eraser){
			Fotos.Clear();
			int Hijos = PadreDeRaws.childCount;
			if(Hijos>0){
				for(int i =0; i<Hijos;i++){
					Destroy(PadreDeRaws.GetChild(i).gameObject);
				}
			}else{
				ActivarRepeticion.interactable = false;
			}
		}
		RepeticionCamera.enabled = false;
		MainCamera.enabled = true;

			
	}

	public void TakeScreen(){
		ActivarRepeticion.interactable = true;

		StartCoroutine(TomarRepeticion());
	}

	private IEnumerator TomarRepeticion() {

		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		GameObject newft =  Instantiate(RawImages,RawImages.transform.position,Quaternion.identity) as GameObject;
		newft.transform.SetParent(PadreDeRaws.transform);
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		tex.Apply();
		Fotos.Add(newft.GetComponent<RawImage>());
		newft.GetComponent<RawImage>().texture = tex;


		if(Shoot.share._isShooting)
			StartCoroutine(TomarRepeticion());
	}


	public IEnumerator VerRepeticionCorutina(){
		for(int i = 0; i < Fotos.Count;i++){
			PadreDeRaws.GetComponent<RawImage>().texture = Fotos[i].texture;
			yield return new WaitForSeconds(.05f);	
		}
	}
}

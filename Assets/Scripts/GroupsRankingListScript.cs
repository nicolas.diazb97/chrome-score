﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class GroupsRanking {

	public int adminID;
	public int groupID;
	public string groupName;
	public string chatServerID;
}

[System.Serializable]
public class GroupsRankingListScript{

	public List<GroupsRanking> dataList;
}

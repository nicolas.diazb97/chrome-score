﻿using UnityEngine;
using System.Collections;

using UnityEngine.SceneManagement;

public class RotarSprite : MonoBehaviour {
	public float tiempoInvocacion;
//	public float velocidadRotacion;
	void OnEnable () {
		InvokeRepeating("Rotar",0,tiempoInvocacion);
		Invoke("Load",32);
	}

	void Rotar () {
		transform.Rotate(Vector3.forward, -40);//-40 porque son 9 pasos
	}
	
	void OnDisable () {
		CancelInvoke("Rotar");
		CancelInvoke("Load");
	}


	void Load(){
		SceneManager.LoadSceneAsync( "TiroTriColor" );
	}
}

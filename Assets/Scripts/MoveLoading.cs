﻿using UnityEngine;
using System.Collections;

public class MoveLoading : MonoBehaviour {


	RectTransform Mytrans;
	public Vector3 InitTrans;
	public float maxX;
	// Use this for initialization
	void Awake () {
		Mytrans = GetComponent<RectTransform>( );
		Debug.Log("MY INIT POS : "+ InitTrans.x);
	}


	void Move(){
		if(Mytrans.position.x > maxX){
			Mytrans.position = new Vector3(InitTrans.x,Mytrans.position.y,Mytrans.position.z);
			Debug.Log("init pos x\t: "+ Mytrans.position.x);
		}else{
			Debug.Log("x: "+ Mytrans.position.x);
			Mytrans.Translate(.05f,0,0);
		}

		

	}

	void OnEnable( ){
		InvokeRepeating("Move",.03f,.03f);
	}

	void OnDisable(){
		CancelInvoke("Move");
	}

}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PersonalizacionPersonaje : Singleton<PersonalizacionPersonaje> {


	//variables staticas
	public static PersonalizacionPersonaje ThisCustom;
	public static int jugadorSel=0;
	public static bool actAnim;



	[Header ("Datos del jugador")]
	public GameObject[] jugadores;
	public string[] nameJugadores;
	public string[] lastNameJugadores;
	public Sprite[] estadisticasJugadores;
	public Image estadisticasJugador;
	public Material [] materialJudores;


	[Header ("Objetos")]
	public GameObject estadio;
	public Text nameText;
	public GameObject register, panelRetos, panelTiros, panelCompra;
	public Button jugar , cReloj , retos, btnIzq, btnDer;
	public GameObject candado;
	public AudioSource MusicHome;
	public AudioClip Hm1,Hm2;
	public GameObject Loading, select;
	public GameObject baseJugadores;


	//variables privadas
	private Quaternion baseJugadoresRotacion;
	private Quaternion[] rotacionInicialJugadores;
	private Vector3 [] escalaInicialJugadores;
	private Vector3 posicionInicialJugador;
	private Quaternion rotacionInicialEstadio;
	[SerializeField]
	private bool rotar;
	private float direccion;
	[SerializeField]
	private bool izq, der;
	[SerializeField]
	private int contador=0;
	[SerializeField]
	private string opcAux;
	[SerializeField]
	private bool primerGiro, girando, giroStart, aumentar;
	private Vector3 mousePosicionInicial;
	private Vector3 mousePosicionFinal;
	private Vector3[] posicionJugadores;

    public bool tryJugador;


	// Use this for initialization
	void Start () {
		ThisCustom = this;
		SkinnedMeshRenderer[] meshJudores = new SkinnedMeshRenderer[(jugadores.Length*2)];
		rotacionInicialJugadores = new Quaternion[jugadores.Length];
		escalaInicialJugadores = new Vector3[jugadores.Length];
		posicionJugadores = new Vector3[jugadores.Length];
        //
        if(estadio)
		    rotacionInicialEstadio = estadio.transform.rotation;

		JugadorUnlock ();

		for (int i = 0; i < jugadores.Length; i++) {
			rotacionInicialJugadores [i] = jugadores [i].transform.localRotation;
			escalaInicialJugadores[i] = jugadores [i].transform.localScale;
			posicionJugadores[i] = jugadores [i].transform.localPosition;
		}

		CambiarAnimacion ();
		AsignarNombreJugadorSel ();

        //TODO Restablecer para comprobar el "pago",  actualmente se usa a Falcao
        //if (PlayerPrefs.GetInt("pago") == 1)
        if (true)
        {
            giroStart = true;
			int lenght = jugadorSel;
			for (int i = 0; i < lenght; i++) {
				print ("rotacion: " + PlayerPrefs.GetString ("rotacion"));
				CambiarJugador ("izq");
			}
			giroStart = false;

		} else {
			jugadorSel =0;
			PlayerPrefs.SetInt("JugadorSel",0);
		}
			

		//if (PlayerPrefs.GetInt ("pago") == 0) {
		//	JugadorLock ();
		//}

	}

//	IEnumerator  RotarBase(bool derecha){
//		float step = 100f * Time.deltaTime;
//		//banderaAux2 = derecha;
//		rotar = false;
//		if (derecha) {
//			Debug.Log ("girar derecha");
//			der = false;
//
//			Quaternion rotacion = Quaternion.Euler (0, -72 * contador, 0);
//			while (baseJugadores.transform.rotation != rotacion) {
//				candado.SetActive(false);
//				baseJugadores.transform.rotation = Quaternion.RotateTowards (baseJugadores.transform.rotation, rotacion, step);
//				yield return null;
//			}
//			//der = true;
//		} 
//		if(!derecha){
//			Debug.Log ("girar izquierda");
//			izq = false;
//			Quaternion rotacion = Quaternion.Euler (0, 72 * contador, 0);
//			while (baseJugadores.transform.rotation != rotacion) {
//				candado.SetActive(false);
//				baseJugadores.transform.rotation = Quaternion.RotateTowards (baseJugadores.transform.rotation, rotacion, step);
//				yield return null;
//			}
//			//izq = true;
//		}
//		yield return new WaitForSeconds (0.15f);
//		if(PlayerPrefs.GetInt("pago")==0)
//			activarCandado = true;
//		AsignarNombreJugadorSel ();
//		ActivarDesactivarCandadoYBotones ();
//		btnDer.interactable = true;
//		btnIzq.interactable = true;
//		//banderaAux1 = derecha;
//	}
	public void RevisarLogin( )
    {
		DataApp2.main.EnableLoading ();
		StartCoroutine( DataApp2.main.CheckInternet( internet => 
        { 
			if( internet )
            {
                Loading.SetActive(false);
                jugadorSel = 0;
                PlayerPrefs.SetInt("JugadorSel", 0);
                panelTiros.SetActive(true);
                DataApp2.main.DisableLoading();
			}
            else
            {
				Debug.Log("LA PUTA CONEXION FALLO");
                DataApp2.main.popUpInformative(true, "Fallo de conexión", "Por favor verifique su conexión o intente más tarde");
                tryJugador = true;
			}
		}));

	}


	public void RevisarRetos( ){
		DataApp2.main.EnableLoading ();
		StartCoroutine( DataApp2.main.CheckInternet( internet => 
            { 
			if( internet )
            {
//				if( !DataApp2.main.IsRegistered( ))
                if(true)
                {
                     panelRetos.SetActive(true);
				}
                else if(DataApp2.main.IsRegistered( ))
                {
                    Debug.Log("abrir registro");
                    register.SetActive (true);
                    RegisterLoginManager.panel =3;
				}

				DataApp2.main.DisableLoading();
			}
            else
            {
				Debug.Log("LA PUTA CONEXION FALLO");
                DataApp2.main.popUpInformative(true, "Fallo de conexión", "Por favor verifique su conexión o intente más tarde");
                tryJugador = true;
			}

		}));

	}

	public void RevisarCandado( )
    {
		DataApp2.main.EnableLoading ();
		StartCoroutine( DataApp2.main.CheckInternet( internet => 
        { 
			if( internet )
            {
                panelCompra.SetActive(true);
                DataApp2.main.DisableLoading ();
//    			//Debug.Log("maldita cosa");
//    			if(!DataApp2.main.IsRegistered( ))
//                {
//    			//	Debug.Log("abrir registro");
//    				register.SetActive (true);
//    				RegisterLoginManager.panel =5;
//    			}
//                else if(DataApp2.main.IsRegistered( ) && PlayerPrefs.GetInt("pago")==0)
//                {
//    				Debug.Log("compra");
//    				panelCompra.SetActive(true);
//    				//candado.GetComponent<Button>().onClick.AddListener( () => CompleteProject.Purchaser.myPurchase.BuySubscription() );
//    			}
//				DataApp2.main.DisableLoading();
			}
            else
            {
			    Debug.Log("LA PUTA CONEXION FALLO");
                tryJugador = true;
                DataApp2.main.popUpInformative(true, "Fallo de conexión", "Por favor verifique su conexión o intente más tarde");

			}

		}));

	}

	private void swipeBaseJugadores(){
		if(Input.GetMouseButtonDown(0)){
			rotar = true;
			mousePosicionInicial = Input.mousePosition;
			//print ("inicial "+mousePosicionInicial);
		}
		if (Input.GetMouseButtonUp (0) && !register.activeSelf) {
			mousePosicionFinal = Input.mousePosition;
			//print ("final "+mousePosicionFinal);
			if(mousePosicionInicial.x!=mousePosicionFinal.x && mousePosicionFinal.x!=0){
				if (mousePosicionInicial.x > mousePosicionFinal.x && rotar && CalcularDistanciaMinimaSwipe(mousePosicionInicial, mousePosicionFinal)>250) {
					CambiarJugador ("izq");
				}

				if(mousePosicionFinal.x > mousePosicionInicial.x && rotar && CalcularDistanciaMinimaSwipe(mousePosicionInicial, mousePosicionFinal)<-250){
					CambiarJugador ("der");
				}
			}
		}


	}
		
	private float CalcularDistanciaMinimaSwipe(Vector3 inicial, Vector3 final ){
		//Debug.Log ("ini: "+inicial.x+ " fin: "+ final.x);
		float calculo =inicial.x - final.x;
		//Debug.Log ("calculo "+calculo);
		return calculo;
	}

	public void Rotacion(float Dir){
		direccion = Dir;
	}

	public void ReiniciarRotacionYEscalaJugadores(){
		for (int i = 0; i < jugadores.Length; i++) {
			jugadores [i].transform.localRotation = rotacionInicialJugadores [i];
			jugadores [i].transform.localScale = escalaInicialJugadores [i];
		}
	}

	public void ReiniciarRotacionEstadio(){
		estadio.transform.rotation = rotacionInicialEstadio;
	} 

	public void CambiarJugador( string opc)
    {
//		ReiniciarRotacionYEscalaJugadores ();

		btnDer.interactable = false;
		btnIzq.interactable = false;

//		if(PlayerPrefs.GetInt("pago")==1){
//			if (opc == "der") {
//				primerGiro = true;
//			}
//		}
//

//		if (contador > 0) {
//			contador--;
//		} else {
//			contador = 5;
//		}
//
//		if(opc != opcAux && primerGiro){
//			if (contador == 2) {
//				contador = 5;
//			}else if(contador == 3){
//				contador = 4;
//			}else if(contador == 4){
//				contador = 3;
//			}else if(contador == 5){
//				contador = 2;
//			}
//		}
		if (!giroStart) {
			PlayerPrefs.SetString ("rotacion", opc);

		} else {
			
		}

		switch(opc)
        {
		case "izq":
			//print ("girar izq");
			//si se gira para el lado contrario hay que darles la posicion anterior
			if (opc != opcAux && primerGiro) {
				Vector3 antPosicion = posicionJugadores [0];
				for (int i = 0; i < jugadores.Length - 1; i++) {
					posicionJugadores [i] = posicionJugadores [i + 1];
				}
				posicionJugadores [jugadores.Length - 1] = antPosicion;
				primerGiro = false;
			}
			izq = true;
			der = false;
			if (!giroStart) {
				jugadorSel += 1;
				if (jugadorSel > jugadores.Length - 1) {
					jugadorSel = 0;
				}
			} 
            Debug.Log ("posicion " + jugadorSel + " nombre jugador " + jugadores[jugadorSel-1].name);

			if (primerGiro) {
				Vector3 sigPosicion = posicionJugadores [jugadores.Length - 1];
				for (int i = jugadores.Length - 1; i > 0; i--) {
					posicionJugadores [i] = posicionJugadores [i - 1];
				}
				posicionJugadores [0] = sigPosicion;
			}
			break;

		case "der":
			//print ("girar der");
			//si se gira para el lado contrario hay que darles la posicion anterior
			if (opc != opcAux && primerGiro) {
			//	print ("girar izq");
				Vector3 sigPosicion = posicionJugadores [jugadores.Length - 1];
				for (int i = jugadores.Length - 1; i > 0; i--) {
					posicionJugadores [i] = posicionJugadores [i - 1];
				}
				posicionJugadores [0] = sigPosicion;
				primerGiro = false;
			}
			der = true;
			izq = false;
			if (!giroStart) {
				jugadorSel -= 1;
				if (jugadorSel < 0) {
					jugadorSel = jugadores.Length - 1;
				}
			}
//			if (opc != opcAux && primerGiro) {
//				primerGiro = false;
//			}
			if (primerGiro) {
				Vector3 antPosicion = posicionJugadores [0];
				for (int i = 0; i < jugadores.Length - 1; i++) {
					posicionJugadores [i] = posicionJugadores [i + 1];
				}
				posicionJugadores [jugadores.Length - 1] = antPosicion;
			}
			break;
		}
		GuardarCambios ();
		opcAux = opc;
		primerGiro = true;


	}

	public void AsignarNombreJugadorSel()
    {
        //estadisticasJugador.sprite = estadisticasJugadores[jugadorSel - 1];
        if (jugadorSel == 0 && DataApp2.main.IsPhoneLog())
        {
            if (jugadorSel == 0)
            {
                if (!DataApp2.main.IsRegistered())
                {
                    nameText.text = "<color=#FFFFFFFF>" + User.main.GetMyPhoneNumber() + "</color>" + " " + "<color=#000000FF>" + "" + "</color>";
                }
                else
                {
                    nameText.text = "<color=#FFFFFFFF>" + User.main.GetMyName() + "</color>" + " " + "<color=#000000FF>" + User.main.GetMyLastName() + "</color>";
                }
            }
        }
        else
        {
            //nameText.text = "<color=#FFFFFFFF>" + nameJugadores[jugadorSel] + "</color>" + " ";
            //nameText.text += "<color=#000000FF>" + lastNameJugadores[jugadorSel] + "</color>";
        }
	}

	private void CambiarAnimacion()
    {
		for (int i = 0; i < jugadores.Length; i++)
        {
            Animator a = jugadores[i].GetComponent<Animator>();
            a.SetBool("JugadorSelec", false);
            a.SetBool("JugadorSelec", true);
        }
	}

	public void JugadorLock()
    {
		for (int i = 0; i < materialJudores.Length; i++)
        {
			materialJudores [i].SetColor ("_Color", Color.black);
		}
		int n = (jugadorSel - 1)*2;
		materialJudores [n].SetColor ("_Color", Color.white);
		materialJudores [n+1].SetColor ("_Color", Color.white);
	}

	public void JugadorUnlock(){
		for (int i = 0; i < materialJudores.Length; i++)
        {
			materialJudores [i].SetColor ("_Color", Color.white);
		}
	}

	private void ActivarDesactivarCandadoYBotones(){
        //TODO Descomentar para la version publica, y poder bloquear los personajes

        //if (jugadorSel != 0 && PlayerPrefs.GetInt("pago") == 0)
        //{
        //    //Debug.Log ("los pollos hermanos");
        //    cReloj.interactable = false;
        //    jugar.interactable = false;
        //    retos.interactable = false;
        //    candado.SetActive(true);
        //}
        //else
        {
			jugar.interactable=true;
			cReloj.interactable=true;
			retos.interactable = true;
			candado.SetActive(false);
		}
	}



	public void GuardarCambios()
    {
		//TransactionalProcess.isActive == 1 && 
		PlayerPrefs.SetInt("JugadorSel",jugadorSel);
		jugadorSel = PlayerPrefs.GetInt("JugadorSel");
		jugar.interactable=true;
		cReloj.interactable = true;
		retos.interactable = true;
		candado.SetActive(false);
	}
}

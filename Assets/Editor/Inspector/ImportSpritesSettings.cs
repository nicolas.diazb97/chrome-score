﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.IO;

public class ImportSpritesSettings : AssetPostprocessor 
{
	const string assetPathStartWith = "Assets/";

	void OnPreprocessTexture()
	{
		if (!assetImporter.assetPath.StartsWith (assetPathStartWith))
			return;

        TextureImporter TexImp = (TextureImporter)assetImporter;

        Debug.Log("Se importo la textura en la carpeta " + assetPath);

        /**Let's make all texture readeble for further configuration,
         and also not use mipmap for this project*/
        TexImp.isReadable = true;
        TexImp.mipmapEnabled = false;
       
        /**If the texture size is higher than 1024, we set it ti 1024 as default, for perfomce purpose*/
        //if (TexImp.maxTextureSize > 1024)
        //{
        //    TexImp.maxTextureSize = 1024;
        //}

        /**It ensure which type of texture is in use, to set the corresponding configuration*/
//        switch (TexImp.textureType)
//        {
//            case TextureImporterType.Default:
//            case TextureImporterType.NormalMap:
//                TexImp.npotScale = TextureImporterNPOTScale.ToNearest;
//                Debug.Log("Texture is either default or normal");
//                #if UNITY_ANDROID
//                TexImp.SetPlatformTextureSettings(TextureSettingPlatformConfig(TexImp, "Android", TexImp.maxTextureSize));
//                #elif UNITY_IOS
//                TexImp.SetPlatformTextureSettings(TextureSettingPlatformConfig(TexImp, "iPhone", TexImp.maxTextureSize));
//                #endif
//                break;
//            case TextureImporterType.Sprite:
//                Debug.Log("Is a Sprite");
//                TexImp.npotScale = TextureImporterNPOTScale.None;
//                #if UNITY_ANDROID
//                TexImp.SetPlatformTextureSettings( SpriteSettingPlatformConfig(TexImp, "Android", TexImp.maxTextureSize));
//                #elif UNITY_IOS
//                TexImp.SetPlatformTextureSettings(SpriteSettingPlatformConfig(TexImp, "iPhone", TexImp.maxTextureSize));
//                #endif
//                break;
//            default:
//                break;
//        }

	}

    /// <summary>
    /// Set the platfomr import settings for Textures.
    /// </summary>
    /// <returns>The setting platform config.</returns>
    /// <param name="_target">Texture Importe Target.</param>
    /// <param name="_platformName">Platform name.</param>
    /// <param name="_maxSize">Max texture size.</param>
    TextureImporterPlatformSettings TextureSettingPlatformConfig(TextureImporter _target, string _platformName, int _maxSize)
    {
        TextureImporterPlatformSettings TexImpPlatformSettings = new TextureImporterPlatformSettings();

        TexImpPlatformSettings.name = _platformName;
        TexImpPlatformSettings.overridden = true;
        TexImpPlatformSettings.crunchedCompression = false;
        TexImpPlatformSettings.maxTextureSize = _maxSize;

        TextureImporterPlatformSettings CurrentPlatSettings = _target.GetPlatformTextureSettings(_platformName);
        Debug.Log("Current texture format = " + CurrentPlatSettings.format.ToString());

        if (CurrentPlatSettings.format == TextureImporterFormat.Automatic ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT1 ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT5 ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT1Crunched ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT5Crunched ||
            CurrentPlatSettings.format == TextureImporterFormat.ETC_RGB4 ||
            CurrentPlatSettings.format == TextureImporterFormat.ETC2_RGB4 ||
            CurrentPlatSettings.format == TextureImporterFormat.ETC2_RGBA8 || 
            CurrentPlatSettings.format == TextureImporterFormat.PVRTC_RGBA2 ||
            CurrentPlatSettings.format == TextureImporterFormat.PVRTC_RGBA4)
        {
            Debug.Log("Set this texture format = PVRTC_RGB2");
            TexImpPlatformSettings.format = TextureImporterFormat.PVRTC_RGB2;
        }     

        return TexImpPlatformSettings;
    }

    /// <summary>
    /// Set the platfomr import settings for Sprites.
    /// </summary>
    /// <returns>The setting platform config.</returns>
    /// <param name="_target">Texture Inporter Target.</param>
    /// <param name="_platformName">Platform name.</param>
    /// <param name="_maxSize">Max sprite size.</param>
    TextureImporterPlatformSettings SpriteSettingPlatformConfig(TextureImporter _target, string _platformName, int _maxSize)
    {
        TextureImporterPlatformSettings TexImpPlatformSettings = new TextureImporterPlatformSettings();

        TexImpPlatformSettings.name = _platformName;
        TexImpPlatformSettings.overridden = true;
        TexImpPlatformSettings.crunchedCompression = false;
        TexImpPlatformSettings.maxTextureSize = _maxSize;

        TextureImporterPlatformSettings CurrentPlatSettings = _target.GetPlatformTextureSettings(_platformName);

        if (CurrentPlatSettings.format == TextureImporterFormat.Automatic ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT1 ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT5 ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT1Crunched ||
            CurrentPlatSettings.format == TextureImporterFormat.DXT5Crunched ||
            CurrentPlatSettings.format == TextureImporterFormat.ETC_RGB4 ||
            CurrentPlatSettings.format == TextureImporterFormat.ETC2_RGB4 || 
            CurrentPlatSettings.format == TextureImporterFormat.ETC2_RGBA8 || 
            CurrentPlatSettings.format == TextureImporterFormat.PVRTC_RGBA2 ||
            CurrentPlatSettings.format == TextureImporterFormat.PVRTC_RGB2 ||
            CurrentPlatSettings.format == TextureImporterFormat.PVRTC_RGB4 ||
            CurrentPlatSettings.format == TextureImporterFormat.PVRTC_RGBA4)
        {
            TexImpPlatformSettings.format = TextureImporterFormat.ASTC_8x8;
        }

        return TexImpPlatformSettings;
    }
}

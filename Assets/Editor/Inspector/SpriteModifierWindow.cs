﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SpriteModifierWindow : EditorWindow 
{
	public string srcPath;
	public Sprite [] m_Sprites;

	[MenuItem("MyAssetManager/Sprite Modifier")]
	public static void ShowWindow()
	{
		GetWindow<SpriteModifierWindow> ("Sprite Modifier");
	}

	void OnGUI()
	{
		srcPath = EditorGUILayout.TextField ("Source Path", srcPath);

		if (GUILayout.Button ("Select Sprites on the Source Path")) 
		{
			Debug.Log ("Whatever you wrote on the text field was..." + srcPath);
			//And then should get a list of all the sprites on the folder
			Object[] m_Objs = AssetDatabase.LoadAllAssetsAtPath(srcPath);
			Object m_Obj = AssetDatabase.LoadMainAssetAtPath(srcPath);

			if (m_Objs != null && m_Objs.Length > 0)
			{
				foreach (Object obj in m_Objs)
				{
					Debug.Log ("Object's exist!!!!" + obj.name);
				}
			}

			if (m_Obj != null)
			{
				if (m_Obj is SpriteRenderer)
				{
					Debug.Log ("This object is a sprite");
				}	
			}
				
			Debug.Log (AssetDatabase.GetMainAssetTypeAtPath (srcPath));

		}	

		if (m_Sprites != null && m_Sprites.Length > 0)
		{
			ScriptableObject target = this;
			SerializedObject so = new SerializedObject (target);
			SerializedProperty spritesProperty = so.FindProperty ("m_Sprites");

			EditorGUILayout.PropertyField (spritesProperty, true);
			so.ApplyModifiedProperties ();
		}
	}
}

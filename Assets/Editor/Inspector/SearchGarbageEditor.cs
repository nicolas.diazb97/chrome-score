﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SearchGarbageAssets))]
public class SearchGarbageEditor : Editor
{
	
    public override void OnInspectorGUI()
    {
        SearchGarbageAssets CurGC = (SearchGarbageAssets)target;
        DrawDefaultInspector();

        if (CurGC.AllTextures.Count > 0)
        {
            if (GUILayout.Button("Check Textures"))
            {
                foreach (Texture tex in CurGC.AllTextures)
                {
                   
                }
            }
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;

public class MoveAssetsWindow : EditorWindow 
{
	TextAsset m_Text;

	public string[] showPaths;
	public string[] allPaths;
	public string[] uniquePaths;
	public string NewPath = "Assets/_Eduardo/ProvisionalTextures/";

	public float width, height;

	Vector2 scrollPos;

	[MenuItem("MyAssetManager/Move Assets")]
	public static void ShowWindow()
	{
		GetWindow<MoveAssetsWindow> ("Move Assets");
	}
	
	// Update is called once per frame
	void OnGUI () 
	{
		width = EditorWindow.GetWindow<MoveAssetsWindow> ("Move Assets").position.width;
		height = EditorWindow.GetWindow<MoveAssetsWindow> ("Move Assets").position.height;

		EditorGUILayout.BeginVertical ();
		scrollPos = EditorGUILayout.BeginScrollView (scrollPos, false, true, GUILayout.Width (width), GUILayout.Height (height), GUILayout.ExpandHeight(true));
		/**********************************************************************************************************/
		m_Text = EditorGUILayout.ObjectField ("Selected Object", m_Text, typeof(TextAsset), true) as TextAsset;

		if (m_Text) 
		{
			//Show button to split the txt file
			GUILayout.Space(20);
			if (GUILayout.Button ("Get Assets Paths", GUILayout.Width (width/2))) 
			{
				allPaths = Regex.Split (m_Text.text, ";");
				uniquePaths = allPaths.Distinct().ToArray();

				for (int i = 0; i < uniquePaths.Length; i++)
				{
					string fullPath = allPaths [i];
					uniquePaths [i] = "Assets/" + fullPath;
				}
			}

			if (allPaths != null)
			{
				showPaths = uniquePaths;

				ScriptableObject target = this;
				SerializedObject so = new SerializedObject (target);
				SerializedProperty stringsProperty = so.FindProperty ("showPaths");

				EditorGUILayout.PropertyField (stringsProperty, true);
				so.ApplyModifiedProperties ();
//
				//Show other button to perform the logic of moving all the assets to a provisional folder
				GUILayout.Space(20);
				if (GUILayout.Button ("Move Assets To Provisional Folder")) 
				{
					for (int i = 0; i < uniquePaths.Length; i++)
					{
						string srcPath = uniquePaths [i];
						string destPath = "Assets/_Eduardo/ProvisionalTextures/" + Path.GetFileName(uniquePaths[i]);

						if (srcPath != destPath)
						{
							AssetDatabase.MoveAsset (uniquePaths[i], destPath);
						}

					}
				}
			}
		}
	
		GUILayout.FlexibleSpace ();
		/*************************************************************************/
		EditorGUILayout.EndScrollView ();
		EditorGUILayout.EndVertical ();
	}
}

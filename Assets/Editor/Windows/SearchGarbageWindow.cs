﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine.UI;
using System;

public class SearchGarbageWindow : EditorWindow 
{
    UnityEngine.Object go_Root;
    ReorderableList m_ReorderableList = null;

    //Window UI Atributtes
    const int TOP_PADDING = 2;
    const string HELP_TEXT = "Can not find 'Animals' component on any GameObject in the scene";

    static Vector2 s_WindowMinSize = Vector2.one * 300.0f;
    static Rect s_HelpRect = new Rect(0.0f, 0.0f, 300.0f, 100.0f);
    static Rect s_ListRect = new Rect(Vector2.zero, s_WindowMinSize);

    private ReorderableList list;
    public List<Texture> TexturesList;

    public TextureImporter texImporter;

    [MenuItem("Garbage Search/Garbage Search Window")]
    public static void ShowWindow()
    {
        //GetWindow<SearchGarbageWindow>("Search for Garbage");
        SearchGarbageWindow window = GetWindow<SearchGarbageWindow>(true, "Search for Garbage");
        window.minSize = s_WindowMinSize;
    }


    void OnEnable()
    {
//        list = new ReorderableList(TexturesList, typeof(Texture), true, true, true, true);
    }

//    void OnInspectorUpdate()
//    {
//        Repaint();    
//    }

    SerializedObject serializedObj = null;
    SerializedProperty serializedPropertyList = null;

    void OnGUI()
    {
//        go_Root = EditorGUILayout.ObjectField("Root object for 'Garbage Search'", go_Root, typeof(TestList), true) as TestList;
//
//        if (GUILayout.Button("Validate Root Object"))
//        {
//            if (go_Root != null)
//            {
//                go_Root.GetChildrens();
//
//                Debug.Log("Root Object is valid!");
//
//                if (go_Root.AllChilds != null && go_Root.AllChilds.Count > 0)
//                {
//                    serializedObj = new SerializedObject(go_Root);
//                    serializedPropertyList = serializedObj.FindProperty("AllChilds");
//                    m_ReorderableList = new ReorderableList(serializedObj, serializedPropertyList, true, true, true, true);
//
//                    m_ReorderableList.drawHeaderCallback = (rect) => EditorGUI.LabelField(rect, "Childs Transforms");
//                    m_ReorderableList.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
//                        {
//                            rect.y += TOP_PADDING;
//                            rect.height = EditorGUIUtility.singleLineHeight;
//                            EditorGUI.PropertyField(rect, m_ReorderableList.serializedProperty.GetArrayElementAtIndex(index));
//                        };
//
//                }
//
//            }
//            else
//            {
//                Debug.LogWarning("Root object not set!");
//            }
//        }
        go_Root = EditorGUILayout.ObjectField("Texture Importer test", go_Root, typeof(UnityEngine.Object), true) as UnityEngine.Object;

//        if (serializedObj != null)
//        {
//            serializedObj.Update();
//            m_ReorderableList.DoList(s_ListRect);
//            serializedObj.ApplyModifiedProperties();
//        }
       
//        list.DoLayoutList();
//        serializedObj.ApplyModifiedProperties();
    }
}

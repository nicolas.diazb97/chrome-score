﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserChroma
{
    public string username;
    public string email;
    public string userID;
    public string[] cromos;

    public UserChroma()
    {
    }

    public UserChroma(string username, string email, string _userID)
    {
        this.username = username;
        this.email = email;
        this.userID = _userID;
    }
}
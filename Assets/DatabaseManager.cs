using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using Firebase.Extensions;
using UnityEngine.SceneManagement;
using Firebase.Database;
using TMPro;


public class DatabaseManager : Singleton<DatabaseManager>
{
    private FirebaseAuth auth;
    private FirebaseUser user;
    public string userID;
    public string displayName;
    public string emailAddress;
    public string photoUrl;
    public string pngData;
    public OnUserAuth OnSignUp;
    public UnityEvent OnPasswordReset;
    public bool newuser = false;
    DatabaseReference reference;
    public TextMeshProUGUI testText;
    public TextMeshProUGUI testText2;
    int countt = 0;
    public UnityEvent OnMatchFound;
    public StringEvent OnEnemyGoal;
    public UnityEvent OnMatchEnded;
    string currentHostID;
    public List<string> cardsIDS;
    // Start is called before the first frame update
    void Start()
    {
        InitializeFirebase();
        OnSignUp.AddListener((Firebase.Auth.FirebaseUser newUser) =>
        {
            displayName = newUser.DisplayName;
            emailAddress = newUser.Email;
            userID = newUser.UserId;
        });
        reference = FirebaseDatabase.DefaultInstance.RootReference;
        //WriteNewUser("prueba", "nickname prueba", "email prueba", "nodata", DataManager.main.buyedTattoos.ToArray());
        //TryToAuthWithEmail("nicolas.diazb97@gmail.com", "Nico12las");
        //SendVerificationEmail();
        //TryToCreateUserWithEmail("nicolas.diazb97@gmail.com", "Nico12las");
    }
    public void RegisterForNewPlayers(string _hostID, TextMeshProUGUI _text, TextMeshProUGUI _text2)
    {

        FirebaseDatabase.DefaultInstance.GetReference("AvailableMatches").Child(_hostID).Child("players").ValueChanged += HandleValueChanged;
        testText = _text2;
        testText2 = _text;
    }
    public void RegisterForNewKick()
    {
        Debug.LogError(". .agAsmRhFPyQzlVWEKwKhTZRL7QI3. ." + MatchManager.main.data.hostID + ". .");
        FirebaseDatabase.DefaultInstance.GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).ValueChanged += HandleResultChanged;
        //FirebaseDatabase.DefaultInstance.GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).ChildAdded += HandleChildAdded;
    }
    void HandleChildAdded(object sender, ChildChangedEventArgs args)
    {
        Debug.LogError("CHILD ADDED");
        bool p1Ready = false;
        bool p2Ready = false;
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        if (args.Snapshot.Child("P1ShootCounter").GetValue(true).ToString() !=null)
        {
        //Debug.LogError("shoot counter" + args.Snapshot.Child("P1ShootCounter").Value.ToString());
            p1Ready = true;
            Debug.LogError("chi P1 ES TRUEEEEE");
        }
        if (args.Snapshot.Child("P2ShootCounter").Exists)
        {
            p2Ready = true;
        }
        // Do something with the data in args.Snapshot
        if (p1Ready && p2Ready)
        {
            OnMatchEnded.Invoke();
            Debug.LogError("se acaboooooooooooooooooo");
        }
        // Do something with the data in args.Snapshot
    }
    void HandleResultChanged(object sender, ValueChangedEventArgs args)
    {
        bool p1Ready = false;
        bool p2Ready = false;
        Debug.LogError("ahiviene yupi");
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        List<string> tempResultsP1 = new List<string>();
        List<string> tempResultsP2 = new List<string>();
        foreach (DataSnapshot s in args.Snapshot.Child("resultsP1").Children)
        {
            if (s.Value.ToString() != "")
                tempResultsP1.Add(s.Value.ToString());
        }
        foreach (DataSnapshot s in args.Snapshot.Child("resultsP2").Children)
        {
            if (s.Value.ToString() != "")
                tempResultsP2.Add(s.Value.ToString());
        }
        if (args.Snapshot.Child("P1ShootCounter").Value.ToString() != "")
        {
            p1Ready = true;
            Debug.LogError("P1 true");
        }
        if (args.Snapshot.Child("P2ShootCounter").Value.ToString()!="")
        {
            p2Ready = true;
            Debug.LogError("P2 true");
        }
        // Do something with the data in args.Snapshot
        if (p1Ready && p2Ready)
        {
            OnMatchEnded.Invoke();
            Debug.LogError("se acaboooooooooooooooooo");
        }
        OnEnemyGoal.Invoke(tempResultsP1.ToArray(), tempResultsP2.ToArray());
    }
    public void UnRegisterForNewPlayers()
    {
        FirebaseDatabase.DefaultInstance.GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).Child("players").ValueChanged -= HandleValueChanged;
    }

    void HandleValueChanged(object sender, ValueChangedEventArgs args)
    {
        if (args.DatabaseError != null)
        {
            Debug.LogError(args.DatabaseError.Message);
            return;
        }
        List<string> tempPlayers = new List<string>();
        foreach (DataSnapshot s in args.Snapshot.Children)
        {
            if (s.Value.ToString() != "")
                tempPlayers.Add(s.Value.ToString());
        }
        testText2.text = tempPlayers[0];
        if (tempPlayers.Count >= 2)
        {
            testText2.text = tempPlayers[0];
            testText.text = tempPlayers[1];
            SetCurrentMatchData();
            OnMatchFound.Invoke();
        }
        // Do something with the data in args.Snapshot
    }
    public void SetCurrentMatchData()
    {
        FirebaseDatabase.DefaultInstance
     .GetReference("AvailableMatches").Child(currentHostID)
     .GetValueAsync().ContinueWithOnMainThread(task =>
     {
         if (task.IsFaulted)
         {
             // Handle the error...
         }
         else if (task.IsCompleted)
         {
             DataSnapshot snapshot = task.Result;
             MatchManager.main.data = JsonUtility.FromJson<MatchData>(snapshot.GetRawJsonValue());
             Debug.LogError("data finally setteed" + MatchManager.main.data.hostID + " " + MatchManager.main.data.players[0] + " " + MatchManager.main.data.players[1]);
             // Do something with snapshot...
         }
     });
    }
    void InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
        Debug.LogError("Firebase init");
    }
    private void WriteNewUser(string userId, string name, string email)
    {
        UserChroma user = new UserChroma(name, email, userId);
        string json = JsonUtility.ToJson(user);

        FirebaseDatabase.DefaultInstance
 .GetReference("users").Child(userId).SetRawJsonValueAsync(json);
    }
    public void WriteNewScore(string shootCount, string score, string player)
    {
        string key = FirebaseDatabase.DefaultInstance
 .GetReference("AvailableMatches").Child("agAsmRhFPyQzlVWEKwKhTZRL7QI3").Push().Key;
        if (player == "P1")
        {
            FirebaseDatabase.DefaultInstance
     .GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).Child("resultsP1").Child(shootCount).SetValueAsync(score);
        }
        else
        {
            FirebaseDatabase.DefaultInstance
     .GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).Child("resultsP2").Child(shootCount).SetValueAsync(score);
        }
    }
    public void PlayerEndMatch(string shootCount, string player)
    {
        string key = FirebaseDatabase.DefaultInstance
 .GetReference("AvailableMatches").Child("agAsmRhFPyQzlVWEKwKhTZRL7QI3").Push().Key;
        if (player == "P1")
        {
            FirebaseDatabase.DefaultInstance
     .GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).Child("P1ShootCounter").SetValueAsync(shootCount);
        }
        else
        {
            FirebaseDatabase.DefaultInstance
     .GetReference("AvailableMatches").Child(MatchManager.main.data.hostID).Child("P2ShootCounter").SetValueAsync(shootCount);
        }
    }
    public void SendVerificationEmail()
    {
        Firebase.Auth.FirebaseUser user2 = auth.CurrentUser;
        if (user != null)
        {
            user.SendEmailVerificationAsync().ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("SendEmailVerificationAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SendEmailVerificationAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.Log("Email sent successfully.");
            });
        }
    }
    public void ResetPassword(string _emailToRecover)
    {
        string emailAddress = _emailToRecover;
        auth.SendPasswordResetEmailAsync(emailAddress).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                return;
            }

            Debug.Log("Password reset email sent successfully.");
            OnPasswordReset.Invoke();
        });
    }

    public void UpdateUserData(string _displayName)
    {
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            Firebase.Auth.UserProfile profile = new Firebase.Auth.UserProfile
            {
                DisplayName = _displayName,
                PhotoUrl = new System.Uri("https://example.com/jane-q-user/profile.jpg"),
            };
            user.UpdateUserProfileAsync(profile).ContinueWithOnMainThread(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.Log("User profile updated successfully.");

                displayName = user.DisplayName;
                userID = user.UserId;
                WriteNewUser(user.UserId, user.DisplayName, user.Email);

                SetUserTextureByID("nopngData");
                SceneManager.LoadScene("menuChromaScore");
            });
        }
    }
    public void SetUserData(string _hash, string _tattooTexture, string _name, string[] _tattoosOnSkin, string[] _buyedTattoos)
    {
        Debug.LogError("tattoos data setted");
        reference.Child("users").Child(_hash).Child("tattoosTexture").SetValueAsync(_tattooTexture);
        reference.Child("users").Child(_hash).Child("textureName").SetValueAsync(_name);
        reference.Child("users").Child(_hash).Child("buyedTattoosData").SetValueAsync(_buyedTattoos);
        reference.Child("users").Child(_hash).Child("tattoosOnSkin").SetValueAsync(_tattoosOnSkin);
    }
    public void TryToCreateNewMatch(TextMeshProUGUI _text, TextMeshProUGUI _text2)
    {
        List<MatchData> availableMatchesFound = new List<MatchData>();
        FirebaseDatabase.DefaultInstance
  .GetReference("AvailableMatches").GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          foreach (DataSnapshot s in snapshot.Children)
          {
              foreach (DataSnapshot s2 in s.Child("players").Children)
              {
                  if (s2.Value.ToString() == "")
                  {
                      availableMatchesFound.Add(JsonUtility.FromJson<MatchData>(s.GetRawJsonValue().ToString()));
                      Debug.LogError("DATA FOUND " + snapshot.GetRawJsonValue().ToString());

                  }
              }
          }
          if (availableMatchesFound.Count > 0)
          {
              Debug.LogError("se econtraon juegos disponibles");
              Debug.LogError(availableMatchesFound[0]);
              Debug.LogError("nombre del host: " + JsonUtility.ToJson(availableMatchesFound[0]));
              PlayersInAMatch tempPlayers = new PlayersInAMatch(availableMatchesFound[0].players[0], displayName);
              availableMatchesFound[0].players[1] = displayName;
              string json = JsonUtility.ToJson(availableMatchesFound[0]);
              Debug.LogError("se transformo el siguente json: " + json);
              Debug.LogError("se agrego un nuevo match en host id: " + availableMatchesFound[0].hostID);
              reference.Child("AvailableMatches").Child(availableMatchesFound[0].hostID).SetRawJsonValueAsync(json);
              Debug.LogError("se agrego un nuevo match en host id: " + availableMatchesFound[0].hostID);
              MatchManager.main.data = availableMatchesFound[0];
              currentHostID = availableMatchesFound[0].hostID;
              //       FirebaseDatabase.DefaultInstance
              //.GetReference("AvailableMatches").Child(user.UserId).SetRawJsonValueAsync(json);
              RegisterForNewPlayers(availableMatchesFound[0].hostID, _text2, _text);
              MatchManager.main.currPlayerNumber = "P2";

          }
          else
          {
              Debug.LogError("NO se econtraon juegos disponibles");
              MatchData newMatch = new MatchData(displayName, user.UserId);
              currentHostID = user.UserId;
              string json = JsonUtility.ToJson(newMatch);

              FirebaseDatabase.DefaultInstance
       .GetReference("AvailableMatches").Child(user.UserId).SetRawJsonValueAsync(json);
              RegisterForNewPlayers(user.UserId, _text, _text2);
              MatchManager.main.currPlayerNumber = "P1";
              MatchManager.main.data = new MatchData(displayName, user.UserId);

          }
          Debug.LogError("se completa paps");
          //DataManager.main.ownedTattoos = dictUsers;
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
      }
  });

        //reference.Child("AvailableMatches").Child(user.UserId).Child("Players").SetValueAsync(displayName);
    }
    public void LoadBuyedTattoos()
    {
        string[] buyedTattoos = new string[0];
        Debug.LogError("askinf for firebase");
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(user.UserId).Child("buyedTattoosData")
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
          buyedTattoos = new string[0];
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          List<string> dictUsers = new List<string>();
          foreach (DataSnapshot s in snapshot.Children)
          {
              dictUsers.Add(s.Value.ToString());
          }
          buyedTattoos = dictUsers.ToArray();
          //DataManager.main.ownedTattoos = dictUsers;
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
      }
  });

    }
    public void SetUserTextureByID(string _hash)
    {
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(_hash).Child("tattoosTexture")
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          pngData = snapshot.Value.ToString();
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
          // Do something with snapshot...
      }
  });
    }
    public void SetUserTattoosByID(string _hash, string _buyedTattoos, string _tattooosOnSkin)
    {
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(_hash).Child("buyedTattoos")
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          pngData = snapshot.Value.ToString();
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
          // Do something with snapshot...
      }
  });
    }

    public void TryToCreateUserWithEmail(string _email, string _password)
    {
        //Cuando un usuario nuevo se registre mediante el formulario de registro de la app, realiza los pasos de 
        //validación de la cuenta nueva necesarios, como verificar que se haya escrito correctamente la contraseña 
        //y que cumpla con los requisitos de complejidad.
        newuser = true;
        Debug.LogError("new user " + newuser);
        auth.CreateUserWithEmailAndPasswordAsync(_email, _password).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            OnSignUp.Invoke(newUser);
            Debug.LogError(auth.CurrentUser.Email);
        });
    }
    public void SignOut()
    {
        auth.SignOut();
        SceneManager.LoadScene("loginChroma");
    }

    public void TryToAuthWithEmail(string _email, string _password)
    {
        auth.SignInWithEmailAndPasswordAsync(_email, _password).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                UILoginManager.main.SetError("*incorrect password");
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            //WriteNewUser(newUser.UserId, newUser.DisplayName, newUser.Email, "nodata", DataManager.main.buyedTattoos.ToArray());

            SceneManager.LoadScene("menuChromaScore");
        });
    }
    public void UpdateLocalUserTextureFromDB()
    {
        SetUserTextureByID(user.UserId);
    }
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
                displayName = user.DisplayName ?? "";
                emailAddress = user.Email ?? "";
                userID = user.UserId ?? "";
                if (!newuser)
                {
                    Debug.LogError("pasa directo " + newuser);
                    LoadCromosData();
                    SceneManager.LoadScene("menuChromaScore");
                }
            }
        }
    }
    public void LoadCromosData()
    {
        FirebaseDatabase.DefaultInstance
     .GetReference("users").Child(user.UserId)
     .GetValueAsync().ContinueWithOnMainThread(task =>
     {
         if (task.IsFaulted)
         {
             // Handle the error...
         }
         else if (task.IsCompleted)
         {
             DataSnapshot snapshot = task.Result;
             foreach (DataSnapshot s in snapshot.Children)
             {
                 if (s.Value.ToString() != "")
                     cardsIDS.Add(s.Value.ToString());
             }
             // Do something with snapshot...
         }
     });
    }
    private void WriteNewCromo(string userId, string name, string email)
    {
        UserChroma user = new UserChroma(name, email, userId);
        string json = JsonUtility.ToJson(user);

        FirebaseDatabase.DefaultInstance
 .GetReference("users").Child(userId).SetRawJsonValueAsync(json);
    }
    // Update is called once per frame
    void Update()
    {

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine.UI;

public class UILoginManager : Singleton<UILoginManager>
{

    [Header("Login refs")]
    public TMP_InputField email;
    public TMP_InputField password;
    public TextMeshProUGUI error;
    public TMP_InputField nickname;

    [Header("Sign in refs")]
    public TMP_InputField signEmail;
    public TMP_InputField signPassword;
    public TMP_InputField signPasswordConfirm;
    public TextMeshProUGUI signError;


    [Header("Reset passsword refs")]
    public TMP_InputField resetEmail;
    public TextMeshProUGUI reseterror;

    const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";
    // Start is called before the first frame update
    void Start()
    {

    }
    public void SetError(string _error)
    {
        error.text = _error;
        Debug.LogError("esto pasa " + _error);
    }

    public void TryToSetCollectorsNickname()
    {
        if (nickname.text == "")
        {
            error.text = "*fill empty fields";
        }
        else
        {
            DatabaseManager.main.UpdateUserData(nickname.text);
        }
    }
    public void ResetPassword()
    {
        if (resetEmail.text == "")
        {
            error.text = "*fill empty fields";
        }
        else
        {
            if (ValidateEmail(resetEmail.text))
            {
                DatabaseManager.main.ResetPassword(resetEmail.text);
                Debug.LogError("email validated");
            }
            else
            {
                signError.text = "*Not a valid email";
            }
        }

    }
    public bool HandleInputErrors()
    {
        if (email.text == "")
        {
            error.text = "*fill empty fields email"+email.name;
            return false;
        }
        if (password.text == "")
        {
            error.text = "*fill empty fields password" +password.name;
            return false;
        }
        else
        {
            return true;
        }
    }
    public void SendLogin()
    {
        if (HandleInputErrors())
        {
            DatabaseManager.main.TryToAuthWithEmail(email.text, password.text);
        }
    }
    public void TryToRegisterUser()
    {
        signError.text = "";
        Debug.LogError("trying to regist");
        if (signEmail.text == "" || signPassword.text == "")
        {
            signError.text = "*fill empty fields";
        }
        else
        {
            if (ValidateEmail(signEmail.text))
            {
                if (signPasswordConfirm.text == signPassword.text)
                {
                    if (signPassword.text.Length >= 6)
                    {
                        DatabaseManager.main.TryToCreateUserWithEmail(signEmail.text, signPassword.text);
                    }
                    else
                    {
                        signError.text = "Use a more complex password";
                    }
                }
                else
                {
                    signError.text = "*Passwords does not match";
                }
            }
            else
            {
                signError.text = "*Not a valid email";
            }
        }
    }
    public bool ValidateEmail(string email)
    {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }
    // Update is called once per frame
    void Update()
    {

    }
}

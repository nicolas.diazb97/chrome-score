using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchData
{
    public string hostID;
    public string P1ShootCounter = "";
    public string P2ShootCounter = "";
    public string[] players = new string[2];
    public string[] resultsP1 = new string[15];
    public string[] resultsP2 = new string[15];
    // Start is called before the first frame update
    public MatchData(string _playerName, string _hostID)
    {
        players[0] = _playerName;
        hostID = _hostID;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class MatchMaking : MonoBehaviour
{
    public TextMeshProUGUI ownPlayer;
    public TextMeshProUGUI secondPlayer;
    public UnityEvent OnMatchFound;
    // Start is called before the first frame update
    void Start()
    {
        OnMatchFound.AddListener(() =>
        {
            DatabaseManager.main.UnRegisterForNewPlayers();
        });
    }
    public void testing()
    {
        //DatabaseManager.main.WriteNewScore(20);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void Battle()
    {
        DatabaseManager.main.TryToCreateNewMatch(ownPlayer, secondPlayer);
        DatabaseManager.main.OnMatchFound.AddListener(() =>
        {
            OnMatchFound.Invoke();
        });
    }
}

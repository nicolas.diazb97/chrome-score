﻿using UnityEngine;
using System.Collections;

public class animacionDefensa : MonoBehaviour {

	public Animator anim;
	private bool unaVez;
	public static bool primeraAnimacion;
	// Use this for initialization
	void Start () 
	{
		primeraAnimacion = true;
		anim = GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () 
	{
		if (unaVez == false) 
		{
			if (PlayerPrefs.GetInt ("pateando1") == 1) 
			{
				if(collisionBall.isTouchBall){
					anim.Play ("Salto", -1, 0f);
					unaVez = true;
				}
			}
		}

		if (PlayerPrefs.GetInt ("gol") == 1) {
			//print ("defensa con gol");
			anim.SetTrigger ("Gol");
		} 

		if (PlayerPrefs.GetInt ("pateando1") == 0) 
		{
			//print ("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
			unaVez = false;
		}
	}
}

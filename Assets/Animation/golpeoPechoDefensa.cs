﻿using UnityEngine;
using System.Collections;

public class golpeoPechoDefensa : MonoBehaviour {

	public Animator anim;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void OnCollisionEnter(Collision collision) 
	{

		if(collision.gameObject.tag == "Ball" && animacionDefensa.primeraAnimacion)
		{
			animacionDefensa.primeraAnimacion = false;
			anim.SetTrigger ("Pecho");
		}
	}
}

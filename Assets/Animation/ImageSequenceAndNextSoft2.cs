﻿using UnityEngine;
using System;
using System.Collections;
public class ImageSequenceAndNextSoft2 : MonoBehaviour {
	//An array of Objects that stores the results of the Resources.LoadAll() method
	// private Object[] objects;
	public Texture[] texturasAUsar;
	public bool loop;
    public bool inverse;
    public bool isFinished;
	public int _fps;
	 int lengthSequence;

	Texture nextTexture;
	Texture actualTexture;
	Material goMaterial;
	int frameCounter = 0;	
	int _lastIndex = -1;
	int index ;


	void Awake() 
	{
		lengthSequence = texturasAUsar.Length;
		Application.targetFrameRate =-1;
#if UNITY_EDITOR
        Application.targetFrameRate =60;
#endif
		this.goMaterial = this.GetComponent<Renderer>().material;
	}

	void reStart () {
        if(inverse)
			frameCounter=lengthSequence; 
        actualTexture	=texturasAUsar[frameCounter];
		nextTexture		=texturasAUsar[frameCounter+(inverse?-1:1) ];
		if(nextTexture == null)
		{
			nextTexture =actualTexture ;
		}
	}


	IEnumerator MyWait()
	{
	 	yield return new WaitForEndOfFrame();

		if(frameCounter <= lengthSequence && frameCounter>= -1 )
		{ 
			//print (actualTexture);
			goMaterial.mainTexture = actualTexture; 
			actualTexture = nextTexture;
			nextTexture	= texturasAUsar[ frameCounter+(inverse?-1:1)];
			if(nextTexture == null)
			{
				nextTexture =actualTexture ;
			}
		}


	}

	void Update () 
	{

			index = (int)(Time.realtimeSinceStartup * _fps) % lengthSequence; 

    	if(index != _lastIndex) 
		{
    		_lastIndex = index ;
        	StartCoroutine( MyWait() );
			if(frameCounter <= lengthSequence && !inverse )
			{ 
				++frameCounter;
			} 
			if( frameCounter >=1 && inverse)
			{ 
				--frameCounter;
			}
    	}

		isFinished = frameCounter == lengthSequence?true:false;

		if(frameCounter ==0 && inverse)
		{
        	frameCounter=lengthSequence; 
		}


		if(frameCounter == lengthSequence-1 && loop && !inverse)
		{  //######----->
    		frameCounter =-1; //######----->
		}
	}

	public void SetCounter(int c)
	{
		frameCounter = c;
		goMaterial.mainTexture = actualTexture; 
        actualTexture	= texturasAUsar[frameCounter];
		nextTexture		= texturasAUsar[frameCounter+(inverse?-1:1) ];
	}

	public void Reconfigure( int __fps, int _lengthSequence)
	{
		_fps 				= __fps ;
		lengthSequence 		= _lengthSequence;
	}
		
}
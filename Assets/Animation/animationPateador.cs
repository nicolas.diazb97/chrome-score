﻿using UnityEngine;
using System.Collections;

public class animationPateador : MonoBehaviour {

	public Animator anim;
	public Transform Lookat;
	public Collider _Colllider;
	private bool unaVez;
	private bool InitContact;
	private bool unaVezIdle;
	private Vector3 posMouse,tempPos;
	public Transform Player;
	public static float PosYTouch,PosFinalYTouch;
	float time;
	public static bool isShot;
	public static int CantidadPosCurva;
	public bool leftHanded;
	public static bool pelotaALaDerecha;
	public static bool pelotaALaIzquierda;
	public static float result;
	public float m_Result;
	public float[] posX;
	public float[] posY;
	// Use this for initialization
	void Start ()
	{
		anim = GetComponent<Animator> ();
	}


	public void contadorTouch(){
		if(Input.touchCount > 0 &&  Input.GetTouch(0).phase ==  TouchPhase.Stationary){
			//Debug.Log(time);
			time = time+1*Time.deltaTime;
		}else if (Input.touchCount > 0 &&  Input.GetTouch(0).phase ==  TouchPhase.Ended){
			time=0;
		}
	}	



	// Update is called once per frame
	void Update () 
	{

		m_Result = result;
//		Debug.Log("posicion en z "+PlayerPrefs.GetFloat ("posPateadorZ"));
		this.transform.LookAt(Lookat);
		if (PlayerPrefs.GetFloat ("posPateadorX") > 7) {
			//Debug.Log ("derecha");
			pelotaALaDerecha = true;
			pelotaALaIzquierda = false;
			Shoot.share._TimerShot = 0.01f;
			if(!leftHanded)
			{
				Debug.LogError("aquiiiii 4");
				transform.position = new Vector3 (PlayerPrefs.GetFloat ("posPateadorX")+posX[3], 0 ,PlayerPrefs.GetFloat ("posPateadorZ")+ posY[3]);
			}else {
				//Debug.Log ("Alvez");
				//df
				transform.position = new Vector3 (PlayerPrefs.GetFloat ("posPateadorX")+2f, -0.02F ,PlayerPrefs.GetFloat ("posPateadorZ")-1.9F);
			}
			//cambioAngulo = false;
		}

		if (PlayerPrefs.GetFloat ("posPateadorX") < -3)
		{
			Debug.LogError("aquiiiii");
			pelotaALaIzquierda = true;
			pelotaALaDerecha = false;
			//Debug.Log ("izquierda");
			Shoot.share._TimerShot = 0.01f;
			if(!leftHanded){
				//Debug.Log ("Oyola");
				//dffd
				transform.position = new Vector3 (PlayerPrefs.GetFloat ("posPateadorX")- posX[0], 0 ,PlayerPrefs.GetFloat ("posPateadorZ") + posY[0]);
			}else {
				//Debug.Log ("Alvez");
				//dffd
				transform.position = new Vector3 (PlayerPrefs.GetFloat ("posPateadorX")+0.5f, -0.02F ,PlayerPrefs.GetFloat ("posPateadorZ")-1.5F);
			}
			//cambioAngulo = false;
		}

		if (PlayerPrefs.GetFloat ("posPateadorX") >= -3 && PlayerPrefs.GetFloat ("posPateadorX") <= 7) {
			pelotaALaIzquierda = false;
			Debug.LogError("aquiiiii 3");
			pelotaALaDerecha = false;
			//Debug.Log ("centro");
			Shoot.share._TimerShot = 0.01f;
			if (!leftHanded) {
				//Debug.Log ("oyola");

				transform.position = new Vector3 (PlayerPrefs.GetFloat ("posPateadorX")+ posX[2], 0, PlayerPrefs.GetFloat ("posPateadorZ") + posY[2]);
			} else {
				//Debug.Log ("Alvez");
				transform.position = new Vector3 (PlayerPrefs.GetFloat ("posPateadorX") + 1.2f, -0.02F, PlayerPrefs.GetFloat ("posPateadorZ") - 1.8F);
			}
		} 



		if (PlayerPrefs.GetInt ("pateando1") == 1) 
		{

			Debug.LogError("aquiiiii 2");
			//	unaVezIdle = false;
			if (!unaVez ) {
				//Debug.Log ("posicion del mouse: "+Input.mousePosition.y);
				result = Input.mousePosition.y - PosYTouch;
				if (result < 250) {
//					Debug.Log ("result "+result);
				} else {
//					Debug.Log ("bien");	
				}

                //Debug.Log ("result de mierda: "+result);

                //if (Input.GetMouseButtonUp(0) && result > 70 && Shoot._enableTouchSt && Shoot.share.PosCurva.Count > 3)
                if ((Shoot.share.afterShoot == true && Shoot.share._isShooting == false) && result > 70 && Shoot._enableTouchSt && Shoot.share.PosCurva.Count > 3)
                {
                    collisionBall.main.m_ColliderBall.enabled = true;
                    Debug.Log("Puede Patear animacion");
					//_Colllider.enabled = true;
					if (leftHanded) {
						if (PlayerPrefs.GetFloat ("posPateadorX") > 3) {

							anim.Play ("PateaIzquierda", -1, 0f);

						}
						if (PlayerPrefs.GetFloat ("posPateadorX") < -3) {
							anim.Play ("PateaIzquierda", -1, 0f);


						}
						if (PlayerPrefs.GetFloat ("posPateadorX") >= -3 && PlayerPrefs.GetFloat ("posPateadorX") <= 3) {
							anim.Play ("PateaIzquierda", -1, 0f);

						}
					} else {
						if (PlayerPrefs.GetFloat ("posPateadorX") > 3) {
							anim.Play ("PateaDerecha", -1, 0f);
						}
						if (PlayerPrefs.GetFloat ("posPateadorX") < -3) {
							anim.Play ("PateaDerecha", -1, 0f);
						}
						if (PlayerPrefs.GetFloat ("posPateadorX") >= -3 && PlayerPrefs.GetFloat ("posPateadorX") <= 5) {
							anim.Play ("Despeje", -1, 0f);
						}
					}
					unaVezIdle = false;
					unaVez = true;
				} 

			} 
		}

		if (PlayerPrefs.GetInt ("pateando1") == 0)
		{
			//PlayerPrefs.SetInt ("pateando", 0);
			unaVez = false;
			if (!unaVezIdle) 
			{
				anim.Play ("Reposo", -1, 0f);
				unaVezIdle = true;
			}
		}
	}

	void LoadPerfilPlayer( ){
		if(PlayerPrefs.GetInt("JugadorSel")== 0){
			ShootAI.shareAI._curveLevel =1 ;

		}else if(PlayerPrefs.GetInt("JugadorSel")== 1){
			ShootAI.shareAI._curveLevel =50;
		}else if(PlayerPrefs.GetInt("JugadorSel")== 2){
			ShootAI.shareAI._curveLevel =100 ;
		}
	}
		

}

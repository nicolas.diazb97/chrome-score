﻿using UnityEngine;
using System.Collections;

public class animationArquero : MonoBehaviour 
{
	public Animator anim;
	private float yRotation;
	public static bool LadoSeleccionado;
	public Transform Player;
	public Transform PosInitDer,PosInitIzq;
	//public Transform Cobrador;//Comentado por eduardo
	public Transform Ball;
	Transform FinisshPosition = null;
	public BoxCollider _CenterCollider;

	public static bool _isCentro;

	[SerializeField]
	private GameObject pateador;
	// Use this for initialization
	void Start () 
	{
		//Screen
		pateador = GameObject.FindObjectOfType<InitPlayerSeleccion>().Jugadores[PersonalizacionPersonaje.jugadorSel];
		anim = GetComponent<Animator> ();
		FinisshPosition = GetComponent<Transform> ();
		yRotation = 0;
		//Cobrador = GameObject.FindWithTag("Cobrador").transform;//Comentado por eduardo

	}

	void FixedUpdate(){
		
		rotacion();

	}
	// Update is called once per frame
	void Update () 
	{
		if(collisionBall.isTouchBall){
			
			StartCoroutine(	SeleccionDeLanzamiento());
		}
	
		if (animationPateador.pelotaALaDerecha) {
			transform.position = PosInitIzq.position;
		} else if (animationPateador.pelotaALaIzquierda) {
			transform.position = PosInitDer.position;
		} else {
			transform.position = PosInitIzq.position;
		}


		transform.eulerAngles = new Vector3(0, yRotation, 0);

	}

	void rotacion(){

		if (PlayerPrefs.GetFloat ("posPateadorX") > 2) 
		{
			yRotation = 155;
		}
		else if (PlayerPrefs.GetFloat ("posPateadorX") > 1 && PlayerPrefs.GetFloat ("posPateadorX") <= 2) 
		{
			yRotation = 175; 
		}
		else if (PlayerPrefs.GetFloat ("posPateadorX") > -1 && PlayerPrefs.GetFloat ("posPateadorX") <= 1) 
		{
			yRotation = 180;
		}
		else if (PlayerPrefs.GetFloat ("posPateadorX") > -2 && PlayerPrefs.GetFloat ("posPateadorX") <= -1) 
		{
			yRotation = 190;
		}
		if (PlayerPrefs.GetFloat ("posPateadorX") <= -2) 
		{
			yRotation = 195;
		}
	}


	IEnumerator SeleccionDeLanzamiento(){
		float timer = 0f;
		if (PlayerPrefs.GetInt ("Creloj") == 0) {
			if (respawn.nivel == 1)
				timer = 0.1f;
			else if (respawn.nivel == 2)
				timer = 0.05f;
			else
				timer = 0f;
		}
		
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if(GoalDetermine.share._goalCheck){  
			if( Physics.Raycast(ray,out hit)  && !LadoSeleccionado){
				Debug.DrawLine(ray.origin,hit.point);
				yield return new WaitForSeconds(timer);
				if (hit.transform.tag == "DerArriba") {
					//Debug.Log ("Arquero derecha arriba");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("SuperiorDer", -1, 0f);

				} else if (hit.transform.tag == "DerCentro") {
					//Debug.Log ("Arquero derecha centro");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("MitadDer", -1, 0f);

				} else if (hit.transform.tag == "DerAbajo") {
					//Debug.Log ("Arquero derecha abajo");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("InferiorDer", -1, 0.2f);

				} else if (hit.transform.tag == "CentroDer") {
					//Debug.Log ("Arquero centro derecho");
					_CenterCollider.enabled = true;
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("CentroIzq", -1, 0f);
					_isCentro = true;
				} else if (hit.transform.tag == "CentroIzq") {
					//Debug.Log ("Arquero centro izq");
					_CenterCollider.enabled = true;
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("CentroDer", -1, 0f);
					_isCentro = true;
				} else if (hit.transform.tag == "IzqArriba") {
					//Debug.Log ("Arquero izq arriba");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("SuperiorIzq", -1, 0f);
				} else if (hit.transform.tag == "IzqCentro") {
					//Debug.Log ("Arquero izq centro");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("MitadIzq", -1, 0f);

				} else if (hit.transform.tag == "IzqAbajo") {
					//Debug.Log ("Arquero izq abajo");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("InferiorIzq", -1, 0f);

				} else if (hit.transform.tag == "CentroIzqAbajo") {
					//Debug.Log ("Arquero centro abajo");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("InferiorIzq", -1, 0f);
				} else if (hit.transform.tag == "CentroSupDer") {
					//Debug.Log ("Arquero centro sup der");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("CentroSupDer", -1, 0f);
				} else if (hit.transform.tag == "CentroSupIzq") {
					//Debug.Log ("Arquero centro sup izq");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("CentroSupIzq", -1, 0f);
				} else if (hit.transform.tag == "CentroDerAbajo") {
					//Debug.Log ("Arquero centro sup der");
					LadoSeleccionado = true;
					collisionBall.isTouchBall = false;
					anim.Play ("InferiorDer", -1, 0f);
				}
			}
		}
	
		// AL FINALIZAR RESETEEOOO 	LadoSeleccionado  en falso  EN SHOOT. RESET ////
	}

}
